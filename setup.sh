#!/bin/bash

setupATLAS
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
#lsetup "views LCG_98python3 x86_64-centos7-gcc9-opt"

# figure out the actual directory of the high-pt extrapolation repository
export XTRAP_ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

export PYTHONPATH=${PYTHONPATH}:${XTRAP_ROOTDIR}

export CDI_PATH=$XTRAP_ROOTDIR/Config/2020-21-13TeV-MC16-CDI-2020-03-11_v2.root
