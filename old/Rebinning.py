import ROOT
from ROOT import TFile
import numpy as np 
from array import array
from argparse import ArgumentParser



# nominal_infile = "/afs/cern.ch/work/s/ssindhu/private/pflow_jets/high-pT-extrapolation/All_outputs/hists/10/combined/10/DL1r_MUNC_PS_Zprime_AntiKt4EMPFlowJets.root"
# nominal_infile = "/afs/cern.ch/work/s/ssindhu/private/pflow_jets/high-pT-extrapolation/All_outputs/hists/fine_bin/fsrtest/combined/c_jet_01/DL1r_MUNC_FSR_Zprime_AntiKt4EMPFlowJets.root" 
# nominal_infile = "/afs/cern.ch/work/s/ssindhu/private/pflow_jets/high-pT-extrapolation/All_outputs/hists/fine_bin/finer/combined/c_jet_01/DL1r_MUNC_PS_Zprime_AntiKt4EMPFlowJets.root" 

def merge_bins(hist, bin1, bin2):

    final_bin = hist.GetBinContent(bin1) + hist.GetBinContent(bin2)
    final_uncertainty = np.sqrt((hist.GetBinError(bin1))**2+(hist.GetBinError(bin2))**2)

    return final_bin, final_uncertainty

def merge(hist, merge_start, merge_end):
    # first, get the original bin edges
    l_edges_old = []
    for bin in range(1, hist.GetSize()):
        l_edges_old.append(hist.GetBinLowEdge(bin))

        merged_bin_value, merged_bin_error = merge_bins(hist, merge_start, merge_end)
        
    # now get the new bin edges after merging the requested bins
    l_edges_new = l_edges_old[:merge_start] + l_edges_old[merge_end:]
    l_edges_new_arr = array('d', l_edges_new)

    hist_processed = hist.Clone()
    hist_processed.SetBins(len(l_edges_new) - 1, l_edges_new_arr)

    # print("original bin edges: '{}'".format(l_edges_old))
    # print("new bin edges: '{}'".format(l_edges_new))

    # now set the bin values and uncertainties
    for bin in range(1, merge_start):
        # print("{} -> {}".format(bin, bin))
        hist_processed.SetBinContent(bin, hist.GetBinContent(bin))
        hist_processed.SetBinError(bin, hist.GetBinError(bin))

    # print("{} -> {}".format("merged", merge_start))
    hist_processed.SetBinContent(merge_start, merged_bin_value)
    hist_processed.SetBinError(merge_start, merged_bin_error)

    for bin in range(merge_start + 1, hist_processed.GetSize()):
        # print("{} -> {}".format(bin + merge_end - merge_start, bin))
        hist_processed.SetBinContent(bin, hist.GetBinContent(bin + merge_end - merge_start))
        hist_processed.SetBinError(bin, hist.GetBinError(bin + merge_end - merge_start))
            
    return hist_processed

def merge_main(nom_curve, var_curve, merge_start, merge_end, stat_unc_percent):

    def merge_step(cur_nom_curve, cur_var_curve, merge_start, merge_end, sigth = 0.0, direction = 'left'):
        
        # break immediately if there is nothing more to merge
        if cur_nom_curve.GetSize()-2 == 1 or cur_var_curve.GetSize() -2 == 1:
            return cur_nom_curve, cur_var_curve, 0

        # find the indices of the bins in the active region and make sure it is allowed
        active_bins = range(max(0, cur_var_curve.FindBin(merge_start)), 
                                min(cur_var_curve.FindBin(merge_end) + 1, cur_var_curve.GetSize()-1))

        # print("active_bins = {}".format(active_bins))

        # also stop if there is just a single bin left to consider
        # if len(active_bins) < 2:
        #     return cur_nom_curve, cur_var_curve, 0                

            
        if direction == 'right':
            active_bins.reverse()


            # merge two bins if *both* have too low a significance
        perform_merging = lambda x_value, x_value_neighbour: cur_var_curve.GetBinError(x_value)/cur_var_curve.GetBinContent(x_value)*100 > stat_unc_percent #and cur_var_curve.GetBinError(x_value)/cur_var_curve.GetBinContent(x_value)*100 > cur_var_curve.GetBinError(x_value -1)/cur_var_curve.GetBinContent(x_value -1)*100

        # go through the list of bins in the correct order and perform one merging step
        for bin, neighbour in zip(active_bins[:-1], active_bins[1:]):
            bin_center = cur_var_curve.GetBinCenter(bin)
            neighbour_center = cur_var_curve.GetBinCenter(neighbour)
            # print("checking whether bin {} (centered at {}) should be merged with {} (centered at {})".format(bin, bin_center, neighbour, neighbour_center))

            # while neighbour == active_bins[-1] and cur_var_curve.GetBinError(neighbour)/cur_var_curve.GetBinContent(neighbour)*100 > 3:
            #     print("last_bin")
            #     cur_nom_curve = merge(cur_nom_curve, bin-1,  bin)
            #     cur_var_curve = merge(cur_var_curve, bin-1, bin )
            #     bin = bin-1
            #     print(cur_nom_curve.GetSize())

            # check if these bins should be merged
            if perform_merging(bin, neighbour):
                # print("yes, bin ={}, bin content ={}, bin error ={} have sig(bin) = {}, sig(neighbour) = {}".format(bin,cur_var_curve.GetBinContent(bin), cur_var_curve.GetBinError(bin), cur_var_curve.GetBinError(bin)/cur_var_curve.GetBinContent(bin)*100, cur_var_curve.GetBinError(neighbour)/cur_var_curve.GetBinContent(neighbour)*100))
                # if so, merge these bins in both the nominal and the varied efficiency curve ...
                cur_nom_curve = merge(cur_nom_curve, bin, neighbour )
                cur_var_curve = merge(cur_var_curve, bin, neighbour )

        
                # ... and then return them
                return cur_nom_curve, cur_var_curve, 1

            
        # nothing happened, return the original curves
        return cur_nom_curve, cur_var_curve, 0

    cur_nom_curve = nom_curve.Clone()
    cur_var_curve = var_curve.Clone()
    while True: 
        # need to merge bins in the 'truth' and 'tagged' histograms in a synchronized way, updating the changed efficiency along the way
        # print("+++++++++++++ merge step +++++++++++++++")
        cur_nom_curve, cur_var_curve, bins_merged = merge_step(cur_nom_curve, cur_var_curve, merge_start , merge_end, direction = 'left')
        # print("+++++++++++++ ++++++++++++++++ +++++++++++++++")
        
        if bins_merged == 0:
            break
    active_bins = range(max(0, cur_var_curve.FindBin(merge_start)), 
                            min(cur_var_curve.FindBin(merge_end) + 1, cur_var_curve.GetSize()-1))

    # while cur_var_curve.GetBinError(active_bins[-1])/cur_var_curve.GetBinContent(active_bins[-1])*100 > 1.2:
    #     print("last_bin")
    # cur_nom_curve = merge(cur_nom_curve, cur_nom_curve.GetBin(3000) -1, cur_nom_curve.GetBin(3000)  )
    # cur_var_curve = merge(cur_var_curve, cur_nom_curve.GetBin(3000) -1, cur_var_curve.GetBin(3000) )
    #     print(cur_nom_curve.GetSize())
    print(cur_nom_curve.GetBin(3000), active_bins[-1])

    return cur_nom_curve, cur_var_curve





def Rebinning(infile, start, stat_unc_percent):
    t_file = ROOT.TFile.Open(infile)
    truth_hist = t_file.Get("AntiKt4EMPFlowJets_BTagging201903/Nominal/nominal/truth_nominal_bootstrap_0").Clone()
    tagged_hist = t_file.Get("AntiKt4EMPFlowJets_BTagging201903/Nominal/nominal/tagged_FixedCutBEff_60|nominal_bootstrap_0").Clone()  
    truth_hist, tagged_hist = merge_main(truth_hist, tagged_hist, start, 3000, stat_unc_percent)

    # print(tagged_hist.GetSize())
    l_edges =[]
    n_events = 0
    for bin in range(1, tagged_hist.GetSize()-1):
        print("Bin_number ={}, Bin_content ={}, Bin Left edge ={}, Bin_error ={}, %={}".format(bin, tagged_hist.GetBinContent(bin), tagged_hist.GetBinLowEdge(bin), tagged_hist.GetBinError(bin),tagged_hist.GetBinError(bin)/tagged_hist.GetBinContent(bin)*100))
        l_edges.append(tagged_hist.GetBinLowEdge(bin))
        n_events = n_events + tagged_hist.GetBinContent(bin)
    print("Left Edges={}".format(l_edges))
    print(n_events)

if __name__ == "__main__":
    parser = ArgumentParser(description = "orchestrates the production of extrapolation CDI text inputs")
    parser.add_argument("--indir", action = "store", dest = "indir")
    parser.add_argument("--unc_percent", action = "store", dest = "stat_unc_percent", type = float)
    parser.add_argument("--start", action = "store", dest = "start", type = float)
    args = vars(parser.parse_args())
    indir = args["indir"]
    start = args["start"]
    stat_unc_percent = args["stat_unc_percent"]
    print("processing '{}'".format(indir))
    # infiles = glob.glob(os.path.join(indir, "*MUNC*.root"))
    Rebinning(indir, start, stat_unc_percent)


















