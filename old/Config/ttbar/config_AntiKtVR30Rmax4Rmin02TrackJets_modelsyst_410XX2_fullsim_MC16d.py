# ---------------------------------------------------------
# this section is for self-submitting jobs
# ---------------------------------------------------------
# have the following variables available by default:
#    self.thisfilepath ... full path to this file
#    self.thisfilename ... file name of this file
#    self.slicenumber ... slice number of this file (if any)
#    self.slice_suffix ... full slice suffix of this file (if any)
#    sp ... subprocess module

#// START_UGS
#// PYTHON(campaign_name = "2020_06_16_ttbar_MC16d")
#// PYTHON(IN_DS = "mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e6348_e5984_s3126_r10201_r10210_p4060")

# luminosity for 2017 data
#// PYTHON(lumi_data = 44307.4)

#//CFS START_SLICE(systematics)
#// PYTHON(weightspec = "[nominal_s3126]<.*nominal><.*PUWeight>;[MC_ISR_DOWN]<.*Var3cDown><.*muR=2.0,muF=2.0><.*PUWeight>/<.*nominal>;[MC_FSR_UP]<.*isr:muRfac=1.0_fsr:muRfac=2.0><.*PUWeight>;[MC_FSR_DOWN]<.*isr:muRfac=1.0_fsr:muRfac=0.5><.*PUWeight>")
#//CFS PER_SLICE(1)

#// PYTHON(taggers = ["DL1r", "DL1"])
#// PYTHON(jetcoll = "AntiKtVR30Rmax4Rmin02TrackJets")
#// PYTHON(binconfig = "ttbar/410XXX_CDI_extrapolation_VRTrackJets.conf")

#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".410472.AntiKtVRTrackJets.MUNC_fullsim." + self.slicenumber)
#// PYTHON(opts = ["--inDS", IN_DS, "--outDS", OUT_DS, "--nFilesPerJob", "1"])
#// PYTHON(package_name = "btagAnalysis")
#// PYTHON(job_option_file = "jobOptions_AntiKtVR30Rmax4Rmin02TrackJets_tracksyst.py")
#// PYTHON(pos_opts = [package_name + "/" + self.thisfilename, package_name + "/" + job_option_file])
#// PYTHON(print(" ".join(["pathena"] + pos_opts + opts)))
#// PYTHON(sp.check_output(["pathena"] + pos_opts + opts))
#// END_UGS
# ---------------------------------------------------------

JetCollections = ['AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903']
TruthJetCollections = ['AntiKt4TruthJets']

# jet definition as expected by some of the required tools
JetDefinitions = ['AntiKtVR30Rmax4Rmin02Track']

# which jet collection to use for the TIDE uncertainties
TIDEJetCollections = ['AntiKt4EMTopoJets']

# set the name of the systematic variation to be performed for tracks
systVariations = {
#//CFS START_SLICE(systematics)
    "Nominal":                 [],
#//CFS PER_SLICE(1)
    }

# configuration files for pileup reweighting
lumicalc_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
    ]
config_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",
    "btagAnalysis/410472.NTUP_PILEUP_MC16d_s3126.root"    
    ]

# store all MC weights
stored_MC_weights = ["*"]

MC_type = "MC16"

# manually use a run number in 2017
runNumber = 334736

# nominal ttbar sample
input_file = "/data/atlas/atlasdata/windischhofer/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e6928_e5984_s3126_r10201_r10210_p4060/DAOD_FTAG1.20305515._001378.pool.root.1"

