# ---------------------------------------------------------
# this section is for self-submitting jobs
# ---------------------------------------------------------
# have the following variables available by default:
#    self.thisfilepath ... full path to this file
#    self.thisfilename ... file name of this file
#    self.slicenumber ... slice number of this file (if any)
#    self.slice_suffix ... full slice suffix of this file (if any)
#    sp ... subprocess module

#// START_UGS
#// PYTHON(campaign_name = "2020_06_16_ttbar_MC16e")

#//CFS START_SLICE(modeling)
#// PYTHON(IN_DS = "mc16_13TeV:mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e6348_e5984_a875_r10724_r10726_p4060")
#// PYTHON(IN_DS = "mc16_13TeV:mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_FTAG1.e6366_e5984_a875_r10724_r10726_p4060")
#//CFS PER_SLICE(1)

# luminosity for 2018 data
#// PYTHON(lumi_data = 59937.2)

#//CFS START_SLICE(modeling)
#// PYTHON(weightspec = "[nominal_a875]<.*nominal><.*PUWeight>")
#// PYTHON(weightspec = "[MC_FRAG]<.*PUWeight><.*nominal>")
#//CFS PER_SLICE(1)

#// PYTHON(taggers = ["DL1r", "DL1"])
#// PYTHON(jetcoll = "AntiKtVR30Rmax4Rmin02TrackJets")
#// PYTHON(binconfig = "ttbar/410XXX_CDI_extrapolation_VRTrackJets.conf")

#//CFS START_SLICE(modeling)
#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".410472.AntiKtVRTrackJets.MUNC_fastsim." + self.slicenumber)
#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".410558.AntiKtVRTrackJets.MUNC_fastsim." + self.slicenumber)
#//CFS PER_SLICE(1)

#// PYTHON(opts = ["--inDS", IN_DS, "--outDS", OUT_DS, "--nFilesPerJob", "1"])
#// PYTHON(package_name = "btagAnalysis")
#// PYTHON(job_option_file = "jobOptions_AntiKtVR30Rmax4Rmin02TrackJets_tracksyst.py")
#// PYTHON(pos_opts = [package_name + "/" + self.thisfilename, package_name + "/" + job_option_file])
#// PYTHON(print(" ".join(["pathena"] + pos_opts + opts)))
#// PYTHON(sp.check_output(["pathena"] + pos_opts + opts))

#// END_UGS
# ---------------------------------------------------------

JetCollections = ['AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903']
TruthJetCollections = ['AntiKt4TruthJets']

# jet definition as expected by some of the required tools
JetDefinitions = ['AntiKtVR30Rmax4Rmin02Track']

# which jet collection to use for the TIDE uncertainties
TIDEJetCollections = ['AntiKt4EMTopoJets']

# set the name of the systematic variation to be performed for tracks
systVariations = {
    "Nominal":              []
    }

# store all weights
stored_MC_weights = ["*"]

data_type = "afii"
MC_type = "AFII"

# manually use a run number in 2018
runNumber = 348885

# configuration files for pileup reweighting
lumicalc_files = [
    "GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"
    ]
config_files = [
    "GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",
#//CFS START_SLICE(modeling)
    "btagAnalysis/410472.NTUP_PILEUP_MC16e_a875.root",
    "btagAnalysis/410558.NTUP_PILEUP_MC16e_a875.root"
#//CFS PER_SLICE(1)
    ]

#//CFS START_SLICE(modeling)
input_file = "/data/atlas/atlasdata/windischhofer/mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e6348_e5984_a875_r10201_r10210_p4060/DAOD_FTAG1.20304222._000579.pool.root.1"
input_file = "/data/atlas/atlasdata/windischhofer/mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_FTAG1.e6366_a875_r10201_p4060/DAOD_FTAG1.20303724._000194.pool.root.1"
#//CFS PER_SLICE(1)

