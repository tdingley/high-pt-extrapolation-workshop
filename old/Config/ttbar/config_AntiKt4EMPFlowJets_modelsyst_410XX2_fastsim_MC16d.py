# ---------------------------------------------------------
# this section is for self-submitting jobs
# ---------------------------------------------------------
# have the following variables available by default:
#    self.thisfilepath ... full path to this file
#    self.thisfilename ... file name of this file
#    self.slicenumber ... slice number of this file (if any)
#    self.slice_suffix ... full slice suffix of this file (if any)
#    sp ... subprocess module

#// START_UGS
#// PYTHON(campaign_name = "2019_10_01_ttbar_MC16d")

#//CFS START_SLICE(modeling)
#// PYTHON(IN_DS = "mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e6348_e5984_a875_r10201_r10210_p3970")
#// PYTHON(IN_DS = "mc16_13TeV:mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_FTAG1.e6366_a875_r10201_p3970")
#//CFS PER_SLICE(1)

# luminosity for 2017 data
#// PYTHON(lumi_data = 44307.4)

#//CFS START_SLICE(modeling)
#// PYTHON(weightspec = "[nominal_a875]<.*nominal><.*PUWeight>")
#// PYTHON(weightspec = "[MC_FRAG]<.*PUWeight><.*nominal>")
#//CFS PER_SLICE(1)

#// PYTHON(taggers = ["DL1r", "DL1", "MV2c10"])
#// PYTHON(jetcoll = "AntiKt4EMPFlowJets")
#// PYTHON(binconfig = "ttbar/410XXX_CDI_extrapolation_fine.conf")

#//CFS START_SLICE(modeling)
#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".410472.AntiKt4EMPFlowJets.MUNC_fastsim." + self.slicenumber)
#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".410558.AntiKt4EMPFlowJets.MUNC_fastsim." + self.slicenumber)
#//CFS PER_SLICE(1)

#// PYTHON(opts = ["--inDS", IN_DS, "--outDS", OUT_DS, "--nFilesPerJob", "1"])
#// PYTHON(package_name = "btagAnalysis")
#// PYTHON(job_option_file = "jobOptions_AntiKt4EMPFlowJets_tracksyst.py")
#// PYTHON(pos_opts = [package_name + "/" + self.thisfilename, package_name + "/" + job_option_file])
#// PYTHON(print(" ".join(["pathena"] + pos_opts + opts)))
#// PYTHON(sp.check_output(["pathena"] + pos_opts + opts))

#// END_UGS
# ---------------------------------------------------------

JetCollections = ['AntiKt4EMPFlowJets']

# jet definition as expected by some of the required tools
JetDefinitions = ['AntiKt4EMPFlow']
TruthJetCollections = ['AntiKt4TruthJets']

# set the name of the systematic variation to be performed for tracks
systVariations = {
    "Nominal":              []
    }

calibrate_jets_before_retagging = False
calibrate_jets_after_retagging = True

# store all weights
stored_MC_weights = ["*"]

data_type = "afii"
MC_type = "AFII"

# manually use a run number in 2017
runNumber = 334736

# configuration files for pileup reweighting
lumicalc_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
    ]
config_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",
#//CFS START_SLICE(modeling)
    "btagAnalysis/410472.NTUP_PILEUP_MC16d_a875.root",
    "btagAnalysis/410558.NTUP_PILEUP_MC16d_a875.root",
#//CFS PER_SLICE(1)
    ]

#//CFS START_SLICE(modeling)
input_file = ""
input_file = ""
#//CFS PER_SLICE(1)

