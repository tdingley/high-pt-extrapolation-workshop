# ---------------------------------------------------------
# this section is for self-submitting jobs
# ---------------------------------------------------------
# have the following variables available by default:
#    self.thisfilepath ... full path to this file
#    self.thisfilename ... file name of this file
#    self.slicenumber ... slice number of this file (if any)
#    self.slice_suffix ... full slice suffix of this file (if any)
#    sp ... subprocess module

#// START_UGS
#// PYTHON(campaign_name = "2019_10_01_ttbar_MC16a")
#// PYTHON(IN_DS = "mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e6348_e5984_s3126_r9364_r9315_p3970")

# luminosity for 2015/16 data
#// PYTHON(lumi_data = 32988.1 + 3219.56)

#//CFS START_SLICE(systematics)
#// PYTHON(weightspec = "[nominal_s3126]<.*nominal><.*PUWeight>;[MC_ISR_DOWN]<.*Var3cDown><.*muR=2.0,muF=2.0><.*PUWeight>/<.*nominal>;[MC_FSR_UP]<.*isr:muRfac=1.0_fsr:muRfac=2.0><.*PUWeight>;[MC_FSR_DOWN]<.*isr:muRfac=1.0_fsr:muRfac=0.5><.*PUWeight>")
#//CFS PER_SLICE(1)

#// PYTHON(taggers = ["DL1r", "DL1", "MV2c10"])
#// PYTHON(jetcoll = "AntiKt4EMPFlowJets")
#// PYTHON(binconfig = "ttbar/410XXX_CDI_extrapolation.conf")

#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".410472.AntiKt4EMPFlowJets.MUNC_fullsim." + self.slicenumber)
#// PYTHON(opts = ["--inDS", IN_DS, "--outDS", OUT_DS, "--nFilesPerJob", "1"])
#// PYTHON(package_name = "btagAnalysis")
#// PYTHON(job_option_file = "jobOptions_AntiKt4EMPFlowJets_tracksyst.py")
#// PYTHON(pos_opts = [package_name + "/" + self.thisfilename, package_name + "/" + job_option_file])
#// PYTHON(print(" ".join(["pathena"] + pos_opts + opts)))
#// PYTHON(sp.check_output(["pathena"] + pos_opts + opts))
#// END_UGS
# ---------------------------------------------------------

JetCollections = ['AntiKt4EMPFlowJets']
TruthJetCollections = ['AntiKt4TruthJets']

# jet definition as expected by some of the required tools
JetDefinitions = ['AntiKt4EMPFlow']

# set the name of the systematic variation to be performed for tracks
systVariations = {
#//CFS START_SLICE(systematics)
    "Nominal":                 [],
#//CFS PER_SLICE(1)
    }

calibrate_jets_before_retagging = False
calibrate_jets_after_retagging = True

# configuration files for pileup reweighting
lumicalc_files = [
    "GoodRunsLists/data15_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root",
    "GoodRunsLists/data16_13TeV/20161101/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root"
    ]
config_files = [
    "btagAnalysis/410472.NTUP_PILEUP_MC16a_s3126.root"    
    ]

# store all MC weights
stored_MC_weights = ["*"]

data_type = "mc"
MC_type = "MC16"

# manually use a run number in 2015/16
runNumber = 284484

# nominal ttbar sample
#input_file = "/data/atlas/atlasdata/windischhofer/mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e6348_e5984_s3126_r9364_r9315_p3703/DAOD_FTAG1.18080782._000214.pool.root.1"
input_file = ""
