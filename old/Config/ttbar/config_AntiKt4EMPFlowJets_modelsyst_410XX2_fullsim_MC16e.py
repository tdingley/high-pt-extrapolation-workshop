# ---------------------------------------------------------
# this section is for self-submitting jobs
# ---------------------------------------------------------
# have the following variables available by default:
#    self.thisfilepath ... full path to this file
#    self.thisfilename ... file name of this file
#    self.slicenumber ... slice number of this file (if any)
#    self.slice_suffix ... full slice suffix of this file (if any)
#    sp ... subprocess module

#// START_UGS
#// PYTHON(campaign_name = "2019_10_01_ttbar_MC16e")
#// PYTHON(IN_DS = "mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e6348_e5984_s3126_r10724_r10726_p3970")

# luminosity for 2018 data
#// PYTHON(lumi_data = 59937.2)

#//CFS START_SLICE(systematics)
#// PYTHON(weightspec = "[nominal_s3126]<.*nominal><.*PUWeight>;[MC_ISR_DOWN]<.*Var3cDown><.*muR=2.0,muF=2.0><.*PUWeight>/<.*nominal>;[MC_FSR_UP]<.*isr:muRfac=1.0_fsr:muRfac=2.0><.*PUWeight>;[MC_FSR_DOWN]<.*isr:muRfac=1.0_fsr:muRfac=0.5><.*PUWeight>")
#//CFS PER_SLICE(1)

#// PYTHON(taggers = ["DL1r", "DL1", "MV2c10"])
#// PYTHON(jetcoll = "AntiKt4EMPFlowJets")
#// PYTHON(binconfig = "ttbar/410XXX_CDI_extrapolation.conf")

#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".410472.AntiKt4EMPFlowJets.MUNC_fullsim." + self.slicenumber)
#// PYTHON(opts = ["--inDS", IN_DS, "--outDS", OUT_DS, "--nFilesPerJob", "1"])
#// PYTHON(package_name = "btagAnalysis")
#// PYTHON(job_option_file = "jobOptions_AntiKt4EMPFlowJets_tracksyst.py")
#// PYTHON(pos_opts = [package_name + "/" + self.thisfilename, package_name + "/" + job_option_file])
#// PYTHON(print(" ".join(["pathena"] + pos_opts + opts)))
#// PYTHON(sp.check_output(["pathena"] + pos_opts + opts))
#// END_UGS
# ---------------------------------------------------------

JetCollections = ['AntiKt4EMPFlowJets']
TruthJetCollections = ['AntiKt4TruthJets']

# jet definition as expected by some of the required tools
JetDefinitions = ['AntiKt4EMPFlow']

# set the name of the systematic variation to be performed for tracks
systVariations = {
#//CFS START_SLICE(systematics)
    "Nominal":                 [],
#//CFS PER_SLICE(1)
    }

calibrate_jets_before_retagging = False
calibrate_jets_after_retagging = True

# configuration files for pileup reweighting
lumicalc_files = [
    "GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"
    ]
config_files = [
    "GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",
    "btagAnalysis/410472.NTUP_PILEUP_MC16e_s3126.root"    
    ]

# store all MC weights
stored_MC_weights = ["*"]

data_type = "mc"
MC_type = "MC16"

# manually use a run number in 2018
runNumber = 348885

# nominal ttbar sample
input_file = ""
