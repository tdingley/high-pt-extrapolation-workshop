# ---------------------------------------------------------
# this section is for self-submitting jobs
# ---------------------------------------------------------
# have the following variables available by default:
#    self.thisfilepath ... full path to this file
#    self.thisfilename ... file name of this file
#    self.slicenumber ... slice number of this file (if any)
#    self.slice_suffix ... full slice suffix of this file (if any)
#    sp ... subprocess module

#// START_UGS
#// PYTHON(campaign_name = "2020_07_29_Zprime_FSR")
#// PYTHON(IN_DS = "mc16_13TeV:mc16_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_e7400_s3126_r10201_r10210_p4060")

# luminosity for 2017 data
#// PYTHON(lumi_data = 44307.4)

#// PYTHON(weightspec = "[nominal]<.*nominal><.*PUWeight>;[MC_FSR_UP]<.*isr:muRfac=1.0_fsr:muRfac=2.0><.*PUWeight>;[MC_FSR_DOWN]<.*isr:muRfac=1.0_fsr:muRfac=0.625><.*PUWeight>")
#// PYTHON(taggers = ["DL1r", "DL1"])
#// PYTHON(jetcoll = "AntiKt4EMPFlowJets")
#// PYTHON(binconfig_bjets = "Zprime/427081_CDI_extrapolation_bjets.conf")
#// PYTHON(binconfig_cjets = "Zprime/427081_CDI_extrapolation_cjets.conf")
#// PYTHON(binconfig_ljets = "Zprime/427081_CDI_extrapolation_ljets.conf")

#// PYTHON(OUT_DS = "user.ssindhu." + campaign_name + ".800030.AntiKt4EMPFlowJets.MUNC_FSR." + self.slicenumber)
#// PYTHON(opts = ["--inDS", IN_DS, "--outDS", OUT_DS, "--nFilesPerJob", "1"])
#// PYTHON(package_name = "btagAnalysis")
#// PYTHON(job_option_file = "jobOptions_AntiKt4EMPFlowJets_tracksyst.py")
#// PYTHON(pos_opts = [package_name + "/" + self.thisfilename, package_name + "/" + job_option_file])
#// PYTHON(print(" ".join(["pathena"] + pos_opts + opts)))
#// PYTHON(sp.check_output(["pathena"] + pos_opts + opts))
#// END_UGS
# ---------------------------------------------------------

JetCollections = ['AntiKt4EMPFlowJets_BTagging201903']
TruthJetCollections = ['AntiKt4TruthJets']

# jet definition as expected by some of the required tools
JetDefinitions = ['AntiKt4EMPFlow']

# which jet collection to use for the TIDE uncertainties
TIDEJetCollections = ['AntiKt4EMTopoJets']

# set the name of the systematic variation to be performed for tracks
systVariations = {
    "Nominal":                    [],
}

# store all weights
stored_MC_weights = ["*"]

calibrate_jets_before_retagging = False
calibrate_jets_after_retagging = True

# configuration files for pileup reweighting
lumicalc_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
    ]
config_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",
    "btagAnalysis/800030.NTUP_PILEUP.root"
    ]

# store all MC weights
stored_MC_weights = ["*"]

data_type = "mc"
MC_type = "MC16"

# manually use a run number in 2017
runNumber = 334736

# local test derivation
input_file = "/eos/home-s/ssindhu/QT/datasets/mc16_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_e7400_s3126_r10201_r10210_p4060/DAOD_FTAG1.21862677._000731.pool.root.1"
