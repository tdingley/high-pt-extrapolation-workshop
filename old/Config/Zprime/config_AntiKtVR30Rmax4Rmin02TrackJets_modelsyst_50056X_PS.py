# ---------------------------------------------------------
# this section is for self-submitting jobs
# ---------------------------------------------------------
# have the following variables available by default:
#    self.thisfilepath ... full path to this file
#    self.thisfilename ... file name of this file
#    self.slicenumber ... slice number of this file (if any)
#    self.slice_suffix ... full slice suffix of this file (if any)
#    sp ... subprocess module

#// START_UGS
#// PYTHON(campaign_name = "2020_07_21_Zprime_PS")

#//CFS START_SLICE(modeling)
#// PYTHON(IN_DS = "mc16_13TeV:mc16_13TeV.500568.MGPy8EG_NNPDF23ME_A14_ZPrime.deriv.DAOD_FTAG1.e7954_e7400_s3126_r10201_r10210_p4060")
#// PYTHON(IN_DS = "mc16_13TeV:mc16_13TeV.500567.MGH7EG_NNPDF23ME_Zprime.deriv.DAOD_FTAG1.e7954_e7400_s3126_r10201_r10210_p4060")
#//CFS PER_SLICE(1)

# luminosity for 2017 data
#// PYTHON(lumi_data = 44307.4)

#//CFS START_SLICE(modeling)
#// PYTHON(weightspec = "[nominal]<.*nominal><.*PUWeight>")
#// PYTHON(weightspec = "[MC_FRAG]<.*nominal><.*PUWeight>")
#//CFS PER_SLICE(1)

#// PYTHON(taggers = ["DL1r", "DL1"])
#// PYTHON(jetcoll = "AntiKtVR30Rmax4Rmin02TrackJets")
#// PYTHON(binconfig = "Zprime/427081_CDI_extrapolation_VRTrackJets.conf")

#//CFS START_SLICE(modeling)
#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".500568.AntiKtVRTrackJets.MUNC_PS." + self.slicenumber)
#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".500567.AntiKtVRTrackJets.MUNC_PS." + self.slicenumber)
#//CFS PER_SLICE(1)

#// PYTHON(opts = ["--inDS", IN_DS, "--outDS", OUT_DS, "--nFilesPerJob", "1"])
#// PYTHON(package_name = "btagAnalysis")
#// PYTHON(job_option_file = "jobOptions_AntiKtVR30Rmax4Rmin02TrackJets_tracksyst.py")
#// PYTHON(pos_opts = [package_name + "/" + self.thisfilename, package_name + "/" + job_option_file])
#// PYTHON(print(" ".join(["pathena"] + pos_opts + opts)))
#// PYTHON(sp.check_output(["pathena"] + pos_opts + opts))

#// END_UGS
# ---------------------------------------------------------

JetCollections = ['AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903']
TruthJetCollections = ['AntiKt4TruthJets']

# jet definition as expected by some of the required tools
JetDefinitions = ['AntiKtVR30Rmax4Rmin02Track']

# which jet collection to use for the TIDE uncertainties
TIDEJetCollections = ['AntiKt4EMTopoJets']

# set the name of the systematic variation to be performed for tracks
systVariations = {
    "Nominal":              []
    }

# store all weights
stored_MC_weights = ["*"]

data_type = "mc"
MC_type = "MC16"

# manually use a run number in 2017
runNumber = 334736

# configuration files for pileup reweighting
lumicalc_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
    ]
config_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",
#//CFS START_SLICE(modeling)
    "btagAnalysis/500568.NTUP_PILEUP.root",
    "btagAnalysis/500567.NTUP_PILEUP.root",
#//CFS PER_SLICE(1)
    ]

#//CFS START_SLICE(modeling)
input_file = "/data/atlas/atlasdata/windischhofer/mc16_13TeV.500568.MGPy8EG_NNPDF23ME_A14_ZPrime.deriv.DAOD_FTAG1.e7954_e7400_s3126_r10201_r10210_p4060_tid21862680_00/DAOD_FTAG1.21862680._000021.pool.root.1"
input_file = "/data/atlas/atlasdata/windischhofer/mc16_13TeV.500567.MGH7EG_NNPDF23ME_Zprime.deriv.DAOD_FTAG1.e7954_e7400_s3126_r10201_r10210_p4060/DAOD_FTAG1.21862674._000758.pool.root.1"
#//CFS PER_SLICE(1)
