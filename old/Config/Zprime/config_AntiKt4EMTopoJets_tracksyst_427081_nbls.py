# ---------------------------------------------------------
# this section is for self-submitting jobs
# ---------------------------------------------------------
# have the following variables available by default:
#    self.thisfilepath ... full path to this file
#    self.thisfilename ... file name of this file
#    self.slicenumber ... slice number of this file (if any)
#    self.slice_suffix ... full slice suffix of this file (if any)
#    sp ... subprocess module

#// START_UGS
#// PYTHON(campaign_name = "2019_05_16_Zprime")
#// PYTHON(IN_DS = "mc16_13TeV:mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e6928_e5984_s3126_r10201_p3703")

# luminosity for 2017 data
#// PYTHON(lumi_data = 44307.4)

#// PYTHON(weightspec = "[nominal]<.*nominal><.*PUWeight>")
#// PYTHON(taggers = ["DL1r", "DL1", "MV2c10"])
#// PYTHON(jetcoll = "AntiKt4EMTopoJets")
#// PYTHON(binconfig = "Zprime/427081_CDI_extrapolation.conf")

#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".427081.AntiKt4EMTopoJets.TUNC_NBLS." + self.slicenumber)
#// PYTHON(opts = ["--inDS", IN_DS, "--outDS", OUT_DS, "--nFilesPerJob", "1"])
#// PYTHON(package_name = "btagAnalysis")
#// PYTHON(job_option_file = "jobOptions_AntiKt4EMTopoJets_tracksyst.py")
#// PYTHON(pos_opts = [package_name + "/" + self.thisfilename, package_name + "/" + job_option_file])
#// PYTHON(print(" ".join(["pathena"] + pos_opts + opts)))
#// PYTHON(sp.check_output(["pathena"] + pos_opts + opts))
#// END_UGS
# ---------------------------------------------------------

JetCollections = ['AntiKt4EMTopoJets']
TruthJetCollections = ['AntiKt4TruthJets']

# jet definition as expected by some of the required tools
JetDefinitions = ['AntiKt4EMTopo']

# set the name of the systematic variation to be performed for tracks
systVariations = {
    "Nominal":                    [],
    "TRK_FAKE_RATE_LOOSE":        ["TRK_FAKE_RATE_LOOSE"],
    "TRK_FAKE_RATE_LOOSE_TIDE":   ["TRK_FAKE_RATE_LOOSE_TIDE"],
    "TRK_EFF_LOOSE_TIDE":         ["TRK_EFF_LOOSE_TIDE"],
    "TRK_EFF_LOOSE_IBL":          ["TRK_EFF_LOOSE_IBL"],
    "TRK_EFF_LOOSE_PP0":          ["TRK_EFF_LOOSE_PP0"],
    "TRK_EFF_LOOSE_GLOBAL":       ["TRK_EFF_LOOSE_GLOBAL"],
    "TRK_EFF_LOOSE_PHYSMODEL":    ["TRK_EFF_LOOSE_PHYSMODEL"],
    "TRK_BIAS_D0_WM":             ["TRK_BIAS_D0_WM"],
    "TRK_BIAS_Z0_WM":             ["TRK_BIAS_Z0_WM"],
    "TRK_BIAS_QOVERP_SAGITTA_WM": ["TRK_BIAS_QOVERP_SAGITTA_WM"],
    "TRK_RES_D0_DEAD":            ["TRK_RES_D0_DEAD"],
    "TRK_RES_Z0_DEAD":            ["TRK_RES_Z0_DEAD"]
    }

calibrate_jets_before_retagging = False
calibrate_jets_after_retagging = True

# configuration files for pileup reweighting
lumicalc_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
    ]
config_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",
    "btagAnalysis/427081.NTUP_PILEUP.root"
    ]

# store all MC weights
stored_MC_weights = ["*"]

data_type = "mc"
MC_type = "MC16"

# manually use a run number in 2017
runNumber = 334736

# extended Zprime sample
input_file = "/data/atlas/atlasdata/windischhofer/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e6928_e5984_s3126_r10201_p3703/DAOD_FTAG1.16200001._000335.pool.root.1"
