# ---------------------------------------------------------
# this section is for self-submitting jobs
# ---------------------------------------------------------
# have the following variables available by default:
#    self.thisfilepath ... full path to this file
#    self.thisfilename ... file name of this file
#    self.slicenumber ... slice number of this file (if any)
#    self.slice_suffix ... full slice suffix of this file (if any)
#    sp ... subprocess module

#// START_UGS
#// PYTHON(campaign_name = "2019_05_16_Zprime")
#// PYTHON(IN_DS = "mc16_13TeV:mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e6928_e5984_s3126_r10201_p3703")

# luminosity for 2017 data
#// PYTHON(lumi_data = 44307.4)

#// PYTHON(weightspec = "[nominal]<.*nominal><.*PUWeight>")
#// PYTHON(taggers = ["DL1r", "DL1", "MV2c10"])
#// PYTHON(jetcoll = "AntiKt4EMTopoJets")
#// PYTHON(binconfig = "Zprime/427081_CDI_extrapolation.conf")

#// PYTHON(OUT_DS = "user.phwindis." + campaign_name + ".427081.AntiKt4EMTopoJets.JUNC_noretagging." + self.slicenumber)
#// PYTHON(opts = ["--inDS", IN_DS, "--outDS", OUT_DS, "--nFilesPerJob", "1"])
#// PYTHON(package_name = "btagAnalysis")
#// PYTHON(job_option_file = "jobOptions_AntiKt4EMTopoJets_jetsyst_noretagging.py")
#// PYTHON(pos_opts = [package_name + "/" + self.thisfilename, package_name + "/" + job_option_file])
#// PYTHON(print(" ".join(["pathena"] + pos_opts + opts)))
#// PYTHON(sp.check_output(["pathena"] + pos_opts + opts))
#// END_UGS
# ---------------------------------------------------------

JetCollections = ['AntiKt4EMTopoJets']
TruthJetCollections = ['AntiKt4TruthJets']

# # jet definition as expected by some of the required tools
JetDefinitions = ['AntiKt4EMTopo']

# set the name of the systematic variation to be performed for tracks
systVariations = {
#//CFS START_SLICE(systematics)
    "Nominal":                                          [],
    "JET_BJES_Response_1up":                            ["JET_BJES_Response__1up"],
    "JET_BJES_Response_1down":                          ["JET_BJES_Response__1down"],
    "JET_EffectiveNP_Detector1__1up":                   ["JET_EffectiveNP_Detector1__1up"],
    "JET_EffectiveNP_Detector1__1down":                 ["JET_EffectiveNP_Detector1__1down"],
    "JET_EffectiveNP_Mixed1__1up":                      ["JET_EffectiveNP_Mixed1__1up"],
    "JET_EffectiveNP_Mixed1__1down":                    ["JET_EffectiveNP_Mixed1__1down"],
    "JET_EffectiveNP_Mixed2__1up":                      ["JET_EffectiveNP_Mixed2__1up"],
    "JET_EffectiveNP_Mixed2__1down":                    ["JET_EffectiveNP_Mixed2__1down"],
    "JET_EffectiveNP_Mixed3__1up":                      ["JET_EffectiveNP_Mixed3__1up"],
    "JET_EffectiveNP_Mixed3__1down":                    ["JET_EffectiveNP_Mixed3__1down"],
    "JET_EffectiveNP_Modelling1__1up":                  ["JET_EffectiveNP_Modelling1__1up"],
    "JET_EffectiveNP_Modelling1__1down":                ["JET_EffectiveNP_Modelling1__1down"],
    "JET_EffectiveNP_Modelling2__1up":                  ["JET_EffectiveNP_Modelling2__1up"],
    "JET_EffectiveNP_Modelling2__1down":                ["JET_EffectiveNP_Modelling2__1down"],
    "JET_EffectiveNP_Modelling3__1up":                  ["JET_EffectiveNP_Modelling3__1up"],
    "JET_EffectiveNP_Modelling3__1down":                ["JET_EffectiveNP_Modelling3__1down"],
    "JET_EffectiveNP_Modelling4__1up":                  ["JET_EffectiveNP_Modelling4__1up"],
    "JET_EffectiveNP_Modelling4__1down":                ["JET_EffectiveNP_Modelling4__1down"],
    "JET_EffectiveNP_Statistical1__1up":                ["JET_EffectiveNP_Statistical1__1up"],
    "JET_EffectiveNP_Statistical1__1down":              ["JET_EffectiveNP_Statistical1__1down"],
    "JET_EffectiveNP_Statistical2__1up":                ["JET_EffectiveNP_Statistical2__1up"],
    "JET_EffectiveNP_Statistical2__1down":              ["JET_EffectiveNP_Statistical2__1down"],
    "JET_EffectiveNP_Statistical3__1up":                ["JET_EffectiveNP_Statistical3__1up"],
    "JET_EffectiveNP_Statistical3__1down":              ["JET_EffectiveNP_Statistical3__1down"],
    "JET_EffectiveNP_Statistical4__1up":                ["JET_EffectiveNP_Statistical4__1up"],
    "JET_EffectiveNP_Statistical4__1down":              ["JET_EffectiveNP_Statistical4__1down"],
    "JET_EffectiveNP_Statistical5__1up":                ["JET_EffectiveNP_Statistical5__1up"],
    "JET_EffectiveNP_Statistical5__1down":              ["JET_EffectiveNP_Statistical5__1down"],
    "JET_EffectiveNP_Statistical6__1up":                ["JET_EffectiveNP_Statistical6__1up"],
    "JET_EffectiveNP_Statistical6__1down":              ["JET_EffectiveNP_Statistical6__1down"],
    "JET_EtaIntercalibration_Modelling__1up":           ["JET_EtaIntercalibration_Modelling__1up"],
    "JET_EtaIntercalibration_Modelling__1down":         ["JET_EtaIntercalibration_Modelling__1down"],
    "JET_EtaIntercalibration_NonClosure_highE__1up":    ["JET_EtaIntercalibration_NonClosure_highE__1up"],
    "JET_EtaIntercalibration_NonClosure_highE__1down":  ["JET_EtaIntercalibration_NonClosure_highE__1down"],
    "JET_EtaIntercalibration_NonClosure_negEta__1up":   ["JET_EtaIntercalibration_NonClosure_negEta__1up"],
    "JET_EtaIntercalibration_NonClosure_negEta__1down": ["JET_EtaIntercalibration_NonClosure_negEta__1down"],
    "JET_EtaIntercalibration_NonClosure_posEta__1up":   ["JET_EtaIntercalibration_NonClosure_posEta__1up"],
    "JET_EtaIntercalibration_NonClosure_posEta__1down": ["JET_EtaIntercalibration_NonClosure_posEta__1down"],
    "JET_EtaIntercalibration_TotalStat__1up":           ["JET_EtaIntercalibration_TotalStat__1up"],
    "JET_EtaIntercalibration_TotalStat__1down":         ["JET_EtaIntercalibration_TotalStat__1down"],
    "JET_Flavor_Composition__1up":                      ["JET_Flavor_Composition__1up"],
    "JET_Flavor_Composition__1down":                    ["JET_Flavor_Composition__1down"],
    "JET_Flavor_Response__1up":                         ["JET_Flavor_Response__1up"],
    "JET_Flavor_Response__1down":                       ["JET_Flavor_Response__1down"],
    "JET_JER_DataVsMC__1up":                            ["JET_JER_DataVsMC__1up"],
    "JET_JER_DataVsMC__1down":                          ["JET_JER_DataVsMC__1down"],
    "JET_JER_EffectiveNP_1__1up":                       ["JET_JER_EffectiveNP_1__1up"],
    "JET_JER_EffectiveNP_1__1down":                     ["JET_JER_EffectiveNP_1__1down"],
    "JET_JER_EffectiveNP_2__1up":                       ["JET_JER_EffectiveNP_2__1up"],
    "JET_JER_EffectiveNP_2__1down":                     ["JET_JER_EffectiveNP_2__1down"],
    "JET_JER_EffectiveNP_3__1up":                       ["JET_JER_EffectiveNP_3__1up"],
    "JET_JER_EffectiveNP_3__1down":                     ["JET_JER_EffectiveNP_3__1down"],
    "JET_JER_EffectiveNP_4__1up":                       ["JET_JER_EffectiveNP_4__1up"],
    "JET_JER_EffectiveNP_4__1down":                     ["JET_JER_EffectiveNP_4__1down"],
    "JET_JER_EffectiveNP_5__1up":                       ["JET_JER_EffectiveNP_5__1up"],
    "JET_JER_EffectiveNP_5__1down":                     ["JET_JER_EffectiveNP_5__1down"],
    "JET_JER_EffectiveNP_6__1up":                       ["JET_JER_EffectiveNP_6__1up"],
    "JET_JER_EffectiveNP_6__1down":                     ["JET_JER_EffectiveNP_6__1down"],
    "JET_JER_EffectiveNP_7restTerm__1up":               ["JET_JER_EffectiveNP_7restTerm__1up"],
    "JET_JER_EffectiveNP_7restTerm__1down":             ["JET_JER_EffectiveNP_7restTerm__1down"],
    "JET_Pileup_OffsetMu__1up":                         ["JET_Pileup_OffsetMu__1up"],
    "JET_Pileup_OffsetMu__1down":                       ["JET_Pileup_OffsetMu__1down"],
    "JET_Pileup_OffsetNPV__1up":                        ["JET_Pileup_OffsetNPV__1up"],
    "JET_Pileup_OffsetNPV__1down":                      ["JET_Pileup_OffsetNPV__1down"],
    "JET_Pileup_PtTerm__1up":                           ["JET_Pileup_PtTerm__1up"],
    "JET_Pileup_PtTerm__1down":                         ["JET_Pileup_PtTerm__1down"],
    "JET_Pileup_RhoTopology__1up":                      ["JET_Pileup_RhoTopology__1up"],
    "JET_Pileup_RhoTopology__1down":                    ["JET_Pileup_RhoTopology__1down"],
    "JET_PunchThrough_MC16__1up":                       ["JET_PunchThrough_MC16__1up"],
    "JET_PunchThrough_MC16__1down":                     ["JET_PunchThrough_MC16__1down"],
    "JET_SingleParticle_HighPt__1up":                   ["JET_SingleParticle_HighPt__1up"],
    "JET_SingleParticle_HighPt__1down":                 ["JET_SingleParticle_HighPt__1down"],
#//CFS PER_SLICE(10)
  }

tracks_syst_baseline = []

# store all MC weights
stored_MC_weights = ["*"]

data_type = "mc"
MC_type = "MC16"

# manually use a run number in 2017
runNumber = 334736

# configuration files for pileup reweighting
lumicalc_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
    ]
config_files = [
    "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root",
    "btagAnalysis/427081.NTUP_PILEUP.root"
    ]

# extended Zprime sample
input_file = "/data/atlas/atlasdata/windischhofer/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e6928_e5984_s3126_r10201_p3703/DAOD_FTAG1.16200001._000335.pool.root.1"
