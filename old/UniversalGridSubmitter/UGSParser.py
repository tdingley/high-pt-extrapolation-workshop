import os, re

class UGSParser:
    
    def __init__(self, infile):
        self.infile = infile

        self.thisfilepath = self.infile.name
        self.thisfilename = os.path.basename(self.thisfilepath)
        self.slicenumber = self._get_slice_number(self.thisfilepath)
        self.slice_suffix = self._get_slice_suffix(self.thisfilepath)

    def extract_variable_definition(self, var_names):
        # parse the UGS section line by line, but execute only the variable definitions. don't want to submit the job again accidentally!
        active = False
        _locals = locals()

        cmdbuffer = []
        for line in self.infile:
            # take care of activating and deactivating the UGS environment
            if not active and self._is_start_UGS_line(line):
                active = True

            if active and self._is_end_UGS_line(line):
                active = False
                break

            if active:
                cmds = self._extract_python_lines(line)
                if cmds:
                    for cmd in cmds:
                        if self._is_global_variable_definition(cmd) or self._is_import_statement(cmd):
                            cmdbuffer.append(cmd)
                            #exec(cmd, globals(), _locals)
        
        exec("\n".join(cmdbuffer), globals(), _locals)

        retval = {}

        for spec in var_names:
            if spec in locals():
                retval[spec] = locals()[spec]
        
        return retval

    def execute(self):
        active = False
        _locals = locals()

        for line in self.infile:
            
            # take care of activating and deactivating the UGS environment
            if not active and self._is_start_UGS_line(line):
                active = True

            if active and self._is_end_UGS_line(line):
                active = False
                break

            # here, the actual UGS commands get parsed
            if active:
                cmds = self._extract_python_lines(line)
                if cmds:
                    for cmd in cmds:
                        print(cmd)
                        exec(cmd, globals(), _locals)


    def _get_slice_number(self, line):
        slice_number_re = re.compile(".*slice_(.*).py")
        slice_numbers = slice_number_re.findall(line)
        if len(slice_numbers) == 1:
            return slice_numbers[0]
        else:
            return ""

    def _get_slice_suffix(self, line):
        slice_suffix_re = re.compile(".*(slice_.*).py")
        slice_suffixes = slice_suffix_re.findall(line)
        if len(slice_suffixes) == 1:
            return slice_suffixes[0]
        else:
            return ""

    def _is_import_statement(self, line):
        startswith_import = re.compile("^import")
        return startswith_import.match(line)

    def _is_global_variable_definition(self, line):
        startswith_whitespace = re.compile("^\s")
        return self._is_variable_definition(line.lstrip()) and not startswith_whitespace.match(line)

    def _is_variable_definition(self, line):
        vardef_re = re.compile("^(.+?)\s*=\s*(.+?)$")
        return True if vardef_re.match(line) else False

    def _is_start_UGS_line(self, line):
        start_re = re.compile("#\/\/.*START_UGS")
        return True if start_re.match(line) else False

    def _is_end_UGS_line(self, line):
        end_re = re.compile("#\/\/.*END_UGS")
        return True if end_re.match(line) else False

    def _extract_python_lines(self, line):
        python_re = re.compile("#\/\/.*PYTHON\((.*)\)")
        return python_re.findall(line)

