import uuid, re, os

class RucioScriptGenerator:

    preamble = ("#!/bin/bash\n" +
                "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n"+
                "export RUCIO_ACCOUNT=ssindhu\n" + 
                "export X509_USER_PROXY=/afs/cern.ch/user/s/ssindhu/.gridProxy\n" + 
                "source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh\n" + 
                "lsetup rucio\n")

    postscript = ("cd $DEST_DIR\n" + 
                  "rucio -v download --protocol srm --ndownloader 5 $DS\n")
    
    @staticmethod
    def generate_job_script_from_dataset_id(dataset_id, output_dir, job_script_dir = None):
        if not job_script_dir:
            job_script_dir = output_dir # if not set explicitly, put the job scripts into the same directory as the downloaded data
        
        job_script_name = str(uuid.uuid4()) + ".sh"
        job_script_path = os.path.join(job_script_dir, job_script_name)
        print("generating job script with name '" + job_script_name + "'")
        
        # write the job script to disk and return the path to it
        with open(job_script_path, 'w') as outfile:
            outfile.write(RucioScriptGenerator.preamble)

            # now write the actual content of the script
            outfile.write('DEST_DIR="' + output_dir + '"\n')
            outfile.write('DS="' + dataset_id + '"\n')

            # then write the postscript, which is again common for all cases
            outfile.write(RucioScriptGenerator.postscript)

        return job_script_path
