# this is the universal dataset downloader
import sys
from optparse import OptionParser
from DatasetDownloader import DatasetDownloader
from JobSubmitter import JobSubmitter
from RucioFetcher import RucioFetcher

def main():

    default_data_directory = "/afs/cern.ch/work/s/ssindhu/private/pflow_jets/high-pT-extrapolation/logfiles/"
    
    parser = OptionParser()
    parser.add_option("--outdir", action = "store", dest = "outdir")
    parser.add_option("--scriptdir", action = "store", dest = "scriptdir")

    (options, args) = parser.parse_args()
    syst_list = ["user.ssindhu.800030.TRK_RES_Z0_MEAS", "user.ssindhu.800030.TRK_RES_Z0_MEAS", "user.ssindhu.800030.TRK_FAKE_RATE_LOOSE", "user.ssindhu.800030.TRK_BIAS_QOVERP_SAGITTA_WM", "user.ssindhu.800030.TRK_FAKE_RATE_LOOSE_TIDE", "user.ssindhu.800030.TRK_EFF_LOOSE_TIDE", "user.ssindhu.800030.TRK_RES_Z0_DEAD", "user.ssindhu.800030.TRK_RES_D0_DEAD", "user.ssindhu.800030.TRK_EFF_TIGHT_IBL", "user.ssindhu.800030.TRK_EFF_TIGHT_GLOBAL", "user.ssindhu.800030.TRK_EFF_LOOSE_PHYSMODEL", "user.ssindhu.800030.TRK_EFF_LOOSE_PP0", "user.ssindhu.800030.nominal", "user.ssindhu.800030.TRK_EFF_LOOSE_GLOBAL", "user.ssindhu.800030.TRK_EFF_LOOSE_IBL" ]
    
    output_dir = options.outdir if options.outdir is not None else default_data_directory
    script_dir = options.scriptdir

    for syst in syst_list:
        print("now parsing '" + syst + "'")
        dd = DatasetDownloader(syst)
        script_paths = dd.generate_job_script(syst, output_dir = output_dir, job_script_dir = script_dir)

        # then submit the job script to the correct queue on the cluster
        for script_path in script_paths:
            JobSubmitter.submit_job(job_script_path = script_path,)

if __name__ == "__main__":
    main()
