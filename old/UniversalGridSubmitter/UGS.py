# this script is for "self-submitting jobs"
import sys
from SubmitterContainer import SubmitterContainer

def main():
    
    input_file_paths = sys.argv[1:]

    for input_file_path in input_file_paths:
        print("now parsing '" + input_file_path + "'")
        with open(input_file_path, 'r') as input_file:
            cont = SubmitterContainer(input_file)
            cont.parse()

if __name__ == "__main__":
    main()
