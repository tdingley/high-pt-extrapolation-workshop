import subprocess as sp
import os, time

class JobSubmitter:


    # create the .submit file and submit it to the batch
    @staticmethod
    def submit_job(job_script_path, custom_submission_cmds = [], job_threshold = 400):

        # check what kind of file we got
        if not os.path.splitext(job_script_path)[1] == ".submit":
            # need to create the submit file
            job_script_base, _ = os.path.splitext(job_script_path)
            job_dir = os.path.dirname(job_script_path)
            submit_file_path = job_script_base + ".submit"

            while True:
                try:
                    with open(submit_file_path, 'w') as submit_file:
                        submit_file_sceleton="""executable = {job_script_path}
universe = vanilla
output = datatransfer.output.$(clusterId)
error = datatransfer.error.$(clusterId)
log = datatransfer.log.$(clusterId)
use_x509userproxy = true
x509userproxy = /afs/cern.ch/user/s/ssindhu/.gridProxy

+JobFlavour = "nextweek"

{custom}
queue 1
"""
                        opts = {"job_script_path": job_script_path, "custom": '\n'.join(custom_submission_cmds)}
                        submit_file.write(submit_file_sceleton.format(**opts))
                
                    break
                except:
                    print("problem writing job script -- retrying")
                    time.sleep(10)

        else:
            # are given the submit file directly
            submit_file_path = job_script_path

        while True:
            running_jobs = JobSubmitter.queued_jobs()
            if running_jobs < job_threshold:
                break
            print("have {} jobs running - wait a bit".format(running_jobs))
            time.sleep(30)

        while True:
            try:
                # call the job submitter
                sp.check_output(["condor_submit", submit_file_path])
                print("submitted '" + submit_file_path + "'")
                break
            except:
                print("Problem submitting job - retrying in 10 seconds!")
                time.sleep(10)

    @staticmethod
    def queued_jobs(queue_status = "condor_q"):
        while True:
            try:
                running_jobs = len(sp.check_output([queue_status]).split('\n')) - 6
                return running_jobs
            except sp.CalledProcessError:
                print("{} error - retrying!".format(queue_status))
                time.sleep(10)

