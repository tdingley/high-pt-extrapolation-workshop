# this is the simple version of the dataset downloader. simply reads the dataset names from a text file and launches one job per dataset
from optparse import OptionParser
from RucioScriptGenerator import RucioScriptGenerator
from JobSubmitter import JobSubmitter

def main():
    
    parser = OptionParser()
    parser.add_option("--outdir", action = "store", dest = "outdir")
    parser.add_option("--scriptdir", action = "store", dest = "scriptdir")

    (options, args) = parser.parse_args()

    input_file_paths = args
    output_dir = options.outdir
    script_dir = options.scriptdir if options.scriptdir is not None else output_dir

    for input_file_path in input_file_paths:
        with open(input_file_path, 'r') as cur_input_file:
            for line in cur_input_file:
                dataset_id = line.rstrip()
                script_path = RucioScriptGenerator.generate_job_script_from_dataset_id(dataset_id = dataset_id, output_dir = output_dir, job_script_dir = script_dir)

                JobSubmitter.submit_job(job_script_path = script_path, custom_submission_cmds = ["+DataTransfer = true", "requirements = TARGET.DataTransfer"])

if __name__ == "__main__":
    main()
