import subprocess as sp
import re

class RucioFetcher:

    def __init__(self):
        pass

    # runs a certain rucio command and returns its output, parsing the table it produces
    def rucio(self, opts):
        raw_output = sp.check_output(["rucio"] + opts)
        
        # now parse the table headers
        output_lines = raw_output.split('\n')
        columns = self._parse_table_row(output_lines[1])

        # remove the table header as well as the trailing line from the table
        del output_lines[0:3]
        del output_lines[-1]
        
        # prepare the dictionary that is going to be returned
        retval = {column: [] for column in columns}

        # parse the remaining lines and extract the values of the individual columns
        for cur_row in output_lines:
            vals = self._parse_table_row(cur_row)

            for col, val in zip(retval.keys(), vals):
                retval[col].append(val)

        return retval

    # this parses the header of the table returned by rucio and extracts the column names
    def _parse_table_row(self, line):
        columns_re = re.compile("\|?\s*(.+?)\s*\|")
        columns = columns_re.findall(line)
        return columns
        
