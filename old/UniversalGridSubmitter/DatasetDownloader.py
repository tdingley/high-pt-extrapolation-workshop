import uuid, re, os
from RucioScriptGenerator import RucioScriptGenerator
from RucioFetcher import RucioFetcher
from UGSParser import UGSParser

class DatasetDownloader:

    def __init__(self, infile):
        self.infile = infile

        # guesses for how the dataset may be specified in the configuration file
 
        self.dataset_specification = "OUT_DS"

    def generate_job_script_from_dataset_id(self, dataset_id, output_dir, job_script_dir = None):
        if not job_script_dir:
            job_script_dir = output_dir # if not set explicitly, put the job scripts into the same directory as the downloaded data
        
        job_script_name = str(uuid.uuid4()) + ".sh"
        job_script_path = os.path.join(job_script_dir, job_script_name)
        print("generating job script with name '" + job_script_name + "'in dir"+ job_script_dir)
        
        # write the job script to disk and return the path to it
        with open(job_script_path, 'w') as outfile:
            outfile.write(self.preamble)

            # now write the actual content of the script
            outfile.write('DEST_DIR="' + output_dir + '"\n')
            outfile.write('DS="' + dataset_id + '"\n')

            # then write the postscript, which is again common for all cases
            outfile.write(self.postscript)

        return job_script_path
        
    def generate_job_script(self, filename, output_dir, job_script_dir):
        if not job_script_dir:
            job_script_dir = output_dir # if not set explicitly, put the job scripts into the same directory as the downloaded data
        
        job_script_name = str(uuid.uuid4()) + ".sh"
        job_script_path = os.path.join(job_script_dir, job_script_name)
        print("generating job script with name '" + job_script_name + "'")

        # determine the dataset to download
        input_dataset = filename
        print("found " + filename + " as dataset specification")

        # look up the dataset on rucio to make sure we fetch the right one
        rf = RucioFetcher()
        output = rf.rucio(['list-dids', filename + '*'])

        # filter the output such that retain only CONTAINERs ...
        container_inds = [ind for ind, cur_type in enumerate(output["[DID TYPE]"]) if cur_type == "DIDType.CONTAINER"  ]
        container_names = [output["SCOPE:NAME"][ind] for ind in container_inds]
	inter = enumerate(output["[DID TYPE]"])
        # ... and also strip away the *.log and GLOBAL containers, then download all the rest
        container_names_filtered = [cur_name for cur_name in container_names if "GLOBAL" not in cur_name and ".log" not in cur_name]
	print(inter)
        print("rucio lookup resulted in the following datasets that are going to be downloaded:","ind",ind,"cur_type",cur_type, "container_inds ", container_inds,"container_names", container_names, "filtered", container_names_filtered, "out", output, output["SCOPE:NAME"][ind])
        for container_name_filtered in container_names_filtered:
            print(container_name_filtered)

        # generate the job script
        job_script_paths = []

        for dataset_id in container_names_filtered:
            job_script_path = RucioScriptGenerator.generate_job_script_from_dataset_id(dataset_id, output_dir, job_script_dir)
            job_script_paths.append(job_script_path)

        return job_script_paths

