import subprocess as sp
import re, os

class SubmitterContainer:

    def __init__(self, job_config_file):
        self.job_config_file = job_config_file

        # prepare the variables that are accessible by default from the UGS part of the job config file
        self.thisfilepath = self.job_config_file.name
        self.thisfilename = os.path.basename(self.thisfilepath)
        self.slicenumber = self._get_slice_number(self.thisfilepath)
        self.slice_suffix = self._get_slice_suffix(self.thisfilepath)

    def parse(self):
        active = False
        _locals = locals()

        cmdbuffer = []

        for line in self.job_config_file:
            
            # take care of activating and deactivating the UGS environment
            if not active and self._is_start_UGS_line(line):
                active = True

            if active and self._is_end_UGS_line(line):
                active = False
                break

            # here, the actual UGS commands get parsed
            if active:
                cmds = self._extract_python_lines(line)
                if cmds:
                    for cmd in cmds:
                        print(cmd)
                        cmdbuffer.append(cmd)
                        #exec(cmd, globals(), _locals)

        # execute everything at the end
        try:
            exec("\n".join(cmdbuffer), globals(), _locals)
        except OSError:
            raise Exception("Problem executing submission command. Did you do 'lsetup panda'?")

    def _get_slice_number(self, line):
        slice_number_re = re.compile(".*slice_(.*).py")
        slice_numbers = slice_number_re.findall(line)
        if len(slice_numbers) == 1:
            return slice_numbers[0]
        else:
            return ""

    def _get_slice_suffix(self, line):
        slice_suffix_re = re.compile(".*(slice_.*).py")
        slice_suffixes = slice_suffix_re.findall(line)
        if len(slice_suffixes) == 1:
            return slice_suffixes[0]
        else:
            return ""
    
    def _is_start_UGS_line(self, line):
        start_re = re.compile("#\/\/.*START_UGS")
        return True if start_re.match(line) else False

    def _is_end_UGS_line(self, line):
        end_re = re.compile("#\/\/.*END_UGS")
        return True if end_re.match(line) else False

    def _extract_python_lines(self, line):
        python_re = re.compile("#\/\/.*PYTHON\((.*)\)")
        return python_re.findall(line)
