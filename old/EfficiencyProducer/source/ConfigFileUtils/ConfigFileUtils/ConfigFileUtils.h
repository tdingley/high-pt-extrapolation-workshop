#ifndef ConfigFileUtils_h
#define ConfigFileUtils_h

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <map>
#include <utility>
#include <functional>
#include <algorithm>

#include "TString.h"

class ConfigFileUtils
{
public:
    static std::map<TString, TString> ParsePythonDict(std::string raw);
    static std::vector<TString> ParsePythonList(std::string raw);

    static std::vector<std::string> Split(std::string raw, std::string delimiter);
    static std::string Join(std::vector<std::string> strlist, std::string delimiter);
};

#endif
