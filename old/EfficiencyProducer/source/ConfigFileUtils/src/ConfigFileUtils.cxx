#include "ConfigFileUtils/ConfigFileUtils.h"

std::map<TString, TString> ConfigFileUtils::ParsePythonDict(std::string raw)
{
    std::map<TString, TString> retval;

    std::vector<std::string> keyvals = ConfigFileUtils::Split(raw, ",");

    for(auto& keyval: keyvals)
    {
	std::vector<std::string> entry = ConfigFileUtils::Split(keyval, ": ");
	
	TString key(entry[0].c_str());
	TString value(entry[1].c_str());

	retval[key] = value;
    }

    return retval;
}

std::vector<TString> ConfigFileUtils::ParsePythonList(std::string raw)
{
    std::vector<TString> retval;

    std::vector<std::string> entries = ConfigFileUtils::Split(raw, "; ");

    for(auto& entry: entries)
    {
	retval.push_back(entry);
    }

    return retval;
}

std::vector<std::string> ConfigFileUtils::Split(std::string raw, std::string delimiters)
{
    std::vector<std::string> split_components;

    const char* delimiters_str = delimiters.c_str();

    char* raw_str = strdup(raw.c_str());
    char* pch;

    pch = strtok(raw_str, delimiters_str);
    while(pch != NULL)
    {
	std::string split_component(pch);
	split_components.push_back(split_component);

	pch = strtok(NULL, delimiters_str);
    }

    return split_components;
}

std::string ConfigFileUtils::Join(std::vector<std::string> strlist, std::string delimiter)
{
    std::string retstring = "";
    bool first = true;

    for(auto cur_fragment: strlist)
    {
	if(!first)
	    retstring += delimiter;

	retstring += cur_fragment;
	first = false;
    }

    return retstring;
}
