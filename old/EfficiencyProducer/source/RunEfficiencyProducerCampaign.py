import glob, uuid, os, sys, glob, re
import subprocess as sp
from optparse import OptionParser
import ROOT
ROOT.gROOT.SetBatch()

from UniversalGridSubmitter.UGSParser import UGSParser
from UniversalGridSubmitter.JobSubmitter import JobSubmitter
from CDITools.TFileUtils import TFileUtils

def chunks(inlist, chunklength):
    for i in xrange(0, len(inlist), chunklength):
        yield inlist[i:i + chunklength]

def get_available_trees(file_path):
    return TFileUtils.get_objectlist(file_path, "TTree")

def rootls(file_path):
    raw_trees = sp.check_output(["rootls", file_path]).split()
    return raw_trees

def prepare_histogrammer_job_script(infile_path, output_dir, options):
    xtrap_rootdir = os.environ["XTRAP_ROOTDIR"]
    if not xtrap_rootdir:
        raise Exception("Error: 'XTRAP_ROOTDIR' not defined. Please do 'source high-pT-extrapolation/setup.sh'")

    bin_dir = os.path.join(xtrap_rootdir, "EfficiencyProducer/build/")
    setup_env = "x86_64-centos7-gcc62-opt/setup.sh"
    histogrammer_bin = "x86_64-centos7-gcc62-opt/bin/runEfficiencyProducer"

    job_id = str(uuid.uuid4())
    jobfile_path = os.path.join(output_dir, job_id + ".sh")

    # create the job script
    with open(jobfile_path, "w") as job_script:
        job_script.write('#!/bin/bash' + '\n')
        job_script.write('cd ' + bin_dir + '\n')
        job_script.write('export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase' + '\n')
        job_script.write('source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh' + '\n')
        job_script.write('asetup --restore' + '\n')
        job_script.write('source ' + os.path.join(bin_dir, setup_env) + '\n')
        job_script.write('cmd="' + os.path.join(bin_dir, histogrammer_bin) + ' ' + infile_path + ' ' + os.path.join(output_dir, job_id + ".root") + ' ' + ' '.join(options) + '"\n')
        job_script.write('exec $cmd' + '\n')

    return jobfile_path

def main():
    xtrap_rootdir = os.environ["XTRAP_ROOTDIR"]
    CDI_path = os.environ["CDI_PATH"]

    parser = OptionParser()
    parser.add_option("--infile", action = "store", dest = "infiles")
    parser.add_option("--outdir", action = "store", dest = "outdir")
    parser.add_option("--RSE", action = "store_const", const = True, default = False)
    parser.add_option("--nfilesperjob", action = "store", dest = "filesperjob")
    parser.add_option("--evtMax", action = "store", dest = "evtMax", default = "-1")
    parser.add_option("--runname", action = "store", dest = "runname", default = "")
    parser.add_option("--dryrun", action = "store_const", const = True, default = False)
    parser.add_option("--nominalonly", action = "store_const", const = True, default = False)
    parser.add_option("--create_reweights", action = "store_const", const = True, default = False)
    parser.add_option("--reweight_file", action = "store", dest = "reweight_file", default = "")
    parser.add_option("--reweight_name", action = "store", dest = "reweight_name", default = "")
    parser.add_option("--indir", action = "store", dest = "indir")
    parser.add_option("--jet_type", action = "store", dest = "jettype")
    (options, args) = parser.parse_args()
    RSE = options.RSE
    if RSE:
        local_dataset = options.infiles
    else:
        local_data_directory = options.indir
    submit_scripts_directory = options.outdir
    # read the configuration files that have been passed and extract information about the location of the datasets (locally), as well as the
    # weights that are to be used when constructing the histograms
    input_config_files = args
    jet_type = str(options.jettype)
    # for each file, determine this information
    input_dirs = []
    taggers = []
    output_dirs = []
    weightspecs = []
    jetcolls = []

    lumifile_opts = []

    for input_config_file in input_config_files:
        with open(input_config_file, 'r') as cur_file:
            pars = UGSParser(cur_file)
            defs = pars.extract_variable_definition(["weightspec", "OUT_DS", "taggers", "binconfig_bjets", "binconfig_cjets", "binconfig_ljets", "jetcoll"])
            input_dataset = defs["OUT_DS"]
            weight_spec = defs["weightspec"]
            taggernames = defs["taggers"]
            jetcoll = defs["jetcoll"]
            binconfig_bjets = defs["binconfig_bjets"] if "binconfig_bjets" in defs else None
            binconfig_cjets = defs["binconfig_cjets"] if "binconfig_cjets" in defs else None
            binconfig_ljets = defs["binconfig_ljets"] if "binconfig_ljets" in defs else None

            # convert the binning specification into an absolute path
            if jet_type == "b_jet":
                binconfig = os.path.join(xtrap_rootdir, "Config", binconfig_bjets)
            elif jet_type == "c_jet":
                binconfig = os.path.join(xtrap_rootdir, "Config", binconfig_cjets)
            elif jet_type == "l_jet":
                binconfig = os.path.join(xtrap_rootdir, "Config", binconfig_ljets)
            else:
                binconfig = None
                
            print("extracted input_dataset = " + input_dataset)
            print("taggers = " + " ".join(taggernames))

            if binconfig:
                print("binconfig = " + binconfig)
            
            # try to find the matching dataset locally
            if RSE:
                datasetname = os.path.splitext(local_dataset)[0]
                print(datasetname + ' = datasetname')

            else:
                local_dataset = glob.glob(os.path.join(local_data_directory, input_dataset + '_*'))
            
                # exclude any histogrammed datasets
                local_dataset = [cur_dataset for cur_dataset in local_dataset if "hists" not in cur_dataset]

                if len(local_dataset) == 1:
                    # found unique matching dataset
                    local_dataset = local_dataset[0]
                else:
                    print("found no or multiple local candidate datasets:")
                    for local_dataset in local_dataset:
                        print(local_dataset)

                    sys.exit(-1)

                print("dataset found locally: " + local_dataset)
                print("weight specification found: " + weight_spec)

            # check for the presence of a lumi config file in the NTuple directory
            lumifile_path = os.path.join(local_dataset, "lumi.conf")
            if os.path.isfile(lumifile_path):
                print("lumi config file detected at '{}'".format(lumifile_path))
                cur_lumifile_opts = ["-l", lumifile_path]
            else:
                print("no lumi config file present")
                cur_lumifile_opts = []

            for taggername in taggernames:
                # output_dir = os.path.join(local_data_directory, local_dataset + "_hists_" + taggername + options.runname)
                output_dir = os.path.join(submit_scripts_directory, input_dataset + "_hists_" + taggername +"_"+jet_type+"_"+ options.runname)
                print("This is the output dir", output_dir)
                print("submit script directory", submit_scripts_directory)

                input_dirs.append(local_dataset)
                weightspecs.append(weight_spec)
                output_dirs.append(output_dir)
                jetcolls.append(jetcoll)
                lumifile_opts.append(cur_lumifile_opts)
                taggers.append(taggername)

    # create all required output directories
    for output_dir in output_dirs:
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

    # compose options to do with reweighting
    reweight_opts = []
    if options.create_reweights:
        reweight_opts.append("-w")
    if options.reweight_file:
        reweight_opts += ["-r", options.reweight_file]
        if options.reweight_name:
            reweight_opts += ["-i", options.reweight_name]

    for input_dir, jetcoll, output_dir, tagger, weightspec, cur_lumifile_opts in zip(input_dirs, jetcolls, output_dirs, taggers, weightspecs, lumifile_opts):
        if RSE:
#            peek_file_path = []
            with open(local_dataset, 'r') as full_list:
                infile_paths = full_list.readlines()
                peek_file_path = infile_paths[1].strip()
        else:
            infile_paths = glob.glob(os.path.join(input_dir, '*.root'))
            # take a random file in this directory for peeking
            peek_file_path = glob.glob(os.path.join(input_dir, '*.root'))
            
            if len(peek_file_path) > 0:
                peek_file_path = peek_file_path[0]
            else:
                raise FileNotFoundError("given input directory is empty!")

        print("using the following file for peeking: " + peek_file_path)

        all_trees = get_available_trees(peek_file_path)
        data_trees = [tree for tree in all_trees if "MCWeight" not in tree and "PUWeight" not in tree and "JVTWeight" not in tree]

        if options.nominalonly:
            # only take the nominal tree
            active_trees = filter(lambda cur: re.compile(".*[Nn]ominal.*").match(cur), data_trees)
            assert len(active_trees) == 1
        else:
            # run over all trees
            active_trees = data_trees

        print("have the following trees available:")
        for available_tree in active_trees:
            print(available_tree)

        # get the list of all available input files and then split them into batches of N files per job
            
        print("have " + str(len(infile_paths))+ " input files available")        

        # if nothing specified, generate a single job for all files
        if not options.filesperjob:
            files_per_job = len(infile_paths)
        else:
            files_per_job = options.filesperjob

        print("using " + str(files_per_job) + " files per job")

        # create a separate batch job to take care of each tree,
        # i.e. each systematic variation
        for available_tree in active_trees:
            for cur_infiles in chunks(infile_paths, int(files_per_job)):
                infile_path = " ".join(cur_infiles)

                if binconfig:
                    binconfig_opts = ['-c', binconfig]
                else:
                    binconfig_opts = []

                cur_job_script = prepare_histogrammer_job_script(infile_path = infile_path, 
                                                                 output_dir = output_dir,
                                                                 options = ['-t', available_tree, '-a', tagger, '-v', '\\"' + weightspec + '\\"', '-n', options.evtMax, '-j', jetcoll, '-k', jet_type, '-f', CDI_path] + binconfig_opts + cur_lumifile_opts + reweight_opts
                                                                 )

                # only submit jobs if this is not a dryrun
                if not options.dryrun:
                    JobSubmitter.submit_job(cur_job_script)
                
if __name__ == "__main__":
    main()
