import sys, os, glob, re
from argparse import ArgumentParser   
import subprocess as sp
from subprocess import Popen, PIPE, STDOUT

def run_EfficiencyProducer(indir, outdir, runname, nfilesperjob, jet_type, config, dryrun):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "EfficiencyProducer/source")
    print(dryrun)
    if dryrun == True:
        cmd = ["python", os.path.join(pythondir, "RunEfficiencyProducerCampaign.py"), "--indir", indir, "--outdir", outdir, "--runname", runname, "--nfilesperjob", nfilesperjob, "--jet_type", jet_type, "--dryrun", config]

    else:
        cmd = ["python", os.path.join(pythondir, "RunEfficiencyProducerCampaign.py"), "--indir", indir, "--outdir", outdir, "--runname", runname, "--nfilesperjob", nfilesperjob, "--jet_type", jet_type, config]

    print(" ".join(cmd))
    command = sp.check_output(cmd)
    print(command)
#"config_AntiKt4EMPFlowJets_modelsyst_50056X_PS_slice_0.py","config_AntiKt4EMPFlowJets_modelsyst_800030_FSR_slice_0.py", "config_AntiKt4EMPFlowJets_modelsyst_50056X_PS_slice_1.py",
def run_all(indir, outdir, runname, nfilesperjob, jet_type, dryrun):
    configs = ["config_AntiKt4EMPFlowJets_tracksyst_427081_bls_slice_0.py", "config_AntiKt4EMPFlowJets_tracksyst_427081_nbls_slice_0.py", "config_AntiKt4EMPFlowJets_jetsyst_retagging_427081_slice_0.py", "config_AntiKt4EMPFlowJets_jetsyst_retagging_427081_slice_1.py", "config_AntiKt4EMPFlowJets_jetsyst_retagging_427081_slice_2.py", "config_AntiKt4EMPFlowJets_jetsyst_retagging_427081_slice_3.py", "config_AntiKt4EMPFlowJets_jetsyst_retagging_427081_slice_4.py", "config_AntiKt4EMPFlowJets_jetsyst_retagging_427081_slice_5.py", "config_AntiKt4EMPFlowJets_jetsyst_retagging_427081_slice_6.py", "config_AntiKt4EMPFlowJets_jetsyst_retagging_427081_slice_7.py"]

    for config in configs:
        config_file = os.path.join(os.environ["XTRAP_ROOTDIR"], "Config/Zprime", config)

        run_EfficiencyProducer(indir, outdir, runname, nfilesperjob, jet_type, config_file, dryrun)


if __name__ == "__main__":
    if not os.environ["XTRAP_ROOTDIR"]:
        raise Exception("Error: 'XTRAP_ROOTDIR' not defined. Please do 'source high-pT-extrapolation/setup.sh'")

    parser = ArgumentParser(description = "Run all efficiency producer files")
    parser.add_argument("--indir", action = "store", dest = "indir")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    parser.add_argument("--runname", action = "store", dest = "runname")
    parser.add_argument("--nfilesperjob", action = "store", dest = "nfilesperjob")
    parser.add_argument("--jet_type", action = "store", dest = "jet_type")
    parser.add_argument("--dryrun", action = "store_const", dest = "dryrun", const = True, default = False)
    args = vars(parser.parse_args())
    run_all(**args)
