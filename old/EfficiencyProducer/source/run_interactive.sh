#!/bin/bash

TUNC_INPUT_FILES="/data/atlas/atlasdata/windischhofer/user.phwindis.TUNC_2018_11_17_AntiKt4EMTopoJets_TUNC.216164060/user.phwindis.16072186.AntiKt4EMTopoJets_TUNC._000224.root"
JUNC_INPUT_FILES="/data/atlas/atlasdata/windischhofer/user.phwindis.JUNC_2018_11_17_AntiKt4EMTopoJets_JUNC.216204014/user.phwindis.16072189.AntiKt4EMTopoJets_JUNC._000499.root"

BIN_DIR="/home/windischhofer/BTagEffMCExtrapolation/BTagNtupleReader/build/x86_64-slc6-gcc62-opt/"
SETUP_ENV="setup.sh"
NTUPLIZER_BIN="/bin/runBTagging"

OUTPUT_DIR="/home/windischhofer/NtupleOutputTest/"
NOMINAL_RUN="AntiKt4EMTopoJets_Nominal"

source $BIN_DIR$SETUP_ENV

mkdir -p $OUTPUT_DIR

# # NTuples for all jet systematic variations *exlucing* the nominal case
# $BIN_DIR$NTUPLIZER_BIN $JUNC_INPUT_FILES "$OUTPUT_DIR/JUNC_variations.root" -e $NOMINAL_RUN

# # NTuples for all track systematic variations *exlucing* the nominal case
# $BIN_DIR$NTUPLIZER_BIN $TUNC_INPUT_FILES "$OUTPUT_DIR/TUNC_variations.root" -e $NOMINAL_RUN

# # NTuples for the nominal jet systematic case, but MC weights modified
# $BIN_DIR$NTUPLIZER_BIN $JUNC_INPUT_FILES "$OUTPUT_DIR/JUNC_MC_variations.root" -t $NOMINAL_RUN -v

# # this should give identical results as a cross check
# $BIN_DIR$NTUPLIZER_BIN $TUNC_INPUT_FILES "$OUTPUT_DIR/TUNC_MC_variations.root" -t $NOMINAL_RUN -v

# can now combine all of the files into a combined one
hadd "$OUTPUT_DIR/combined.root" "$OUTPUT_DIR/JUNC_variations.root" "$OUTPUT_DIR/TUNC_variations.root" "$OUTPUT_DIR/TUNC_MC_variations.root"
