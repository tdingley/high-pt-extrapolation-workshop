#include "BootstrapUtils/TH1DBootstrap.h"

TH1DBootstrap::TH1DBootstrap(TString name, TString title, int nbins, double* xbins) : TH1Bootstrap()
{
    TString bootstrap_suffix = "_bootstrap";

    // prepare the nominal histogram and also its bootstrap replicas
    TH1D* nominal = new TH1D(name + bootstrap_suffix + "_0", title + bootstrap_suffix + "_0", nbins, xbins);

    std::vector<TH1*> replicas;
    for(unsigned int rep = 0; rep < numberReplicas; rep++)
    {
	TH1D* cur_replica = new TH1D(name + bootstrap_suffix + "_" + std::to_string(rep + 1), 
				     title + bootstrap_suffix + "_" + std::to_string(rep + 1), nbins, xbins);
	replicas.push_back(cur_replica);
    }
    
    setNominal(nominal);
    setReplicas(replicas);
}

TH1DBootstrap::~TH1DBootstrap()
{
    // the base class takes care of deleting the nominal histogram and its replica
}
