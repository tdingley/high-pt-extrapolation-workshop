#include "BootstrapUtils/BootstrapRNG.h"

BootstrapRNG::BootstrapRNG()
{
    // initialize the RNG that is used internally
    rng = new TRandom1();
}

BootstrapRNG::~BootstrapRNG()
{
    delete rng;
}

std::vector<unsigned int> BootstrapRNG::getBootstrapWeights(int numReplicas, int runNumber, int eventNumber, int MCChannelNumber)
{
    // first, set the seed to a value that is determined by this event:
    rng -> SetSeed(runNumber + eventNumber + MCChannelNumber);

    std::vector<unsigned int> retval;

    // then, generate the bootstrap weights
    for(int i = 0; i < numReplicas; i++)
    {
	retval.push_back(rng -> Poisson(1));
    }

    return retval;
}
