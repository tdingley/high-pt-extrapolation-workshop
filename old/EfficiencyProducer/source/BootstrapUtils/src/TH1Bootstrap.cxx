#include "BootstrapUtils/TH1Bootstrap.h"

TH1Bootstrap::TH1Bootstrap()
{
    // set up the RNG
    rng = new BootstrapRNG();
}

TH1Bootstrap::TH1Bootstrap(TH1* nominal, std::vector<TH1*> replicas)
{
    setNominal(nominal);
    setReplicas(replicas);

    // set up the RNG
    rng = new BootstrapRNG();
}

TH1Bootstrap::~TH1Bootstrap()
{
    delete nominal;
    
    for(auto cur: replicas)
    {
	delete cur;
    }
}

void TH1Bootstrap::Fill(double x, double weight, long runNumber, long eventNumber, long MCChannelNumber)
{
    // get the bootstrap weights:
    std::vector<unsigned int> bootstrapWeights = rng -> getBootstrapWeights(replicas.size(), runNumber, eventNumber, MCChannelNumber);

    // fill the nominal histogram and also the replicas
    nominal -> Fill(x, weight);

    for(unsigned int rep = 0; rep < replicas.size(); rep++)
    {
	unsigned int curBootstrapWeight = bootstrapWeights.at(rep);

	for(unsigned int it = 0; it < curBootstrapWeight; it++)
	{
	    replicas.at(rep) -> Fill(x, weight);
	}
    }
}

void TH1Bootstrap::setReplicas(std::vector<TH1*> replicas)
{
    this -> replicas = replicas;
}

void TH1Bootstrap::addReplica(TH1* to_add)
{
    replicas.push_back(to_add);
}

void TH1Bootstrap::setNominal(TH1* nominal)
{
    this -> nominal = nominal;
}

void TH1Bootstrap::Write()
{
    // just call the write() methods of the nominal histogram and its bootstrap replicas
    nominal -> Write();
    for(auto cur: replicas)
    {
	cur -> Write();
    }
}
