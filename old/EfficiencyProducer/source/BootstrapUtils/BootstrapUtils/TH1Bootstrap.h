#ifndef __TH1_BOOTSTRAP
#define __TH1_BOOTSTRAP

#include <iostream>
#include <TH1.h>
#include "BootstrapUtils/BootstrapRNG.h"

/* ------------------------------------------------------------------------------------------------------- */
/* contains a few ideas from                                                                               */
/* https://gitlab.cern.ch/atlas-physics/sm/StandardModelTools_BootstrapGenerator                           */
/* ------------------------------------------------------------------------------------------------------- */

class TH1Bootstrap {

public:
    TH1Bootstrap();
    TH1Bootstrap(TH1* nominal, std::vector<TH1*> replicas);
    ~TH1Bootstrap();

    void Fill(double x, double weight, long runNumber, long eventNumber, long MCChannelNumber);
    void Write();

protected:
    void addReplica(TH1* to_add);
    void setNominal(TH1* nominal);
    void setReplicas(std::vector<TH1*> replicas);

private:
    TH1* nominal;
    std::vector<TH1*> replicas;

    BootstrapRNG* rng;
};

#endif
