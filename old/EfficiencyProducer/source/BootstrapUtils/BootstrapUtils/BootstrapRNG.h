#ifndef __BOOTSTRAP_RNG
#define __BOOTSTRAP_RNG

#include <vector>
#include "TRandom1.h"

class BootstrapRNG
{
public: 
    BootstrapRNG();
    ~BootstrapRNG();

    std::vector<unsigned int> getBootstrapWeights(int numReplicas, int runNumber, int eventNumber, int MCChannelNumber);

private:
    TRandom1* rng;
};

#endif
