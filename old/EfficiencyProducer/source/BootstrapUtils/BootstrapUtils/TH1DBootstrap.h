#ifndef __TH1D_BOOTSTRAP
#define __TH1D_BOOTSTRAP

#include <TString.h>
#include <TH1D.h>
#include "BootstrapUtils/TH1Bootstrap.h"

class TH1DBootstrap: public TH1Bootstrap
{
public:
    TH1DBootstrap(TString name, TString title, int nbins, double* xbins);
    ~TH1DBootstrap();

private:
    unsigned int numberReplicas = 200;
};

#endif
