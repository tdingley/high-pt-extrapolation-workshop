import sys, os, fnmatch, re
import subprocess as sp
import glob
import configparser
from ConfigFileUtils import parse_list

def main():
    
    if len(sys.argv) != 2:
        print("Error: need exactly one argument, namely the path to global.conf")
        sys.exit()

    configpath = sys.argv[1]

    # parse the configuration file and extract the needed information
    config = configparser.ConfigParser()
    config.read(configpath)

    global_config = config["Global"]
    internal_config = config["Internal"]

    # to be changed: this is the output_dir from the point of view of btaganalysis, but actually the input directory for the NTupleReader!
    input_dir = global_config["tuples_output_dir"]

    bootstrap_path = os.environ["BTAGMC_ROOT"]

    ntuple_reader = os.path.join(bootstrap_path, "..", internal_config["NTupleReader_bin"])

    systematic_variations = parse_list(global_config["systematic_variations"], lambda x: x)

    jet_collections = parse_list(global_config["jet_collections"], lambda x: x)

    combine_dir = os.path.join(input_dir, 'combined_tuples')
    output_dir = global_config["histograms_output_dir"]

    if not os.path.exists(combine_dir):
        os.makedirs(combine_dir)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # first, need to hadd together the corresponding files
    print("working on " + input_dir)

    ntuple_files = []

    for systematic_variation in systematic_variations:
        print("=================================")
        print("looking for files corresponding to " + systematic_variation)
        print("---------------------------------")        
        p = re.compile(".*/" + systematic_variation + "/.*" + systematic_variation + ".*")

        source_files = []
        for root, dirnames, filenames in os.walk(input_dir):
            for filename in filenames:
                cur_path = os.path.join(root, filename)
                if p.match(cur_path):
                    source_files.append(cur_path)

        print("found the following files")
        for source_file in source_files:
            print(source_file)
        print("=================================")

        output_file = os.path.join(combine_dir, os.path.basename(source_files[0]))
        ntuple_files.append(output_file)

        # output = sp.check_output(["hadd", output_file] + source_files)
        # print(output)

        # prepare the files holding metadata information that will be needed later for plotting
        metadata_file = os.path.join(output_dir, os.path.basename(output_file).replace(".root", ".conf"))
        plot_config = configparser.ConfigParser()
        plot_config["EfficiencyEvaluator"] = {}
        plot_config["EfficiencyEvaluator"]["systematic_variation"] = systematic_variation
        plot_config["EfficiencyEvaluator"]["label"] = systematic_variation
        # add more metadata / plotting information here

        # then save it to disk
        with open(metadata_file, 'w') as plotconfig_file:
            plot_config.write(plotconfig_file)

    # and then run the NTupleReader on each of them to produce the histograms
    for ntuple_file in ntuple_files:
        print("now running NTupleReader on " + ntuple_file)
        
        output_file = os.path.join(output_dir, os.path.basename(ntuple_file))

        for jet_collection in jet_collections:
            output = sp.check_output([ntuple_reader, ntuple_file, jet_collection, output_file, configpath])
            print(output)
        
if __name__ == "__main__":
    main()
