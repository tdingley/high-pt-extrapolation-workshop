#ifndef __TFILEHANDLER_H
#define __TFILEHANDLER_H

#include <iostream>
#include <vector>
#include <string>
#include <TFile.h>
#include <TObject.h>
#include <TString.h>
#include <TList.h>
#include <TH1D.h>

class TFileHandler
{
public:
    TFileHandler(TString path, TString mode);
    ~TFileHandler();
    
    std::vector<TString> get_obj_list();
    TH1D* get_hist(TString obj_name);
    void close();

private:

    bool need_to_close = false;
    TFile* fileobj;
};

#endif
