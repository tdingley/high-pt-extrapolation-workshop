#ifndef __PROFILER_H
#define __PROFILER_H

#include <iostream>
#include <TChain.h>
#include <TH2D.h>
#include <TH1.h>

#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "EfficiencyProducer/WeightFetcher.h"
#include "EfficiencyProducer/DataTree.h"

class Profiler {

public:
    typedef std::map<std::string, float> WeightCollection;
    typedef std::map<std::string, WeightFetcher*> WeightFetcherCollection;

    Profiler();
    ~Profiler();

    void Profile(DataTree* tree, Profiler::WeightFetcherCollection wfs, const std::function<bool(DataTree*)> cut, const std::function<void(DataTree*, Profiler::WeightCollection)> fill_callback, long evtMax = -1);
};

#endif
