#ifndef __REWEIGHTER1D_H
#define __REWEIGHTER1D_H

#include <iostream>
#include <functional>
#include <TH1D.h>
#include <TFile.h>

#include "EfficiencyProducer/DataTree.h"
#include "EfficiencyProducer/TFileHandler.h"

class Reweighter1D
{
public:
    Reweighter1D(std::string path);
    ~Reweighter1D();
    double getWeight(double reweighted_var);

private:
    TH1D* weight_hist;
};

#endif
