#ifndef __WEIGHT_FETCHER_H
#define __WEIGHT_FETCHER_H

#include <TChain.h>
#include <iostream>
#include <vector>
#include <regex>

#include "EfficiencyProducer/RegexUtils.h"
#include "ConfigFileUtils/ConfigFileHandler.h"

class WeightFetcher
{
public:
    WeightFetcher(std::string weight_spec, std::string lumi_file_path = "");
    WeightFetcher(std::vector<TChain*> weight_chains, std::string weight_spec, std::string lumi_file_path = "");
    ~WeightFetcher();

    void setWeightChains(std::vector<TChain*> weight_chains);

    double getLumiWeight();
    double getWeight(Long64_t jentry);

    std::string getID();
    bool isNominal();

    void dumpSelections();
    void compile();

private:
    std::vector<std::string> selection_regexes_num;
    std::vector<std::string> selection_regexes_den;

    std::vector<TChain*> weight_chains;

    // the TChains that hold the weights for the numerator ...
    std::vector<TChain*> active_chains_num;

    // ... and the denominator
    std::vector<TChain*> active_chains_den;

    std::string active_name;
    std::string weight_spec;

    std::string lumi_file_path;

    // branches to hold the MC weights read from the active trees
    float mcwg;
    TBranch* b_mcwg;

    // cached luminosity weight
    double lumi_weight = 1.0;

    void extractName(std::string weight_spec);
    void extractLumiWeight(std::string lumi_file_path);
};

#endif
