#ifndef TFILEUTILS_H
#define TFILEUTILS_H

#include <fstream>
#include "TFile.h"
#include "TKey.h"

class TFileUtils
{
public:
    static std::vector<TString> peek_file(TString filepath);
    static std::vector<TString> get_datatrees(TString filepath);
    static std::vector<TString> get_MC_weight_trees(TString filepath, TString jet_collection_name);
    static bool file_exists(std::string fileName);
};

#endif
