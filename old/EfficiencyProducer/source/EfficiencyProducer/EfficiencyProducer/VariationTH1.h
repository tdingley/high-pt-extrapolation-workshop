#ifndef VARIATION_TH1_H
#define VARIATION_TH1_H

#include <map>
#include <vector>

#include "TString.h"

template <class T> 
class VariationTH1
{
public:
    // Note: this assumes that 'T' follows the normal TH1* constructor convention
    VariationTH1(std::vector<std::string> var_names, TString name, TString title, int nbins, double* xbins);
    ~VariationTH1();

    T* operator[](std::string key);
    T* at(std::string key);

private:
    std::map<std::string, T*> histmap;
};

template <class T> 
VariationTH1<T>::VariationTH1(std::vector<std::string> var_names, TString name, TString title, int nbins, double* xbins)
{  
    // initialize histograms for all variations
    for(auto var_name: var_names)
    {
	TString cur_name = name + var_name;
	TString cur_title = title + var_name;
	histmap[var_name] = new T(cur_name, cur_title, nbins, xbins);
    }
}

template <class T> 
VariationTH1<T>::~VariationTH1()
{ 
    for(auto cur_hist: histmap)
    {
	T* td = cur_hist.second; 
	delete td;
    }
}

template <class T>
T* VariationTH1<T>::at(std::string key)
{
    return this -> operator[](key);
}

template <class T>
T* VariationTH1<T>::operator[](std::string key)
{
    return histmap[key];
}

#endif
