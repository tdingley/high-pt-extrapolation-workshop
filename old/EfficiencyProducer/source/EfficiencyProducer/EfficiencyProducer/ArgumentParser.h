#ifndef ARGUMENT_PARSER_H
#define ARGUMENT_PARSER_H

#include <unistd.h>
#include <string>
#include <map>
#include <vector>

class ArgumentParser
{
public:

    typedef std::map<std::string, std::string> OptArgDict;
    typedef std::vector<std::string> PosArgs;

    static std::pair<ArgumentParser::OptArgDict, ArgumentParser::PosArgs> parseArgs(int argc, char** argv);
};

#endif
