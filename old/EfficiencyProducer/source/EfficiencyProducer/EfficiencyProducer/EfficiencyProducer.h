#ifndef __EFFICIENCYPRODUCER_H
#define __EFFICIENCYPRODUCER_H

#include <iostream>
#include <vector>
#include <algorithm>

#include <TChain.h>
#include <TFile.h>

#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"

#include "EfficiencyProducer/WeightFetcher.h"
#include "EfficiencyProducer/HighLevelTaggerAugmentedDataTree.h"
#include "EfficiencyProducer/Profiler.h"
#include "EfficiencyProducer/VariationTH1.h"
#include "EfficiencyProducer/Reweighter1D.h"
#include "ConfigFileUtils/ConfigFileUtils.h"
#include "BootstrapUtils/TH1DBootstrap.h"

class EfficiencyProducer 
{
public:
   EfficiencyProducer(TChain* inChain, std::string jetcollection_name, std::string jet_type, std::vector<double> bin_edges, 
		      std::vector<WeightFetcher*> weight_fetchers, TDirectory* outdir, std::string tagger_name, 
		      std::vector<std::string> WPs, std::string CDI_path, Long64_t maxEvt = -1,
		      bool createReweighting = false, Reweighter1D* reweighter = nullptr, bool userJVTCut = false);
   ~EfficiencyProducer();
   void Loop();
   void Write();

   std::vector<std::string> m_WPs;
   std::map<std::string, BTaggingSelectionTool*> m_btagSelectionTool;

   typedef std::map<std::string, VariationTH1<TH1DBootstrap>*> WPHistMap;
   VariationTH1<TH1DBootstrap>* h_truth;
   WPHistMap* h_tagged;

private:
   void initialize();
   void finalize();

   TDirectory* m_outdir;

   std::vector<double> bin_edges;
   bool pass_JVT_cut(double eta, double pt, double jvt);
   std::vector<int> getNHardestTruthJets(unsigned int n, std::vector<float>* jet_pt, std::vector<int>* jet_LabDr_HadF);

   Long64_t evtMax = -1;
   bool createReweighting = false;
   Reweighter1D* reweighter = nullptr;

   std::string tagger_name;
   std::string jetcollection_name;
   std::string jetcollection_name_CDI;
   std::vector<std::string> weight_names;
   std::string CDI_path;
   std::string jet_type;
   Profiler::WeightFetcherCollection wfc;

   HighLevelTaggerAugmentedDataTree* datatree;

   bool userJVTCut = false;
};

#endif
