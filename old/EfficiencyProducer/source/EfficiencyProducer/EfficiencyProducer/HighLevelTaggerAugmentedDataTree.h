#ifndef HIGH_LEVEL_TAGGER_AUGMENTED_DATA_TREE_H
#define HIGH_LEVEL_TAGGER_AUGMENTED_DATA_TREE_H

#include "EfficiencyProducer/DataTree.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"

class HighLevelTaggerAugmentedDataTree: public DataTree
{
public:
    HighLevelTaggerAugmentedDataTree(TChain* chain, std::string CDI_path, std::string jetcollection_name);
    ~HighLevelTaggerAugmentedDataTree();

    void GetEvent(long entry);

    std::map<std::string, std::vector<double>*> jet_highlevel_tagscore;
    
private:
    BTaggingSelectionTool* DL1x_btst;

    // these are augmented data branches that are not available in the original DataTree
    std::vector<double>* jet_dl1;
    std::vector<double>* jet_dl1r;
};

#endif
