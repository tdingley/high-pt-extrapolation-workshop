#ifndef __REGEX_UTILS_H
#define __REGEX_UTILS_H

#include <regex>
#include <vector>
#include <string>

class RegexUtils {
public:
    static std::vector<std::string> regex_search(std::string instring, std::regex rgx);
    static std::vector<std::string> regex_search(std::string instring, std::string rgx_string);
};

#endif
