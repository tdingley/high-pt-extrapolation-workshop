#ifndef DATA_TREE_H
#define DATA_TREE_H

#include <TChain.h>
#include <vector>

class DataTree
{
public:
    DataTree(TChain* chain);
    ~DataTree();

    virtual void GetEvent(long entry);
    long GetEntries();

    int runnb;
    int eventnb;
    int mcchan;
    int eventClean;
    
    std::vector<float>* jet_pt;
    std::vector<float>* jet_eta;
    std::vector<float>* jet_phi;

    std::vector<float>* had_pt;
    std::vector<float>* had_eta;
    std::vector<float>* had_phi;

    std::vector<int>* jet_LabDr_HadF;
    std::vector<int>* jet_aliveAfterOR;
    std::vector<int>* jet_aliveAfterORmu;
    std::vector<int>* jet_aliveAfterCollOR;
    std::vector<int>* jet_aliveAfterJVT;
    std::vector<int>* jet_nJetConstituents;
    
    // JVT branch
    std::vector<float>* jet_JVT;
    std::vector<float>* jet_JVT_weight;
    
    // tagger branches
    std::vector<double>* jet_dl1_pb;    
    std::vector<double>* jet_dl1_pc;
    std::vector<double>* jet_dl1_pu;

    std::vector<double>* jet_dl1r_pu;
    std::vector<double>* jet_dl1r_pb;
    std::vector<double>* jet_dl1r_pc;
   
protected:
    void Init();

private:
    TChain* chain;
    
    TBranch* b_runnb;
    TBranch* b_eventnb;
    TBranch* b_mcchan;
    TBranch* b_eventClean;
    
    TBranch* b_jet_JVT;
    TBranch* b_jet_JVT_weight;
    
    TBranch* b_jet_pt;
    TBranch* b_jet_eta;
    TBranch* b_jet_phi;

    TBranch* b_had_pt;
    TBranch* b_had_eta;
    TBranch* b_had_phi;

    TBranch* b_jet_LabDr_HadF;
    TBranch* b_jet_aliveAfterOR;
    TBranch* b_jet_aliveAfterORmu;
    TBranch* b_jet_aliveAfterCollOR;
    TBranch* b_jet_aliveAfterJVT;
    TBranch* b_jet_nJetConstituents;
    
    TBranch* b_jet_dl1_pb;
    TBranch* b_jet_dl1_pc;    
    TBranch* b_jet_dl1_pu;

    TBranch* b_jet_dl1r_pu;
    TBranch* b_jet_dl1r_pb;
    TBranch* b_jet_dl1r_pc;
};

#endif
