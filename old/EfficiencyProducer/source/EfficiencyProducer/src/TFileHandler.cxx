#include "EfficiencyProducer/TFileHandler.h"

TFileHandler::TFileHandler(TString path, TString mode)
{
    fileobj = TFile::Open(path, mode);
    need_to_close = true;
}

TFileHandler::~TFileHandler()
{
    if(need_to_close)
    {
	close();
	need_to_close = false;
    }
}

void TFileHandler::close()
{
    if(need_to_close)
    {
	fileobj -> Close();
    }
}

std::vector<TString> TFileHandler::get_obj_list()
{
    std::vector<TString> obj_list;

    TList* available_keys = fileobj -> CurrentDirectory() -> GetListOfKeys();
    TIter next(available_keys);
    while(TObject* obj = next())
    {
	obj_list.push_back(obj -> GetName());
    }

    return obj_list;
}

TH1D* TFileHandler::get_hist(TString obj_name)
{
    TH1D* obj = (TH1D*)(fileobj -> CurrentDirectory() -> Get(obj_name) -> Clone());
    obj -> SetDirectory(0);

    return obj;
}
