#include "EfficiencyProducer/HighLevelTaggerAugmentedDataTree.h"

HighLevelTaggerAugmentedDataTree::HighLevelTaggerAugmentedDataTree(TChain* chain, std::string CDI_path, std::string jetcollection_name): DataTree(chain)
{  
    jet_dl1 = new std::vector<double>();
    jet_dl1r = new std::vector<double>();

    jet_highlevel_tagscore.insert(std::make_pair("DL1", jet_dl1));
    jet_highlevel_tagscore.insert(std::make_pair("DL1r", jet_dl1r));

    // Note: the only thing this tool is used for is to compute the DL1x tagger score.
    // These are independent of the working point name, so can just use a dummy value.
    this -> DL1x_btst = new BTaggingSelectionTool("tagweight_DL1x_btst");
    this -> DL1x_btst -> setProperty("TaggerName", "DL1");
    this -> DL1x_btst -> setProperty("FlvTagCutDefinitionsFileName", CDI_path);
    this -> DL1x_btst -> setProperty("MinPt", 20e3);
    this -> DL1x_btst -> setProperty("MaxRangePt", 3000e3);
    this -> DL1x_btst -> setProperty("MaxEta", 2.5);
    this -> DL1x_btst -> setProperty("JetAuthor", jetcollection_name);
    this -> DL1x_btst -> setProperty("OperatingPoint", "FixedCutBEff_77");
    this -> DL1x_btst -> initialize();

    Init();
}

HighLevelTaggerAugmentedDataTree::~HighLevelTaggerAugmentedDataTree()
{  }

void HighLevelTaggerAugmentedDataTree::GetEvent(long entry)
{
    double eps = std::numeric_limits< double >::min(); // to ensure that all tagger scores are nonzero

    jet_dl1 -> clear();
    jet_dl1r -> clear();

    DataTree::GetEvent(entry);

    for(unsigned int jet = 0; jet < jet_pt -> size(); jet++)
    {
	double dl1_tagscore, dl1r_tagscore;

	if(DL1x_btst -> getTaggerWeight(
	       std::max(jet_dl1_pb -> at(jet), eps),
	       std::max(jet_dl1_pc -> at(jet), eps),
	       std::max(jet_dl1_pu -> at(jet), eps),
	       dl1_tagscore,
	       false) != CP::CorrectionCode::Ok)
	{
	    std::cerr << "Error: Problem in getTaggerWeight()" << std::endl;
	}

	if(DL1x_btst -> getTaggerWeight(
	       std::max(jet_dl1r_pb -> at(jet), eps),
	       std::max(jet_dl1r_pc -> at(jet), eps),
	       std::max(jet_dl1r_pu -> at(jet), eps),
	       dl1r_tagscore,
	       false) != CP::CorrectionCode::Ok)
	{
	    std::cerr << "Error: Problem in getTaggerWeight()" << std::endl;	    
	}

	jet_dl1 -> push_back(dl1_tagscore);
	jet_dl1r -> push_back(dl1r_tagscore);
    }
}
