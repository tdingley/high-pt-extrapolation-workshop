#include "EfficiencyProducer/Profiler.h"

Profiler::Profiler()
{  }

Profiler::~Profiler()
{  }

void Profiler::Profile(DataTree* tree, Profiler::WeightFetcherCollection wfs, const std::function<bool(DataTree*)> cut, const std::function<void(DataTree*, Profiler::WeightCollection)> fill_callback, long evtMax)
{
    long nentries = tree -> GetEntries();
    std::cout << "have chain with " << nentries << " entries" << std::endl;

    long nend = std::min(evtMax, nentries);
    nend = nend < 0 ? nentries : nend;

    for(long jentry = 0; jentry < nend; jentry++)
    {
	if(!(jentry % 5000))
	{
	    std::cout << "currently on event #" << jentry << std::endl;
	}

	// fetch the current event data
	tree -> GetEvent(jentry);
	
	// also fetch the weights
	Profiler::WeightCollection wc;
	for(auto const& cur_wf: wfs)
	{
	    std::string name = cur_wf.first;
	    wc[name] = cur_wf.second -> getWeight(jentry);
	    wc[name + "_lumiweight"] = cur_wf.second -> getLumiWeight();
	}

	// then activate the callback
	if(cut(tree))
	{
	    fill_callback(tree, wc);
	}
    }
}
