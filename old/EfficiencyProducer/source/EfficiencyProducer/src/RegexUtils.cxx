#include "EfficiencyProducer/RegexUtils.h"

std::vector<std::string> RegexUtils::regex_search(std::string instring, std::string rgx_string)
{
    std::regex rgx(rgx_string);
    return RegexUtils::regex_search(instring, rgx);
}

std::vector<std::string> RegexUtils::regex_search(std::string instring, std::regex rgx)
{
    std::vector<std::string> retval;

    for(auto i = std::sregex_iterator(instring.begin(), instring.end(), rgx);
	i != std::sregex_iterator();
	i++)
    {
	// store the match values, i.e. those from the capturing group(s)
	std::smatch m = *i;
	for(unsigned int j = 1; j < m.size(); j++)
	{
	    retval.push_back(m[j].str());
	}
    }

    return retval;
}

