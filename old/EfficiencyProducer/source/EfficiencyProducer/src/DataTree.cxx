#include "EfficiencyProducer/DataTree.h"

DataTree::DataTree(TChain* chain)
{  
    this -> chain = chain;
    Init();
}

DataTree::~DataTree()
{  }

void DataTree::Init()
{
    // make sure to have no dangling pointers
    runnb = 0;
    eventnb = 0;
    mcchan = 0;
    eventClean = 0;

    jet_pt = 0;
    jet_eta = 0;
    jet_phi = 0;

    had_pt = 0;
    had_eta = 0;
    had_phi = 0;

    jet_LabDr_HadF = 0;
    jet_aliveAfterOR = 0;
    jet_aliveAfterORmu = 0;
    jet_aliveAfterCollOR = 0;
    jet_aliveAfterJVT = 0;
    jet_nJetConstituents = 0;

    jet_JVT = 0;
    jet_JVT_weight = 0;

    jet_dl1_pb = 0;
    jet_dl1_pc = 0;
    jet_dl1_pu = 0;

    jet_dl1r_pb = 0;
    jet_dl1r_pc = 0;
    jet_dl1r_pu = 0;

    b_runnb = 0;
    b_eventnb = 0;
    b_mcchan = 0;
    b_eventClean = 0;

    b_jet_JVT = 0;
    b_jet_JVT_weight = 0;
    
    b_jet_pt = 0;   
    b_jet_eta = 0;   
    b_jet_phi = 0;   

    b_had_pt = 0;   
    b_had_eta = 0;   
    b_had_phi = 0;   

    b_jet_LabDr_HadF = 0;   
    b_jet_aliveAfterOR = 0;   
    b_jet_aliveAfterORmu = 0;   
    b_jet_aliveAfterCollOR = 0;
    b_jet_aliveAfterJVT = 0;
    b_jet_nJetConstituents = 0;

    b_jet_dl1_pb = 0;
    b_jet_dl1_pc = 0;
    b_jet_dl1_pu = 0;

    b_jet_dl1r_pu = 0;
    b_jet_dl1r_pb = 0;
    b_jet_dl1r_pc = 0;

    // Set branch addresses and branch pointers
    if (!(this -> chain)) return;

    this -> chain -> SetBranchAddress("runnb", &runnb, &b_runnb);
    this -> chain -> SetBranchAddress("eventnb", &eventnb, &b_eventnb);
    this -> chain -> SetBranchAddress("mcchan", &mcchan, &b_mcchan);
    this -> chain -> SetBranchAddress("eventClean", &eventClean, &b_eventClean);

    this -> chain -> SetBranchAddress("jet_JVT", &jet_JVT, &b_jet_JVT);
    this -> chain -> SetBranchAddress("jet_JVT_weight", &jet_JVT_weight, &b_jet_JVT_weight);

    this -> chain -> SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
    this -> chain -> SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
    this -> chain -> SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);

    this -> chain -> SetBranchAddress("b_hadron_pt", &had_pt, &b_had_pt);
    this -> chain -> SetBranchAddress("b_hadron_eta", &had_eta, &b_had_eta);
    this -> chain -> SetBranchAddress("b_hadron_phi", &had_phi, &b_had_phi);

    this -> chain -> SetBranchAddress("jet_aliveAfterOR", &jet_aliveAfterOR, &b_jet_aliveAfterOR);
    this -> chain -> SetBranchAddress("jet_aliveAfterORmu", &jet_aliveAfterORmu, &b_jet_aliveAfterORmu);
    this -> chain -> SetBranchAddress("jet_aliveAfterCollOR", &jet_aliveAfterCollOR, &b_jet_aliveAfterCollOR);
    this -> chain -> SetBranchAddress("jet_aliveAfterJVT", &jet_aliveAfterJVT, &b_jet_aliveAfterJVT);
    this -> chain -> SetBranchAddress("jet_nJetConstituents", &jet_nJetConstituents, &b_jet_nJetConstituents);
    this -> chain -> SetBranchAddress("jet_LabDr_HadF", &jet_LabDr_HadF, &b_jet_LabDr_HadF);

    this -> chain -> SetBranchAddress("jet_dl1_pb", &jet_dl1_pb, &b_jet_dl1_pb);
    this -> chain -> SetBranchAddress("jet_dl1_pc", &jet_dl1_pc, &b_jet_dl1_pc);
    this -> chain -> SetBranchAddress("jet_dl1_pu", &jet_dl1_pu, &b_jet_dl1_pu);

    this -> chain -> SetBranchAddress("jet_dl1r_pu", &jet_dl1r_pu, &b_jet_dl1r_pu);
    this -> chain -> SetBranchAddress("jet_dl1r_pb", &jet_dl1r_pb, &b_jet_dl1r_pb);
    this -> chain -> SetBranchAddress("jet_dl1r_pc", &jet_dl1r_pc, &b_jet_dl1r_pc);
}

void DataTree::GetEvent(long entry)
{
    this -> chain -> GetEvent(entry);
}

long DataTree::GetEntries()
{
    return this -> chain -> GetEntries();
}
