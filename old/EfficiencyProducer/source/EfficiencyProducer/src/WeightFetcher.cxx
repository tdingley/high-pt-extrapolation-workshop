#include "EfficiencyProducer/WeightFetcher.h"

WeightFetcher::WeightFetcher(std::string weight_spec, std::string lumi_file_path):
    active_name("")
{
    this -> weight_spec = "";
    this -> lumi_file_path = lumi_file_path;
    selection_regexes_num = RegexUtils::regex_search(weight_spec, "<(.+?)>");
    extractName(weight_spec);
    extractLumiWeight(lumi_file_path);

    // first, split the weight specification string into separate parts for numerator and denominator
    std::vector<std::string> weight_spec_groups = RegexUtils::regex_search(weight_spec, "^([^/]*)/{0,1}([^/]*)$");
    std::cout << "NUM: " << weight_spec_groups[0] << std::endl;
    std::cout << "DEN: " << weight_spec_groups[1] << std::endl;
    selection_regexes_num = RegexUtils::regex_search(weight_spec_groups[0], "<(.+?)>");
    selection_regexes_den = RegexUtils::regex_search(weight_spec_groups[1], "<(.+?)>");
}

WeightFetcher::WeightFetcher(std::vector<TChain*> weight_chains, std::string weight_spec, std::string lumi_file_path):
    active_name("")
{
    this -> weight_chains = weight_chains;
    this -> weight_spec = weight_spec;
    this -> lumi_file_path = lumi_file_path;
    
    // first, split the weight specification string into separate parts for numerator and denominator
    std::vector<std::string> weight_spec_groups = RegexUtils::regex_search(weight_spec, "^([^/]*)/{0,1}([^/]*)$");
    std::cout << "NUM: " << weight_spec_groups[0] << std::endl;
    std::cout << "DEN: " << weight_spec_groups[1] << std::endl;
    selection_regexes_num = RegexUtils::regex_search(weight_spec_groups[0], "<(.+?)>");
    selection_regexes_den = RegexUtils::regex_search(weight_spec_groups[1], "<(.+?)>");

    extractName(weight_spec);
    extractLumiWeight(lumi_file_path);
}

void WeightFetcher::extractLumiWeight(std::string lumi_file_path)
{
    double lumi_weight = 1.0;

    // check if a path to the lumi config file has been given. If so, check if there
    // is some data to be found under the name of this WeightFetcher
    if(!lumi_file_path.empty())
    {
	ConfigFileHandler* conf = new ConfigFileHandler(lumi_file_path, "read");

	// the cross section is stored in units of pb^-1!
	float xsec = std::stod((conf -> GetField("global", "xsec")).Data());

	// the luminosity is stored in terms of pb!
	float lumi = std::stod((conf -> GetField("global", "lumi")).Data());
	float SOW = std::stod((conf -> GetField(this -> active_name, "sow")).Data());

	lumi_weight = (xsec * lumi) / SOW;
    }

    std::cout << "WeightFetcher '" << this -> active_name << "': extracted lumi_weight = " << std::scientific << lumi_weight << std::fixed << std::endl;

    this -> lumi_weight = lumi_weight;
}

// private helper method to extract the name of this weight combination
void WeightFetcher::extractName(std::string weight_spec)
{
    // extract the name (if any) given to this weight variation combination
    std::vector<std::string> active_names(RegexUtils::regex_search(weight_spec, "\\[(.+?)\\]"));

    if(active_names.size() > 1)
    {
	std::cout << "Warning: more than one name found for this weight combination!" << std::endl;
    }
    else if(active_names.size() == 1)
    {
	active_name = active_names[0];
	std::cout << "Using name = " << active_name << std::endl;
    }    
    else
    {
	std::cout << "no user defined name found, will use default name" << std::endl;
    }
}

void WeightFetcher::compile()
{
    // clear the current list of active chains, then recompile it from scratch
    active_chains_num.clear();
    active_chains_den.clear();

    // store the chains that match one of the regexes, can then later use always these ones
    for(auto cur_chain: weight_chains)
    {
	for(auto cur_selection: selection_regexes_num)
	{
	    std::regex cur_rgx(cur_selection);

	    if(std::regex_match(cur_chain -> GetName(), cur_rgx))
	    {
		active_chains_num.push_back(cur_chain); // this is an active numerator chain
	    }
	}

	for(auto cur_selection: selection_regexes_den)
	{
	    std::regex cur_rgx(cur_selection);

	    if(std::regex_match(cur_chain -> GetName(), cur_rgx))
	    {
		active_chains_den.push_back(cur_chain); // this is an active denominator chain
	    }
	}
    }    

    for(unsigned int cur = 0; cur < active_chains_num.size(); cur++)
    {
    	active_chains_num[cur] -> SetBranchAddress("mcwg", &mcwg, &b_mcwg);
    }

    for(unsigned int cur = 0; cur < active_chains_den.size(); cur++)
    {
    	active_chains_den[cur] -> SetBranchAddress("mcwg", &mcwg, &b_mcwg);
    }
}

void WeightFetcher::setWeightChains(std::vector<TChain*> weight_chains)
{
    this -> weight_chains = weight_chains;
}

double WeightFetcher::getLumiWeight()
{
    return(this -> lumi_weight);
}

double WeightFetcher::getWeight(Long64_t jentry)
{
    double retval = 1.0;

    // need to multiply the weights from all the chains that match one of the regexes
    // go through all the active weight chains and apply their weights
    for(unsigned int cur = 0; cur < active_chains_num.size(); cur++)
    {
	active_chains_num[cur] -> GetEvent(jentry);
	retval *= mcwg;
    }

    for(unsigned int cur = 0; cur < active_chains_den.size(); cur++)
    {
	active_chains_den[cur] -> GetEvent(jentry);
	retval /= mcwg;
    }

    return retval;
}

void WeightFetcher::dumpSelections()
{
    std::cout << " -- " << std::endl;
    std::cout << "using the following active chains for the numerator" << std::endl;
    for(auto cur_active_chain: active_chains_num)
    {
	std::cout << cur_active_chain -> GetName() << std::endl;
    }
    std::cout << "using the following active chains for the denominator" << std::endl;
    for(auto cur_active_chain: active_chains_den)
    {
	std::cout << cur_active_chain -> GetName() << std::endl;
    }
    std::cout << " -- " << std::endl;
}

// checks if this WeightFetcher corresponds to the nominal weight.
// returns True if it contains a single weight only, and the name of the corresponding
// weight tree contains "MCWeight_nominal"
bool WeightFetcher::isNominal()
{
    if((active_chains_num.size() == 1) && (active_chains_den.size() == 0))
    {
	if(TString(active_chains_num[0] -> GetName()).Contains("MCWeight_nominal"))
	{
	    return true;
	}
    }

    return false;
}

// compose a string that reflects the MC weights that this WeightFetcher applies
std::string WeightFetcher::getID()
{
    std::string retval;

    if(active_name.empty())
    {
	// compose a name based on the names of the individual systematic variations that go into this combination
	for(auto cur_active_chain: active_chains_num)
	{
	    std::vector<std::string> name_elems = RegexUtils::regex_search(cur_active_chain -> GetName(), "([^\\|]+)");
	    std::string subdir_name = name_elems.back();
	    
	    retval += subdir_name;
	}

	// compose a name based on the names of the individual systematic variations that go into this combination
	for(auto cur_active_chain: active_chains_den)
	{
	    std::vector<std::string> name_elems = RegexUtils::regex_search(cur_active_chain -> GetName(), "([^\\|]+)");
	    std::string subdir_name = name_elems.back();
	    
	    retval += subdir_name;
	}
    }
    else
    {
	// otherwise use the user-defined name
	retval = active_name;
    }

    return retval;
}
