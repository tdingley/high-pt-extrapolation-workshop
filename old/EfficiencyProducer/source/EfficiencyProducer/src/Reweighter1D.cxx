#include "EfficiencyProducer/Reweighter1D.h"

Reweighter1D::Reweighter1D(std::string path) {
    
    // try to open the reweighting histogram
    TFileHandler handler(path, "READ");
    std::vector<TString> available_hists = handler.get_obj_list();

    if(available_hists.size() != 1)
    {
	std::cerr << "Found more than one or no reweighting histogram!" << std::endl;
	throw;
    }

    TString reweighting_histogram_name = available_hists[0];
    std::cout << "will use '" << reweighting_histogram_name << "' for reweighting" << std::endl;
    
    weight_hist = (TH1D*)handler.get_hist(reweighting_histogram_name);
}

Reweighter1D::~Reweighter1D() {

}

double Reweighter1D::getWeight(double reweighted_var) {

    // look up the weight
    float weight = weight_hist -> GetBinContent(weight_hist -> FindBin(reweighted_var));

    return weight;
}
