#include "EfficiencyProducer/TFileUtils.h"

// gets a list of all the tree names available in this file
std::vector<TString> TFileUtils::peek_file(TString filepath)
{
    std::vector<TString> retlist;

    TFile* peekfile = new TFile(filepath, "READ");
    TIter nextkey( peekfile -> GetListOfKeys() );
    TKey *key;

    while ((key = (TKey*)nextkey()))
    {
	if(strcmp(key->GetClassName(), "TTree")) 
	    continue;

	retlist.push_back(key -> GetName());
    }    

    return retlist;
}

// lists the data trees only (i.e. excluding those that hold only MC weights)
std::vector<TString> TFileUtils::get_datatrees(TString filepath)
{
    std::vector<TString> all_trees = peek_file(filepath);
    std::vector<TString> datatrees;

    for(auto cur_tree: all_trees)
    {
	if(!cur_tree.Contains("MCWeight") && !cur_tree.Contains("PUWeight"))
	    datatrees.push_back(cur_tree);
    }

    return datatrees;
}

// lists the available MC weight trees associated to a certain data tree
std::vector<TString> TFileUtils::get_MC_weight_trees(TString filepath, TString jet_collection_name)
{
    std::vector<TString> all_trees = TFileUtils::peek_file(filepath);
    std::vector<TString> associated_MC_trees;

    for(auto cur_tree: all_trees)
    {
	if((cur_tree.Contains("MCWeight") || cur_tree.Contains("PUWeight")) && cur_tree.Contains(jet_collection_name))
	    associated_MC_trees.push_back(cur_tree);
    }
    
    return associated_MC_trees;
}

bool TFileUtils::file_exists(std::string fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
}

