#include "EfficiencyProducer/EfficiencyProducer.h"

EfficiencyProducer::EfficiencyProducer(TChain *inChain, std::string jetcollection_name, std::string jet_type, std::vector<double> bin_edges, std::vector<WeightFetcher*> weight_fetchers, TDirectory* outdir, std::string tagger_name, std::vector<std::string> WPs, std::string CDI_path, Long64_t evtMax, bool createReweighting, Reweighter1D* reweighter, bool userJVTCut) 
{    
    // the total number of events to be processed
    this -> evtMax = evtMax;

    // in which mode to run
    this -> createReweighting = createReweighting;
    this -> reweighter = reweighter;

    // prepare the WeightFetcherCollection
    for(auto* wf: weight_fetchers)
    {
	wfc.insert(std::make_pair(wf -> getID(), wf));
	weight_names.push_back(wf -> getID());
    }

    this -> tagger_name = tagger_name;
    this -> bin_edges = bin_edges;
    this -> m_outdir = outdir;
    this -> m_WPs = WPs;
    this -> jetcollection_name = jetcollection_name;
    this -> CDI_path = CDI_path;

    this -> userJVTCut = userJVTCut;
    this -> jet_type = jet_type;
    // since the provision of multiple tagger trainings in the derivations, a slightly different jet collection name is used in the CDI; convert it here
    std::map<std::string, std::string> jetcollection_translator = {{"AntiKt4EMTopoJets", "AntiKt4EMTopoJets_BTagging201810"},
								   {"AntiKt4EMPFlowJets", "AntiKt4EMPFlowJets_BTagging201903"},
								   {"AntiKtVR30Rmax4Rmin02TrackJets", "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903"}};
    jetcollection_name_CDI = jetcollection_translator[jetcollection_name];
    
    datatree = new HighLevelTaggerAugmentedDataTree(inChain, CDI_path, jetcollection_name_CDI);

    initialize();
}

EfficiencyProducer::~EfficiencyProducer()
{
    finalize();
}

bool EfficiencyProducer::pass_JVT_cut(double eta, double pt, double jvt)
{
    std::map<std::string, double> pt_cut = {{"AntiKt4EMTopoJets", 120e3},
					    {"AntiKt4EMPFlowJets", 120e3},
					    {"AntiKtVR30Rmax4Rmin02TrackJets", -1.0}};
    
    std::map<std::string, double> jvt_cut = {{"AntiKt4EMTopoJets", 0.59},
					     {"AntiKt4EMPFlowJets", 0.50},
					     {"AntiKtVR30Rmax4Rmin02TrackJets", -1.0}};
    
    std::map<std::string, double> jvt_cut_border = {{"AntiKt4EMTopoJets", 0.11},
						    {"AntiKt4EMPFlowJets", 0.50},
						    {"AntiKtVR30Rmax4Rmin02TrackJets", -1.0}};
    
    if(pt < pt_cut[jetcollection_name])
    {
	if(abs(eta) < 2.4)
	{
	    return abs(jvt) > jvt_cut[jetcollection_name];
	}
	if(abs(eta) < 2.5 && abs(eta) >= 2.4)
	{
	    return abs(jvt) > jvt_cut_border[jetcollection_name];
	}
    }
    
    return true;
}

std::vector<int> EfficiencyProducer::getNHardestTruthJets(unsigned int n, std::vector<float>* jet_pt, std::vector<int>* jet_LabDr_HadF)
{
    // first, prepare a list of jets only based on the type, keeping track of their original indices in the jet list

    std::vector<int> inds;
    {
      for(unsigned int jet = 0; jet < jet_LabDr_HadF -> size(); jet++)
	{
	  if(std::abs(jet_LabDr_HadF -> at(jet)) == 5)
	    {
	      if(jet_type == "b_jet")
		{
		  inds.push_back(jet);
		}
	    }
	  else if(std::abs(jet_LabDr_HadF -> at(jet)) == 4)
	    {
	      if(jet_type == "c_jet")
		{
		  inds.push_back(jet);
		}
	    }

	  else if(std::abs(jet_LabDr_HadF -> at(jet)) == 15)
	    {
	      if(jet_type == "tau_jet")
		{
		  inds.push_back(jet);
		}
	    }
	  else
	    {
	      if(jet_type == "l_jet")
		{
		  inds.push_back(jet);
		}
	    }

	}
    }
    // then, sort the list of indices based on the jet pt
    std::sort(inds.begin(), inds.end(),
	      [jet_pt](size_t i1, size_t i2) {return jet_pt -> at(i1) > jet_pt -> at(i2);});

    // extract the subvector holding the indices of the two hardest b-jets
    if(inds.size() >= n)
    {
	std::vector<int> retval(inds.begin(), inds.begin() + 2);
	return retval;
    }
    else
    {
	return inds;
    }
}



void EfficiencyProducer::Loop()
{
    // use only events that have been approved by the jet cleaning
    auto cut = [&](DataTree* dt) -> bool {
	return dt -> eventClean == 1;
    };

    auto fill_callback = [&](DataTree* dt, Profiler::WeightCollection weight_coll) -> void {

	HighLevelTaggerAugmentedDataTree* hldt = dynamic_cast<HighLevelTaggerAugmentedDataTree*>(dt);
	
	// extract the leading two truth b-jets
	auto variable_to_sort = (jetcollection_name == "AntiKtVR30Rmax4Rmin02TrackJets") ? dt -> had_pt : dt -> jet_pt;
	std::vector<int> truth_jets = getNHardestTruthJets(2, variable_to_sort, dt -> jet_LabDr_HadF);
	
	// loop over MC weight variations, i.e. the individual weight fetchers
	for(auto weight_name: weight_names)
	{
	    double cur_weight = weight_coll[weight_name];
	    double cur_lumiweight = weight_coll[weight_name + "_lumiweight"];

	    double JVT_weight = 1.0;
	    if(jetcollection_name != "AntiKtVR30Rmax4Rmin02TrackJets")
	    {
		for(auto jet: truth_jets)
		{
		    JVT_weight *= dt -> jet_JVT_weight -> at(jet);
		}
	    }

	    for(auto jet: truth_jets)
	    {	
		float cur_jet_pt_MeV = dt -> jet_pt -> at(jet);
		float cur_jet_pt_GeV = cur_jet_pt_MeV / 1000.;
		float cur_jet_eta = dt -> jet_eta -> at(jet);

		float cur_had_pt_MeV = jetcollection_name == "AntiKtVR30Rmax4Rmin02TrackJets" ? dt -> had_pt -> at(jet) : 0.0;
		float cur_had_pt_GeV = cur_had_pt_MeV / 1000.;

		// for VR track jets: need to have a valid B-hadron (otherwise pT(had)-reweighting does not make sense)
		if((jetcollection_name == "AntiKtVR30Rmax4Rmin02TrackJets") && (cur_had_pt_GeV < 0.0))
		    continue;

		int cur_jet_aliveAfterOR = dt -> jet_aliveAfterOR -> at(jet);
		int cur_jet_aliveAfterORmu = dt -> jet_aliveAfterORmu -> at(jet);
		int cur_jet_aliveAfterCollOR = jetcollection_name == "AntiKtVR30Rmax4Rmin02TrackJets" ? dt -> jet_aliveAfterCollOR -> at(jet) : 1; // check for collinarity, but only for TrackJets
		int cur_jet_aliveAfterJVT = jetcollection_name == "AntiKtVR30Rmax4Rmin02TrackJets" ? 1 : (userJVTCut ? pass_JVT_cut(cur_jet_eta, cur_jet_pt_MeV, dt -> jet_JVT -> at(jet)) : dt -> jet_aliveAfterJVT -> at(jet));

		// make sure to only look at VR track jets with at least two tracks
		int number_jet_constituents = jetcollection_name == "AntiKtVR30Rmax4Rmin02TrackJets" ? dt -> jet_nJetConstituents -> at(jet) : 0;
		int jet_OKForTagging = jetcollection_name == "AntiKtVR30Rmax4Rmin02TrackJets" ? number_jet_constituents > 1 : 1;

		float var_to_histogram = createReweighting || (jetcollection_name == "AntiKtVR30Rmax4Rmin02TrackJets") ? cur_had_pt_GeV : cur_jet_pt_GeV;
		float var_to_reweight = cur_had_pt_GeV;
		
		float reweight = 1.0;
		if(reweighter)
		{
		    reweight = reweighter -> getWeight(var_to_reweight);
		}
		// if(cur_weight > 80)
		// {
		//   std::cout<<" weight_name "<<weight_name<<" cur_weight "<<cur_weight<<std::endl;
		// }
		if(std::abs(cur_jet_eta) < 2.5 && cur_jet_pt_GeV > 20.0 && //cur_weight < 80 &&
		   cur_jet_aliveAfterOR && cur_jet_aliveAfterORmu &&
		   cur_jet_aliveAfterJVT && cur_jet_aliveAfterCollOR && jet_OKForTagging)
		{     			
		    // the Poisson bootstrap depends on the current event
		    long eventNumber = dt -> eventnb;
		    int runNumber = dt -> runnb;
		    int MCChannelNumber = dt -> mcchan;
		    
		    h_truth -> at(weight_name) -> Fill(var_to_histogram, cur_weight * cur_lumiweight * JVT_weight * reweight, eventNumber, runNumber, MCChannelNumber);
		    for(auto cur_WP: m_WPs)
		    {
			double tagger_score = hldt -> jet_highlevel_tagscore[tagger_name] -> at(jet);
			bool istagged = m_btagSelectionTool[cur_WP] -> accept(cur_jet_pt_MeV, cur_jet_eta, tagger_score);
			//std::cout<<"istagged "<<istagged<<" cur_jet_pt_MeV "<<cur_jet_pt_MeV<<" cur_jet_eta "<<cur_jet_eta<<" tagger_score "<<tagger_score<<"tagger_name"<<tagger_name<<"  "<<" weight_name "<<weight_name<<" cur_weight "<<cur_weight<<std::endl;
			if(istagged)
			{			    
			    h_tagged -> at(cur_WP) -> at(weight_name) -> Fill(var_to_histogram, cur_weight * cur_lumiweight * JVT_weight * reweight, eventNumber, runNumber, MCChannelNumber);
			}
		    }	    		    
		}
	    }
	}
    };

    Profiler* prof = new Profiler();
    prof -> Profile(datatree, wfc, cut, fill_callback, evtMax);
}

void EfficiencyProducer::Write()
{
    for(auto weight_name: weight_names)
    {
	m_outdir -> cd();
	std::string subdir_name = weight_name;

	TDirectory* cur_sub = m_outdir -> mkdir(subdir_name.c_str());

	cur_sub -> cd();
	h_truth -> at(weight_name) -> Write();
        
	for(auto cur_WP: m_WPs)
	{
	    h_tagged -> at(cur_WP) -> at(weight_name) -> Write();
	}
    }

    std::cout << "finished writing histograms" << std::endl;
}

void EfficiencyProducer::initialize() 
{
    std::cout << "Initializing EfficiencyProducer" << std::endl;
    
    h_truth = new VariationTH1<TH1DBootstrap>(weight_names, "truthb_", "truth jet pT ", bin_edges.size() - 1, bin_edges.data());
    h_tagged = new WPHistMap();

    for(auto cur_WP: m_WPs)
    {
	h_tagged -> insert(std::make_pair(cur_WP, new VariationTH1<TH1DBootstrap>(weight_names, "taggedb_" + cur_WP + "|", "tagged jet pT " + cur_WP, bin_edges.size() - 1, bin_edges.data())));
    }
    
    for(auto cur_WP: m_WPs)
    {
	TString toolname="btst" + cur_WP;

	BTaggingSelectionTool* b = new BTaggingSelectionTool("btst" + cur_WP);
	b -> setProperty("TaggerName", tagger_name);
	b -> setProperty("MinPt", 20000.);
	b -> setProperty("MaxRangePt", 3000000.);
	b -> setProperty("MaxEta", 2.5);
	b -> setProperty("JetAuthor", this -> jetcollection_name_CDI);
	b -> setProperty("OperatingPoint", cur_WP);
	b -> setProperty("FlvTagCutDefinitionsFileName", this -> CDI_path);
	b -> initialize();
    
	m_btagSelectionTool.insert(std::make_pair(cur_WP, b));
    }
}

void EfficiencyProducer::finalize() 
{
    delete h_truth;

    for(auto cur_hist: *h_tagged)
    {
	delete cur_hist.second;
    }
}
