#include "EfficiencyProducer/ArgumentParser.h"
#include <iostream>

std::pair<ArgumentParser::OptArgDict, ArgumentParser::PosArgs> ArgumentParser::parseArgs(int argc, char** argv)
{
    // prepare defaults
    std::map<std::string, std::string> opt_args = {
	{"lumifile_path", ""},
	{"binningfile_path", ""},
	{"weightspec", ""},
	{"tree", ""},
	{"tagger", ""},
	{"jetcollection_name", ""},
	{"jet_type", ""},
	{"number_events", ""},
	{"CDI_path", ""},
	{"create_reweighting", "false"},
	{"reweighting_path", ""},
	{"reweighting_name", ""}
    };

    std::vector<std::string> pos_args;

    int c;
    std::cout << "starting to parse" << std::endl;
    while((c = getopt(argc, argv, "c:n:t:e:v:a:l:j:k:f:wr:i:")) != -1)
    {
	std::cout << "now parsing '" << (char)c << "'" << std::endl;
	switch(c)
	{
	    //path to configuration file
            case 'c':
		opt_args["binningfile_path"] = optarg;
		break;

	    // number of events to iterate on
	    case 'n':
		opt_args["number_events"] = optarg;
		break;

	    // whether or not to use the modified MC weights available in the file
	    case 'v':
		opt_args["weightspec"] = optarg;
		break;
		
	    // manually select a single tree to use, instead of the entire collection found in the file
	    case 't':
		opt_args["tree"] = optarg;
		break;
	
	    // the name of the tagging algorithm that is to be used
            case 'a':
		opt_args["tagger"] = optarg;
		break;

	    // the path to the lumifile
            case 'l':
		opt_args["lumifile_path"] = optarg;
                break;

	    // the path to the CDI file
	    case 'f':
		opt_args["CDI_path"] = optarg;
		break;

	    // the name of the jet collection
      	    case 'j':
		opt_args["jetcollection_name"] = optarg;
		break;

	    // type of jet
	    case 'k': 
	       opt_args["jet_type"] = optarg;
	       break;

	    // run to histogram the variable in terms of which the reweighting is done
	    case 'w':
		opt_args["create_reweighting"] = "true";
		break;

	    // the path to a potential reweighting file
	    case 'r':
		opt_args["reweighting_path"] = optarg;
		break;

	    // the additional name associated to this reweighting
	    case 'i':
		opt_args["reweighting_name"] = optarg;
		break;

	    default:
		abort();
	}
    }

    int pos = optind;

    // prepare the input file paths
    for(pos = optind; pos < argc; pos++)
    {
	pos_args.push_back(argv[pos]);
    }

    return std::make_pair(opt_args, pos_args);
}
