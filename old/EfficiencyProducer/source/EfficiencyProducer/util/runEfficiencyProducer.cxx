#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>

#include "TTree.h"
#include "TFile.h"
#include "TString.h"

#include "EfficiencyProducer/EfficiencyProducer.h"
#include "EfficiencyProducer/RegexUtils.h"
#include "EfficiencyProducer/TFileUtils.h"
#include "EfficiencyProducer/WeightFetcher.h"
#include "EfficiencyProducer/ArgumentParser.h"
#include "EfficiencyProducer/Reweighter1D.h"
#include "ConfigFileUtils/ConfigFileHandler.h"
#include "ConfigFileUtils/ConfigFileUtils.h"

int main(int argc, char **argv)
{
    auto args = ArgumentParser::parseArgs(argc, argv);
    ArgumentParser::OptArgDict opt_args = args.first;
    ArgumentParser::PosArgs pos_args = args.second;

    if(pos_args.size() < 2)
    {
    	std::cerr << "require at least one input file and one output file!" << std::endl;
    	return(-1);
    }

    std::string outfile_path = pos_args.back();
    pos_args.pop_back();
    std::vector<std::string> infile_paths = pos_args;

    // add the input files to iterate over to the TChain in a randomized way
    // this should help reduce slowdowns when many jobs are running on the same files,
    // since at any given time, only a small number of them should be using the same file
    std::random_shuffle(infile_paths.begin(), infile_paths.end());

    std::cout << "using the following input files:" << std::endl;
    for(auto cur_file: infile_paths)
    {
    	std::cout << cur_file << "\t";
    }

    std::cout << std::endl << "using the following output file:" << std::endl;
    std::cout << outfile_path << std::endl;

    std::cout << "using the following options:" << std::endl;
    for(auto cur: opt_args)
    {
	std::cout << cur.first << " = " << cur.second << std::endl;
    }

    // parse the specification of MC weights to be used
    std::vector<WeightFetcher*> weight_fetchers;

    // parse the weight options here: split them apart into groups of weights, then instantiate the correct number of WeightFetchers to handle them	
    std::vector<std::string> weight_groups = RegexUtils::regex_search(opt_args["weightspec"], "([^;]+)");

    std::cout << "creating WeightFetchers:" << std::endl;
    std::vector<TString> available_weight_trees = TFileUtils::get_MC_weight_trees(infile_paths[0], opt_args["jetcollection_name"]);
    for(auto weight_group: weight_groups)
    {
	WeightFetcher* cwf = new WeightFetcher(weight_group, opt_args["lumifile_path"]);

	std::vector<TChain*> available_weight_chains;
	
	for(auto cur_weight_tree: available_weight_trees)
	{	
	    TChain* curChain = new TChain(cur_weight_tree);
	    for(auto infile_path: infile_paths)
		curChain -> Add(infile_path.c_str());
	    
	    available_weight_chains.push_back(curChain);
	}	
	
	cwf -> setWeightChains(available_weight_chains);
	cwf -> compile();

	weight_fetchers.push_back(cwf);
    }

    // prepare the binning
    std::vector<double> xbin = {20., 50., 100., 150., 200., 250., 300., 350., 400., 450., 500., 560., 620., 680., 740., 800., 880., 960., 1080., 1200., 1500., 3000.};
    if(!opt_args["binningfile_path"].empty())
    {
    	// read the binning from the configuration file and replace the default one above
	xbin.clear();
    	ConfigFileHandler* conf = new ConfigFileHandler(opt_args["binningfile_path"], "read");

    	std::vector<TString> bin_list = ConfigFileUtils::ParsePythonList(conf -> GetField("NTupleReader", "bins").Data());
    	for(auto cur: bin_list)
    	{
    	    xbin.push_back(std::stod(cur.Data()));
    	}
    }
    std::cout << "using the following binning: ";
    for(auto cur: xbin)
    	std::cout << cur << " ";
    std::cout << std::endl;

    long evtMax = -1;
    if(!opt_args["number_events"].empty())
    {
	evtMax = std::stol(opt_args["number_events"]);
    }

    // set up what to do in terms of reweighting
    bool create_reweighting = opt_args["create_reweighting"] == "true" ? true : false;
    if(create_reweighting) {
	std::cout << "Will create weight file for reweighting." << std::endl;
    }

    std::string reweight_path = opt_args["reweighting_path"];
    std::vector<std::string> additionalSystNames;
    
    Reweighter1D* reweighter = nullptr;
    if(!reweight_path.empty()) 
    {
	std::cout << "Will use the following reweighting path: " << reweight_path << std::endl;
	reweighter = new Reweighter1D(reweight_path);
	if(!opt_args["reweighting_name"].empty())
	{
	    additionalSystNames.push_back(opt_args["reweighting_name"]);
	}
    }
    else
    {
	std::cout << "Will not perform reweighting." << std::endl;
    }

    // peek into the input file and find the available trees
    std::vector<TString> available_trees = {opt_args["tree"]};

    // prepare the output file
    TFile* outfile = new TFile(outfile_path.c_str(), "RECREATE");

    // loop over the trees and dump them into histograms
    for(auto cur_tree: available_trees)
    {
    	// split the current tree name into the basename of the jet collection and the systematic variation that has already been applied to it
    	std::vector<std::string> systList = ConfigFileUtils::Split(cur_tree.Data(), "|");
	systList.insert(std::end(systList), std::begin(additionalSystNames), std::end(additionalSystNames));
    	std::string jetCollection = systList[0];
    	std::string systName = "";

    	if(systList.size() > 1)
    	{
    	    systList.erase(systList.begin());
    	    systName = ConfigFileUtils::Join(systList, "|");
    	}
	
    	std::cout << "currently on JetCollection = " << jetCollection << " | systName = " << systName << std::endl;

    	TChain* chain = new TChain(cur_tree);
    	if(chain == 0)
	{
	    throw "Chain not found!";
	}

    	for(auto infile_path: infile_paths)
    	{
    	    chain -> Add(infile_path.c_str());
    	}

    	TDirectory* cur_sub = outfile -> mkdir((jetCollection + "/" + systName).c_str());
    	cur_sub = cur_sub -> GetDirectory(systName.c_str());

	std::vector<std::string> WPs = {"FixedCutBEff_60", "FixedCutBEff_70", "FixedCutBEff_77", "FixedCutBEff_85"};
    	EfficiencyProducer* t = new EfficiencyProducer(chain, opt_args["jetcollection_name"], opt_args["jet_type"], xbin, weight_fetchers, cur_sub, opt_args["tagger"], WPs, opt_args["CDI_path"], evtMax, create_reweighting, reweighter);
    	t -> Loop();
    	t -> Write();
    }

    outfile -> Close();

    std::cout << "done" << std::endl;
    return(0);
}
