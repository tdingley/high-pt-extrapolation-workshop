import re, os, ROOT
from argparse import ArgumentParser

class TFileHandler(object):
    
    def __init__(self, fileobj, mode = 'RECREATE'):
        self.fileobj = fileobj
        self.need_to_close = False
        if isinstance(self.fileobj, str):
            if mode == 'READ':
                self.fileobj = ROOT.TFile.Open(self.fileobj)
            else:
                self.fileobj = ROOT.TFile(self.fileobj, mode)

            ROOT.SetOwnership(self.fileobj, False)
            self.need_to_close = True

    def __del__(self):
        if self.need_to_close:
            self.fileobj.Close()

    def cd(self, dirname):
        self.fileobj.cd(dirname)

    def get_obj_list(self, obj_type):
        obj_list = []

        for cur_key in self.fileobj.CurrentDirectory().GetListOfKeys():
            if cur_key.GetClassName() == obj_type:
                obj_list.append(cur_key.GetName())
        
        return obj_list

    def get_obj(self, obj_name):
        obj = self.fileobj.CurrentDirectory().Get(obj_name).Clone()
        obj.SetDirectory(0)

        return obj

    def dump_objs(self, objdict):
        self.fileobj.cd()

        for obj_name, obj in objdict.items():
            obj.SetName(obj_name)
            obj.Write()

class HistUtils:

    @staticmethod
    def get_bin_edges(hist):
        edges = []
        for bin in range(1, hist.GetSize()):
            edges.append(hist.GetBinLowEdge(bin))
            
        return edges

    @staticmethod
    def normalise(hist):
        rethist = hist.Clone()
        rethist.Scale(1.0 / rethist.Integral("width"))
        return rethist

    @staticmethod
    def ratio(num_hist, den_hist):
        ratio_hist = num_hist.Clone()
        ratio_hist.Divide(den_hist)
        return ratio_hist

def MakeReweighterHistogram(reweight_from, reweight_to, outfile_path):

    file_source = TFileHandler(reweight_from, mode = "READ")

    # parse the (known) file structure of the EfficiencyProducer file
    available_jet_collections = file_source.get_obj_list("TDirectoryFile")
    if len(available_jet_collections) != 1:
        raise RuntimeError("Error: found more than one available jet collection, this is unexpected!")
    available_jet_collection = available_jet_collections[0]

    file_source.cd(os.path.join(available_jet_collection, "Nominal", "nominal"))
    available_source_histograms = file_source.get_obj_list("TH1D")
    nominal_source_histograms = filter(lambda cur: re.compile(".*truth_nominal_bootstrap_0.*").match(cur), available_source_histograms)
    if len(nominal_source_histograms) != 1:
        raise RuntimeError("Error: file seems to contain no or more than one nominal histogram!")

    nominal_source_histogram_name = nominal_source_histograms[0]
    reweighting_source_histogram = file_source.get_obj(nominal_source_histogram_name)
    del file_source

    print("using the following source histogram for the reweighting")
    print(nominal_source_histogram_name)

    # assume to find exactly one histogram in 'reweight_to' (namely the unique target histogram of the reweighting)
    file_target = TFileHandler(reweight_to, mode = "READ")

    available_target_histograms = file_target.get_obj_list("TH1D")
    if len(available_target_histograms) != 1:
        raise RuntimeError("Error: found no or multiple histograms in the passed target file; require an unambiguous target histogram for the reweighting!")

    target_histogram_name = available_target_histograms[0]
    reweighting_target_histogram = file_target.get_obj(target_histogram_name)
    del file_target

    print("using the following target histogram for the reweighting")
    print(target_histogram_name)

    # perform some sanity checks to make sure the target and source histograms are compatible in terms of their binning
    if HistUtils.get_bin_edges(reweighting_source_histogram) != HistUtils.get_bin_edges(reweighting_target_histogram):
        raise RuntimeError("Error: the histograms specified as source and target for the reweighting have incompatible binning")
    
    # make sure that the total yield is not changed by the reweighting
    reweighting_source_histogram = HistUtils.normalise(reweighting_source_histogram)
    reweighting_target_histogram = HistUtils.normalise(reweighting_target_histogram)

    # compute the reweighting histogram as the simple ratio ...
    reweighting_hist = HistUtils.ratio(reweighting_target_histogram, reweighting_source_histogram)

    # ... and save it
    outfile = TFileHandler(outfile_path, mode = "RECREATE")
    outfile.dump_objs({"reweighting_hist": reweighting_hist})
    del outfile

if __name__ == "__main__":
    
    parser = ArgumentParser(description = "create the final data histogram for one-dimensional reweightings")
    parser.add_argument("--reweight_from", action = "store")
    parser.add_argument("--reweight_to", action = "store")
    parser.add_argument("--outfile", action = "store", dest = "outfile_path")
    args = vars(parser.parse_args())

    MakeReweighterHistogram(**args)
