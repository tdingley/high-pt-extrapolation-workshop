Setup Instructions
==================

Build the package
```
mkdir build run
cd build
asetup AthDerivation,21.2.87.0,here  # this is the minimal version required to have the retrained VRTrackJet taggers from the 201903 optimisation campaign available
mv CMakeLists.txt ../source
cmake ../source
cmake --build .
source [x64...]/setup.sh
```

Processing samples on the Grid
==============================

The main "meta" configuration files in `Config/` are "sliceable", i.e. a single file can contain information for multiple runs. Before they can be used to launch jobs, these files need to be "sliced", i.e. given a single meta configuration file, a number of configuration files are generated, each containing information about a single run only.

This is performed by `ConfigFileSweeper/ConfigFileSweeper.py`, which is run as follows
```
python ConfigFileSweeper/ConfigFileSweeper.py $CONFIGFILE
```
where `$CONFIGFILE` is the path to the meta configuration file. This command will generate a number of sliced files in the same directory, each suffixed with `_slice_N.py`. An example how to use the `ConfigFileSweeper` is provided in `ConfigFileSweeper/example.py`.

Note: after slicing the configuration files, the `NTupleProducer` package needs to be rebuilt in order to generate the symlinks to the newly generated files in the ```/build/x86[...]/jobOptions``` directory:
```
cd build
cmake ..
cmake --build .
```

Then, submit jobs using these configuration files:
```
lsetup panda
voms-proxy-init -voms atlas --valid 48:00
python UniversalGridSubmitter/UGS.py $SLICED_CONFIGFILE_1 $SLICED_CONFIGFILE_2 ...
```
where the `$SLICED_CONFIGFILE` are the paths to the sliced config files that should be submitted.

Downloading the produced NTuples
================================

To download the NTuples once the grid jobs are finished, use
```
lsetup rucio
voms-proxy-init -voms atlas --valid 48:00
python UniversalGridSubmitter/UDD.py --outdir $DATADIR SLICED_CONFIGFILE_1 $SLICED_CONFIGFILE_2
```
This will take the list of provided sliced configuration files, extract the corresponding containers and launch `rucio` jobs to download them.

Generating lumi config files for combining samples from different years
=======================================================================

When samples from different years need to be combined, they need to pick up the correct relative
weights. These depend on the integrated luminosity (as per the GRL) as well as the cross section
of the sample (as per AMI). [For the Rel 21 results for the taggers from the 201903 training campaign, this is the case for the ttbar samples used to assess modelling uncertainties.]

For each dataset, these are stored in lumi.conf files. These can easily be generated in the
following way
```
python RunLumiFileCampaign.py --outdir $OUT_DIR $SLICED_CONFIGFILE
```

Note: to work flawlessly, this assumes you have access to a `pbs` batch system. If this is not your case, you can easily modify the details of the job submission in `UniversalGridSubmitter/JobSubmitter.py`.