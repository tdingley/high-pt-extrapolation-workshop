import re, os, sys, glob
from argparse import ArgumentParser
from configparser import ConfigParser

from UniversalGridSubmitter.UGSParser import UGSParser
from CDITools.TFileUtils import TFileUtils

import ROOT
from ROOT import TFile

def IsFileGood(input_file):
    cur_file = TFile(input_file, 'READ')
    retval = True
    if cur_file.IsZombie():
        retval = False
    cur_file.Close()

    return retval

def GetGlobalSOW(input_file, weight_regex):
    if isinstance(weight_regex, str):
        weight_regex = re.compile(weight_regex)

    # get the full histogram name from the peek file
    cur_file = TFile(input_file, 'READ')
    hist_list = TFileUtils.get_objectlist(cur_file, "TH1D")

    # now apply the weight_regex
    SOW_regex = re.compile("SumOfWeights_.*")
    matching_hists = []
    for cur_hist in hist_list:
        if SOW_regex.match(cur_hist) and weight_regex.match(cur_hist):
            matching_hists.append(cur_hist)

    if len(matching_hists) != 1:
        matching_hist = "None"
        retval = 0
        print("ignoring file {}".format(input_file))
    else:
        matching_hist = matching_hists[0]
        hist = cur_file.Get(matching_hist)
        retval = hist.GetBinContent(1)

    print("{} -> {}".format(matching_hist, retval))

    cur_file.Close()

    return retval

def MakeLumiFile(input_dir, lumifile_path, weightspec, xsec = None, lumi = None):
    """
    Generate a configuration file for a certain run. Write the following
    to lumifile_path:
       * luminosity (usually taken from GRL)
       * process cross section (usually taken from AMI)

    Parameters:
    input_dir: path to the directory holding all the NTuples
    lumifile_path: path of the output file that should be written
    weightspec: regular expression object specifying the weight variation
    """

    # Note: only temporary; this removes all weights that do not correspond to
    # a MC generator weight (but e.g. come from pileup or JVT)
    def strip_non_generator_weights(weight_regexes):
        to_strip = [".*PUWeight"]

        for cur in to_strip:
            if cur in weight_regexes:
                weight_regexes.remove(cur)

        return weight_regexes

    print("attempting to generate lumi file for '{}'".format(input_dir))
    print("weightspec = {}".format(weightspec))

    # if the configuration file already exists in this directory, open it
    lumiconfig = ConfigParser()
    lumiconfig.read(lumifile_path)

    # if given, store the luminosity and cross-section
    if not "global" in lumiconfig:
        lumiconfig["global"] = {}

    global_config = lumiconfig["global"]

    if xsec:
        global_config["xsec"] = str(xsec)

    if lumi:
        global_config["lumi"] = str(lumi)

    # next, fetch the variations for which the sum-of-weights are required
    weight_groups = re.findall("([^;]+)", weightspec)

    print("found the following weight groups: ")
    print(weight_groups)

    # for each weight group, get the name, numerator and denominator contents
    for weight_group in weight_groups:
        m = re.match("^([^/]*)/{0,1}([^/]*)$", weight_group)
        num = m.group(1)
        den = m.group(2)
        
        # now extract the individual numerator and denominator regexes
        weight_group_name = re.match("\[(.+)\]", num).group(1)
        num_weight_regexes = strip_non_generator_weights(re.findall("<(.+?)>", num))
        den_weight_regexes = strip_non_generator_weights(re.findall("<(.+?)>", den))

        print(weight_group_name)

        # finally, just need to get the total Sum-Of-Weights over all files in the run,
        # combine them as per their numerators and denominators, and write them out to
        # the lumifile
        total_SOW = 0.0

        # first, get a list of all available ROOT files in this directory
        available_files = glob.glob(os.path.join(input_dir, "*.root"))
        for cur_file in available_files:
            if not IsFileGood(cur_file):
                continue

            file_SOW = 1.0

            for cur_weight_regex in num_weight_regexes:
                file_SOW *= GetGlobalSOW(input_file = cur_file, weight_regex = cur_weight_regex)

            for cur_weight_regex in den_weight_regexes:
                file_SOW /= GetGlobalSOW(input_file = cur_file, weight_regex = cur_weight_regex)

            total_SOW += file_SOW

        if weight_group_name not in lumiconfig:
            lumiconfig[weight_group_name] = {}

        lumiconfig[weight_group_name]["SOW"] = str(total_SOW)
        
    # finally store the complete lumifile
    with open(lumifile_path, 'w') as outfile:
        lumiconfig.write(outfile)

if __name__ == "__main__":
    ROOT.gROOT.SetBatch(True)

    parser = ArgumentParser(description = "compute configuration files for proper event weighting")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    parser.add_argument("--lumi", action = "store", dest = "lumi")
    parser.add_argument("--xsec", action = "store", dest = "xsec")
    parser.add_argument("infile", nargs = '*')
    args = vars(parser.parse_args())

    outdir = args["outdir"]
    config_files = args["infile"]

    # note: as per the LHC default, integrated luminosities are given in pb^-1,
    # also cross sections are measured in pb
    lumi = args["lumi"]
    xsec = args["xsec"]

    for config_file in config_files:
        # read the UGS section of the run config file
        with open(config_file, 'r') as ugs_file:
            pars = UGSParser(ugs_file)
            defs = pars.extract_variable_definition(["weightspec", "OUT_DS", "IN_DS", "lumi_data"])
            
            original_dataset = defs["IN_DS"]
            # strip away the leading part before any colon, otherwise AMI complains
            original_dataset = original_dataset.split(':')[-1]

            if not lumi:
                print("no explicit luminosity value given, attempt to load it from config file")
                lumi = float(defs["lumi_data"])

            if not xsec:
                print("no explicit cross section value given, attempt to load it from pyAMI")
                import pyAMI.atlas.api as atlasAPI 
                import pyAMI.client

                client = pyAMI.client.Client('atlas')
                atlasAPI.init()

                dsInfo = atlasAPI.get_dataset_info(client, original_dataset)[0]
                #dsInfo = {cur[0]: cur[1] for cur in dsInfo}

                gen_xsec = float(dsInfo["crossSection@MCGN"]) * 1000 # AMI lists cross sections in nb!
                gen_filteff = float(dsInfo["genFiltEff@MCGN"])
                print("extracted generator-level cross section = {} pb".format(gen_xsec))
                print("extracted generator-level filter efficiency = {}".format(gen_filteff))

                xsec = str(gen_xsec * gen_filteff)
                print("using final cross section = {} pb".format(xsec))

            input_dataset = defs["OUT_DS"]
            weightspec = defs["weightspec"]

            print("extracted input_dataset = " + input_dataset)
            local_dataset = glob.glob(os.path.join(outdir, input_dataset + '_*'))
            
            # exclude any histogrammed datasets
            local_dataset = [cur_dataset for cur_dataset in local_dataset if "hists" not in cur_dataset]

            if len(local_dataset) == 1:
                # found unique matching dataset
                local_dataset = local_dataset[0]
            else:
                print("found multiple local candidate datasets:")
                for local_dataset in local_dataset:
                    print(local_dataset)

                sys.exit(-1)

            print("dataset found locally: " + local_dataset)

            MakeLumiFile(input_dir = local_dataset, lumifile_path = os.path.join(local_dataset, "lumi.conf"), weightspec = weightspec,
                         xsec = xsec, lumi = lumi)
