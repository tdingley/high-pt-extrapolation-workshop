import ast, sys, re
from optparse import OptionParser
import subprocess as sp

def get_weight_dict(file_path):
    cmSG = sp.Popen(["checkMetaSG", file_path], stdout = sp.PIPE)
    output = sp.check_output(["grep", "HepMCWeightNames"], stdin = cmSG.stdout)

    dict_extr = re.compile("{(.+?)}")
    dict_rep = dict_extr.findall(output)
    dict_rep = '{' + dict_rep[0] + '}'

    weight_dict = ast.literal_eval(dict_rep)

    return weight_dict

def main():

    # the lambda that converts weight names into names of systematic variations
    weight_lambda = lambda x: x.replace('=', '')

    parser = OptionParser()
    parser.add_option("--range", action = "store", dest = "range")
    parser.add_option("--weight", action = "store", dest = "weight")

    (options, args) = parser.parse_args()
    file_path = args[0]

    if options.range:
        terminators_re = re.compile("(.+?)\s*-\s*(.+?)\s*$")
        terms = terminators_re.findall(options.range)[0]
        start = int(terms[0])
        end = int(terms[1])
        requested_weights = range(start, end + 1)

    if options.weight:
        requested_weights = [int(options.weight)]

    print("getting names for the following weights:")
    for weight in requested_weights:
        print(weight)

    # extract the dictionary with all the weights
    weight_dict = get_weight_dict(file_path)

    # then, invert them (and, in the process, remove all whitespaces)
    inv_weight_dict = {val: key.replace(' ', '') for key, val in weight_dict.iteritems()}

    print(inv_weight_dict)

    outstr = ""

    # finally, get the requested weights
    for weight in requested_weights:
        weight_name = inv_weight_dict[weight]
        outstr += '[' + weight_lambda(weight_name) + ']' + '<' + weight_name + '>;'

    print(outstr)

if __name__ == "__main__":
    main()
