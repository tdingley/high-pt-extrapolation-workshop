import os, sys, uuid
from argparse import ArgumentParser

from UniversalGridSubmitter.JobSubmitter import JobSubmitter

def RunLumiFileCampaign(outdir, config_file):
    # make sure that the environment is well-defined
    xtrap_rootdir = os.environ["XTRAP_ROOTDIR"]
    if not xtrap_rootdir:
        raise Exception("Error: 'XTRAP_ROOTDIR' not defined. Please do 'source high-pT-extrapolation/setup.sh'")

    # prepare the job script
    job_id = str(uuid.uuid4())
    jobfile_path = os.path.join(outdir, job_id + ".sh")

    sceleton = """#!/bin/bash
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    export X509_USER_PROXY=/home/windischhofer/.gridProxy
    source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
    lsetup pyAMI
    asetup AthDerivation,21.2.60.0
    source {xtrap_setup}
    lsetup root # make sure to get the right ROOT version
    cmd="python {LumiFileMakerExec} --outdir {outdir} {config_file}"
    exec $cmd
    """

    opts = {"xtrap_setup": os.path.join(xtrap_rootdir, "setup.sh"), 
            "LumiFileMakerExec": os.path.join(xtrap_rootdir, "NTupleProducer/source/scripts/LumiFileMaker.py"),
            "outdir": outdir,
            "config_file": config_file}

    with open(jobfile_path, "w") as job_script:
        job_script.write(sceleton.format(**opts))

    JobSubmitter.submit_job(job_script_path = jobfile_path)

if __name__ == "__main__":
    parser = ArgumentParser(description = "run the production of lumi files on the batch system")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    parser.add_argument("infile", nargs = '*')
    args = vars(parser.parse_args())

    for config_file in args["infile"]:
        RunLumiFileCampaign(outdir = args["outdir"], config_file = config_file)
