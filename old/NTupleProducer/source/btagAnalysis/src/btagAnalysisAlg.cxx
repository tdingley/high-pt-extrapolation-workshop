#include "btagAnalysis/btagAnalysisAlg.h"

#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "ParticleJetTools/JetFlavourInfo.h"
#include <xAODCore/ShallowCopy.h>

#include "JetInterface/IJetSelector.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "xAODEventShape/EventShape.h"

#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "TrackVertexAssociationTool/ITrackVertexAssociationTool.h"
#include "PileupReweighting/PileupReweightingTool.h"

#include "JetInterface/IJetUpdateJvt.h"
#include "JetJvtEfficiency/JetJvtEfficiency.h"

#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicsUtil.h"

#include "TDatabasePDG.h"
#include "TParticlePDG.h"

btagAnalysisAlg::btagAnalysisAlg(const std::string& name, ISvcLocator *pSvcLocator):
  AthHistogramAlgorithm(name, pSvcLocator),
  histSvc("THistSvc", name),
  m_jet_properties_branches(),
  m_jet_JVT_branches(),
  m_tagger_scores_branches()
{
  declareProperty("Stream", m_stream);
  declareProperty("JetCollectionName", m_jetCollectionName = "AntiKt4EMTopoJets");
  declareProperty("TruthJetCollectionName", m_truthJetCollectionName = "AntiKt4TruthJets");
  declareProperty("TrackAssociator",m_track_associator = "BTagTrackToJetAssociator");
  declareProperty("JVTtool", m_jvt);

  declareProperty("CalibrateJets", m_calibrateJets = true);
  declareProperty("JetCalibrationTool", m_jetCalibrationTool);
  declareProperty("JVTEfficiencyTool", m_JVTEfficiencyTool);
  declareProperty("OutputTreeName", m_outputTreeName);
  declareProperty("MCWeightTreeNamePrefix", m_MCWeightTreeNamePrefix);
  declareProperty("MCWeightNames", m_MC_weightnames);
  declareProperty("StorePUWeights", m_store_PU_weights);
  declareProperty("PUWeightTreeNamePrefix", m_PUWeightTreeNamePrefix);
  declareProperty("PRWTool", m_PUtool);
}

btagAnalysisAlg::~btagAnalysisAlg() {

}

StatusCode btagAnalysisAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  CHECK( histSvc.retrieve() );
  ATH_MSG_INFO(m_jetCollectionName);

  m_tree = new TTree(m_outputTreeName.c_str(), m_outputTreeName.c_str());
  ATH_MSG_INFO ("registering tree in stream: " << m_stream);
  CHECK( histSvc -> regTree("/" + m_stream + "/tree_" + m_jetCollectionName, m_tree) );
  CHECK( initializeTools() );

  //EventInfo
  m_tree -> Branch("runnb", &runnumber);
  m_tree -> Branch("eventnb", &eventnumber);
  m_tree -> Branch("mcchan", &mcchannel);
  m_tree -> Branch("mcwg", &mcweight_nominal);
  m_tree -> Branch("avgmu", &mu);
  m_tree -> Branch("actmu", &Act_mu);
  m_tree -> Branch("eventClean", &event_clean);

  if(m_store_PU_weights)
  {
      ATH_MSG_INFO("creating trees for pileup weights");

      // create and register the trees needed for the pileup weights
      m_tree_PU_weight = new TTree(m_PUWeightTreeNamePrefix.c_str(), m_PUWeightTreeNamePrefix.c_str());
      m_tree_PU_weight -> Branch("mcwg", &PU_weight);
      CHECK( histSvc -> regTree("/" + m_stream + "/tree_" + m_jetCollectionName + "PUweight", m_tree_PU_weight) );

      m_tree_PU_weight_SFup = new TTree((m_PUWeightTreeNamePrefix + "_SFup").c_str(), (m_PUWeightTreeNamePrefix + "_SFup").c_str());
      m_tree_PU_weight_SFup -> Branch("mcwg", &PU_weight_SFup);
      CHECK( histSvc -> regTree("/" + m_stream + "/tree_" + m_jetCollectionName + "PUweight_SFup", m_tree_PU_weight_SFup) );

      m_tree_PU_weight_SFdown = new TTree((m_PUWeightTreeNamePrefix + "_SFdown").c_str(), (m_PUWeightTreeNamePrefix + "_SFdown").c_str());
      m_tree_PU_weight_SFdown -> Branch("mcwg", &PU_weight_SFdown);
      CHECK( histSvc -> regTree("/" + m_stream + "/tree_" + m_jetCollectionName + "PUweight_SFdown", m_tree_PU_weight_SFdown) );
  }

  m_jet_properties_branches.set_tree(*m_tree);
  m_jet_JVT_branches.set_tree(*m_tree);
  m_tagger_scores_branches.set_tree(*m_tree);

  return StatusCode::SUCCESS;
}

StatusCode btagAnalysisAlg::initializeTools()
{
    if(!m_jvt.empty()) CHECK(m_jvt.retrieve());
    if(!m_JVTEfficiencyTool.empty()) CHECK(m_JVTEfficiencyTool.retrieve());
    
    if (m_calibrateJets) CHECK( m_jetCalibrationTool.retrieve() );
    
    CHECK( m_PUtool.retrieve() );
    
    m_weightTool.setTypeAndName("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");
    ATH_CHECK(m_weightTool.retrieve());
    
    ATH_MSG_INFO("will forward the following MC weights:");
    for(auto cur_name: m_MC_weightnames)
    {
	ATH_MSG_INFO(cur_name);
    }
    
    return StatusCode::SUCCESS;
}

StatusCode btagAnalysisAlg::getCutBookkeeperByWeightName(std::string weight_name, xAOD::CutBookkeeper** retval)
{
    // first, check if the available CutBookkeepers know about the LHE3 weight names:
    ServiceHandle<StoreGateSvc> inputMetaStore("StoreGateSvc/InputMetaDataStore","");
    const xAOD::CutBookkeeperContainer* bks = 0;
    CHECK( inputMetaStore -> retrieve(bks, "CutBookkeepers") );

    xAOD::CutBookkeeper* found_cbk = nullptr;

    bool LHENamesAvailable = false;
    for(auto cur_cbk: *bks)
    {
	if(cur_cbk -> name().find("LHE3Weight_") != std::string::npos)
	{
	    LHENamesAvailable = true;
	    break;
	}
    }

    if(LHENamesAvailable)
    {
	// if yes, can just find the correct bookkeeper by looking for the weight name
	// (always need to take the one with the largest number of cycles)
	int maxCycle = -1;
	for(auto cur_cbk: *bks)
	{
	    if(cur_cbk -> name().find("LHE3Weight_" + weight_name) != std::string::npos)
	    {
		if(cur_cbk -> cycle() > maxCycle)
		{
		    maxCycle = cur_cbk -> cycle();
		    found_cbk = cur_cbk;
		}
	    }
	}
    }
    else
    {
	// if not, need to perform reverse lookup using the PMGTruthWeightTool
	// first, convert the name of the requested weight into its index in the MC weight vector
	std::vector<std::string> available_weight_names = m_weightTool -> getWeightNames();

	// Note: this vector will contain the weights in the same order as they are available in 
	// the MC weight vector
	auto it = std::find(available_weight_names.begin(), available_weight_names.end(), weight_name);
	if(it != available_weight_names.end())
	{
	    auto index = std::distance(available_weight_names.begin(), it);

	    std::string cbk_name;
	    if(index == 0)
	    {
		cbk_name = "AllExecutedEvents";
	    }
	    else
	    {
		cbk_name = "AllExecutedEvents_NonNominalMCWeight_" + std::to_string(index);
	    }

	    // now fetch the corresponding CutBookkeeper
	    int maxCycle = -1;
	    for(auto cur_cbk: *bks)
	    {
		if(cur_cbk -> name() == cbk_name && cur_cbk -> cycle() > maxCycle)
		{
		    maxCycle = cur_cbk -> cycle();
		    found_cbk = cur_cbk;
		}
	    }	    
	}
    }

    *retval = found_cbk;
    return StatusCode::SUCCESS;
}

StatusCode btagAnalysisAlg::initializeOnFirstEvent()
{
    // read in all the weights that are available in this file
    ATH_MSG_INFO("have the following weights available in this file");
    std::vector<std::string> available_weight_names = m_weightTool -> getWeightNames();
    for(auto available_weight_name: available_weight_names) 
    {	    
	ATH_MSG_INFO(available_weight_name);
    }

    // if no explicit MC weight given, will store all available weights
    if(m_MC_weightnames.size() == 1 && m_MC_weightnames.at(0) == "*")
    {
	ATH_MSG_INFO("will store all MC weights");
	m_MC_weightnames = available_weight_names;
    }

    // initialize the trees that will hold the MC weights for each event
    m_MC_weight_trees = new std::map<std::string, TTree*>();
    for(auto weight_name: m_MC_weightnames)
    {
	std::string condensed_weight_name = weight_name;

	// remove extra spaces in the MC weight name
	condensed_weight_name.erase(std::remove(condensed_weight_name.begin(), condensed_weight_name.end(), ' '), condensed_weight_name.end());
      
	std::string tree_name = (m_MCWeightTreeNamePrefix + condensed_weight_name).c_str();	
	ATH_MSG_INFO("creating tree for MC weights: " << tree_name);
	TTree* cur_tree = new TTree(tree_name.c_str(), tree_name.c_str());
	cur_tree -> Branch("mcwg", &mcweight_cur);

	CHECK( histSvc -> regTree("/" + m_stream + "/tree_" + m_jetCollectionName + condensed_weight_name, cur_tree) );
	m_MC_weight_trees -> insert(std::make_pair(weight_name, cur_tree));

	// create the histograms that hold the CutBookkeeper information
	m_hist_TotalEntries = new TH1D(("TotalEntries_" + condensed_weight_name).c_str(), ("TotalEntries" + condensed_weight_name).c_str(), 1, 0.5, 1.5);
	CHECK( histSvc -> regHist("/" + m_stream + "/hist_TotalEntries_" + condensed_weight_name, m_hist_TotalEntries) );
	
	m_hist_SumOfWeights = new TH1D(("SumOfWeights_" + condensed_weight_name).c_str(), ("SumOfWeights_" + condensed_weight_name).c_str(), 1, 0.5, 1.5);
	CHECK( histSvc -> regHist("/" + m_stream + "/hist_SumOfWeights_" + condensed_weight_name, m_hist_SumOfWeights) );
	
	m_hist_SumOfWeightsSquared = new TH1D(("SumOfWeightsSquared_" + condensed_weight_name).c_str(), ("SumOfWeightsSquared_" + condensed_weight_name).c_str(), 1, 0.5, 1.5);
	CHECK( histSvc -> regHist("/" + m_stream + "/hist_SumOfWeightsSquared_" + condensed_weight_name, m_hist_SumOfWeightsSquared) );

	// now, fetch the CutBookkeeper for this weight ...
	xAOD::CutBookkeeper* cur_cbk;
	CHECK(getCutBookkeeperByWeightName(weight_name, &cur_cbk));

	// ... and store its information into the histograms
	double total_entries = cur_cbk -> nAcceptedEvents();
	double sow = cur_cbk -> sumOfEventWeights();
	double sowsq = cur_cbk -> sumOfEventWeightsSquared();
	
	m_hist_TotalEntries -> SetBinContent(1, total_entries);
	m_hist_SumOfWeights -> SetBinContent(1, sow);
	m_hist_SumOfWeightsSquared -> SetBinContent(1, sowsq);
    }
    
    return StatusCode::SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////////
StatusCode btagAnalysisAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  if(m_calibrateJets)
  {
      CHECK( m_jetCalibrationTool.release() );
  }

  return StatusCode::SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////////

StatusCode btagAnalysisAlg::execute() {

    typedef ElementLink<xAOD::TrackParticleContainer> TrackLink;
    typedef std::vector<TrackLink> TrackLinks;

    ATH_MSG_DEBUG ("Executing " << name() << "...");

    //-------------------------
    // Event information
    //---------------------------
    const xAOD::EventInfo* eventInfo = 0;

    ATH_MSG_DEBUG(" Retrieving Event Info " );

    CHECK( evtStore() -> retrieve(eventInfo) );

    runnumber   = eventInfo -> runNumber();
    eventnumber = eventInfo -> eventNumber();
    mcchannel   = eventInfo -> mcChannelNumber();
    mcweight_nominal = eventInfo -> mcEventWeight();
    mu          = eventInfo -> averageInteractionsPerCrossing();
    Act_mu      = eventInfo -> actualInteractionsPerCrossing();
    event_clean = (int)(eventInfo -> auxdata<char>("DFCommonJets_eventClean_LooseBad"));

    float tmpMu = m_PUtool -> getCorrectedAverageInteractionsPerCrossing( *eventInfo );
    mu = tmpMu;

    if(firstEvent)
    {
	CHECK( initializeOnFirstEvent() );
	firstEvent = false;
    }
	
    for(auto weight_name: m_MC_weightnames)
    {
	mcweight_cur = m_weightTool -> getWeight(weight_name);
	m_MC_weight_trees -> at(weight_name) -> Fill();
    }

    //-------------------------
    // pileup weights
    //-------------------------
    if(m_store_PU_weights)
    {
	CP::SystematicSet s;

	// fill the nominal PU weight
	s.clear();
	m_PUtool -> applySystematicVariation(s);
	PU_weight = m_PUtool -> getCombinedWeight(*eventInfo);
	m_tree_PU_weight -> Fill();

	// fill the UP variation
	s.clear();
	s.insert( CP::SystematicVariation("PRW_DATASF",1) );
	m_PUtool -> applySystematicVariation(s);
	PU_weight_SFup = m_PUtool -> getCombinedWeight(*eventInfo);
	m_tree_PU_weight_SFup -> Fill();

	// fill the DOWN variation
	s.clear();
	s.insert( CP::SystematicVariation("PRW_DATASF",-1) );
	m_PUtool -> applySystematicVariation(s);
	PU_weight_SFdown = m_PUtool -> getCombinedWeight(*eventInfo);
	m_tree_PU_weight_SFdown -> Fill();
    }

    // primary vertex
    const xAOD::VertexContainer* Primary_vertices = 0;
    ATH_MSG_DEBUG("retrieve PrimaryVertices");
    CHECK(evtStore() -> retrieve(Primary_vertices, "PrimaryVertices"));

    int* npv_p = 0;
    StatusCode vx_count_status = evtStore() -> retrieve(npv_p, "BTaggingNumberOfPrimaryVertices");

    if (vx_count_status.isFailure()) {
	ATH_MSG_FATAL("could not retrieve number of primary vertices from file");
	return StatusCode::FAILURE;
    }

    int npv = *npv_p;

    if (npv < 1) {
	ATH_MSG_WARNING( ".... rejecting the event due to missing PV!!!!");
	return StatusCode::SUCCESS;
    }

    const xAOD::TruthEventContainer *xTruthEventContainer = NULL;
    CHECK( evtStore() -> retrieve(xTruthEventContainer, "TruthEvents") );

    std::vector<TLorentzVector> truth_electrons;
    std::vector<TLorentzVector> truth_muons;

    //---------------------------
    // prepare lists of truth Muons & Electrons overlapping with jets
    //---------------------------
    for ( const auto* truth : *xTruthEventContainer ) {

	ATH_MSG_DEBUG( " loop over truth particles in the truth event container " );
	for(unsigned int i = 0; i < truth -> nTruthParticles(); i++) {
	    const xAOD::TruthParticle *particle = truth -> truthParticle(i);

	    if(!particle){ continue; }

	    if (particle -> pt() < 10e3)     continue;
	    if (particle -> status() != 1)   continue;
	    if (particle -> barcode() > 2e5) continue;

	    if ( fabs(particle -> pdgId()) != 11 && fabs(particle -> pdgId()) != 13) continue;

	    if(!isFromWZ(particle)) continue;

	    TLorentzVector telec(particle -> p4());
	    if (fabs(particle -> pdgId()) == 11) truth_electrons.push_back(telec);
	    if (fabs(particle -> pdgId()) == 13) truth_muons.push_back(telec);
	}
    }

    ATH_MSG_DEBUG("retrieve " + m_jetCollectionName);

    //---------------------------
    // Jets
    //---------------------------
    const xAOD::JetContainer *jets = 0;
    CHECK(evtStore() -> retrieve(jets, m_jetCollectionName));

    const xAOD::JetContainer* truthjets(nullptr);

    ATH_MSG_DEBUG( " retrieve truth jet collection " );
    CHECK( evtStore() -> retrieve( truthjets, m_truthJetCollectionName) );

    // if requested, work with calibrated jets from now on
    // Note: need to make a deep copy here, to make sure not to run into problems caused by
    // the JetCollection being a view copy (SG::VIEW_ELEMENTS)
    auto calibratedJets = std::make_unique<xAOD::JetContainer>();
    auto calibratedJetsAux = std::make_unique<xAOD::JetAuxContainer>();
    calibratedJets -> setStore(calibratedJetsAux.get());
    for(auto cur_jet: *jets) {
	calibratedJets -> push_back(new xAOD::Jet(*cur_jet));
    }

    if(m_calibrateJets)
    {
    	m_jetCalibrationTool -> modify(*calibratedJets);
    	jets = calibratedJets.get();
    }

    if(!m_JVTEfficiencyTool.empty()) CHECK(m_JVTEfficiencyTool -> tagTruth(jets, truthjets));

    ATH_MSG_DEBUG( " Main jet loop " );
    for (const auto* jet : *jets) {
        // update the JVT value on top of the calibrated jets
	float jvtV = 0;
	if(!m_jvt.empty()) jvtV = m_jvt -> updateJvt(*jet);

	// Jet isolation
	float dRiso = 10;
	for(const auto* jet_b: *jets)
	{
	    if(jet != jet_b)
	    {
		float dr = jet -> p4().DeltaR(jet_b -> p4());
		if (dr < dRiso) dRiso = dr;
	    }
	}

	const xAOD::BTagging* bjet(nullptr);
	bjet = jet -> btagging();

	ATH_MSG_DEBUG( " filling TaggerScores " );
	m_tagger_scores_branches.fill(*jet);

	// just get the raw number of associated tracks here
	TrackLinks assocTracks = bjet -> auxdata<TrackLinks>(m_track_associator);
	int nJetTracks = assocTracks.size(); 

	// also get the number of constituents of the jet
	// (for track jets, this is just the number of tracks clustered into the jet)
	int nJetConstituents = jet -> numConstituents();

	//---------------------------
	// get per-jet JVT weight
	//---------------------------
	int jet_isAliveAfterJVT = 1;
	float jvt_weight = 1.0;
	float jvt_weight_up = 1.0;
	float jvt_weight_down = 1.0;

	std::function<void(xAOD::Jet, float&)> getJVTSF;

	ATH_MSG_DEBUG( " retrieving JVT values " );	
	if(!m_JVTEfficiencyTool.empty())
	{
	    // get the JVT weight as well as the result of applying the JVT cut to the jet
	    if(m_JVTEfficiencyTool -> passesJvtCut(*jet))
	    {
		jet_isAliveAfterJVT = 1;
		
		getJVTSF = [&](xAOD::Jet jet, float& val) -> void {
		    m_JVTEfficiencyTool -> getEfficiencyScaleFactor(jet, val);
		};	    
	    }
	    else
	    {
		jet_isAliveAfterJVT = 0;
		
		getJVTSF = [&](xAOD::Jet jet, float& val) -> void {
		    m_JVTEfficiencyTool -> getInefficiencyScaleFactor(jet, val);
		};	    
	    }
	    
	    CP::SystematicSet s;
	    
	    // fill the UP variation
	    s.clear();
	    s.insert(CP::SystematicVariation("JET_JvtEfficiency", 1));
	    m_JVTEfficiencyTool -> sysApplySystematicVariation(s);
	    getJVTSF(*jet, jvt_weight_up);
	    
	    // fill the DOWN variation
	    s.clear();
	    s.insert(CP::SystematicVariation("JET_JvtEfficiency", -1));
	    m_JVTEfficiencyTool -> sysApplySystematicVariation(s);
	    getJVTSF(*jet, jvt_weight_down);
	    
	    // fill the nominal efficiency weight
	    s.clear();
	    m_JVTEfficiencyTool -> sysApplySystematicVariation(s);
	    getJVTSF(*jet, jvt_weight);
	}

	ATH_MSG_DEBUG( " filling JVT branches " );
	m_jet_JVT_branches.fill(jvt_weight, jvt_weight_up, jvt_weight_down, jet_isAliveAfterJVT);

	ATH_MSG_DEBUG( " filling JetProperties " );
	m_jet_properties_branches.fill(*jet, jvtV, dRiso, truth_electrons,
				       truth_muons,  truthjets, nJetTracks, nJetConstituents);
    }

    ATH_MSG_DEBUG("done with preparing");

    m_tree -> Fill();

    ATH_MSG_DEBUG("done with filling");

    m_jet_JVT_branches.clear();
    m_jet_properties_branches.clear();
    m_tagger_scores_branches.clear();

    ATH_MSG_DEBUG("done with clearing");

    return StatusCode::SUCCESS;
}

bool btagAnalysisAlg::isFromWZ( const xAOD::TruthParticle* particle ) {

  if ( particle==0 ) return false;

  if ( fabs(particle -> pdgId())!= 11 && fabs(particle -> pdgId())!= 13) return false;

  const xAOD::TruthVertex* prodvtx = particle -> prodVtx();
  if ( prodvtx==0 ){ ATH_MSG_DEBUG(" isFromWZ : particle without prodvtx "); return false; }

  ATH_MSG_DEBUG(" isFromWZ : particle has prodVtx ");

  if (  prodvtx -> nIncomingParticles()==0 ) return false;

  if (  prodvtx -> nIncomingParticles()>1 ) {
    int nCharge=0;
    int nNeutral=0;

    for(unsigned j = 0; j < prodvtx -> nIncomingParticles(); j++){

      if ( fabs( prodvtx -> incomingParticle(j) -> pdgId() )==11 || fabs( prodvtx -> incomingParticle(j) -> pdgId() )==13 ) nCharge++;
      if ( fabs( prodvtx -> incomingParticle(j) -> pdgId() )==12 || fabs( prodvtx -> incomingParticle(j) -> pdgId() )==14 ) nNeutral++;

    }

    if ( nCharge>1 ) return true;
    if ( nCharge+nNeutral>1 ) return true;

    return false;
  }
  int absPDG=fabs(prodvtx -> incomingParticle(0) -> pdgId());
  if ( absPDG==15) return false;
  else if ( absPDG==24 || absPDG==23 ) return true;
  return isFromWZ( prodvtx -> incomingParticle(0) );
}
