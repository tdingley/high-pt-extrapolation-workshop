#include "btagAnalysis/CollVRTrackJetVetoAlg.h"

CollVRTrackJetVetoAlg::CollVRTrackJetVetoAlg(const std::string& name, ISvcLocator* pSvcLocator) : AthFilterAlgorithm(name, pSvcLocator)
{
    declareProperty("JetCollectionName", m_jetCollectionName = "AntiKtVR30Rmax4Rmin02TrackJets");
}

CollVRTrackJetVetoAlg::~CollVRTrackJetVetoAlg()
{

}

StatusCode CollVRTrackJetVetoAlg::initialize()
{
    ATH_MSG_INFO("intializing '" << name() << "'!");
    return StatusCode::SUCCESS;
}

StatusCode CollVRTrackJetVetoAlg::finalize()
{
    ATH_MSG_INFO("finalizing '" << name() << "'!");
    return StatusCode::SUCCESS;
}

StatusCode CollVRTrackJetVetoAlg::execute()
{
    ATH_MSG_INFO("executing '" << name() << "'!");

    const xAOD::JetContainer* jets = 0;
    CHECK( evtStore()->retrieve(jets, m_jetCollectionName) );

    // need to check the minimum distance (in the eta/phi plane) between two track jets and veto the event if any pair of jets risks to be overlapping / collinear
    for(const auto& jet_1: *jets)
    {
    	for(const auto& jet_2: *jets)
    	{
    	    if(jet_1 != jet_2)
    	    {
    		if(jet_1 -> pt() > 5000. && jet_2 -> pt() > 10000. 
    		   && getNTrk(*jet_1) > 1 && getNTrk(*jet_2) > 1)
    		{
    		    double size_1 = getJetR(jet_1 -> pt());
    		    double size_2 = getJetR(jet_2 -> pt());

    		    if(deltaR(jet_1 -> eta(), jet_2 -> eta(), jet_1 -> phi(), jet_2 -> phi()) < std::min(size_1, size_2))
    		    {			    
    			ATH_MSG_INFO("size_1 = " << size_1);
    			ATH_MSG_INFO("size_2 = " << size_2);

    			ATH_MSG_INFO("pt1 = " << jet_1 -> pt());
    			ATH_MSG_INFO("pt2 = " << jet_2 -> pt());

    			ATH_MSG_INFO("eta1 = " << jet_1 -> eta());
    			ATH_MSG_INFO("eta2 = " << jet_2 -> eta());

    			ATH_MSG_INFO("phi1 = " << jet_1 -> phi());
    			ATH_MSG_INFO("phi2 = " << jet_2 -> phi());

    			ATH_MSG_INFO("VETO APPLIED");
    			setFilterPassed(false);
    			return StatusCode::SUCCESS;		    
    		    }
    		}
    	    }
    	}
    }

    setFilterPassed(true);

    return StatusCode::SUCCESS;
}

int CollVRTrackJetVetoAlg::getNTrk(xAOD::Jet jet)
{
    typedef ElementLink<xAOD::TrackParticleContainer> TrackLink;
    typedef std::vector<TrackLink> TrackLinks;

    const xAOD::BTagging* bjet = jet.btagging();
    TrackLinks assocTracks = bjet->auxdata<TrackLinks>("BTagTrackToJetAssociator");

    return assocTracks.size();
}

double CollVRTrackJetVetoAlg::deltaR(double eta_1, double eta_2, double phi_1, double phi_2)
{
    return TMath::Sqrt(TMath::Power(eta_1 - eta_2, 2.0) + TMath::Power(phi_1 - phi_2, 2.0));
}

double CollVRTrackJetVetoAlg::getJetR(double pt)
{
    // Note: the extra factor 1000 is to convert MeV to GeV
    return std::max(0.02, std::min(0.4, 30.0 * 1000.0 / pt));
}
