
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../../btagAnalysis/btagAnalysisAlg.h"
#include "../../btagAnalysis/CollVRTrackJetVetoAlg.h"
#include "../../btagAnalysis/NHardestBJetTaggerAlg.h"
#include "../../btagAnalysis/CollNHardestVRTrackJetVetoAlg.h"
#include "../../btagAnalysis/CollNHardestVRTrackJetDecoratorAlg.h"

DECLARE_ALGORITHM_FACTORY( btagAnalysisAlg )
DECLARE_ALGORITHM_FACTORY( CollVRTrackJetVetoAlg )
DECLARE_ALGORITHM_FACTORY( NHardestBJetTaggerAlg )
DECLARE_ALGORITHM_FACTORY( CollNHardestVRTrackJetVetoAlg )
DECLARE_ALGORITHM_FACTORY( CollNHardestVRTrackJetDecoratorAlg )

DECLARE_FACTORY_ENTRIES( btagAnalysis )
{
  DECLARE_ALGORITHM( btagAnalysisAlg );
  DECLARE_ALGORITHM( CollVRTrackJetVetoAlg );
  DECLARE_ALGORITHM( NHardestBJetTaggerAlg );
  DECLARE_ALGORITHM( CollNHardestVRTrackJetVetoAlg );
  DECLARE_ALGORITHM( CollNHardestVRTrackJetDecoratorAlg );
}
