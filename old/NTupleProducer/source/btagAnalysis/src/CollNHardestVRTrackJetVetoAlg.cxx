#include "btagAnalysis/CollNHardestVRTrackJetVetoAlg.h"

CollNHardestVRTrackJetVetoAlg::CollNHardestVRTrackJetVetoAlg(const std::string& name, ISvcLocator* pSvcLocator) : AthFilterAlgorithm(name, pSvcLocator)
{
    declareProperty("JetCollectionName", m_jetCollectionName = "AntiKtVR30Rmax4Rmin02TrackJets");
}

CollNHardestVRTrackJetVetoAlg::~CollNHardestVRTrackJetVetoAlg()
{

}

StatusCode CollNHardestVRTrackJetVetoAlg::initialize()
{
    ATH_MSG_INFO("intializing '" << name() << "'!");
    return StatusCode::SUCCESS;
}

StatusCode CollNHardestVRTrackJetVetoAlg::finalize()
{
    ATH_MSG_INFO("finalizing '" << name() << "'!");
    return StatusCode::SUCCESS;
}

StatusCode CollNHardestVRTrackJetVetoAlg::execute()
{
    const xAOD::JetContainer* jets = 0;
    CHECK( evtStore()->retrieve(jets, m_jetCollectionName) );

    // loop over all selected jets and check that they do not overlap with ANY other jet (selected or non-selected)
    for(const auto& jet_1: *jets)
    {
    	for(const auto& jet_2: *jets)
	{
    	    if(jet_1 != jet_2)	
    	    {
		bool jet_1_selected = jet_1 -> auxdata<int>("selected_bjet");

		if(jet_1_selected &&
		   jet_1 -> pt() > 5000. && jet_2 -> pt() > 10000. &&
		   getNTrk(*jet_1) > 1 && getNTrk(*jet_2) > 1)
		{
		    double size_1 = getJetR(jet_1 -> pt());
		    double size_2 = getJetR(jet_2 -> pt());

		    ATH_MSG_INFO("size_1 = " << size_1);
		    ATH_MSG_INFO("size_2 = " << size_2);
			
		    ATH_MSG_INFO("pt1 = " << jet_1 -> pt());
		    ATH_MSG_INFO("pt2 = " << jet_2 -> pt());
			
		    ATH_MSG_INFO("eta1 = " << jet_1 -> eta());
		    ATH_MSG_INFO("eta2 = " << jet_2 -> eta());
			
		    ATH_MSG_INFO("phi1 = " << jet_1 -> phi());
		    ATH_MSG_INFO("phi2 = " << jet_2 -> phi());
		    
		    if(deltaR(jet_1 -> eta(), jet_2 -> eta(), jet_1 -> phi(), jet_2 -> phi()) < std::min(size_1, size_2))
		    {			    			
			ATH_MSG_INFO("VETO APPLIED");
			setFilterPassed(false);
			return StatusCode::SUCCESS;		    
		    }
		}
	    }
	}
    }

    return StatusCode::SUCCESS;
}

int CollNHardestVRTrackJetVetoAlg::getNTrk(xAOD::Jet jet)
{
    // what is required for the Collinear OR is the number of jet constituents, NOT
    // the number of tracks in the b-tagging association cone!
    return jet.numConstituents();
}

double CollNHardestVRTrackJetVetoAlg::deltaPhi(double phi_1, double phi_2)
{
    double delta_phi = std::abs(phi_1 - phi_2);
    if(delta_phi > M_PI)
    {
	delta_phi = 2 * M_PI - delta_phi;
    }
    return delta_phi;
}

double CollNHardestVRTrackJetVetoAlg::deltaR(double eta_1, double eta_2, double phi_1, double phi_2)
{
    double delta_phi = deltaPhi(phi_1, phi_2);
    ATH_MSG_INFO("delta_phi = " << delta_phi);

    return TMath::Sqrt(TMath::Power(eta_1 - eta_2, 2.0) + TMath::Power(delta_phi, 2.0));
}

double CollNHardestVRTrackJetVetoAlg::getJetR(double pt)
{
    // Note: the extra factor 1000 is to convert MeV to GeV
    return std::max(0.02, std::min(0.4, 30.0 * 1000.0 / pt));
}
