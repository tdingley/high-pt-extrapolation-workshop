#include "../btagAnalysis/JetPropertiesBranches.hh"
#include "../btagAnalysis/JetPropertiesBranchBuffer.hh"

#include "AthContainers/exceptions.h"
#include "TTree.h"

#include <iostream>
#include <algorithm>

//!-----------------------------------------------------------------------------------------------------------------------------!//
JetPropertiesBranches::JetPropertiesBranches():
  m_branches(new JetPropertiesBranchBuffer)
{
  // instantiate all the vectors here ...
  m_branches->v_jet_pt             = new std::vector<float>();
  m_branches->v_jet_eta            = new std::vector<float>();
  m_branches->v_jet_phi            = new std::vector<float>();
  m_branches->v_jet_E              = new std::vector<float>();

  m_branches->v_b_hadron_pt        = new std::vector<float>();
  m_branches->v_b_hadron_eta       = new std::vector<float>();
  m_branches->v_b_hadron_phi       = new std::vector<float>();
  m_branches->v_b_hadron_E         = new std::vector<float>();

  m_branches->v_jet_LabDr_HadF     = new std::vector<int>();
  m_branches->v_jet_DoubleHadLabel = new std::vector<int>();
  m_branches->v_jet_JVT            = new std::vector<float>();

  m_branches->v_jet_m              = new std::vector<float>();
  m_branches->v_jet_nConst         = new std::vector<float>();
  m_branches->v_jet_dRiso          = new std::vector<float>();
  m_branches->v_jet_truthMatch     = new std::vector<int>();
  m_branches->v_jet_isPU           = new std::vector<int>();
  m_branches->v_jet_aliveAfterOR   = new std::vector<int>();
  m_branches->v_jet_aliveAfterORmu = new std::vector<int>();
  m_branches->v_jet_aliveAfterCollOR = new std::vector<int>();
  m_branches->v_jet_leading_bjet   = new std::vector<int>();
  m_branches->v_jet_truthPt        = new std::vector<float>();
  
  m_branches->v_jet_nTracks = new std::vector<Int_t>();
  m_branches->v_jet_nJetConstituents = new std::vector<Int_t>();
}

//!-----------------------------------------------------------------------------------------------------------------------------!//
JetPropertiesBranches::~JetPropertiesBranches() {
  // delete all the vectors here ...
  delete m_branches->v_jet_pt            ;
  delete m_branches->v_jet_eta           ;
  delete m_branches->v_jet_phi           ;
  delete m_branches->v_jet_E             ;

  delete m_branches->v_b_hadron_pt       ;
  delete m_branches->v_b_hadron_eta      ;
  delete m_branches->v_b_hadron_phi      ;
  delete m_branches->v_b_hadron_E        ;

  delete m_branches->v_jet_LabDr_HadF    ;
  delete m_branches->v_jet_DoubleHadLabel;
  delete m_branches->v_jet_JVT           ;
  delete m_branches->v_jet_m             ;
  delete m_branches->v_jet_nConst        ;
  delete m_branches->v_jet_dRiso         ;
  delete m_branches->v_jet_truthMatch    ;
  delete m_branches->v_jet_isPU          ;
  delete m_branches->v_jet_aliveAfterOR  ;
  delete m_branches->v_jet_aliveAfterORmu;
  delete m_branches->v_jet_aliveAfterCollOR;
  delete m_branches->v_jet_leading_bjet;
  delete m_branches->v_jet_truthPt       ;
  delete m_branches->v_jet_nTracks;
  delete m_branches->v_jet_nJetConstituents;

  delete m_branches;

}

void JetPropertiesBranches::set_tree(TTree& output_tree){

  output_tree.Branch( "jet_pt" , &m_branches->v_jet_pt    );
  output_tree.Branch( "jet_eta" , &m_branches->v_jet_eta  );
  output_tree.Branch( "jet_phi" , &m_branches->v_jet_phi  );
  output_tree.Branch( "jet_E" , &m_branches->v_jet_E );

  output_tree.Branch( "b_hadron_pt" , &m_branches->v_b_hadron_pt    );
  output_tree.Branch( "b_hadron_eta" , &m_branches->v_b_hadron_eta  );
  output_tree.Branch( "b_hadron_phi" , &m_branches->v_b_hadron_phi  );
  output_tree.Branch( "b_hadron_E" , &m_branches->v_b_hadron_E );

  output_tree.Branch( "jet_LabDr_HadF" , &m_branches->v_jet_LabDr_HadF    );
  output_tree.Branch( "jet_DoubleHadLabel" , &m_branches->v_jet_DoubleHadLabel);
  output_tree.Branch( "jet_JVT" , &m_branches->v_jet_JVT     );
  output_tree.Branch( "jet_m" , &m_branches->v_jet_m );
  output_tree.Branch( "jet_nConst" , &m_branches->v_jet_nConst );
  output_tree.Branch( "jet_dRiso" , &m_branches->v_jet_dRiso );
  output_tree.Branch( "jet_truthMatch" , &m_branches->v_jet_truthMatch );
  output_tree.Branch( "jet_isPU" , &m_branches->v_jet_isPU );
  output_tree.Branch( "jet_aliveAfterOR" , &m_branches->v_jet_aliveAfterOR );
  output_tree.Branch( "jet_aliveAfterORmu" , &m_branches->v_jet_aliveAfterORmu );
  output_tree.Branch( "jet_aliveAfterCollOR", &m_branches->v_jet_aliveAfterCollOR );
  output_tree.Branch( "jet_leading_bjet", &m_branches->v_jet_leading_bjet );
  output_tree.Branch( "jet_truthPt" , &m_branches->v_jet_truthPt );

  output_tree.Branch( "jet_nTracks" , &m_branches->v_jet_nTracks );
  output_tree.Branch( "jet_nJetConstituents" , &m_branches->v_jet_nJetConstituents );

}

//!-----------------------------------------------------------------------------------------------------------------------------!//

void JetPropertiesBranches::fill(const xAOD::Jet& calib_jet, float JVT, float dRiso,
				 std::vector<TLorentzVector> truth_electrons, std::vector<TLorentzVector> truth_muons,
				 const xAOD::JetContainer *truthjets,
				 Int_t nTracks, Int_t nJetConstituents) {

    // store information on the ghost-associated truth B-hadron, if it exists
    std::vector<const xAOD::TruthParticle*> ghostB;
    calib_jet.getAssociatedObjects<xAOD::TruthParticle>("GhostBHadronsFinal", ghostB);

    if(ghostB.size() > 0) {
	
	// make sure that it is sorted with the hardest B-hadron first
	auto pT_sorter = [](const xAOD::TruthParticle* lhs, const xAOD::TruthParticle* rhs) -> bool { return (lhs -> pt()) > (rhs -> pt()); };
	std::sort(ghostB.begin(), ghostB.end(), pT_sorter);

	// store the kinematics of the hardest B-hadron
	const xAOD::TruthParticle* hardest_BHadron = ghostB[0];
	m_branches -> v_b_hadron_pt -> push_back(hardest_BHadron -> pt());
	m_branches -> v_b_hadron_eta -> push_back(hardest_BHadron -> eta());
	m_branches -> v_b_hadron_phi -> push_back(hardest_BHadron -> phi());
	m_branches -> v_b_hadron_E -> push_back(hardest_BHadron -> e());
    }
    else {
	m_branches -> v_b_hadron_pt -> push_back(-99);
	m_branches -> v_b_hadron_eta -> push_back(-99);
	m_branches -> v_b_hadron_phi -> push_back(-99);
	m_branches -> v_b_hadron_E -> push_back(-99);
    }

    m_branches->v_jet_nTracks->push_back(nTracks);
    m_branches->v_jet_nJetConstituents->push_back(nJetConstituents);
    m_branches->v_jet_pt->push_back(calib_jet.pt());
    m_branches->v_jet_eta->push_back(calib_jet.eta());
    m_branches->v_jet_phi->push_back(calib_jet.phi());
    m_branches->v_jet_E->push_back(calib_jet.e());
    
    m_branches->v_jet_m->push_back( calib_jet.m() );
    m_branches->v_jet_nConst->push_back( calib_jet.numConstituents() );
    m_branches->v_jet_dRiso->push_back( dRiso );
    
    m_branches->v_jet_JVT->push_back(JVT);

    int aliveCollLabel = 1;
    try {
	calib_jet.getAttribute("jet_aliveAfterCollOR", aliveCollLabel);
    } catch(...) {};
    m_branches->v_jet_aliveAfterCollOR->push_back(aliveCollLabel);
    
    int tmpLabel = -1;
    try {
	calib_jet.getAttribute("HadronConeExclTruthLabelID", tmpLabel);
    } catch(...) {};
    
    int tmpLabelDoubleHadron = -1;
    try {
	calib_jet.getAttribute("HadronConeExclExtendedTruthLabelID", tmpLabelDoubleHadron);
    } catch(...) {};
    
    m_branches->v_jet_LabDr_HadF->push_back(tmpLabel);
    m_branches->v_jet_DoubleHadLabel->push_back(tmpLabelDoubleHadron);

    int is_selected = 0;
    try
    {
	calib_jet.getAttribute("selected_bjet", is_selected);
    }
    catch(...) {};
    m_branches->v_jet_leading_bjet->push_back(is_selected);
    
    // matching reco jets to truth jets and recording the truth jet pT
    // picking the highest pT truth jet (with pT > 7GeV) that satisfies dR < 0.3
    // N.B. this assumes that truth jets are pT ordered
    int matchedPt = 0;
    float dRmatch = 100;
    bool truthFree = true;

    if(truthjets){
    for (const auto* tjet : *truthjets) {

      if (tjet->pt() < 10e3) continue;
      float dr = calib_jet.p4().DeltaR(tjet->p4());

        if (dr < 0.6) {
          truthFree = false;
        }

      if (dr < 0.3 && dr < dRmatch) {
              dRmatch = dr;
              matchedPt = tjet->pt();
      }
    }
    }

    if (dRmatch < 0.3) {
        m_branches->v_jet_truthMatch->push_back(1);
        m_branches->v_jet_truthPt->push_back(matchedPt);
    }
    else {
        m_branches->v_jet_truthMatch->push_back(0);
        m_branches->v_jet_truthPt->push_back(0);
    }

     m_branches->v_jet_isPU->push_back(truthFree);


    //////////////////////////////////////////////////////////////////
    // flagging jets that overlap with electron

    bool iseljetoverlap = false;
    for(unsigned int i = 0; i < truth_electrons.size(); i++) {
      float dr = calib_jet.p4().DeltaR(truth_electrons.at(i));
      if (dr < 0.3){ iseljetoverlap = true;
        break; }
    }
    m_branches->v_jet_aliveAfterOR->push_back( !iseljetoverlap );

    iseljetoverlap = false;
    for(unsigned int i= 0; i < truth_muons.size(); i++){
      float dr = calib_jet.p4().DeltaR(truth_muons.at(i));
      if(dr < 0.3){ iseljetoverlap = true;
        break;}
    }
    m_branches->v_jet_aliveAfterORmu->push_back( !iseljetoverlap );

}

//!-----------------------------------------------------------------------------------------------------------------------------!//
void JetPropertiesBranches::clear() {
  // clear vectors
  m_branches->v_jet_pt->clear();
  m_branches->v_jet_eta->clear();
  m_branches->v_jet_phi->clear();
  m_branches->v_jet_E->clear();

  m_branches->v_b_hadron_pt->clear();
  m_branches->v_b_hadron_eta->clear();
  m_branches->v_b_hadron_phi->clear();
  m_branches->v_b_hadron_E->clear();

  m_branches->v_jet_LabDr_HadF->clear();
  m_branches->v_jet_DoubleHadLabel->clear();
  m_branches->v_jet_JVT->clear();
  m_branches->v_jet_m->clear();
  m_branches->v_jet_nConst->clear();
  m_branches->v_jet_dRiso->clear();
  m_branches->v_jet_truthMatch->clear();
  m_branches->v_jet_isPU->clear();
  m_branches->v_jet_aliveAfterOR->clear();
  m_branches->v_jet_aliveAfterORmu->clear();
  m_branches->v_jet_aliveAfterCollOR->clear();
  m_branches->v_jet_leading_bjet->clear();
  m_branches->v_jet_truthPt->clear();
  m_branches->v_jet_nTracks->clear();
  m_branches->v_jet_nJetConstituents->clear();
}
