#include "btagAnalysis/NHardestBJetTaggerAlg.h"

NHardestBJetTaggerAlg::NHardestBJetTaggerAlg(const std::string& name, ISvcLocator* pSvcLocator): AthAlgorithm(name, pSvcLocator)
{
    declareProperty("JetCollectionName", m_jetCollectionName = "AntiKtVR30Rmax4Rmin02TrackJets");
    declareProperty("n", n = 2);

    ATH_MSG_INFO("setting up n-hardest b-jet selector for jet collection '" << m_jetCollectionName << "'");
}

NHardestBJetTaggerAlg::~NHardestBJetTaggerAlg()
{

}

StatusCode NHardestBJetTaggerAlg::initialize()
{
    ATH_MSG_INFO("intializing '" << name() << "'!");
    return StatusCode::SUCCESS;
}

StatusCode NHardestBJetTaggerAlg::finalize()
{
    ATH_MSG_INFO("finalizing '" << name() << "'!");
    return StatusCode::SUCCESS;
}

StatusCode NHardestBJetTaggerAlg::execute()
{
    std::vector<int> inds;
    std::vector<float> jet_pt;

    const xAOD::JetContainer* jets = 0;
    CHECK(evtStore() -> retrieve(jets, m_jetCollectionName));

    for(const auto& jet: *jets)
    {
	jet -> auxdecor<int>("selected_bjet") = false;
	int cur_flavor;
	jet -> getAttribute("HadronConeExclTruthLabelID", cur_flavor);
	ATH_MSG_INFO("have jet " << jet -> pt() << " with flavor = " << cur_flavor);
    }

    // iterate through the jet collection and flag the N hardest b-jets
    for(unsigned int jet = 0; jet < jets -> size(); jet++)
    {
	int jetFlavor;
	jets -> get(jet) -> getAttribute("HadronConeExclTruthLabelID", jetFlavor);

	if(jetFlavor == 5)
	{
	    inds.push_back(jet);
	}

	jet_pt.push_back(jets -> get(jet) -> pt());	    
    }

    // then, sort the list of indices based on the jet pt
    sort(inds.begin(), inds.end(),
	 [&jet_pt](size_t i1, size_t i2) {return jet_pt.at(i1) > jet_pt.at(i2);});

    // extract the subvector holding the indices of the N hardest b-jets
    std::vector<int> selected_jets = inds.size() >= n ? std::vector<int>(inds.begin(), inds.begin() + n) : std::vector<int>(inds);

    // iterate over the selected jets and modify the needed decorators
    for(auto selected_jet: selected_jets)
    {
	ATH_MSG_INFO("selected jet " << selected_jet);
	jets -> get(selected_jet) -> auxdecor<int>("selected_bjet") = true;
    }

    return StatusCode::SUCCESS;
}
