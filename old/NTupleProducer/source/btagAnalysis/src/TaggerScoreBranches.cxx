#include "../btagAnalysis/TaggerScoreBranches.hh"
#include "../btagAnalysis/TaggerScoreBranchBuffer.hh"

#include "xAODJet/Jet.h"
#include "AthContainers/exceptions.h"
#include "TTree.h"



//!-----------------------------------------------------------------------------------------------------------------------------!//
TaggerScoreBranches::TaggerScoreBranches():
  m_branches(new TaggerScoreBranchBuffer)
{
  // instantiate all the vectors here ...
  m_branches->v_jet_dl1_pb    = new std::vector<double>();
  m_branches->v_jet_dl1_pc    = new std::vector<double>();
  m_branches->v_jet_dl1_pu    = new std::vector<double>();
  m_branches->v_jet_dl1r_pb = new std::vector<double>();
  m_branches->v_jet_dl1r_pc = new std::vector<double>();
  m_branches->v_jet_dl1r_pu = new std::vector<double>();

  m_branches->v_jet_rnnip_pb  = new std::vector<double>();
  m_branches->v_jet_rnnip_pc  = new std::vector<double>();
  m_branches->v_jet_rnnip_pu  = new std::vector<double>();
}

//!-----------------------------------------------------------------------------------------------------------------------------!//
TaggerScoreBranches::~TaggerScoreBranches() {
  // delete all the vectors here ...
  delete m_branches->v_jet_dl1_pb;
  delete m_branches->v_jet_dl1_pc;
  delete m_branches->v_jet_dl1_pu;
  delete m_branches->v_jet_dl1r_pb;
  delete m_branches->v_jet_dl1r_pc;
  delete m_branches->v_jet_dl1r_pu;

  delete m_branches->v_jet_rnnip_pb;
  delete m_branches->v_jet_rnnip_pc;
  delete m_branches->v_jet_rnnip_pu;

  delete m_branches;
}

void TaggerScoreBranches::set_tree(TTree& output_tree) const {

  output_tree.Branch( "jet_dl1_pb", &m_branches->v_jet_dl1_pb );
  output_tree.Branch( "jet_dl1_pc", &m_branches->v_jet_dl1_pc );
  output_tree.Branch( "jet_dl1_pu", &m_branches->v_jet_dl1_pu );

  output_tree.Branch( "jet_dl1r_pb", &m_branches->v_jet_dl1r_pb );
  output_tree.Branch( "jet_dl1r_pc", &m_branches->v_jet_dl1r_pc );
  output_tree.Branch( "jet_dl1r_pu", &m_branches->v_jet_dl1r_pu );

  output_tree.Branch( "jet_rnnip_pb", &m_branches->v_jet_rnnip_pb );
  output_tree.Branch( "jet_rnnip_pc", &m_branches->v_jet_rnnip_pc );
  output_tree.Branch( "jet_rnnip_pu", &m_branches->v_jet_rnnip_pu );
}

//!-----------------------------------------------------------------------------------------------------------------------------!//
void TaggerScoreBranches::fill(const xAOD::Jet& jet) {

    const xAOD::BTagging *bjet = jet.btagging();

    // ------------------------------------------------
    // RNNIP-based high-level taggers
    // ------------------------------------------------
    // RNNIP
    try {
      m_branches->v_jet_rnnip_pb->push_back(bjet->auxdata<double>("rnnip_pb"));
      m_branches->v_jet_rnnip_pc->push_back(bjet->auxdata<double>("rnnip_pc"));
      m_branches->v_jet_rnnip_pu->push_back(bjet->auxdata<double>("rnnip_pu"));
    } catch(...) {
      m_branches->v_jet_rnnip_pb->push_back(-99);
      m_branches->v_jet_rnnip_pc->push_back(-99);
      m_branches->v_jet_rnnip_pu->push_back(-99);
    }

    // DL1r
    try {
      m_branches->v_jet_dl1r_pb->push_back(bjet->auxdata<double>("DL1r_pb"));
      m_branches->v_jet_dl1r_pc->push_back(bjet->auxdata<double>("DL1r_pc"));
      m_branches->v_jet_dl1r_pu->push_back(bjet->auxdata<double>("DL1r_pu"));
    } catch(...) {
      m_branches->v_jet_dl1r_pb->push_back(-99);
      m_branches->v_jet_dl1r_pc->push_back(-99);
      m_branches->v_jet_dl1r_pu->push_back(-99);
    }

    // ------------------------------------------------
    // high-level taggers w/o RNNIP
    // ------------------------------------------------
    // DL1
    try {
      m_branches->v_jet_dl1_pb->push_back(bjet->auxdata<double>("DL1_pb"));
      m_branches->v_jet_dl1_pc->push_back(bjet->auxdata<double>("DL1_pc"));
      m_branches->v_jet_dl1_pu->push_back(bjet->auxdata<double>("DL1_pu"));
    } catch(...) {
      m_branches->v_jet_dl1_pb->push_back(-99);
      m_branches->v_jet_dl1_pc->push_back(-99);
      m_branches->v_jet_dl1_pu->push_back(-99);
    }

}

//!-----------------------------------------------------------------------------------------------------------------------------!//
void TaggerScoreBranches::clear() {
  // clear vectors
  m_branches->v_jet_dl1_pb->clear();
  m_branches->v_jet_dl1_pc->clear();
  m_branches->v_jet_dl1_pu->clear();

  m_branches->v_jet_dl1r_pb->clear();
  m_branches->v_jet_dl1r_pc->clear();
  m_branches->v_jet_dl1r_pu->clear();

  m_branches->v_jet_rnnip_pb->clear();
  m_branches->v_jet_rnnip_pc->clear();
  m_branches->v_jet_rnnip_pu->clear();
}

