#include "btagAnalysis/JetJVTBranches.hh"
#include "btagAnalysis/JetJVTBranchBuffer.hh"

#include "TTree.h"
#include <iostream>

JetJVTBranches::JetJVTBranches():
    m_branches(new JetJVTBranchBuffer())
{
    m_branches -> v_jet_JVT_weight = new std::vector<float>();
    m_branches -> v_jet_JVT_weight_up = new std::vector<float>();
    m_branches -> v_jet_JVT_weight_down = new std::vector<float>();
    m_branches -> v_jet_aliveAfterJVT = new std::vector<int>();
}

JetJVTBranches::~JetJVTBranches()
{
    delete m_branches -> v_jet_JVT_weight;
    delete m_branches -> v_jet_JVT_weight_up;
    delete m_branches -> v_jet_JVT_weight_down;
    delete m_branches -> v_jet_aliveAfterJVT;
}

void JetJVTBranches::set_tree(TTree& output_tree)
{
    // create the necessary branches
    output_tree.Branch("jet_JVT_weight", &m_branches -> v_jet_JVT_weight);
    output_tree.Branch("jet_JVT_weight_up", &m_branches -> v_jet_JVT_weight_up);
    output_tree.Branch("jet_JVT_weight_down", &m_branches -> v_jet_JVT_weight_down);
    output_tree.Branch("jet_aliveAfterJVT", &m_branches -> v_jet_aliveAfterJVT);
}

void JetJVTBranches::fill(float jet_JVT_weight, float jet_JVT_weight_up, float jet_JVT_weight_down, int jet_aliveAfterJVT)
{
    m_branches -> v_jet_JVT_weight -> push_back(jet_JVT_weight);
    m_branches -> v_jet_JVT_weight_up -> push_back(jet_JVT_weight_up);
    m_branches -> v_jet_JVT_weight_down -> push_back(jet_JVT_weight_down);
    m_branches -> v_jet_aliveAfterJVT -> push_back(jet_aliveAfterJVT);
}

void JetJVTBranches::clear()
{
    m_branches -> v_jet_JVT_weight -> clear();
    m_branches -> v_jet_JVT_weight_up -> clear();
    m_branches -> v_jet_JVT_weight_down -> clear();
    m_branches -> v_jet_aliveAfterJVT -> clear();
}
