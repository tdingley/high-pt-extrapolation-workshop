from BTagging.BTaggingFlags import BTaggingFlags

def RetaggingConfiguration(JetCollections, TrackCollections, ToolSvc, algSeq, outputLevel,
                           AssociatedTrackCollectionName = "MatchedTracks_re", AssociatedMuonCollectionName = "MatchedMuons_re",
                           taggers = [ 'IP2D', 'IP3D', 'MultiSVbb1',  'MultiSVbb2', 'SV1', 'JetFitterNN', 'SoftMu', 'JetVertexCharge', 'DL1', 'DL1r', 'DL1rmu',  'RNNIP','DL1mu' ],
                           unique_prefix = ""
                           ):

  JetCollectionList = [ (JetCollection,
                         JetCollection.replace('ZTrack', 'Track').replace('PV0Track', 'Track'))
                        for JetCollection in JetCollections ]

  # make aliases that identify the passed JetCollections with the corresponding base collection
  for JetCollection in JetCollections:
    BTaggingFlags.CalibrationChannelAliases += [JetCollection + "->AntiKt4EMTopo"]

  BTaggingFlags.CalibrationTag = "BTagCalibRUN12-08-49" # use the calibration tag for derivation cache that includes the new training

  # make sure to explicitly use the retrained taggers
  if BTaggingFlags.Do2019Retraining:
    from BTagging.JetCollectionToTrainingMaps import preTagDL2JetToTrainingMap, postTagDL2JetToTrainingMap, blacklistedJetCollections
    for jetcoll in JetCollections:

      # make sure to schedule the new taggers for our jet collections ...
      if "AntiKtVR" in jetcoll:
        preTagDL2JetToTrainingMap[jetcoll] = preTagDL2JetToTrainingMap["AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903"]
        postTagDL2JetToTrainingMap[jetcoll] = postTagDL2JetToTrainingMap["AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903"]
      elif "AntiKt4EMPFlow" in jetcoll:
        preTagDL2JetToTrainingMap[jetcoll] = preTagDL2JetToTrainingMap["AntiKt4EMPFlow"]
        postTagDL2JetToTrainingMap[jetcoll] = postTagDL2JetToTrainingMap["AntiKt4EMPFlow"]        

      # ... and avoid scheduling the old ones!
      blacklistedJetCollections.append(jetcoll)

  BTaggingFlags.Jets = [jet_coll for jet_coll in JetCollections]

  from BTagging.BTaggingConfiguration import getConfiguration

  BTagConf = getConfiguration()
  BTagConf.doNotCheckForTaggerObstacles()

  from BTagging.BTaggingConf import Analysis__StandAloneJetBTaggerAlg as StandAloneJetBTaggerAlg

  for i, (jet, track_collection) in enumerate(zip(JetCollectionList, TrackCollections)):

    print("------------------------------------")
    print(" setting up b-retagging tool ")
    
    print("JetCollection = " + jet[0])
    print("TrackCollection = " + track_collection)

    # set up algorithms to augment the original track collections
    from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import (
      Trk__TrackToVertexIPEstimator as IPEstimator)
    ipetool = IPEstimator(name = "TrackAugmenterTool" + str(i))
    ToolSvc += ipetool

    from BTagging.BTaggingConf import Analysis__BTagTrackAugmenterAlg
    augalg = Analysis__BTagTrackAugmenterAlg(
      name = 'AODFixBTagTrackAugmenter' + str(i),
      prefix = 'btagIp_',
      TrackToVertexIPEstimator = ipetool,
      trackContainer = track_collection)

    algSeq += augalg
   
    # set up algorithms to (re)match tracks and muons to jets
    from ParticleJetTools.ParticleJetToolsConf import JetAssocConstAlg
    from ParticleJetTools.ParticleJetToolsConf import JetParticleShrinkingConeAssociation, JetParticleFixedConeAssociation

    # these associators modify the jet objects themselves
    trackAssoc = JetParticleShrinkingConeAssociation(
      name = unique_prefix + "_" + "DefaultBTaggingTrackAssoc" + str(i),
      InputParticleCollectionName=track_collection,
      OutputCollectionName=AssociatedTrackCollectionName + str(i),
      coneSizeFitPar1=+0.239,
      coneSizeFitPar2=-1.220,
      coneSizeFitPar3=-1.64e-5
      )

    ToolSvc += trackAssoc

    muonAssoc = JetParticleFixedConeAssociation(
      name = unique_prefix + "_" + "DefaultBTaggingMuonAssoc" + str(i),
      InputParticleCollectionName="Muons",
      OutputCollectionName=AssociatedMuonCollectionName + str(i),
      coneSize=0.4,
      )

    ToolSvc += muonAssoc

    assocalg = JetAssocConstAlg(
      name = unique_prefix + "_" + "BTaggingRetagAssocAlg" + str(i),
      JetCollections=[jet[0]],
      Associators=[trackAssoc, muonAssoc]
      )

    algSeq += assocalg

    # set up the tool to link tracks to the b-tagging object
    from BTagging.BTaggingConf import Analysis__BTagTrackAssociation
    assoc = Analysis__BTagTrackAssociation(
      name = unique_prefix + "_" + 'thisBTagTrackAssociation_MANUAL_TOOL'+str(i),
      AssociatedTrackLinks = AssociatedTrackCollectionName + str(i),
      AssociatedMuonLinks = AssociatedMuonCollectionName + str(i),
      TrackContainerName = track_collection,
      MuonContainerName = "Muons",
      TrackAssociationName = "BTagTrackToJetAssociator",
      MuonAssociationName = "Muons",
      OutputLevel = outputLevel
      )
        
    ToolSvc += assoc    

    # set up the b-tagging tool itself
    btagger = BTagConf.setupJetBTaggerTool(ToolSvc = ToolSvc, 
                                           JetCollection = jet[0], 
                                           TaggerList = taggers,
                                           AddToToolSvc = True,
                                           Verbose = True,
                                           options = {"name": unique_prefix + "_" + "JetBTaggerTool" + str(i),
                                                      "BTagJFVtxName": "JFVtx",
                                                      "BTagSVName"   : "SecVtx",
                                                      "BTagTrackAssocTool" : assoc,
                                                      "OutputLevel" : outputLevel
                                                      }
                                           )

    print(" JetCollectionName = " + jet[0])
    
    SAbtagger = StandAloneJetBTaggerAlg(name = unique_prefix + "_" + "StandAloneJetBTaggerAlg" + str(i),
                                        JetBTaggerTool = btagger,
                                        JetCollectionName = jet[0],
                                        OutputLevel = outputLevel,
                                        DuplicatePFlow = False # no need to tag twice in this application
                                        )
    
    # add the btagger algorithm to the sequencer; this is code that will be run upstream of the bTagAnalysis code!
    algSeq += SAbtagger
    print SAbtagger
    
    print("------------------------------------")
