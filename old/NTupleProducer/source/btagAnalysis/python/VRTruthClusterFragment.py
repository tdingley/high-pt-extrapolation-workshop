from JetRec.JetRecStandard import jtm, jetlog
from ParticleJetTools.ParticleJetToolsConf import CopyTruthJetParticles
from MCTruthClassifier.MCTruthClassifierConf import MCTruthClassifier
from JetRec.JetRecConf import PseudoJetGetter
from JetRec.JetRecConf import JetAlgorithm

def VRTruthClusteringConfiguration(OutputJetCollection, ToolSvc, algSeq, outputLevel, ChargedParticlesOnly = True,
                                   JetAlgorithmIdentifier = "AntiKt", JetRadius = 0.4, variableRMinRadius = 0.02, variableRMassScale = 30000):

    # first, prepare the collection of TruthParticles
    TruthParticleCollection = "Input" + OutputJetCollection
    JetMCTruthClassifier = MCTruthClassifier(name = OutputJetCollection + "JetMCTruthClassifier", ParticleCaloExtensionTool = "")
    ToolSvc += JetMCTruthClassifier
    
    TruthParticleCopyTool = CopyTruthJetParticles(name = OutputJetCollection + "truthpartscharged", OutputName = TruthParticleCollection,
                                                  MCTruthClassifier = JetMCTruthClassifier, ChargedParticlesOnly = ChargedParticlesOnly,
                                                  OutputLevel = outputLevel)
    ToolSvc += TruthParticleCopyTool

    # prepare the corresponding PseudoJetGetter
    jet_pseudobuilder = PseudoJetGetter(
        OutputJetCollection + "PseudoJetGetter",
        Label = "TruthCharged" if ChargedParticlesOnly else "Truth",
        InputContainer = TruthParticleCollection,
        GhostScale = 0.0,
        SkipNegativeEnergy = True
        )
    ToolSvc += jet_pseudobuilder
    
    # prepare the actual jet finding
    jtm.addJetFinder(OutputJetCollection, alg = JetAlgorithmIdentifier, radius = JetRadius, gettersin = [jet_pseudobuilder], modifiersin = [], ghostArea = 0, ptmin = 2000, ptminFilter = 40000, ivtxin = None,
                     variableRMinRadius = variableRMinRadius, variableRMassScale = variableRMassScale, calibOpt = "none")

    # need to set up a jet algorithm to execute CopyTruthJetParticles
    alg = JetAlgorithm(OutputJetCollection + "Clusterer", Tools = [TruthParticleCopyTool] + [jtm[OutputJetCollection]],
                       OutputLevel = outputLevel)
    algSeq += alg
