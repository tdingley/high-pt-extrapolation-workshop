from AthenaCommon.Constants import *

def get_jvt_efficiency_tool(CfgMgr, JetCollection, toolName):
    if "AntiKt4EMTopoJets" in JetCollection:
        config_file = "JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root"
        working_point = "Medium"
    elif "AntiKt4EMPFlowJets" in JetCollection:
        config_file = "JetJvtEfficiency/Moriond2018/JvtSFFile_EMPFlow.root"
        working_point = "Tight"
    elif "AntiKtVR30Rmax4Rmin02TrackJets" in JetCollection:
        raise Exception("Error: you would not want to use the JVT with VRTrackJets")
        
    tool = CfgMgr.CP__JetJvtEfficiency(toolName,
                                       WorkingPoint = working_point,
                                       SFFile = config_file
                                       )
    return tool
    
def get_calibration_tool(CfgMgr, JetCollection, toolName, MC_type = "MC16"):
    """
    updated on Nov 29 2018
    https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ApplyJetCalibrationR21
    """

    if "AntiKt4EMTopoJets" in JetCollection:
        collectionForTool = "AntiKt4EMTopo"
        calibfile         = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config" if MC_type == "MC16" else "JES_MC16Recommendation_AFII_EMTopo_Apr2019_Rel21.config"
        calSeq            = "JetArea_Residual_EtaJES_GSC_Smear"
        calibArea         = "00-04-82"  
    elif "AntiKt4EMPFlowJets" in JetCollection:
        collectionForTool = "AntiKt4EMPFlow"
        calibfile         = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config" if MC_type == "MC16" else "JES_MC16Recommendation_AFII_PFlow_Apr2019_Rel21.config"
        calSeq            = "JetArea_Residual_EtaJES_GSC_Smear"
        calibArea         = "00-04-82"
    elif "AntiKtVR30Rmax4Rmin02TrackJets" in JetCollection:
        raise Exception("Error: you would not want to calibrate VRTrackJets in this way!")

    tool = CfgMgr.JetCalibrationTool(
        toolName,
        IsData = False,
        ConfigFile = calibfile,
        CalibSequence = calSeq,
        JetCollection = collectionForTool,
        CalibArea = calibArea)

    return tool
