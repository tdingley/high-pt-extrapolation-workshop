from JetRec.JetRecStandard import jtm, jetlog
from InDetTrackSelectionTool.InDetTrackSelectionToolConf import InDet__InDetTrackSelectionTool
from JetRecTools.JetRecToolsConf import JetTrackSelectionTool, SimpleJetTrackSelectionTool
from TrackVertexAssociationTool.TrackVertexAssociationToolConf import CP__TightTrackVertexAssociationTool
from JetRecTools.JetRecToolsConf import TrackPseudoJetGetter, TrackVertexAssociationTool
from ParticleJetTools.ParticleJetToolsConf import ParticleJetDeltaRLabelTool

def VRReclusteringConfiguration(InputParticleCollection, OutputJetCollection, ToolSvc, algSeq, outputLevel,
                                JetAlgorithmIdentifier = "AntiKt", JetRadius = 0.4, variableRMinRadius = 0.02, variableRMassScale = 30000):
    # prepare the getters
    det_tracksel = InDet__InDetTrackSelectionTool(
      OutputJetCollection + "InDetTrackSelectionTool" + OutputJetCollection,
      CutLevel = "Loose",
      minPt    = 500
      )
    ToolSvc += det_tracksel

    jet_tracksel = JetTrackSelectionTool(
      OutputJetCollection + "JetTrackSelectionTool" + OutputJetCollection,
      InputContainer  = InputParticleCollection,
      OutputContainer = "JetSelectedTracks_" + OutputJetCollection,
      Selector        = det_tracksel
      )
    ToolSvc += jet_tracksel

    jet_simple_tracksel = SimpleJetTrackSelectionTool(
      OutputJetCollection + "SimpleJetTrackSelectionTool" + OutputJetCollection,
      PtMin = 500.0,
      InputContainer  = InputParticleCollection,
      OutputContainer = "JetSelectedGhostTracks_" + OutputJetCollection
      )
    ToolSvc += jet_simple_tracksel

    tvt = CP__TightTrackVertexAssociationTool("tvt" + OutputJetCollection, dzSinTheta_cut = 3, doPV = True)
    ToolSvc += tvt

    tv_associator = TrackVertexAssociationTool(
      OutputJetCollection + "TrackVertexAssociationTool" + OutputJetCollection,
      TrackParticleContainer = InputParticleCollection,
      TrackVertexAssociation = "JetTrackVtxAssoc" + OutputJetCollection,
      VertexContainer = "PrimaryVertices",
      TrackVertexAssoTool = tvt
      )
    ToolSvc += tv_associator

    jet_pseudobuilder = TrackPseudoJetGetter(
      OutputJetCollection + "TrackPseudoJetGetter" + OutputJetCollection,
      InputContainer = "JetSelectedTracks_" + OutputJetCollection,
      Label = "Track",
      OutputContainer = OutputJetCollection + "PseudoTrackJets",
      TrackVertexAssociation = "JetTrackVtxAssoc" + OutputJetCollection,
      SkipNegativeEnergy = True,
      GhostScale = 0.0
      )
    ToolSvc += jet_pseudobuilder

    jet_ghost_pseudobuilder = TrackPseudoJetGetter(
      OutputJetCollection + "GhostrackPseudoJetGetter" + OutputJetCollection,
      InputContainer = "JetSelectedGhostTracks_" + OutputJetCollection,
      Label = "GhostTrack",
      OutputContainer = OutputJetCollection + "PseudoJetGhostTracks",
      TrackVertexAssociation = "JetTrackVtxAssoc" + OutputJetCollection,
      SkipNegativeEnergy = True,
      GhostScale = 1e-20
      )
    ToolSvc += jet_ghost_pseudobuilder

    # ----------------------------------------------------------------------------
    # prepare the flavor labelling of the jets
    # ----------------------------------------------------------------------------
    # prepare the truth particle collections
    from ParticleJetTools.ParticleJetToolsConf import CopyFlavorLabelTruthParticles
    truthtools = []
    for ptype in ["BHadronsFinal", "CHadronsFinal", "TausFinal"]:
      tt = CopyFlavorLabelTruthParticles(OutputJetCollection + ptype,
                                         ParticleType = ptype,
                                         OutputName = "TruthLabel" + ptype + OutputJetCollection,
                                         PtMin = 5000)
      ToolSvc += tt
      truthtools.append(tt)

    # add the jet flavor decorators
    labeler = ParticleJetDeltaRLabelTool(
      OutputJetCollection + "trackjetdrlabeler" + OutputJetCollection,
      LabelName = "HadronConeExclTruthLabelID",
      DoubleLabelName = "HadronConeExclExtendedTruthLabelID",
      BLabelName = "ConeExclBHadronsFinal",
      CLabelName = "ConeExclCHadronsFinal",
      TauLabelName = "ConeExclTausFinal",
      BParticleCollection = "TruthLabelBHadronsFinal" + OutputJetCollection,
      CParticleCollection = "TruthLabelCHadronsFinal" + OutputJetCollection,
      TauParticleCollection = "TruthLabelTausFinal" + OutputJetCollection,
      PartPtMin = 5000.0,
      JetPtMin = 4500.0,
      DRMax = 0.3,
      MatchMode = "MinDR"
      )
    ToolSvc += labeler

    # ----------------------------------------------------------------------------
    # prepare the ghost association of truth B-/C-hadrons:
    # ----------------------------------------------------------------------------
    # the contents of all pseudojet getters are automatically associated onto
    # the resulting xAOD::Jet by "JetFromPseudojet", using their "Label" as the
    # name of the decoration.
    # ----------------------------------------------------------------------------
    from JetRec.JetRecConf import PseudoJetGetter
    truth_pseudobuilders = []
    for ptype in ["BHadronsFinal", "CHadronsFinal"]:
        cur_pseudobuilder = PseudoJetGetter(
            "gtruthget_" + ptype + OutputJetCollection,
            InputContainer = "TruthLabel" + ptype + OutputJetCollection,
            Label = "Ghost" + ptype,
            OutputContainer = "PseudoJetGhost" + ptype + OutputJetCollection,
            SkipNegativeEnergy = True,
            GhostScale = 1e-40
        )
        ToolSvc += cur_pseudobuilder
        truth_pseudobuilders.append(cur_pseudobuilder)

    # ----------------------------------------------------------------------------
    # set up the top-level jet algorithm
    # ----------------------------------------------------------------------------
    from JetRec.JetRecConf import JetToolRunner
    jtr = JetToolRunner(OutputJetCollection + "jetrun",
                        Tools = truthtools,
                        EventShapeTools=[]
                        )
    ToolSvc += jtr

    # re-cluster VRTrackJets, starting from the modified track collections from above
    jtm.addJetFinder(OutputJetCollection, alg = JetAlgorithmIdentifier, radius = JetRadius, gettersin = [jet_pseudobuilder, jet_ghost_pseudobuilder] + truth_pseudobuilders, modifiersin = [labeler], ghostArea = 0, ptmin = 4000, ivtxin = 0,
                     variableRMinRadius = variableRMinRadius, variableRMassScale = variableRMassScale, calibOpt = "none")

    from JetRec.JetRecConf import JetAlgorithm
    alg = JetAlgorithm(OutputJetCollection + "Clusterer", Tools = [jtr, jet_tracksel, tv_associator] + [jtm[OutputJetCollection]], OutputLevel = outputLevel)
    algSeq += alg
