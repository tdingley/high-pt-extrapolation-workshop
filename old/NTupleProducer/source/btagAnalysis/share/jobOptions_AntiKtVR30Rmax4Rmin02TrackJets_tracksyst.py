from btagAnalysis.MV2defaults import default_values
import btagAnalysis.configHelpers as configHelpers
from AthenaCommon.AthenaCommonFlags import jobproperties as jp

globalOutputLevel = VERBOSE

#---------------------------------------------------
# and output ntuple file name
# --------------------------------------------------
jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock(-1)
jp.AthenaCommonFlags.FilesInput = [input_file]

evtPrintoutInterval = vars().get('EVTPRINT', 5000)
svcMgr += CfgMgr.AthenaEventLoopMgr( EventPrintoutInterval=evtPrintoutInterval)
svcMgr += CfgMgr.THistSvc()

OutputStreamIDs = [JetCollection + "_TUNC" for JetCollection in JetCollections]

#---------------------------------------------------
# defines the output streams
# --------------------------------------------------
for OutputStreamID in OutputStreamIDs:
  svcMgr.THistSvc.Output += [OutputStreamID + " DATAFILE='flav_" + OutputStreamID + ".root' OPT='RECREATE'"]

#---------------------------------------------------
# reconstruction flags
# --------------------------------------------------
from RecExConfig.RecFlags import rec
rec.doESD.set_Value_and_Lock(False)
rec.doWriteESD.set_Value_and_Lock(False)
rec.doAOD.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock(False)
rec.doWriteTAG.set_Value_and_Lock(False)
rec.doDPD.set_Value_and_Lock(False)
rec.doTruth.set_Value_and_Lock(False)
rec.doApplyAODFix.set_Value_and_Lock(False)
include("RecExCommon/RecExCommon_topOptions.py")

from AthenaCommon.AlgSequence import AlgSequence
algSeq = AlgSequence()

truthSeq = CfgMgr.AthSequencer("truthSeq")
retagSeq = CfgMgr.AthSequencer("retagSeq")

from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackSmearingTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackTruthOriginTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackTruthFilterTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__JetTrackFilterTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackBiasingTool
from BTagTrackSystematicsAlgs.BTagTrackSystematicsAlgsConf import InDet__TrackSystematicsAlg

tracksInputKey = "InDetTrackParticles"
OutputTrackCollections = []
OutputJetCollections = []

OutputTrackCollectionsRetagging = []
OutputJetCollectionsRetagging = []

OutputTreeNames = {}
JetStreamMapping = {}
TruthCollectionMapping = {}
InputJetCollections = {}

# set up algorithms to augment the original track collections
from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import (
  Trk__TrackToVertexIPEstimator as IPEstimator)
ipetool = IPEstimator(name = "TrackAugmenterTool_InDetTrackParticles")
ToolSvc += ipetool

# ---------------------------------------
# loop over jet collections and systematic variations
# ---------------------------------------
for JetCollection, TruthJetCollection, TIDEJetCollection, JetDefinition, OutputStreamID in zip(JetCollections, TruthJetCollections, TIDEJetCollections, JetDefinitions, OutputStreamIDs):  

  # ---------------------------------------
  # prepare the TruthJet collection for VRTrackJets
  # ---------------------------------------
  from btagAnalysis.VRTruthClusterFragment import VRTruthClusteringConfiguration
  VRTruthClusteringConfiguration(OutputJetCollection = TruthJetCollection, ToolSvc = ToolSvc, 
                                 algSeq = truthSeq, outputLevel = globalOutputLevel)
  algSeq += truthSeq

  for ind, (systName, systVariation) in enumerate(systVariations.items()):

    selector = CfgMgr.NHardestBJetTaggerAlg(name = "BJetSelector" + JetCollection + str(ind),
                                            JetCollectionName = JetCollection,
                                            n = 2)
    retagSeq += selector

    # add a decoration to veto collinear VR track jets
    deco = CfgMgr.CollNHardestVRTrackJetDecoratorAlg(name = "CollNHardestVRTrackJetDecorator" + JetCollection + str(ind),
                                                     JetCollectionName = JetCollection)
    retagSeq += deco

    OutputJetCollection = JetCollection + str(ind)
    tracksOutputKey = tracksInputKey + "_" + JetCollection + "".join(systName)

    OutputJetCollections.append(OutputJetCollection)
    OutputTreeNames[OutputJetCollection] = JetCollection + "|" + systName
    InputJetCollections[OutputJetCollection] = JetCollection

    JetStreamMapping[OutputJetCollection] = OutputStreamID
    TruthCollectionMapping[OutputJetCollection] = TruthJetCollection
    OutputTrackCollections.append(tracksOutputKey)

    OutputTrackCollectionsRetagging.append(tracksOutputKey)
    OutputJetCollectionsRetagging.append(OutputJetCollection)

    # ---------------------------------------
    # prepare the modified track collections
    # ---------------------------------------
    trackSmearingTool = InDet__InDetTrackSmearingTool("TrackSmearingTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    trackSmearingTool.Seed = 12345
    trackSmearingTool.useDijetMaps = True
    trackSmearingTool.runNumber = runNumber
    ToolSvc += trackSmearingTool  
    
    trackOriginTool = InDet__InDetTrackTruthOriginTool("TrackTruthOriginTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    ToolSvc += trackOriginTool  
    
    trackFilterTool = InDet__InDetTrackTruthFilterTool("InDetTrackTruthFilterTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    trackFilterTool.Seed = 1234
    trackFilterTool.trackOriginTool = trackOriginTool
    ToolSvc += trackFilterTool
    
    jetTrackFilterTool = InDet__JetTrackFilterTool("JetTrackFilterTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    jetTrackFilterTool.Seed = 4321
    ToolSvc += jetTrackFilterTool  
    
    trackBiasingTool = InDet__InDetTrackBiasingTool("InDetTrackBiasingTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    trackBiasingTool.isSimulation = True
    trackBiasingTool.runNumber = runNumber
    ToolSvc += trackBiasingTool
    
    # the algorithm that actually applies the tools to the events
    alg = InDet__TrackSystematicsAlg("TrackSystematicsAlg_" + JetCollection + "".join(systName) + str(ind))
    alg.OutputLevel = globalOutputLevel
    alg.trackFilterTool = trackFilterTool
    alg.jetTrackFilterTool = jetTrackFilterTool
    alg.trackSmearingTool = trackSmearingTool
    alg.trackBiasingTool = trackBiasingTool

    print("adding " + str(systVariation))
    
    alg.systematicVariations = systVariation
    
    alg.trackCollectionName = tracksInputKey
    alg.outputTrackCollectionName = tracksOutputKey
    alg.jetCollectionName = TIDEJetCollection
    
    retagSeq += alg

    # ---------------------------------------
    # prepare the jet collections to which the track collections will be matched
    # ---------------------------------------
      
    # there is NO need to calibrate the track jets, either before or after the retagging!
    alg = CfgMgr.JetCalibrationAlgs(OutputLevel = globalOutputLevel,
                                    name = OutputJetCollection,
                                    JetCollectionName = JetCollection,
                                    OutputJetCollectionName = OutputJetCollection,
                                    JetCalibrationTool = None,
                                    calibrateJets = False
                                    )
    
    retagSeq += alg

# -----------------------------------------------------
# set up the retagging of all defined JetCollections, 
# taking into account the modified tracks
# -----------------------------------------------------
print(" arguments being passed to retagger: ")
print(" JetCollections = " + str(OutputJetCollections))
print(" TrackCollections = " + str(OutputTrackCollections))

from btagAnalysis.RetagFragment import RetaggingConfiguration
RetaggingConfiguration(OutputJetCollections, OutputTrackCollections, ToolSvc, retagSeq, globalOutputLevel)

# -----------------------------------------------------
# set up the retagging of all defined JetCollections, 
# taking into account the modified tracks
# -----------------------------------------------------
jvt = CfgMgr.JetVertexTaggerTool('JVT')
ToolSvc += jvt

prw = CfgMgr.CP__PileupReweightingTool("prw",
                                       OutputLevel = globalOutputLevel,
                                       ConfigFiles = config_files,
                                       LumiCalcFiles = lumicalc_files
                                       )
ToolSvc += prw

from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import Trk__TrackToVertexIPEstimator
ToolSvc+=Trk__TrackToVertexIPEstimator("trkIPEstimator")

# -------------------------------------------------------
# this does some preprocessing on the b tagging vertices
# -------------------------------------------------------
retagSeq += CfgMgr.BTagVertexAugmenter()

# -------------------------------------------------------
# set up the dumper algorithms that save the NTuples
# -------------------------------------------------------
trees_per_stream = {}
for JetCollection, OutputStreamID in JetStreamMapping.items():
  print("setting up dumper for pairing " + JetCollection + " -> " + OutputStreamID)

  if not OutputStreamID in trees_per_stream:
    trees_per_stream[OutputStreamID] = 0

  alg = CfgMgr.btagAnalysisAlg(name = "BTagDumpAlg_" + JetCollection,
                               OutputLevel = globalOutputLevel,
                               Stream = OutputStreamID,
                               JVTtool = None,
                               JetCalibrationTool = None,
                               MCWeightNames = stored_MC_weights if trees_per_stream[OutputStreamID] == 0 else [],
                               MCWeightTreeNamePrefix = InputJetCollections[JetCollection] + "|MCWeight_",
                               StorePUWeights = True if trees_per_stream[OutputStreamID] == 0 else False,
                               PRWTool = prw,
                               PUWeightTreeNamePrefix = InputJetCollections[JetCollection] + "|PUWeight",
                               JVTEfficiencyTool = None,
                               JetCollectionName = JetCollection,
                               TruthJetCollectionName = TruthCollectionMapping[JetCollection],
                               OutputTreeName = OutputTreeNames[JetCollection],
                               CalibrateJets = False
                               )

  trees_per_stream[OutputStreamID] += 1
  
  retagSeq += alg

algSeq += retagSeq

# -------------------------------------------------------
# setup the performance monitoring for this job
# -------------------------------------------------------
from PerfMonComps.PerfMonFlags import jobproperties as PerfMon_jp
PerfMon_jp.PerfMonFlags.doMonitoring = False
PerfMon_jp.PerfMonFlags.doFastMon = False
