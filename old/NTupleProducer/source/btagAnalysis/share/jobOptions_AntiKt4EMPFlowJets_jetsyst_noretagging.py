unique_prefix = "noretagging"

# -------------------------------------------------------------------
# this implements the following prescription for handling jet-induced systematic uncertainties:
#  * first, apply the b-tagging to raw, unmodified jets
#  * then, calibrate them and apply the systematic variation
# -------------------------------------------------------------------
from btagAnalysis.MV2defaults import default_values
import btagAnalysis.configHelpers as configHelpers
from AthenaCommon.AthenaCommonFlags import jobproperties as jp

from AsgAnalysisAlgorithms.OverlapAnalysisSequence import makeOverlapAnalysisSequence

globalOutputLevel = INFO

#---------------------------------------------------
# setup output ntuple file name
# --------------------------------------------------
jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock(-1)
jp.AthenaCommonFlags.FilesInput = [input_file]

evtPrintoutInterval = vars().get('EVTPRINT', 5000)
svcMgr += CfgMgr.AthenaEventLoopMgr( EventPrintoutInterval=evtPrintoutInterval)
svcMgr += CfgMgr.THistSvc()

OutputStreamIDs = [JetCollection + "_JUNC_" + unique_prefix for JetCollection in JetCollections]
ORJetCollections = [JetCollection + "_OR" for JetCollection in JetCollections]

#---------------------------------------------------
# defines the output streams
# --------------------------------------------------
for OutputStreamID in OutputStreamIDs:
  try:
    svcMgr.THistSvc.Output += [OutputStreamID + " DATAFILE='" + output_path + "' OPT='RECREATE'"]
  except:
    svcMgr.THistSvc.Output += [OutputStreamID + " DATAFILE='flav_" + OutputStreamID + ".root' OPT='RECREATE'"]

#---------------------------------------------------
# reconstruction flags
# --------------------------------------------------
from RecExConfig.RecFlags import rec
rec.doESD.set_Value_and_Lock(False)
rec.doWriteESD.set_Value_and_Lock(False)
rec.doAOD.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock(False)
rec.doWriteTAG.set_Value_and_Lock(False)
rec.doDPD.set_Value_and_Lock(False)
rec.doTruth.set_Value_and_Lock(False)
rec.doApplyAODFix.set_Value_and_Lock(False)
include("RecExCommon/RecExCommon_topOptions.py")

from AthenaCommon.AlgSequence import AlgSequence
algSeq = AlgSequence()

retagSeq = CfgMgr.AthSequencer("retagSeq")

from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackSmearingTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackTruthOriginTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackTruthFilterTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__JetTrackFilterTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackBiasingTool
from BTagTrackSystematicsAlgs.BTagTrackSystematicsAlgsConf import InDet__TrackSystematicsAlg

OutputTrackCollections = []
PreparedJetCollections = []

#---------------------------------------------------
# apply some baseline track smearing, if this is desired
# (to correct the data/MC differences)
#---------------------------------------------------
for JetCollection in JetCollections:
  trackSmearingTool = InDet__InDetTrackSmearingTool(unique_prefix + "_" + "TrackSmearingTool" + JetCollection, OutputLevel = globalOutputLevel)
  trackSmearingTool.Seed = 12345
  trackSmearingTool.useDijetMaps = True
  trackSmearingTool.runNumber = runNumber
  ToolSvc += trackSmearingTool  
    
  trackOriginTool = InDet__InDetTrackTruthOriginTool(unique_prefix + "_" + "TrackTruthOriginTool" + JetCollection, OutputLevel = globalOutputLevel)
  ToolSvc += trackOriginTool  
    
  trackFilterTool = InDet__InDetTrackTruthFilterTool(unique_prefix + "_" + "InDetTrackTruthFilterTool" + JetCollection, OutputLevel = globalOutputLevel)
  trackFilterTool.Seed = 1234
  trackFilterTool.trackOriginTool = trackOriginTool
  ToolSvc += trackFilterTool
    
  jetTrackFilterTool = InDet__JetTrackFilterTool(unique_prefix + "_" + "JetTrackFilterTool" + JetCollection, OutputLevel = globalOutputLevel)
  jetTrackFilterTool.Seed = 4321
  ToolSvc += jetTrackFilterTool  
    
  trackBiasingTool = InDet__InDetTrackBiasingTool(unique_prefix + "_" + "InDetTrackBiasingTool" + JetCollection, OutputLevel = globalOutputLevel)
  trackBiasingTool.runNumber = runNumber
  ToolSvc += trackBiasingTool
    
  # the algorithm that actually applies the tools to the events
  alg = InDet__TrackSystematicsAlg(unique_prefix + "_" + "TrackSystematicsAlg" + JetCollection)
  alg.OutputLevel = globalOutputLevel
  alg.trackFilterTool = trackFilterTool
  alg.jetTrackFilterTool = jetTrackFilterTool
  alg.trackSmearingTool = trackSmearingTool
  alg.trackBiasingTool = trackBiasingTool
  
  alg.systematicVariations = tracks_syst_baseline

  CurOutputTrackCollectionName = unique_prefix + "_" + "InDetTrackParticles_" + JetCollection + "_prepared"
  OutputTrackCollections.append(CurOutputTrackCollectionName)
    
  alg.trackCollectionName = "InDetTrackParticles"
  alg.outputTrackCollectionName = CurOutputTrackCollectionName
  alg.jetCollectionName = JetCollection
  
  retagSeq += alg

  # ---------------------------------------
  # since we recompute the tagger values on top of the original jet collections (just to be safe),
  # we need to duplicate the jet collection here to avoid conflicting with the original BTagging
  # containers
  # ---------------------------------------
  # prepare the tool to handle the jet calibration
  PreparedJetCollection = JetCollection + "_prepared"      
  alg = CfgMgr.JetCalibrationAlgs(OutputLevel = globalOutputLevel,
                                  name = PreparedJetCollection,
                                  JetCollectionName = JetCollection,
                                  OutputJetCollectionName = PreparedJetCollection,                                    
                                  JetCalibrationTool = None,
                                  calibrateJets = False
                                  )
  retagSeq += alg

  PreparedJetCollections.append(PreparedJetCollection)


# -----------------------------------------------------
# set up the retagging of all defined JetCollections, 
# taking into account the modified tracks
# -----------------------------------------------------
print(" arguments being passed to retagger: ")
print(" JetCollections = " + str(JetCollections))
print(" TrackCollections = " + str(OutputTrackCollections))

from btagAnalysis.RetagFragment import RetaggingConfiguration
RetaggingConfiguration(PreparedJetCollections, OutputTrackCollections, ToolSvc, retagSeq, globalOutputLevel, unique_prefix = unique_prefix)

OutputJetCollections = []
InputJetCollections = {}
OutputTreeNames = {}
JetStreamMapping = {}
TruthCollectionMapping = {}

# set up algorithms to augment the original track collections
from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import (
  Trk__TrackToVertexIPEstimator as IPEstimator)
ipetool = IPEstimator(name = "TrackAugmenterTool" + str(i))
ToolSvc += ipetool

from BTagging.BTaggingConf import Analysis__BTagTrackAugmenterAlg
augalg = Analysis__BTagTrackAugmenterAlg(
  name = 'AODFixBTagTrackAugmenter' + str(i),
  prefix = 'btagIp_',
  TrackToVertexIPEstimator = ipetool,
  trackContainer = "InDetTrackParticles")

algSeq += augalg

from AnaAlgorithm.DualUseConfig import createAlgorithm
algSeq += createAlgorithm( 'CP::SysListLoaderAlg', 'SysLoaderAlg' )
algSeq.SysLoaderAlg.sigmaRecommended = 1

# -----------------------------------------------------
# set up the systematic variations that are to be applied to each jet collection
# -----------------------------------------------------
for JetCollection, OutputJetCollectionName, TruthJetCollection, JetDefinition, OutputStreamID in zip(PreparedJetCollections, JetCollections, TruthJetCollections, JetDefinitions, OutputStreamIDs):

  for ind, (systName, systVariation) in enumerate(systVariations.items()):

    OutputJetCollection = unique_prefix + "_" + JetCollection + str(ind)
    OutputJetCollections.append(OutputJetCollection)

    JetStreamMapping[OutputJetCollection] = OutputStreamID
    TruthCollectionMapping[OutputJetCollection] = TruthJetCollection
    OutputTreeNames[OutputJetCollection] = OutputJetCollectionName + "|" + systName
    InputJetCollections[OutputJetCollection] = JetCollection

    print("currently setting up " + systName)
    print("creating jet collection " + OutputJetCollection)

    # prepare the tool to apply jet uncertainties
    uncName = "JESProv_" + JetCollection + "_" + str(ind);
    print("setting up JetUncertaintyTool with name = " + uncName)
    unc = CfgMgr.JetUncertaintiesTool(unique_prefix + "_" + uncName,
                                      JetDefinition = JetDefinition,
                                      MCType = MC_type,
                                      ConfigFile = "rel21/Summer2019/R4_CategoryReduction_SimpleJER.config",
                                      IsData = False
                                      )
    ToolSvc += unc

    # prepare the tool to handle the jet calibration
    calibName = unique_prefix + "_" + "JETCalib_" + JetCollection + "_" + str(ind);
    print("setting up JetCalibration tool with name = " + calibName)

    calib = configHelpers.get_calibration_tool(CfgMgr, JetCollection, calibName, MC_type = MC_type)
    ToolSvc += calib
    
    # algorithm to apply the jet-related systematic variations and calibrate the jets
    alg = CfgMgr.JetSystematicsAlgs(OutputLevel = globalOutputLevel,
                                    name = unique_prefix + "_" + OutputJetCollection,
                                    JetCollectionName = JetCollection,
                                    OutputJetCollectionName = OutputJetCollection,
                                    systematicVariations = systVariation,
                                    JetUncertaintyTool = unc,
                                    JetCalibrationTool = calib,
                                    calibrateJets = True
                                    )

    retagSeq += alg

# -----------------------------------------------------
# prepare for the pileup reweighting
# -----------------------------------------------------
jvt = CfgMgr.JetVertexTaggerTool(unique_prefix + "_" + 'JVT')
ToolSvc += jvt

prw = CfgMgr.CP__PileupReweightingTool(unique_prefix + "_" + "prw",
                                            OutputLevel = globalOutputLevel,
                                            ConfigFiles = config_files,
                                            LumiCalcFiles = lumicalc_files
                                            )
ToolSvc += prw

from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import Trk__TrackToVertexIPEstimator
ToolSvc+=Trk__TrackToVertexIPEstimator(unique_prefix + "_" + "trkIPEstimator")

# -------------------------------------------------------
# this does some preprocessing on the b tagging vertices
# -------------------------------------------------------
retagSeq += CfgMgr.BTagVertexAugmenter()

# -------------------------------------------------------
# set up the dumper algorithms that save the NTuples
# -------------------------------------------------------
trees_per_stream = {}
for JetCollection, OutputStreamID  in JetStreamMapping.items():

  # set up the OR for the (re-)tagged PFlow jets
  # perform PF OR removal
  from AsgAnalysisAlgorithms.OverlapAnalysisSequence import makeOverlapAnalysisSequence
  ORJetCollection = JetCollection + "OR"
  overlapSequence = makeOverlapAnalysisSequence(data_type,
                                                doMuPFJetOR = True, doMuons = False, doElectrons = False, doJets = True, doTaus = False, doPhotons = False, doFatJets = False, postfix = JetCollection)
  overlapSequence.configure(
    inputName = {'muons': 'Muons', 'jets': JetCollection},
    outputName = {'muons': 'MuonsOR' + JetCollection, 'jets': ORJetCollection},
    affectingSystematics = {'jets': ''}) # note: do not ask the tool to run over all systematics, is handled manually!!
  retagSeq += overlapSequence

  selector = CfgMgr.NHardestBJetTaggerAlg(name = "BJetSelector" + ORJetCollection,
                                          JetCollectionName = ORJetCollection,
                                          n = 2)
  retagSeq += selector

  print("setting up dumper for pairing " + ORJetCollection + " -> " + OutputStreamID)

  if not OutputStreamID in trees_per_stream:
    trees_per_stream[OutputStreamID] = 0

  calibName = unique_prefix + "_" + "BTagDumpAlgCalibTool_" + ORJetCollection
  calib = configHelpers.get_calibration_tool(CfgMgr, ORJetCollection, calibName)
  ToolSvc += calib

  # -----------------------------------------------------
  # for computing JVT efficiency scale factors
  # -----------------------------------------------------
  toolName = "JetJvtEfficiencyTool_" + ORJetCollection
  jvt_efficiency_tool = configHelpers.get_jvt_efficiency_tool(CfgMgr, ORJetCollection, toolName)
  ToolSvc += jvt_efficiency_tool

  alg = CfgMgr.btagAnalysisAlg(name = unique_prefix + "_" + "BTagDumpAlg_" + ORJetCollection,
                               OutputLevel = globalOutputLevel,
                               Stream = OutputStreamID,
                               JVTtool = jvt,
                               JetCalibrationTool = calib,
                               MCWeightNames = stored_MC_weights if trees_per_stream[OutputStreamID] == 0 else [],
                               MCWeightTreeNamePrefix = InputJetCollections[JetCollection] + "|MCWeight_",
                               StorePUWeights = True if trees_per_stream[OutputStreamID] == 0 else False,
                               PRWTool = prw,
                               PUWeightTreeNamePrefix = InputJetCollections[JetCollection] + "|PUWeight",
                               JVTEfficiencyTool = jvt_efficiency_tool,
                               JetCollectionName = ORJetCollection,
                               TruthJetCollectionName = TruthCollectionMapping[JetCollection],
                               OutputTreeName = OutputTreeNames[JetCollection],
                               CalibrateJets = False
                               )

  trees_per_stream[OutputStreamID] += 1
        
  retagSeq += alg

algSeq += retagSeq

# -------------------------------------------------------
# setup the performance monitoring for this job
# -------------------------------------------------------
from PerfMonComps.PerfMonFlags import jobproperties as PerfMon_jp
PerfMon_jp.PerfMonFlags.doMonitoring = False
PerfMon_jp.PerfMonFlags.doFastMon = False
