unique_prefix = "retagging"

# -------------------------------------------------------------------
# this implements the following prescription for handling jet-induced systematic uncertainties:
#  * first, calibrate the jets and apply the systematic variation(s)
#  * then, apply the b-tagging to the calibrated and / or varied jets
# -------------------------------------------------------------------
from btagAnalysis.MV2defaults import default_values
import btagAnalysis.configHelpers as configHelpers
from AthenaCommon.AthenaCommonFlags import jobproperties as jp

from AsgAnalysisAlgorithms.OverlapAnalysisSequence import makeOverlapAnalysisSequence

globalOutputLevel = INFO

#---------------------------------------------------
# setup output ntuple file name
# --------------------------------------------------
jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock(-1)
jp.AthenaCommonFlags.FilesInput = [input_file]

evtPrintoutInterval = vars().get('EVTPRINT', 5000)
svcMgr += CfgMgr.AthenaEventLoopMgr( EventPrintoutInterval=evtPrintoutInterval)
svcMgr += CfgMgr.THistSvc()

OutputStreamIDs = [JetCollection + "_JUNC_" + unique_prefix for JetCollection in JetCollections]
ORJetCollections = [JetCollection + "_OR" for JetCollection in JetCollections]

#---------------------------------------------------
# defines the output streams
# --------------------------------------------------
for OutputStreamID in OutputStreamIDs:
  try:
    svcMgr.THistSvc.Output += [OutputStreamID + " DATAFILE='" + output_path + "' OPT='RECREATE'"]
  except:
    svcMgr.THistSvc.Output += [OutputStreamID + " DATAFILE='flav_" + OutputStreamID + ".root' OPT='RECREATE'"]

#---------------------------------------------------
# reconstruction flags
# --------------------------------------------------
from RecExConfig.RecFlags import rec
rec.doESD.set_Value_and_Lock(False)
rec.doWriteESD.set_Value_and_Lock(False)
rec.doAOD.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock(False)
rec.doWriteTAG.set_Value_and_Lock(False)
rec.doDPD.set_Value_and_Lock(False)
rec.doTruth.set_Value_and_Lock(False)
rec.doApplyAODFix.set_Value_and_Lock(False)
include("RecExCommon/RecExCommon_topOptions.py")

from AthenaCommon.AlgSequence import AlgSequence
algSeq = AlgSequence()

retagSeq = CfgMgr.AthSequencer("retagSeq")

from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackSmearingTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackTruthOriginTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackTruthFilterTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__JetTrackFilterTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackBiasingTool
from BTagTrackSystematicsAlgs.BTagTrackSystematicsAlgsConf import InDet__TrackSystematicsAlg

OutputTrackCollections = []

# set up algorithms to augment the original track collections
from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import (
  Trk__TrackToVertexIPEstimator as IPEstimator)
ipetool = IPEstimator(name = "TrackAugmenterTool" + str(i))
ToolSvc += ipetool

#---------------------------------------------------
# apply some baseline track smearing, if this is desired
# (to correct the data/MC differences)
#---------------------------------------------------
for JetCollection in JetCollections:

  # need to prepare a separate track collection for every systematic variation applied to jets
  for ind, (systName, systVariation) in enumerate(systVariations.items()):
    CurOutputTrackCollectionName = unique_prefix + "_" + "InDetTrackParticles_" + JetCollection + str(ind)
    OutputTrackCollections.append(CurOutputTrackCollectionName)

    trackSmearingTool = InDet__InDetTrackSmearingTool(unique_prefix + "_" + "TrackSmearingTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    trackSmearingTool.Seed = 12345
    trackSmearingTool.useDijetMaps = True
    trackSmearingTool.runNumber = runNumber
    ToolSvc += trackSmearingTool  
  
    trackOriginTool = InDet__InDetTrackTruthOriginTool(unique_prefix + "_" + "TrackTruthOriginTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    ToolSvc += trackOriginTool  
  
    trackFilterTool = InDet__InDetTrackTruthFilterTool(unique_prefix + "_" + "InDetTrackTruthFilterTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    trackFilterTool.Seed = 1234
    trackFilterTool.trackOriginTool = trackOriginTool
    ToolSvc += trackFilterTool
    
    jetTrackFilterTool = InDet__JetTrackFilterTool(unique_prefix + "_" + "JetTrackFilterTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    jetTrackFilterTool.Seed = 4321
    ToolSvc += jetTrackFilterTool  
    
    trackBiasingTool = InDet__InDetTrackBiasingTool(unique_prefix + "_" + "InDetTrackBiasingTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    trackBiasingTool.runNumber = runNumber
    ToolSvc += trackBiasingTool
    
    # the algorithm that actually applies the tools to the events
    alg = InDet__TrackSystematicsAlg(unique_prefix + "_" + "TrackSystematicsAlg" + JetCollection + str(ind))
    alg.OutputLevel = globalOutputLevel
    alg.trackFilterTool = trackFilterTool
    alg.jetTrackFilterTool = jetTrackFilterTool
    alg.trackSmearingTool = trackSmearingTool
    alg.trackBiasingTool = trackBiasingTool
    
    alg.systematicVariations = tracks_syst_baseline
    
    alg.trackCollectionName = "InDetTrackParticles"
    alg.outputTrackCollectionName = CurOutputTrackCollectionName
    alg.jetCollectionName = JetCollection
    
    retagSeq += alg

OutputJetCollections = []
InputJetCollections = {}
OutputTreeNames = {}
JetStreamMapping = {}
TruthCollectionMapping = {}

from AnaAlgorithm.DualUseConfig import createAlgorithm
algSeq += createAlgorithm( 'CP::SysListLoaderAlg', 'SysLoaderAlg' )
algSeq.SysLoaderAlg.sigmaRecommended = 1

# -----------------------------------------------------
# set up the systematic variations that are to be applied to each jet collection
# -----------------------------------------------------
for JetCollection, TruthJetCollection, JetDefinition, OutputStreamID in zip(JetCollections, TruthJetCollections, JetDefinitions, OutputStreamIDs):

  for ind, (systName, systVariation) in enumerate(systVariations.items()):

    OutputJetCollection = unique_prefix + "_" + JetCollection + str(ind)
    OutputJetCollections.append(OutputJetCollection)

    JetStreamMapping[OutputJetCollection] = OutputStreamID
    TruthCollectionMapping[OutputJetCollection] = TruthJetCollection
    OutputTreeNames[OutputJetCollection] = JetCollection + "|" + systName
    InputJetCollections[OutputJetCollection] = JetCollection

    print("currently setting up " + systName)
    print("creating jet collection " + OutputJetCollection)

    # prepare the tool to apply jet uncertainties
    uncName = "JESProv_" + JetCollection + "_" + str(ind);
    print("setting up JetUncertaintyTool with name = " + uncName)
    unc = CfgMgr.JetUncertaintiesTool(unique_prefix + "_" + uncName,
                                      JetDefinition = JetDefinition,
                                      MCType = MC_type,
                                      ConfigFile = "rel21/Summer2019/R4_CategoryReduction_SimpleJER.config",
                                      IsData = False
                                      )
    ToolSvc += unc

    # prepare the tool to handle the jet calibration
    calibName = "JETCalib_" + JetCollection + "_" + str(ind);
    print("setting up JetCalibration tool with name = " + calibName)

    calib = configHelpers.get_calibration_tool(CfgMgr, JetCollection, calibName, MC_type = MC_type)
    ToolSvc += calib
    
    # algorithm to apply the jet-related systematic variations and calibrate the jets
    alg = CfgMgr.JetSystematicsAlgs(OutputLevel = globalOutputLevel,
                                    name = OutputJetCollection,
                                    JetCollectionName = JetCollection,
                                    OutputJetCollectionName = OutputJetCollection,
                                    systematicVariations = systVariation,
                                    JetUncertaintyTool = unc,
                                    JetCalibrationTool = calib,
                                    calibrateJets = True
                                    )

    retagSeq += alg

# -----------------------------------------------------
# set up the retagging of all defined JetCollections, 
# taking into account the modified tracks
# -----------------------------------------------------
print(" arguments being passed to retagger: ")
print(" JetCollections = " + str(OutputJetCollections))
print(" TrackCollections = " + str(OutputTrackCollections))

from btagAnalysis.RetagFragment import RetaggingConfiguration
RetaggingConfiguration(OutputJetCollections, OutputTrackCollections, ToolSvc, retagSeq, globalOutputLevel, unique_prefix = unique_prefix)

# -----------------------------------------------------
# prepare for the pileup reweighting
# -----------------------------------------------------
jvt = CfgMgr.JetVertexTaggerTool(unique_prefix + "_" + 'JVT')
ToolSvc += jvt

prw = CfgMgr.CP__PileupReweightingTool(unique_prefix + "_" + "prw",
                                            OutputLevel = globalOutputLevel,
                                            ConfigFiles = config_files,
                                            LumiCalcFiles = lumicalc_files
                                            )
ToolSvc += prw

from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import Trk__TrackToVertexIPEstimator
ToolSvc+=Trk__TrackToVertexIPEstimator(unique_prefix + "_" + "trkIPEstimator")

# -------------------------------------------------------
# this does some preprocessing on the b tagging vertices
# -------------------------------------------------------
retagSeq += CfgMgr.BTagVertexAugmenter()

# -------------------------------------------------------
# set up the dumper algorithms that save the NTuples
# -------------------------------------------------------
trees_per_stream = {}
for JetCollection, OutputStreamID  in JetStreamMapping.items():

  # set up the OR for the (re-)tagged PFlow jets
  # perform PF OR removal
  from AsgAnalysisAlgorithms.OverlapAnalysisSequence import makeOverlapAnalysisSequence
  ORJetCollection = JetCollection + "OR"
  overlapSequence = makeOverlapAnalysisSequence(data_type,
                                                doMuPFJetOR = True, doMuons = False, doElectrons = False, doJets = True, doTaus = False, doPhotons = False, doFatJets = False, postfix = JetCollection)
  overlapSequence.configure(
    inputName = {'muons': 'Muons', 'jets': JetCollection},
    outputName = {'muons': 'MuonsOR' + JetCollection, 'jets': ORJetCollection},
    affectingSystematics = {'jets': ''}) # note: do not ask the tool to run over all systematics, is handled manually!!
  retagSeq += overlapSequence

  selector = CfgMgr.NHardestBJetTaggerAlg(name = "BJetSelector" + ORJetCollection,
                                          JetCollectionName = ORJetCollection,
                                          n = 2)
  retagSeq += selector

  print("setting up dumper for pairing " + ORJetCollection + " -> " + OutputStreamID)

  if not OutputStreamID in trees_per_stream:
    trees_per_stream[OutputStreamID] = 0

  calibName = unique_prefix + "_" + "BTagDumpAlgCalibTool_" + ORJetCollection
  calib = configHelpers.get_calibration_tool(CfgMgr, ORJetCollection, calibName)
  ToolSvc += calib

  # -----------------------------------------------------
  # for computing JVT efficiency scale factors
  # -----------------------------------------------------
  toolName = "JetJvtEfficiencyTool_" + ORJetCollection
  jvt_efficiency_tool = configHelpers.get_jvt_efficiency_tool(CfgMgr, ORJetCollection, toolName)
  ToolSvc += jvt_efficiency_tool

  alg = CfgMgr.btagAnalysisAlg(name = unique_prefix + "_" + "BTagDumpAlg_" + ORJetCollection,
                               OutputLevel = globalOutputLevel,
                               Stream = OutputStreamID,
                               JVTtool = jvt,
                               JetCalibrationTool = calib,
                               MCWeightNames = stored_MC_weights if trees_per_stream[OutputStreamID] == 0 else [],
                               MCWeightTreeNamePrefix = InputJetCollections[JetCollection] + "|MCWeight_",
                               StorePUWeights = True if trees_per_stream[OutputStreamID] == 0 else False,
                               PUWeightTreeNamePrefix = InputJetCollections[JetCollection] + "|PUWeight",
                               PRWTool = prw,
                               JVTEfficiencyTool = jvt_efficiency_tool,
                               JetCollectionName = ORJetCollection,
                               TruthJetCollectionName = TruthCollectionMapping[JetCollection],
                               OutputTreeName = OutputTreeNames[JetCollection],
                               CalibrateJets = False
                               )

  trees_per_stream[OutputStreamID] += 1
  
  retagSeq += alg

algSeq += retagSeq

# -------------------------------------------------------
# setup the performance monitoring for this job
# -------------------------------------------------------
from PerfMonComps.PerfMonFlags import jobproperties as PerfMon_jp
PerfMon_jp.PerfMonFlags.doMonitoring = False
PerfMon_jp.PerfMonFlags.doFastMon = False
