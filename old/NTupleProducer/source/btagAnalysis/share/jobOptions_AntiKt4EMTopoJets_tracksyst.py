from btagAnalysis.MV2defaults import default_values
import btagAnalysis.configHelpers as configHelpers
from AthenaCommon.AthenaCommonFlags import jobproperties as jp

globalOutputLevel = INFO

#---------------------------------------------------
# setup output ntuple file name
# --------------------------------------------------
jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock(-1)
jp.AthenaCommonFlags.FilesInput = [input_file]

evtPrintoutInterval = vars().get('EVTPRINT', 5000)
svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval = evtPrintoutInterval)
svcMgr += CfgMgr.THistSvc()

OutputStreamIDs = [JetCollection + "_TUNC" for JetCollection in JetCollections]

#---------------------------------------------------
# defines the output streams
# --------------------------------------------------
for OutputStreamID in OutputStreamIDs:
  svcMgr.THistSvc.Output += [OutputStreamID + " DATAFILE='flav_" + OutputStreamID + ".root' OPT='RECREATE'"]

#---------------------------------------------------
# reconstruction flags
# --------------------------------------------------
from RecExConfig.RecFlags import rec
rec.doESD.set_Value_and_Lock(False)
rec.doWriteESD.set_Value_and_Lock(False)
rec.doAOD.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock(False)
rec.doWriteTAG.set_Value_and_Lock(False)
rec.doDPD.set_Value_and_Lock(False)
rec.doTruth.set_Value_and_Lock(False)
rec.doApplyAODFix.set_Value_and_Lock(False)
include("RecExCommon/RecExCommon_topOptions.py")

from AthenaCommon.AlgSequence import AlgSequence
algSeq = AlgSequence()

from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackSmearingTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackTruthOriginTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackTruthFilterTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__JetTrackFilterTool
from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConf import InDet__InDetTrackBiasingTool
from BTagTrackSystematicsAlgs.BTagTrackSystematicsAlgsConf import InDet__TrackSystematicsAlg

tracksInputKey = "InDetTrackParticles"
OutputTrackCollections = []
OutputJetCollections = []

OutputTrackCollectionsRetagging = []
OutputJetCollectionsRetagging = []

OutputTreeNames = {}
JetStreamMapping = {}
TruthCollectionMapping = {}
InputJetCollections = {}

# ---------------------------------------
# loop over jet collections and systematic variations
# ---------------------------------------
for JetCollection, TruthJetCollection, JetDefinition, OutputStreamID in zip(JetCollections, TruthJetCollections, JetDefinitions, OutputStreamIDs):  
  for ind, (systName, systVariation) in enumerate(systVariations.items()):

    OutputJetCollection = JetCollection + str(ind)
    tracksOutputKey = tracksInputKey + "_" + JetCollection + "".join(systName)

    OutputJetCollections.append(OutputJetCollection)
    OutputTreeNames[OutputJetCollection] = JetCollection + "|" + systName
    InputJetCollections[OutputJetCollection] = JetCollection

    JetStreamMapping[OutputJetCollection] = OutputStreamID
    TruthCollectionMapping[OutputJetCollection] = TruthJetCollection
    OutputTrackCollections.append(tracksOutputKey)

    OutputTrackCollectionsRetagging.append(tracksOutputKey)
    OutputJetCollectionsRetagging.append(OutputJetCollection)

    # ---------------------------------------
    # prepare the modified track collections
    # ---------------------------------------
    trackSmearingTool = InDet__InDetTrackSmearingTool("TrackSmearingTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    trackSmearingTool.Seed = 12345
    trackSmearingTool.useDijetMaps = True
    trackSmearingTool.runNumber = runNumber
    ToolSvc += trackSmearingTool  
    
    trackOriginTool = InDet__InDetTrackTruthOriginTool("TrackTruthOriginTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    ToolSvc += trackOriginTool  
    
    trackFilterTool = InDet__InDetTrackTruthFilterTool("InDetTrackTruthFilterTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    trackFilterTool.Seed = 1234
    trackFilterTool.trackOriginTool = trackOriginTool
    ToolSvc += trackFilterTool
    
    jetTrackFilterTool = InDet__JetTrackFilterTool("JetTrackFilterTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    jetTrackFilterTool.Seed = 4321
    ToolSvc += jetTrackFilterTool  
    
    trackBiasingTool = InDet__InDetTrackBiasingTool("InDetTrackBiasingTool" + JetCollection + str(ind), OutputLevel = globalOutputLevel)
    trackBiasingTool.isSimulation = True
    trackBiasingTool.runNumber = runNumber
    ToolSvc += trackBiasingTool
    
    # the algorithm that actually applies the tools to the events
    alg = InDet__TrackSystematicsAlg("TrackSystematicsAlg_" + JetCollection + "".join(systName) + str(ind))
    alg.OutputLevel = globalOutputLevel
    alg.trackFilterTool = trackFilterTool
    alg.jetTrackFilterTool = jetTrackFilterTool
    alg.trackSmearingTool = trackSmearingTool
    alg.trackBiasingTool = trackBiasingTool

    print("adding " + str(systVariation))
    
    alg.systematicVariations = systVariation
    
    alg.trackCollectionName = tracksInputKey
    alg.outputTrackCollectionName = tracksOutputKey
    alg.jetCollectionName = JetCollection
    
    algSeq += alg

    # ---------------------------------------
    # prepare the jet collections to which the track collections will be matched
    # ---------------------------------------
      
    # prepare the tool to handle the jet calibration
    calibName = "JETCalib_" + JetCollection + "_" + str(ind);
    print("setting up JetCalibration tool with name = " + calibName)
    calib = configHelpers.get_calibration_tool(CfgMgr, JetCollection, calibName)      
    ToolSvc += calib
      
    alg = CfgMgr.JetCalibrationAlgs(OutputLevel = globalOutputLevel,
                                    name = OutputJetCollection,
                                    JetCollectionName = JetCollection,
                                    OutputJetCollectionName = OutputJetCollection,                                    
                                    JetCalibrationTool = calib,
                                    calibrateJets = calibrate_jets_before_retagging
                                    )
    
    algSeq += alg

# -----------------------------------------------------
# set up the retagging of all defined JetCollections, 
# taking into account the modified tracks
# -----------------------------------------------------
print(" arguments being passed to retagger: ")
print(" JetCollections = " + str(OutputJetCollections))
print(" TrackCollections = " + str(OutputTrackCollections))

from btagAnalysis.RetagFragment import RetaggingConfiguration
RetaggingConfiguration(OutputJetCollections, OutputTrackCollections, ToolSvc, algSeq, outputLevel = globalOutputLevel)

# -----------------------------------------------------
# prepare for the pileup reweighting
# -----------------------------------------------------
jvt = CfgMgr.JetVertexTaggerTool('JVT')
ToolSvc += jvt

prw = CfgMgr.CP__PileupReweightingTool("prw",
                                       OutputLevel = globalOutputLevel,
                                       ConfigFiles = config_files,
                                       LumiCalcFiles = lumicalc_files
                                       )
ToolSvc += prw

from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import Trk__TrackToVertexIPEstimator
ToolSvc+=Trk__TrackToVertexIPEstimator("trkIPEstimator")

# -------------------------------------------------------
# this does some preprocessing on the b tagging vertices
# -------------------------------------------------------
algSeq += CfgMgr.BTagVertexAugmenter()

# -------------------------------------------------------
# set up the dumper algorithms that save the NTuples
# -------------------------------------------------------
trees_per_stream = {}
for JetCollection, OutputStreamID in JetStreamMapping.items():
  selector = CfgMgr.NHardestBJetTaggerAlg(name = "BJetSelector" + JetCollection,
                                          JetCollectionName = JetCollection,
                                          n = 2)
  algSeq += selector

  print("setting up dumper for pairing " + JetCollection + " -> " + OutputStreamID)

  if not OutputStreamID in trees_per_stream:
    trees_per_stream[OutputStreamID] = 0

  toolName = "BTagDumpAlgCalibTool_" + JetCollection
  calib = configHelpers.get_calibration_tool(CfgMgr, JetCollection, toolName, MC_type = MC_type)
  ToolSvc += calib

  # -----------------------------------------------------
  # for computing JVT efficiency scale factors
  # -----------------------------------------------------
  toolName = "JetJvtEfficiencyTool_" + JetCollection
  jvt_efficiency_tool = configHelpers.get_jvt_efficiency_tool(CfgMgr, JetCollection, toolName)
  ToolSvc += jvt_efficiency_tool

  alg = CfgMgr.btagAnalysisAlg(name = "BTagDumpAlg_" + JetCollection,
                               OutputLevel = globalOutputLevel,
                               Stream = OutputStreamID,
                               JVTtool = ToolSvc.JVT,
                               JetCalibrationTool = calib,
                               MCWeightNames = stored_MC_weights if trees_per_stream[OutputStreamID] == 0 else [],
                               MCWeightTreeNamePrefix = InputJetCollections[JetCollection] + "|MCWeight_",
                               StorePUWeights = True if trees_per_stream[OutputStreamID] == 0 else False,
                               PRWTool = prw,
                               PUWeightTreeNamePrefix = InputJetCollections[JetCollection] + "|PUWeight",
                               JVTEfficiencyTool = jvt_efficiency_tool,
                               JetCollectionName = JetCollection,
                               TruthJetCollectionName = TruthCollectionMapping[JetCollection],
                               OutputTreeName = OutputTreeNames[JetCollection],
                               CalibrateJets = calibrate_jets_after_retagging
                               )
  
  trees_per_stream[OutputStreamID] += 1

  algSeq += alg

# -------------------------------------------------------
# setup the performance monitoring for this job
# -------------------------------------------------------
from PerfMonComps.PerfMonFlags import jobproperties as PerfMon_jp
PerfMon_jp.PerfMonFlags.doMonitoring = False
PerfMon_jp.PerfMonFlags.doFastMon = False
