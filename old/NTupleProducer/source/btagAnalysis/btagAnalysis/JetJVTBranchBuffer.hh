#ifndef JETJVT_BRANCH_BUFFER_HH
#define JETJVT_BRANCH_BUFFER_HH

#include <vector>

struct JetJVTBranchBuffer {

    std::vector<float>* v_jet_JVT_weight;
    std::vector<float>* v_jet_JVT_weight_up;
    std::vector<float>* v_jet_JVT_weight_down;
    std::vector<int>* v_jet_aliveAfterJVT;

};

#endif
