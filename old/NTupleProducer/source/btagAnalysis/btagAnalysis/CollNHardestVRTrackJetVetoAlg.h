#ifndef __COLLNHARDEST_VETO_ALG_H
#define __COLLNHARDEST_VETO_ALG_H

#include <string>
#include <algorithm>
#include <math.h>
#include "AthenaBaseComps/AthFilterAlgorithm.h"
#include "xAODJet/JetContainer.h"

class CollNHardestVRTrackJetVetoAlg: public AthFilterAlgorithm {
public:
    CollNHardestVRTrackJetVetoAlg(const std::string& name, ISvcLocator* pSvcLocator);
    ~CollNHardestVRTrackJetVetoAlg();

    StatusCode initialize();
    StatusCode finalize();
    StatusCode execute();

private:
    double deltaPhi(double phi_1, double phi_2);
    double deltaR(double eta_1, double eta_2, double phi_1, double phi_2);
    double getJetR(double pt);
    int getNTrk(xAOD::Jet jet);

    std::string m_jetCollectionName;
};

#endif
