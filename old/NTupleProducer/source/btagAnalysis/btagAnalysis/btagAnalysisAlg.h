#ifndef BTAGANALYSIS_BTAGANALYSISALG_H
#define BTAGANALYSIS_BTAGANALYSISALG_H

#include "TTree.h"
#include "JetPropertiesBranches.hh"
#include "JetJVTBranches.hh"
#include "TaggerScoreBranches.hh"
#include "BTagTrackAccessors.hh"
#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTruth/TruthParticle.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PMGTools/PMGTruthWeightTool.h"
#include <xAODCutFlow/CutBookkeeper.h>
#include <xAODCutFlow/CutBookkeeperContainer.h>

// forward declarations
class IJetSelector;
class IJetCalibrationTool;
namespace InDet { class IInDetTrackSelectionTool; }
namespace CP {
  class ITrackVertexAssociationTool;
  class IPileupReweightingTool;
  class ISystematicsTool;
  class JetJvtEfficiency;
}

class IJetUpdateJvt;
class ArbitraryJetBranches;

class btagAnalysisAlg: public AthHistogramAlgorithm {

 public:
  btagAnalysisAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~btagAnalysisAlg();

  virtual StatusCode initialize();
  virtual StatusCode initializeTools();
  virtual StatusCode execute();
  virtual StatusCode finalize();

private:
  StatusCode initializeOnFirstEvent();
  StatusCode getCutBookkeeperByWeightName(std::string weight_name, xAOD::CutBookkeeper** retval);
  bool isFromWZ( const xAOD::TruthParticle* particle );

  TTree* m_tree;
  std::map<std::string, TTree*>* m_MC_weight_trees;

  // these are trees that hold the nominal pileup weights
  // as well as their up and down systematic variations
  TTree* m_tree_PU_weight;
  TTree* m_tree_PU_weight_SFup;
  TTree* m_tree_PU_weight_SFdown;

  // the histograms that will hold event metadata
  TH1D* m_hist_TotalEntries;
  TH1D* m_hist_SumOfWeights;
  TH1D* m_hist_SumOfWeightsSquared;

  float PU_weight;
  float PU_weight_SFup;
  float PU_weight_SFdown;

  std::string m_stream = "BTAGSTREAM";

  std::string m_jetCollectionName;
  std::string m_truthJetCollectionName;
  std::string m_track_associator;

  bool m_calibrateJets;
  bool m_doJVT;

  ToolHandle< IJetCalibrationTool > m_jetCalibrationTool;
  ToolHandle<IJetUpdateJvt> m_jvt;
  ToolHandle<CP::JetJvtEfficiency> m_JVTEfficiencyTool;
  ToolHandle<CP::IPileupReweightingTool> m_PUtool;
  ToolHandle<PMGTools::IPMGTruthWeightTool> m_weightTool;
  ServiceHandle<ITHistSvc> histSvc;
 
  //Event Info
  int runnumber;
  int eventnumber;
  int mcchannel;
  int event_clean;
  float mcweight_nominal;
  float mcweight_cur;
  float mu;
  int Act_mu;

  //Branches
  JetPropertiesBranches m_jet_properties_branches;
  JetJVTBranches m_jet_JVT_branches;
  TaggerScoreBranches m_tagger_scores_branches;

  std::string m_outputTreeName;
  std::string m_MCWeightTreeNamePrefix;
  std::string m_PUWeightTreeNamePrefix;
  std::vector<std::string> m_MC_weightnames;

  bool m_store_PU_weights = false;
  bool firstEvent = true;
};

#endif
