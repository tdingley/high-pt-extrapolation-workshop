#ifndef JETJVT_BRANCHES_HH
#define JETJVT_BRANCHES_HH

#include <vector>

class TTree;

struct JetJVTBranchBuffer;

class JetJVTBranches
{
public:
    JetJVTBranches();
    ~JetJVTBranches();

    void set_tree(TTree& output_tree);

    void fill(float jet_JVT_weight, float jet_JVT_weight_up, float jet_JVT_weight_down, int jet_aliveAfterJVT);
    void clear();

private:
    JetJVTBranchBuffer* m_branches;
};

#endif
