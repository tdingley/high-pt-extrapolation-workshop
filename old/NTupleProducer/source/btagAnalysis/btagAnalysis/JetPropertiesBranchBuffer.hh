#ifndef JETPROPERTIES_BRANCH_BUFFER_HH
#define JETPROPERTIES_BRANCH_BUFFER_HH

#include <vector>

struct JetPropertiesBranchBuffer {

  std::vector<float> *v_jet_pt;
  std::vector<float> *v_jet_eta;
  std::vector<float> *v_jet_phi;
  std::vector<float> *v_jet_E;

  std::vector<float> *v_b_hadron_pt;
  std::vector<float> *v_b_hadron_eta;
  std::vector<float> *v_b_hadron_phi;
  std::vector<float> *v_b_hadron_E;

  std::vector<int>   *v_jet_LabDr_HadF;
  std::vector<int>   *v_jet_DoubleHadLabel;
  std::vector<float> *v_jet_JVT;

  std::vector<float> *v_jet_m;
  std::vector<float> *v_jet_nConst;
  std::vector<float> *v_jet_dRiso;
  std::vector<int>   *v_jet_truthMatch;
  std::vector<int>   *v_jet_isPU;
  std::vector<int>   *v_jet_aliveAfterOR;
  std::vector<int>   *v_jet_aliveAfterORmu;
  std::vector<int>   *v_jet_aliveAfterCollOR;
  std::vector<int>   *v_jet_leading_bjet;
  std::vector<int>   *v_jet_isBadMedium;
  std::vector<float> *v_jet_truthPt;

  std::vector<Int_t> *v_jet_nTracks;
  std::vector<Int_t> *v_jet_nJetConstituents;

};

#endif // JETPROPERTIES_BRANCH_BUFFER_HH
