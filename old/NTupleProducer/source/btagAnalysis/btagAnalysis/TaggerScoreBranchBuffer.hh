#ifndef TAGGERSCORE_BRANCH_BUFFER_HH
#define TAGGERSCORE_BRANCH_BUFFER_HH

#include <vector>

struct TaggerScoreBranchBuffer {

	std::vector<double> *v_jet_dl1_pb;
	std::vector<double> *v_jet_dl1_pc;
	std::vector<double> *v_jet_dl1_pu;
	std::vector<double> *v_jet_dl1r_pb;
	std::vector<double> *v_jet_dl1r_pc;
	std::vector<double> *v_jet_dl1r_pu;

    	std::vector<double> *v_jet_rnnip_pu;
    	std::vector<double> *v_jet_rnnip_pc;
        std::vector<double> *v_jet_rnnip_pb;
};

#endif // TAGGERSCORE_BRANCH_BUFFER_HH
