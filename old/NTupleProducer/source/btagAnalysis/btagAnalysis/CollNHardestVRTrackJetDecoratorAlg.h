#ifndef __COLLNHARDESTVRTRACKJETDECORATOR_ALG_H
#define __COLLNHARDESTVRTRACKJETDECORATOR_ALG_H

#include <string>
#include <algorithm>
#include "xAODJet/JetContainer.h"
#include "AthenaBaseComps/AthAlgorithm.h"

class CollNHardestVRTrackJetDecoratorAlg: public AthAlgorithm {
public:
    CollNHardestVRTrackJetDecoratorAlg(const std::string& name, ISvcLocator* pSvcLocator);
    ~CollNHardestVRTrackJetDecoratorAlg();

    StatusCode initialize();
    StatusCode finalize();
    StatusCode execute();

private:
    double deltaPhi(double phi_1, double phi_2);
    double deltaR(double eta_1, double eta_2, double phi_1, double phi_2);
    double getJetR(double pt);
    int getNTrk(xAOD::Jet jet);

    std::string m_jetCollectionName;
};

#endif
