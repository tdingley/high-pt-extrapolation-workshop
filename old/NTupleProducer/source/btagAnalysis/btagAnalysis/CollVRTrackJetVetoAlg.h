#ifndef __EVENT_SELECTION_H
#define __EVENT_SELECTION_H

#include <string>
#include <algorithm>
#include <TMath.h>
#include "AthenaBaseComps/AthFilterAlgorithm.h"
#include "xAODJet/JetContainer.h"

class CollVRTrackJetVetoAlg: public AthFilterAlgorithm {
public:
    CollVRTrackJetVetoAlg(const std::string& name, ISvcLocator* pSvcLocator);
    ~CollVRTrackJetVetoAlg();

    StatusCode initialize();
    StatusCode finalize();
    StatusCode execute();

private:
    std::string m_jetCollectionName;
    double deltaR(double eta_1, double eta_2, double phi_1, double phi_2);
    double getJetR(double pt);
    int getNTrk(xAOD::Jet jet);
};

#endif
