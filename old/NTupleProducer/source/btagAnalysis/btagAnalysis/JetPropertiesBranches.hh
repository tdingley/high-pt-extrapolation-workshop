#ifndef JETPROPERTIES_BRANCHES_HH
#define JETPROPERTIES_BRANCHES_HH

#include "TLorentzVector.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"

class TTree;

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
}

struct JetPropertiesBranchBuffer;

class JetPropertiesBranches
{
public:
  // might want to add a prefix to the constructor for the tree branches
  JetPropertiesBranches();
  ~JetPropertiesBranches();

  // disable copying and assignment
  JetPropertiesBranches& operator=(JetPropertiesBranches) = delete;
  JetPropertiesBranches(const JetPropertiesBranches&) = delete;

  void set_tree(TTree& output_tree);
  void fill(const xAOD::Jet& calib_jet, float JVT, float dRiso,
	    std::vector<TLorentzVector> truth_electrons,  std::vector<TLorentzVector> truth_muons,
	    const xAOD::JetContainer *truthjets,
	    Int_t m_nTracks, Int_t m_nJetConstituents);

  void clear();

private:
  JetPropertiesBranchBuffer* m_branches;
};

#endif // JETPROPERTIES_BRANCHES_HH
