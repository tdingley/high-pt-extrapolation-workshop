#ifndef __NHARDESTBJETTAGGER_ALG_H
#define __NHARDESTBJETTAGGER_ALG_H

#include <string>
#include <algorithm>
#include "AthenaBaseComps/AthFilterAlgorithm.h"
#include "xAODJet/JetContainer.h"

class NHardestBJetTaggerAlg: public AthAlgorithm {
public:
    NHardestBJetTaggerAlg(const std::string& name, ISvcLocator* pSvcLocator);
    ~NHardestBJetTaggerAlg();

    StatusCode initialize();
    StatusCode finalize();
    StatusCode execute();

private:
    std::string m_jetCollectionName;
    unsigned int n;
};

#endif
