#include "JetSystematicsAlgs.h"

JetSystematicsAlgs::JetSystematicsAlgs(const std::string& name, ISvcLocator *pSvcLocator)
    : AthAlgorithm(name, pSvcLocator),
      
      m_JetUncTool("")
{
    declareProperty("JetCollectionName", m_jetCollectionName);
    declareProperty("OutputJetCollectionName", m_OutputJetCollectionName);
    declareProperty("JetCalibrationTool", m_jetCalibrationTool);
    declareProperty("systematicVariations", m_systNames);
    declareProperty("JetUncertaintyTool", m_JetUncTool);
    declareProperty("calibrateJets", m_calibrateJets);
}

JetSystematicsAlgs::~JetSystematicsAlgs() = default;

StatusCode JetSystematicsAlgs::initialize() 
{
    ATH_MSG_INFO("Initializing " << name() << "...");

    CHECK(initializeTools());

    return StatusCode::SUCCESS;
}

StatusCode JetSystematicsAlgs::initializeTools()
{
    // get the JetCalibrationTool
    if(!m_jetCalibrationTool.empty())
    {
	if(m_jetCalibrationTool.retrieve().isFailure())
	{
	    ATH_MSG_INFO("failed to retrieve JetCalibrationTool, will NOT calibrate jets");
	}
	else
	{
	    ATH_MSG_DEBUG("retrieved JetCalibrationTool " << m_jetCalibrationTool);
	}
    }

    // get the JetUncertaintiesTool
    if(!m_JetUncTool.empty())
    {
	if(m_JetUncTool.retrieve().isFailure())
	{
	    ATH_MSG_FATAL("failed to retrieve JetUncertainyTool " << m_JetUncTool);
	    return StatusCode::FAILURE;
	}
	else
	{
	    ATH_MSG_DEBUG("retrieved JetUncertaintyTool " << m_JetUncTool);
	}
    }
    else
    {
	ATH_MSG_FATAL("No JetUncertaintyTool to retrieve!");
	return StatusCode::FAILURE;
    }

    CHECK( m_JetUncTool.retrieve() );

    const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
    const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics();

    // this is the vector of all available systematic variations
    std::vector<CP::SystematicSet> sysList = CP::make_systematics_vector(recommendedSystematics);

    // prepare the systematics that are to be applied
    CP::SystematicSet activeSysts;
    
    for(auto cur_syst_name: m_systNames)
    {
	auto newSyst = std::find_if(std::begin(sysList), 
				    std::end(sysList),
				    [&](CP::SystematicSet cur_syst) -> bool{
					return cur_syst.name() == cur_syst_name;
				    });

	if(newSyst != std::end(sysList))
	{
	    ATH_MSG_INFO("Adding systematic variation: " << newSyst -> name());
	    activeSysts.insert(*newSyst);
	}
	else
	{
	    ATH_MSG_ERROR("Unrecognized systematic variation: " << newSyst -> name());
	    return StatusCode::FAILURE;
	}
    }

    // configure the Jet Uncertainty tool
    ATH_MSG_DEBUG("applying the following (recommended) systematics:");
    for(auto cur_syst: sysList)
    {
	ATH_MSG_DEBUG(cur_syst.name());
    }

    if(m_JetUncTool->applySystematicVariation(activeSysts) != CP::SystematicCode::Ok) {
	ATH_MSG_ERROR("Cannot configure jet uncertainties tool for systematic");
	return StatusCode::FAILURE;	
    }

    return StatusCode::SUCCESS;
}

StatusCode JetSystematicsAlgs::execute() 
{    
    const xAOD::JetContainer *jets = 0;
    CHECK( evtStore() -> retrieve(jets, m_jetCollectionName) );

    // create output jet container
    auto varJets = std::make_unique<xAOD::JetContainer>();
    auto varJetsAux = std::make_unique<xAOD::JetAuxContainer>();
    varJets -> setStore(varJetsAux.get());

    ATH_MSG_DEBUG( " Main jet loop " );
    for (auto cur_jet: *jets) 
    {	
	// store the original (raw) jet pt for future reference
	cur_jet -> auxdecor<float>("jet_pt_raw") = cur_jet -> pt();

	xAOD::Jet* var_jet = new xAOD::Jet();
	*var_jet = *cur_jet;

	// calibrate the jet before applying systematic variations
	if(m_calibrateJets)
	{
	    ATH_MSG_DEBUG("calibrating jet");
	    m_jetCalibrationTool -> modifyJet(*var_jet);
	}

	// need to apply only the jet systematics that have been passed to the tool	
	if (m_JetUncTool -> applyCorrection(*var_jet) != CP::CorrectionCode::Ok) {
	    ATH_MSG_ERROR("Cannot apply systematic variation to the jet");
	    continue; // skip this jet
	}
	ATH_MSG_INFO("Successfully applied systematic variation to the jet");

	xAOD::Jet* out_jet = new xAOD::Jet();
	varJets -> push_back(out_jet);
	*out_jet = *var_jet;

	delete var_jet;

	ATH_MSG_DEBUG("stored jet with pt = " << out_jet -> pt() << " coming from jet with pt = " << cur_jet -> pt());
	ATH_MSG_DEBUG("this jet has: jet px = " << out_jet -> p4().Px() << ", py = " << out_jet -> p4().Py() << ", pz = " << out_jet -> p4().Pz() << ", E = " << out_jet -> p4().E());
    }

    ATH_MSG_DEBUG("wrote jets to collection " + m_OutputJetCollectionName);

    CHECK(evtStore() -> record(varJets.release(), m_OutputJetCollectionName));
    CHECK(evtStore() -> record(varJetsAux.release(), m_OutputJetCollectionName + "Aux"));

    return StatusCode::SUCCESS;
}

StatusCode JetSystematicsAlgs::finalize()
{
    CHECK(m_JetUncTool.release());

    return StatusCode::SUCCESS;
}
