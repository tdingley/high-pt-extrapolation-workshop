#pragma once

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "JetUncertainties/JetUncertaintiesTool.h"

#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicsUtil.h"

#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>

class JetSystematicsAlgs :  public AthAlgorithm { 
public:
  
  /** Constructors and destructors */
  JetSystematicsAlgs(const std::string& name, ISvcLocator *pSvcLocator);
  virtual ~JetSystematicsAlgs();
  
  /** Main routines specific to an ATHENA algorithm */
  virtual StatusCode initialize();
  virtual StatusCode execute();
  virtual StatusCode finalize();
  
private:

  StatusCode initializeTools();

  std::string m_jetCollectionName;
  std::string m_OutputJetCollectionName;
  std::string m_JetUncToolName;
  bool m_calibrateJets = true;

  std::vector<std::string> m_systNames;

  // handles for jet uncertainty and calibration tools
  ToolHandle<ICPJetUncertaintiesTool> m_JetUncTool;
  ToolHandle< IJetCalibrationTool > m_jetCalibrationTool;
};
