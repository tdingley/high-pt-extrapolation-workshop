#pragma once

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

#include "JetCalibTools/IJetCalibrationTool.h"

#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>

class JetCalibrationAlgs :  public AthAlgorithm { 
public:
  
  /** Constructors and destructors */
  JetCalibrationAlgs(const std::string& name, ISvcLocator *pSvcLocator);
  virtual ~JetCalibrationAlgs();
  
  /** Main routines specific to an ATHENA algorithm */
  virtual StatusCode initialize();
  virtual StatusCode execute();
  virtual StatusCode finalize();
  
private:

  StatusCode initializeTools();

  std::string m_jetCollectionName;
  std::string m_OutputJetCollectionName;

  bool m_calibrateJets = true;

  // handles for jet calibration tools
  ToolHandle< IJetCalibrationTool > m_jetCalibrationTool;
};
