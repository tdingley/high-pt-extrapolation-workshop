#include "GaudiKernel/DeclareFactoryEntries.h"
#include "../JetCalibrationAlgs.h"

/** factory entries need to have the name of the package */
DECLARE_ALGORITHM_FACTORY( JetCalibrationAlgs )
