#include "JetCalibrationAlgs.h"

JetCalibrationAlgs::JetCalibrationAlgs(const std::string& name, ISvcLocator *pSvcLocator)
    : AthAlgorithm(name, pSvcLocator)      
{
    declareProperty("JetCollectionName", m_jetCollectionName);
    declareProperty("OutputJetCollectionName", m_OutputJetCollectionName);
    declareProperty("JetCalibrationTool", m_jetCalibrationTool);
    declareProperty("calibrateJets", m_calibrateJets);
}

JetCalibrationAlgs::~JetCalibrationAlgs() = default;

StatusCode JetCalibrationAlgs::initialize() 
{
    ATH_MSG_INFO("Initializing " << name() << "...");

    CHECK(initializeTools());

    return StatusCode::SUCCESS;
}

StatusCode JetCalibrationAlgs::initializeTools()
{
    // get the JetCalibrationTool
    if(m_calibrateJets)
    {
	if(!m_jetCalibrationTool.empty())
	{
	    if(m_jetCalibrationTool.retrieve().isFailure())
	    {
		ATH_MSG_ERROR("failed to retrieve JetCalibrationTool, will NOT calibrate jets");
	    }
	    else
	    {
		ATH_MSG_DEBUG("retrieved JetCalibrationTool " << m_jetCalibrationTool);
	    }
	}
    }
    return StatusCode::SUCCESS;
}

StatusCode JetCalibrationAlgs::execute() 
{    
    const xAOD::JetContainer *jets = 0;
    ATH_MSG_DEBUG("read jets from collection " + m_jetCollectionName);

    CHECK( evtStore() -> retrieve(jets, m_jetCollectionName) );

    // create output jet container
    auto varJets = std::make_unique<xAOD::JetContainer>();
    auto varJetsAux = std::make_unique<xAOD::JetAuxContainer>();
    varJets -> setStore(varJetsAux.get());

    ATH_MSG_DEBUG( " Main jet loop " );
    for (auto cur_jet: *jets) 
    {	
	xAOD::Jet* var_jet = new xAOD::Jet();
	*var_jet = *cur_jet;

	// calibrate the jet before applying systematic variations
	if(m_calibrateJets)
	{
	    ATH_MSG_DEBUG("calibrating jet");
	    m_jetCalibrationTool -> modifyJet(*var_jet);
	}
	
	xAOD::Jet* out_jet = new xAOD::Jet();
	varJets -> push_back(out_jet);
	*out_jet = *var_jet;

	delete var_jet;

	ATH_MSG_DEBUG("stored jet with pt = " << out_jet -> pt() << " coming from jet with pt = " << cur_jet -> pt());
    }

    ATH_MSG_DEBUG("wrote jets to collection " + m_OutputJetCollectionName);

    if (evtStore()->contains<xAOD::JetContainer>(m_OutputJetCollectionName)) {
    	ATH_CHECK(evtStore() -> overwrite(varJets.release(), m_OutputJetCollectionName, true, false));
    	ATH_CHECK(evtStore() -> overwrite(varJetsAux.release(), m_OutputJetCollectionName + "Aux.", true, false));
    }
    else {
    	CHECK(evtStore() -> record(varJets.release(), m_OutputJetCollectionName));
    	CHECK(evtStore() -> record(varJetsAux.release(), m_OutputJetCollectionName + "Aux."));
    }

    return StatusCode::SUCCESS;
}

StatusCode JetCalibrationAlgs::finalize()
{
    return StatusCode::SUCCESS;
}
