#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
lsetup git

# switch to the directory for the NTupleProducer
cd ${XTRAP_ROOTDIR}/NTupleProducer/source/

# get Athena if not done so already
# this fetches a modified version of the PMGTruthWeightTool that compiles 
# against the derivation framework
if [ ! -d "athena" ]; then
    echo "Fetching Athena ..."
    git atlas init-workdir https://:@gitlab.cern.ch:8443/phwindis/athena.git
    cd athena
    git checkout 21.2.87.0_BTagTrackAugmenterAlg
    git atlas addpkg FlavorTagDiscriminants
    git atlas addpkg BTagging
fi

# create the 'build' and 'run' directories
cd ${XTRAP_ROOTDIR}/NTupleProducer/
if [ ! -d "build" ]; then
    mkdir -p ${XTRAP_ROOTDIR}/NTupleProducer/build
fi

if [ ! -d "run" ]; then
    mkdir -p ${XTRAP_ROOTDIR}/NTupleProducer/run
fi
