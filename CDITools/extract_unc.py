import re
import numpy as np
from argparse import ArgumentParser

from CDIInputImporter import CDIInputImporter
from CDIInputExporter import CDIInputExporter
from RelUncertainty import RelUncertainty

def make_increasing(curve):
    retcurve = curve.clone()

    centers = curve.get_bin_centers()
    values = [curve.evaluate(xval) for xval in centers]
    values = np.maximum.accumulate(values)

    for bin_center, bin_value in zip(centers, values):
        retcurve.set(bin_center, bin_value)

    return retcurve

def extract_unc(infile, outfile, copy_regex, force_monotonicity = True):
    if isinstance(copy_regex, str):
        copy_regex = re.compile(copy_regex)

    available_uncertainties = CDIInputImporter._read_available_systematics(infile)
    
    # use the regex to select only those uncertainties that should actually be copied
    selected_uncertainties = filter(copy_regex.match, available_uncertainties)

    print("will extract the following uncertainties:")
    print('\n'.join(selected_uncertainties))

    # also copy over the original metadata, read it in first
    calib_name, final_stat, tagger, WP, jetcoll = CDIInputImporter._read_header(infile)
    options = {"WP": WP, "jet_collection": jetcoll, "tagger": tagger}
    central_value = CDIInputImporter._read_central_values(infile)
    stat_unc = central_value.create_uncertainty_obj()

    stat_uncs_up = [stat_unc]
    stat_uncs_down = [-stat_unc]

    syst_uncs_up = {}
    syst_uncs_down = {}

    # now do the actual copying
    for cur_uncertainty_name in selected_uncertainties:
        cur_uncertainty = RelUncertainty(CDIInputImporter._read_systematics_values(infile, cur_uncertainty_name))

        if force_monotonicity:
            cur_uncertainty = make_increasing(cur_uncertainty)

        syst_uncs_up[cur_uncertainty_name] = cur_uncertainty
        syst_uncs_down[cur_uncertainty_name] = cur_uncertainty

        # export the modified CDI input file
    CDIInputExporter.produce_CDI_input_from_uncertainties(outfile, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)

if __name__ == "__main__":
    parser = ArgumentParser(description = "extract (a) certain uncertainty component(s) from this file and put it into a separate CDI input")
    parser.add_argument("--in", action = "store", dest = "infile")
    parser.add_argument("--out", action = "store", dest = "outfile")
    parser.add_argument("--copy_regex", action = "store", default = ".*")
    args = vars(parser.parse_args())

    extract_unc(**args)
