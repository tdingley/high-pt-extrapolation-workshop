from EfficiencyCurve import EfficiencyCurve
from EfficiencySorter import EfficiencySorter, EfficiencyCurveOrigin, VariationType
from RelUncertainty import RelUncertainty
from UncertaintyCombinator import UncertaintyCombinator
from ColorPicker import ColorPicker
import ROOT
from ROOT import TH1F, TCanvas, TPad, TLegend, TGraphAsymmErrors, TMultiGraph, TGaxis, TLatex, TGraph, TGraphErrors
#from atlasplots import atlas_style as astyle

class EfficiencyPlotter:
    # Generates a plot of the nominal 'efficiency_curve' with its associated statistical and total
    # systematic uncertainty (computed as the quadratic sum of all assigned relative uncertainties)
    # In addition, can overlay the individual contributions from specific uncertainty groups:
    #      expect them to be specified in the form group_spec = [(group_name, group_color), (...), ...]
    @staticmethod
    def plot_uncertainty_ratio_from_variation_group(efficiency_curve, group_spec, plot_label, outfile, inner_label = "", saveROOTFile = False):

        # just need to look up the variations associated to the given groups
        var_spec = []
        for group_name, group_color in group_spec:
            group_variations = efficiency_curve.variations[group_name]
            var_spec.append((group_name, group_color, group_variations))

        EfficiencyPlotter.plot_uncertainty_ratio_from_variations(efficiency_curve, var_spec, plot_label, outfile, inner_label, saveROOTFile)

    # Expect var_spec to look like [(group_name, group_color, [variations])]
    @staticmethod
    def plot_uncertainty_ratio_from_variations(efficiency_curve, var_spec, plot_label, outfile, inner_label = "", saveROOTFile = False, variations_style = 'l'):
        # get the total uncertainty envelope that takes into account all systematic variations
        group_syst_envelopes_up = []
        group_syst_envelopes_down = []

        # iterate only over the actual uncertainty groups that have been added
        group_names = list(map(lambda x: x[0:], EfficiencyCurveOrigin))
        #for group_name, group_variations in efficiency_curve.variations.items():
        for group_name in group_names:
            if group_name in efficiency_curve.variations:
                group_variations = efficiency_curve.variations[group_name]
                group_syst_envelope_up, group_syst_envelope_down = UncertaintyCombinator.from_group(group_name, nominal_curve = efficiency_curve, group_variations = group_variations)

                group_syst_envelopes_up.append(group_syst_envelope_up)
                group_syst_envelopes_down.append(group_syst_envelope_down)
            
        # the total systematic uncertainty is the combination of the uncertainties of the individual groups
        total_syst_envelope_up = RelUncertainty.from_quadrature_sum(group_syst_envelopes_up)
        total_syst_envelope_down = -RelUncertainty.from_quadrature_sum(group_syst_envelopes_down) if group_syst_envelopes_down else None

        # also get the statistical uncertainty associated with the nominal curve (this is symmetric by default, thus make it so)
        stat_envelope_up = efficiency_curve.get_statistical_uncertainty()
        stat_envelope_down = -stat_envelope_up

        # the total uncertainty envelope is the quadratic sum of stat + syst
        total_envelope_up = RelUncertainty.from_quadrature_sum([total_syst_envelope_up, stat_envelope_up])
        total_envelope_down = -RelUncertainty.from_quadrature_sum([total_syst_envelope_down, stat_envelope_down])

        variations_overlay = []
        for group_name, group_color, group_variations in var_spec:
            # obtain the uncertainty envelopes for the individual uncertainty groups
            group_syst_envelope_up, group_syst_envelope_down = UncertaintyCombinator.from_group(group_name, nominal_curve = efficiency_curve, group_variations = group_variations)

            # convert them to the actual curves that should be overlaid on the graph
            variations_overlay.append((group_name, group_color, 
                                       [efficiency_curve * (group_syst_envelope_up + 1), efficiency_curve * (group_syst_envelope_down + 1)]))

        # perform the actual plotting
        EfficiencyPlotter.plot_uncertainty_ratio(efficiency_curve, 
                                                 rel_uncertainty_up = total_envelope_up,
                                                 rel_uncertainty_down = total_envelope_down,
                                                 variations = variations_overlay,
                                                 outfile = outfile,
                                                 outer_label = plot_label,
                                                 inner_label = inner_label,
                                                 saveROOTFile = saveROOTFile,
                                                 variations_style = variations_style)        
    

    # Generates a plot of the 'curve', also showing its associated statistical uncertainties as well as the uncertainty band specified
    # by 'rel_uncertainty_up' and 'rel_uncertainty_down'. This will generally correspond to the *total* uncertainty (i.e. stat. + syst.)
    # In addition, 'variations' is a vector specifying additional curves to be overlaid. It must be of the following form
    #      [(name, color, [curve1, curve2, ...]), ...]
    @staticmethod
    def plot_uncertainty_ratio(curve, rel_uncertainty_up, rel_uncertainty_down, outfile, outer_label = "", inner_label = "", variations = [], saveROOTFile = False, variations_style = 'l', ratio_reference = None):
        eps = 1e-5

        # if no explicit reference curve is given for the reference plot, take it to be the main efficiency curve
        if not ratio_reference:
            ratio_reference = curve

        # axis range of the absolute...
        abs_y_min = 0
        abs_y_max = 1.3

        # ... and relative plot
        rel_y_min = 0.9
        rel_y_max = 1.1

        # ticklengths of the plots
        ticklength_y = 0.01;
        ticklength_x = 0.01;

        # color of the global uncertainty band
        cp = ColorPicker()
        uncertainty_band_color = cp.get_color("greenblue1")

        # some general settings
        ROOT.gStyle.SetLineScalePS(3)
        ROOT.gStyle.SetOptStat(0)

        # -------------------------------------------
        # create the global layout of the plot
        # -------------------------------------------
        canv = TCanvas("canv_ratio_plot", "canv", 10, 10, 2400, 2200)
        ROOT.SetOwnership(canv, False)

        pad_abs = TPad("abs_pad", "abs_pad", 0, 0.33, 1, 1)
        pad_abs.SetBottomMargin(0.02)
        pad_abs.SetFillColor(ROOT.kWhite)
        pad_abs.SetFrameFillStyle(0)
        pad_abs.Draw()

        canv.cd()
        pad_rel = TPad("rel_pad", "rel_pad", 0, 0, 1, 0.33)
        pad_rel.SetTopMargin(0.02)
        pad_rel.SetBottomMargin(0.2)
        pad_rel.SetFillColor(ROOT.kWhite)
        pad_rel.SetFrameFillStyle(0)
        pad_rel.Draw()

        # -------------------------------------------
        # plot the absolute efficiency and its systematic uncertainty
        # -------------------------------------------
        leg_abs = TLegend(0.12, 0.15, 0.22, 0.25)
        leg_abs.SetBorderSize(0)
        leg_abs.SetTextFont(42)
        leg_abs.SetTextSize(0.035)
     
        # obtain the graph for the nominal efficiency curve
        graph_nominal = TGraphAsymmErrors(curve.hist)
        graph_nominal.SetLineColor(ROOT.kBlack)
        graph_nominal.SetMarkerStyle(20)
        graph_nominal.SetMarkerSize(3.5)
        graph_nominal.SetLineWidth(1)
        graph_nominal.SetMarkerColor(ROOT.kBlack)

        # obtain the absolute uncertainties from the passed relative ones
        abs_uncertainty_up = rel_uncertainty_up.convert_to_absolute(curve) if rel_uncertainty_up else None
        abs_uncertainty_down = rel_uncertainty_down.convert_to_absolute(curve) if rel_uncertainty_down else None

        graph_unc_abs = graph_nominal.Clone()
        graph_unc_abs = EfficiencyPlotter._set_uncertainties(graph_unc_abs,
                                                             uncertainty_up = abs_uncertainty_up, 
                                                             uncertainty_down = abs_uncertainty_down)

        graph_unc_abs.SetFillStyle(1001)
        graph_unc_abs.SetLineWidth(0)
        graph_unc_abs.SetLineColor(0)
        graph_unc_abs.SetMarkerColor(uncertainty_band_color)
        graph_unc_abs.SetFillColor(uncertainty_band_color)

        leg_abs.AddEntry(graph_nominal, "stat.", "lpfe")
        leg_abs.AddEntry(graph_unc_abs, "stat. + syst.", "lpfe")

        # draw the inner label
        if inner_label:
            inner_label_obj = TLatex()
            inner_label_obj.SetTextAlign(11)
            inner_label_obj.SetTextFont(42)
            inner_label_obj.SetTextSize(0.035)

        # prepare the multigraphs to plot them
        mg_abs = TMultiGraph()
        mg_abs.Add(graph_unc_abs, "5")

        # if want the individual variations plotted as well, add them to the TMultiGraph
        if variations:
            leg_vars = TLegend(0.12, 0.25, 0.22, 0.35)
            leg_vars.SetBorderSize(0)
            leg_vars.SetTextFont(42)
            leg_vars.SetTextSize(0.035)
            
            # go through each group of variations
            for group in variations:
                group_name = group[0]
                group_color = group[1]
                group_variations = group[2]
                
                for cur_var in group_variations:
                    cur_graph = TGraphErrors(cur_var.hist)
                    cur_graph.SetLineWidth(1)
                    cur_graph.SetMarkerStyle(20)
                    cur_graph.SetMarkerSize(3.5)
                    cur_graph.SetLineColor(group_color)
                    cur_graph.SetMarkerColor(group_color)
                    mg_abs.Add(cur_graph, variations_style)

                leg_vars.AddEntry(cur_graph, group_name, "lfepZ")

        mg_abs.Add(graph_nominal, "pZ")

        # -------------------------------------------
        # show the relative plots
        # -------------------------------------------
        # prepare the graph showing the relative uncertainty baseline
        relative_stat_unc_up = curve.get_statistical_uncertainty()
        relative_stat_unc_down = -relative_stat_unc_up
        graph_ratio = TGraphAsymmErrors((curve / ratio_reference).hist)
        # take over the actual relative statistical uncertainties to show in the ratio plot
        EfficiencyPlotter._set_uncertainties(graph_ratio,
                                             uncertainty_up = relative_stat_unc_up,
                                             uncertainty_down = relative_stat_unc_down)
        graph_ratio.SetLineColor(ROOT.kBlack)
        graph_ratio.SetMarkerStyle(20)
        graph_ratio.SetMarkerSize(3.5)
        graph_ratio.SetLineWidth(1)
        graph_ratio.SetMarkerColor(ROOT.kBlack)

        # prepare the graph showing the total relative uncertainty band
        graph_unc_rel = graph_ratio.Clone()
        graph_unc_rel = EfficiencyPlotter._set_uncertainties(graph_unc_rel,
                                                             uncertainty_up = rel_uncertainty_up,
                                                             uncertainty_down = rel_uncertainty_down)
        graph_unc_rel.SetFillStyle(1001)
        graph_unc_rel.SetLineWidth(0)
        graph_unc_rel.SetLineColor(0)
        graph_unc_rel.SetFillColor(uncertainty_band_color)
        graph_unc_rel.SetMarkerColor(uncertainty_band_color)

        mg_rel = TMultiGraph()
        mg_rel.Add(graph_unc_rel, "5")

        # if want the relative variations plotted, add them here
        if variations:
            # go through each group of variations
            for group in variations:
                group_name = group[0]
                group_color = group[1]
                group_variations = group[2]

                for cur_var in group_variations:
                    cur_unc = RelUncertainty.from_variation(curve, cur_var) # compute the relative difference of the variation w.r.t. the reference curve for the ratio

                    cur_graph = TGraphErrors((cur_unc + 1).hist)
                    cur_graph.SetLineWidth(1)
                    cur_graph.SetLineColor(group_color)
                    cur_graph.SetMarkerStyle(20)
                    cur_graph.SetMarkerColor(group_color)
                    cur_graph.SetMarkerSize(3.5)
            
                    mg_rel.Add(cur_graph, variations_style)

        mg_rel.Add(graph_ratio, "pZ")

        # -------------------------------------------
        # integrate all components onto the canvas
        # -------------------------------------------
        pad_abs.cd()
        mg_abs.Draw("alp")
        mg_abs.GetYaxis().SetRangeUser(abs_y_min, abs_y_max)
        #mg_abs.GetXaxis().SetRangeUser(20, 600)
        mg_abs.GetXaxis().SetLimits(20, 600)
        mg_abs.GetYaxis().SetTickLength(ticklength_y)
        mg_abs.GetXaxis().SetTickLength(2.5 * ticklength_x / (abs_y_max - abs_y_min))
        mg_abs.GetXaxis().SetTitle("p_{T} [GeV]")
        mg_abs.GetYaxis().SetTitle("b efficiency")
        #mg_abs.GetYaxis().SetTitleOffset(1.4)

        # adjust axis titles fonts and sizes
        mg_abs.GetYaxis().SetTitleFont(42)
        mg_abs.GetYaxis().SetTitleSize(0.035)

        mg_abs.GetXaxis().SetTitleFont(42)
        mg_abs.GetXaxis().SetTitleSize(0.035)

        mg_abs.GetYaxis().SetLabelFont(42)
        mg_abs.GetYaxis().SetLabelSize(0.035)

        mg_abs.GetXaxis().SetLabelFont(42)
        mg_abs.GetXaxis().SetLabelSize(0)

        ROOT.gPad.SetTicks()
        mg_abs.Draw("alp")
        leg_abs.Draw()

        if inner_label:
            inner_label_obj.DrawLatexNDC(0.12, 0.09, inner_label)

        if variations:
            leg_vars.Draw()

        pad_rel.cd()
        mg_rel.Draw("alp")
        mg_rel.GetYaxis().SetRangeUser(rel_y_min + eps, rel_y_max - eps)
        #mg_rel.GetXaxis().SetRangeUser(20, 600)
        mg_rel.GetXaxis().SetLimits(20, 600)
        mg_rel.GetYaxis().SetTickLength(ticklength_y)
        mg_rel.GetXaxis().SetTickLength(ticklength_x / (rel_y_max - rel_y_min))
        mg_rel.GetXaxis().SetTitle("p_{T} [GeV]")
        #mg_rel.GetXaxis().SetTitleOffset(3.0)
        mg_rel.GetYaxis().SetTitle("ratio")
        mg_rel.GetYaxis().SetTitleOffset(0.52)

        # adjust axis titles fonts and sizes
        mg_rel.GetYaxis().SetTitleFont(42)
        mg_rel.GetYaxis().SetTitleSize(0.07)

        mg_rel.GetXaxis().SetTitleFont(42)
        mg_rel.GetXaxis().SetTitleSize(0.07)

        mg_rel.GetYaxis().SetLabelFont(42)
        mg_rel.GetYaxis().SetLabelSize(0.07)

        mg_rel.GetXaxis().SetLabelFont(42)
        mg_rel.GetXaxis().SetLabelSize(0.07)
        ROOT.gPad.SetTicks()        
        mg_rel.Draw("alp")

        canv.cd()
#        astyle.ATLASLabel(0.12, 0.88, "Internal")

        # print the outer label
        if outer_label:
            plot_label = TLatex(0.12, 0.84, outer_label)
            plot_label.SetTextAlign(11)
            plot_label.SetTextFont(42)
            plot_label.SetTextSize(0.035)
            plot_label.Draw()

        # redraw the bounding box on top of everything else
        pad_rel.cd()
        ROOT.gPad.Update()
        ROOT.gPad.RedrawAxis()

        pad_abs.cd()
        ROOT.gPad.Update()
        ROOT.gPad.RedrawAxis()

        canv.SaveAs(outfile + ".pdf")
        canv.SaveAs(outfile + ".png")
        
        if saveROOTFile:
            canv.SaveAs(outfile + ".root")

    # take the uncertainties specified by an Uncertainty object and apply them to the graph
    @staticmethod
    def _set_uncertainties(graph, uncertainty_up, uncertainty_down):
        for ind in range(graph.GetN()):
            graph.SetPointEYhigh(ind, uncertainty_up.hist.GetBinContent(ind + 1) if uncertainty_up else 0.0)
            graph.SetPointEYlow(ind, -uncertainty_down.hist.GetBinContent(ind + 1) if uncertainty_down else 0.0)
        return graph

