import os, re
from argparse import ArgumentParser

import ROOT

from CDIInputImporter import CDIInputImporter
from RatioPlotter import RatioPlotter
from ColorPicker import ColorPicker
from RelUncertainty import RelUncertainty
from EnsureMonotonicity import make_monotonic

def get_CDI_uncertainty(CDI_in, unc_regex):
    """
    read a certain number of uncertainties from the CDI and return their combination
    """

    if isinstance(unc_regex, str):
        print("looking for {}".format(unc_regex))
        unc_regex = re.compile(unc_regex)

    available_systematics = CDIInputImporter._read_available_systematics(CDI_in)
    uncs = []
    print(available_systematics)

    for cur_syst in available_systematics:
        if unc_regex.match(cur_syst):
            print("matched {}".format(cur_syst))
            uncs.append(RelUncertainty(CDIInputImporter._read_systematics_values(CDI_in, cur_syst)))
            
    #print(RelUncertainty.from_quadrature_sum(uncs))
    return RelUncertainty.from_quadrature_sum(uncs)

def get_stat_uncertainty(CDI_in, unc_regex):
    """
    read a certain number of uncertainties from the CDI and return their combination
    """
    stat_unc_regex = "STAT_" + unc_regex
    if isinstance(stat_unc_regex, str):
        print("looking for {}".format(stat_unc_regex))
        stat_unc_regex = re.compile(stat_unc_regex)
    uncs = []

    available_systematics = CDIInputImporter._read_available_systematics(CDI_in)
    for cur_syst in available_systematics:
        if stat_unc_regex.match(cur_syst):
            print("matched {}".format(cur_syst))
            uncs.append(RelUncertainty(CDIInputImporter._read_systematics_values(CDI_in, cur_syst)))

    return RelUncertainty.from_quadrature_sum(uncs)


def get_CDI_extrapolation_uncertainty(CDI_in, unc_regex, refpt):
    """
    read a certain number of uncertainties from the CDI, subtract their value at the reference momentum to get the actual extrapolation uncertainty component, and return their combination
    """
    #print(refpt)

    def _force_zero_below(unc, ref_pt):
        print(ref_pt)
        for cur_bin_center in unc.get_bin_centers():
            if cur_bin_center < ref_pt:
                unc.set(cur_bin_center, 0.0)

        return unc

    if isinstance(unc_regex, str):
        print("looking for {}".format(unc_regex))
        unc_regex = re.compile(unc_regex)
            
    available_systematics = CDIInputImporter._read_available_systematics(CDI_in)
    #print(available_systematics)
    uncs = []

    for cur_syst in available_systematics:
        if unc_regex.match(cur_syst):
            print("matched {}".format(cur_syst))

            # compute the extrapolation uncertainty ...
            cur_unc = RelUncertainty(CDIInputImporter._read_systematics_values(CDI_in, cur_syst))
            #print("VALUE OF THE UNCERTAINTY BEFORE: " + str(cur_unc.evaluate(1000)))
            #print("VALUE AT 1000: " + str(cur_unc.evaluate(1000)) + " VALUE AT REFPT: " + str(cur_unc.evaluate(325)))
            cur_unc = cur_unc - cur_unc.evaluate(refpt)
            #print(cur_unc)
            #print("VALUE OF THE UNCERTAINTY AFTER: " + str(cur_unc.evaluate(1000)))

            #print(cur_unc.evaluate(1000))

            # ... and make sure it is indeed zero below the reference momentum
            _force_zero_below(cur_unc, refpt)

            # make sure to only look at its magnitude, i.e. do not care about the sign
            cur_unc = cur_unc.apply(func = lambda cur: abs(cur))

            # Ensure that the resulting single uncertainty component is monotonically increasing with pT
            cur_unc = make_monotonic(cur_unc, refpt)
            
            uncs.append(cur_unc)
            #print(cur_unc)

    # combine them in quadrature 
    i = 0
    for cur_unc in uncs:
        print("BEFORE FROM QUADRATURE: " + str(i) + " "+ str(cur_unc.evaluate(1000)) + "\n")
        i = i + 1

    #print("BEFORE FROM QUADRATURE: " + str(cur_unc.evaluate(1000)))
    extrap_unc = RelUncertainty.from_quadrature_sum(uncs) 
    #for xtrap_unc in extrap_unc:
    j = 0
    #print("AFTER FROM QUADRATURE: " + str(extrap_unc.evaluate(1000)))

    #print("EXTRAPOLATION UNCERTAINTY HERE::")
    #print(extrap_unc.evaluate(1000))

    return extrap_unc

def create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, uncs_colors, is_VR, autorange = False, pT_max = 3000, PCBT_istrue = "False"):
    
    if PCBT_istrue == "True":
        WP_descriptions = {
                "PCBTBEff_100_85": "Pseudo-continuous, 85% < #epsilon_{b}^{MC} < 100%",
                "PCBTBEff_85_77": "Pseudo-continuous, 77% < #epsilon_{b}^{MC} < 85%",
                "PCBTBEff_77_70": "Pseudo-continuous, 70% < #epsilon_{b}^{MC} < 77%",
                "PCBTBEff_70_60": "Pseudo-continuous, 60% < #epsilon_{b}^{MC} < 70%",
                "PCBTBEff_60_0": "Pseudo-continuous, 60% < #epsilon_{b}^{MC} < 0%"
            }
    else:
        WP_descriptions = {
                "FixedCutBEff_60": "Fixed Cut, #epsilon_{b}^{MC} = 60%",
                "FixedCutBEff_70": "Fixed Cut, #epsilon_{b}^{MC} = 70%",
                "FixedCutBEff_77": "Fixed Cut, #epsilon_{b}^{MC} = 77%",
                "FixedCutBEff_85": "Fixed Cut, #epsilon_{b}^{MC} = 85%"
            }
    jet_type_descriptions = {
        "bottom": "b-jets", 
        "charm": "c-jets", 
        "light": "l-jets" 
    }

    jetcoll_descriptions = {
        "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903": "variable-radius track jets",
        "AntiKt4EMPFlowJets": "AntiKt4EMPFlowJets",
        "EMPFlow": "AntiKt4EMPFlowJets",
        "AntiKt4EMPFlowJets_BTagging201903": "particle flow jets"        
    }

    if PCBT_istrue == "True":
        axis_yranges = {

        "charm": {
            "PCBTBEff_100_85":  (0.0, 0.1),
            "PCBTBEff_85_77":  (0.0, 0.1),
            "PCBTBEff_77_70":  (0.0, 0.1),
            "PCBTBEff_70_60":  (0.0, 0.1),
            "PCBTBEff_60_0":  (0.0, 0.1)
        },
        "light": {
            "PCBTBEff_100_85":  (0.0, 1),
            "PCBTBEff_85_77":  (0.0, 1),
            "PCBTBEff_77_70":  (0.0, 1),
            "PCBTBEff_70_60":  (0.0, 1),
            "PCBTBEff_60_0":  (0.0, 1)
        },
        "bottom": {
            "PCBTBEff_100_85":  (0.0, 1),
            "PCBTBEff_85_77":  (0.0, 1),
            "PCBTBEff_77_70":  (0.0, 1),
            "PCBTBEff_70_60":  (0.0, 1),
            "PCBTBEff_60_0":  (0.0, 1)
        }
        }
    else:
        axis_yranges = {
            "charm": {
                "FixedCutBEff_60": (0.0, 1),
                "FixedCutBEff_70": (0.0, 1),
                "FixedCutBEff_77": (0.0, 1),
                "FixedCutBEff_85": (0.0, 1)
            },
            "light": {
                "FixedCutBEff_60": (0.0, 0.5),
                "FixedCutBEff_70": (0.0, 0.5),
                "FixedCutBEff_77": (0.0, 0.5),
                "FixedCutBEff_85": (0.0, 0.5)
            },
            "bottom": {
                "FixedCutBEff_60": (0.0, 1),
                "FixedCutBEff_70": (0.0, 1),
                "FixedCutBEff_77": (0.0, 1),
                "FixedCutBEff_85": (0.0, 1)
            }
        }

    uncs = [get_CDI_uncertainty(CDI_in, cur_regex) for cur_regex in unc_regexes]
    #rel_uncertainties_up = [get_stat_uncertainty(CDI_in, cur_regex) for cur_regex in unc_regexes]
    #rel_uncertainties_down = [-get_stat_uncertainty(CDI_in, cur_regex) for cur_regex in unc_regexes]

    extrap_uncs = [get_CDI_extrapolation_uncertainty(CDI_in, cur_regex, refpt) for cur_regex in unc_regexes]
    #print(refpt)
    #overall_extrap_unc =  get_CDI_extrapolation_uncertainty(CDI_in, "TRK_*", refpt)
   # print(overall_extrap_unc.evaluate(1000))
    calib_name, final_state, tagger, WP, jetcoll = CDIInputImporter._read_header(CDI_in)
    print(WP)
    WP_description = WP_descriptions[WP]
    id_string = "#sqrt{s} = 13 TeV, " + jetcoll_descriptions[jetcoll]
    inner_label = "#splitline{" + id_string + "}{" + tagger + ", " + WP_description + ", "+ jet_type_descriptions[final_state] +  "}"

    #xaxis_range = (refpt * 1.12, 3000)
    pT_max = 3000

    xaxis_range = (refpt, pT_max)
    # xaxis_range = (250, pT_max)
    #xaxis_range = (250, 3000)
    maxunc = max([cur_unc.max() for cur_unc in uncs if cur_unc is not None])
    #print(maxunc)
    #print(uncs[4].evaluate(refpt))
    #print(unc_value)
    if autorange:
        yaxis_range = (0, maxunc * 2)
    else:
        yaxis_range = axis_yranges[final_state][WP]

    #xlabel = "p_{T, had} [GeV]" if is_VR else "p_{T, jet} [GeV]"
    xlabel = "p_{T} [GeV]"
    #central_values = CDIInputImporter._read_central_values(CDI_in)

    # plot the raw uncertainties
    RatioPlotter.create_ratio_plot(curves = uncs,
				   rel_uncertainties_up = [None],
                                   rel_uncertainties_down = [None],
				   labels = unc_labels,
                                   yaxis_range_abs = yaxis_range,
                                   xaxis_range = xaxis_range,
                                   outfile = outfile + "_uncs",
                                   curve_styles = ["" for unc in uncs],
                                   x_label = xlabel,
                                   y_label = "rel. uncertainty",
                                   y_ratio_label = "",
                                   legpos= 'topleft',
                                   marker_colors = uncs_colors,
                                   inner_label = inner_label,
                                   show_ratio = False)    

    # also plot the actual extrapolation uncertainties, which have the respective values at the reference momentum subtracted
    #print(extrap_uncs)
    RatioPlotter.create_hist_plot(curves = extrap_uncs,
                                  labels = unc_labels,
                                  colors = uncs_colors,
                                  inner_label = inner_label,
                                  outfile = outfile + "_extrap_uncs",
                                  x_label = xlabel,
                                  y_range = yaxis_range,
                                  x_range = xaxis_range)

def full_plot(CDI_in, outdir, refpt, plot_all, pT_max = 3000, overall_regex = "TRK_*"):
    #extrap_uncs = [get_CDI_extrapolation_uncertainty(CDI_in, cur_regex, refpt) for cur_regex in unc_regexes]
    overall_unc = [get_CDI_extrapolation_uncertainty(CDI_in, overall_regex, refpt)]
    central_values = [CDIInputImporter._read_central_values(CDI_in)]

    RatioPlotter.create_ratio_plot(curves = [central_values],
                                        rel_uncertainties_up = [-overall_unc],
                                        rel_uncertainties_down = [overall_unc], 
                                        labels = ["Total Tracking"],
                                        marker_colors = [cp.get_color("black")],
                                        uncertainty_fill_colors = [cp.get_color("greenblue1")],
                                        uncertainty_styles = ["5"],
                                        ratio_reference = central_values,
                                        outfile = os.path.join(output_dir, syst),
                                        inner_label = "{}, {}, {}".format(calib_name, final_state, tagger),
                                        outer_label = os.path.splitext(os.path.basename(input_file_path))[0],
                                        x_label = "p_{T} [GeV]",
                                        y_label = "SF_{b}",
                                        y_ratio_label = "ratio",
                                        yaxis_range_abs = yaxis_range,
                                        yaxis_range_rel = (0.9, 1.1),
                                        xaxis_range = xaxis_range,
                                        show_ratio = False)


def plot_CDI_uncertainties(CDI_in, outdir, refpt, plot_all, is_VR, pT_max, PCBT_istrue):
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    cp = ColorPicker()


#  FTAG Plenary Talk 12/06/23
# ********* Track Uncertainty Plotting Groups ***********
# Bias Modes -- omitted Z0 due to being massive, will plot on it's own
    """

    unc_regexes = ["TRK_BIAS_D0_WM", "TRK_BIAS_QOVERP_SAGITTA_WM", "TRK_BIAS*"]
    unc_labels  = ["TRK_BIAS_D0_WM", "TRK_BIAS_QOVERP_SAGITTA_WM", "Total Bias Uncertainties"]
    unc_colors = [cp.get_color("atlasorange1"), cp.get_color("greenblue3")]
    outfile = os.path.join(outdir, "TRK_bias")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)

# Resolution
    unc_regexes = ["TRK_RES_Z0_MEAS","TRK_RES_D0_DEAD","TRK_RES_Z0_DEAD","TRK_RES*"]
    unc_labels  = ["TRK_RES_Z0_MEAS","TRK_RES_D0_DEAD","TRK_RES_Z0_DEAD","TRK_RES*"]
    unc_colors = [cp.get_color("greenblue3"), cp.get_color('blue1'), cp.get_color("orange3"), cp.get_color("black")]
    outfile = os.path.join(outdir, "TRK_res")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)

# Efficiency LOOSE
    unc_regexes = ["TRK_EFF_LOOSE_GLOBAL","TRK_EFF_LOOSE_IBL","TRK_EFF_LOOSE_PHYSMODEL","TRK_EFF_LOOSE_PP0","TRK_EFF_LOOSE_TIDE","TRK_EFF*"]
    unc_labels  = ["TRK_EFF_LOOSE_GLOBAL","TRK_EFF_LOOSE_IBL","TRK_EFF_LOOSE_PHYSMODEL","TRK_EFF_LOOSE_PP0","TRK_EFF_LOOSE_TIDE","Total track efficiency uncertainties"]
    unc_colors = [cp.get_color("blue1"), cp.get_color("green2"), cp.get_color("orange3"), cp.get_color("greenblue3"),  cp.get_color("greenyellow1"),cp.get_color("black")]
    outfile = os.path.join(outdir, "TRK_eff")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    
# Fake Rate
    unc_regexes = ["TRK_FAKE_RATE_LOOSE", "TRK_FAKE_RATE_LOOSE_TIDE", "TRK_FAKE*"]
    unc_labels  = ["TRK_FAKE_RATE_LOOSE", "TRK_FAKE_RATE_LOOSE_TIDE", "Total uncertainty from fakes"]
    unc_colors = [cp.get_color("gray"), cp.get_color("greenblue3"), cp.get_color("black")]
    outfile = os.path.join(outdir, "TRK_fakes")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)

# Total Tracking Uncertainties
    unc_regexes = ["TRK_*"]
    unc_labels = ["Total Tracking Uncertainties"]
    unc_colors = [cp.get_color("green2")]
    outfile = os.path.join(outdir, "TRK_Total")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    
# ********* JET Uncertainty Plotting Groups ***********
# GroupedNP
    
    unc_regexes = ["JET_GroupedNP_1","JET_GroupedNP_2","JET_GroupedNP_3","JET_Grouped*"]
    unc_labels  = ["GroupedNP_1","GroupedNP_2","GroupedNP_3","Total Grouped"]
    unc_colors = [cp.get_color("gray"), cp.get_color("greenblue3"), cp.get_color("orange3"), cp.get_color("black")]
    outfile = os.path.join(outdir, "JET_Grouped_NP")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
# JES Components
    unc_regexes = ["JET_EffectiveNP_1","JET_EffectiveNP_2","JET_EffectiveNP_3","JET_EffectiveNP_4","JET_EffectiveNP_5","JET_EffectiveNP_6","JET_EffectiveNP_7","JET_EffectiveNP_8restTerm", "JET_EffectiveNP*"]
    unc_labels = ["EffectiveNP_1","EffectiveNP_2","EffectiveNP_3","EffectiveNP_4","EffectiveNP_5","EffectiveNP_6","EffectiveNP_7","EffectiveNP_8", "Total Effective JES"]
    unc_colors = [cp.get_color("gray"), cp.get_color("blue1"), cp.get_color("green2"), cp.get_color("orange3"), cp.get_color("greenblue3"), cp.get_color("black"), cp.get_color("red1"), cp.get_color("greenyellow1"), cp.get_color("greenyellow1")]
    outfile = os.path.join(outdir, "JET_JES_EffectiveNP")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)

#JER Components
    unc_regexes = ["JET_JER_EffectiveNP_1","JET_JER_EffectiveNP_2","JET_JER_EffectiveNP_3","JET_JER_EffectiveNP_4","JET_JER_EffectiveNP_5","JET_JER_EffectiveNP_6","JET_JER_EffectiveNP_7restTerm", "JET_JER_EffectiveNP*"]
    unc_labels = ["JER_EffectiveNP_1","JER_EffectiveNP_2","JER_EffectiveNP_3","JER_EffectiveNP_4","JER_EffectiveNP_5","JER_EffectiveNP_6","JER_EffectiveNP_7restTerm", "Total Effective JER"]
    unc_colors = [cp.get_color("gray"), cp.get_color("blue1"), cp.get_color("green2"), cp.get_color("orange3"), cp.get_color("greenblue3"), cp.get_color("black"), cp.get_color("greenyellow1"), cp.get_color("greenyellow1")]
    outfile = os.path.join(outdir, "JET_JER_EffectiveNP")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)

#Eta Intercalibration
    unc_regexes = ["JET_EtaIntercalibration_Modelling", "JET_EtaIntercalibration_TotalStat", "JET_EtaIntercalibration_NonClosure_PreRec", "JET_EtaIntercalibration*"]
    unc_labels = ["EtaIntercalibration_Modelling", "EtaIntercalibration_TotalStat", "EtaIntercalibration_NonClosure_PreRec", "Total EtaIntercalibration"]
    unc_colors = [cp.get_color("gray"), cp.get_color("greenblue3"), cp.get_color("orange3"), cp.get_color("black")]
    outfile = os.path.join(outdir, "JET_Eta-intercalibration")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)

#Pileup
    unc_regexes = ["JET_Pileup_OffsetMu", "JET_Pileup_OffsetNPV", "JET_Pileup_PtTerm","JET_Pileup_RhoTopology","JET_Pileup_*"]
    unc_labels = ["Pileup_OffsetMu", "Pileup_OffsetNPV", "Pileup_PtTerm","Pileup_RhoTopology","Total Pileup"]
    unc_colors = [cp.get_color("gray"), cp.get_color("greenblue3"), cp.get_color("orange3"), cp.get_color("black"), cp.get_color("blue1")]
    outfile = os.path.join(outdir, "JET_Pileup")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)

#Flavour
    unc_regexes = ["JET_Flavour_Composition","JET_Flavour_Response", "JET_BJES_Response"]
    unc_labels = ["Flavour_Composition","Flavour_Response", "BJES_Response"]
    unc_colors = [cp.get_color("blue1")]
    outfile = os.path.join(outdir, "JET_Flavour")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)

#PunchThrough
    unc_regexes = ["JET_PunchThrough_MC16"]
    unc_labels = ["PunchThrough_MC16"]
    unc_colors = [cp.get_color("blue1")]
    outfile = os.path.join(outdir, "JET_PunchThrough")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    
#PreRec JES
    unc_regexes = ["JET_InSitu_NonClosure_PreRec","JET_JESUnc_mc20vsmc21_MCTYPE_PreRec", "JET_JESUnc_Noise_PreRec", "JET_JESUnc_VertexingAlg_PreRec"]
    unc_labels = ["Insitu_NonClosure_PreRec","JESUnc_mc20vsmc21_MCTYPE_PreRec", "JESUnc_Noise_PreRec", "JESUnc_VertexingAlg_PreRec"]
    unc_colors = [cp.get_color("gray"), cp.get_color("greenblue3"), cp.get_color("blue1"), cp.get_color("black")]
    outfile = os.path.join(outdir, "JET_PreRecJES")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)

#PreRec JER
    
    unc_regexes = ["JET_JERUnc_mc20vsmc21_MCTYPE_PreRec", "JET_JERUnc_Noise_PreRec"]
    unc_labels =  ["JERUnc_mc20vsmc21_MCTYPE_PreRec", "JERUnc_Noise_PreRec"]
    unc_colors = [cp.get_color("blue1"),cp.get_color("gray")]
    outfile = os.path.join(outdir, "JET_PreRecJER")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    
# SingleParticle High-pT
    
    unc_regexes = ["JET_SingleParticle_HighPt"]
    unc_labels = ["SingleParticle_HighPt"]
    unc_colors = [cp.get_color("blue1")]
    outfile = os.path.join(outdir, "JET_SingleParticle")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    
# Total Jet Uncertainties
    unc_regexes = ["JET_*"]
    unc_labels = ["Total Jet Uncertainties"]
    unc_colors = [cp.get_color("blue1")]
    outfile = os.path.join(outdir, "JET_Total")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    
# ********* Modelling Uncertainty Plotting Groups ***********
# Total Modelling Uncertainties
# PS
    
    unc_regexes = ["MC_PS"]
    unc_labels = ["Pythia8 v Herwig7 PS uncertainty"]
    unc_colors = [cp.get_color("blue1")]
    outfile = os.path.join(outdir, "MOD_PS")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    

# FSR
    
    unc_regexes = ["MC_FSR"]
    unc_labels = ["FSR Uncertainty"]
    unc_colors = [cp.get_color("blue1")]
    outfile = os.path.join(outdir, "MOD_FSR")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
# MC21 test
    
    unc_regexes = ["TRK_RES_D0_MEAS"]
    unc_labels = ["Track Resolution"]
    unc_colors = [cp.get_color("blue1")]
    outfile = os.path.join(outdir, "D0 Res")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    
    unc_regexes = ["MC_FSR_1_175","MC_FSR_1_15","MC_FSR_1_125","MC_FSR_1_0625","MC_FSR_1_075","MC_FSR_1_0875","MC_FSR_1_2","MC_FSR_1_05"]
    unc_labels = ["MC_ISR_1_FSR_1.75","MC_ISR_1_FSR_1.5","MC_ISR_1_FSR_1.25","MC_ISR_1_FSR_0.625","MC_ISR_1_FSR_0.75","MC_ISR_1_FSR_0.875","MC_ISR_1_FSR_2.0","MC_ISR_1_FSR_0.5"]
    unc_colors = [cp.get_color("gray"), cp.get_color("blue1"), cp.get_color("green2"), cp.get_color("orange3"), cp.get_color("greenblue3"), cp.get_color("black"), cp.get_color("red1"), cp.get_color("greenyellow1")]
    outfile = os.path.join(outdir, "FRS_all")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    """
    unc_regexes = ["MC_PS", "TRK_*", "[TRK/MC]_*"]
    unc_labels = ["Parton Shower", "Tracking", "Total"]
    unc_colors = [cp.get_color("green2"),cp.get_color("orange3"), cp.get_color("black")]
    outfile = os.path.join(outdir, "overall")
    create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, unc_colors, is_VR, autorange = False, pT_max = pT_max)
    
    if plot_all:
        for cur_syst in CDIInputImporter._read_available_systematics(CDI_in):
            unc_regexes = ["^" + cur_syst + "$"]
            unc_labels = [cur_syst]
            unc_colors = [cp.get_color("orange3")]
            outfile = os.path.join(outdir, cur_syst)
            create_plot(CDI_in, outfile, refpt, unc_regexes, unc_labels, is_VR, unc_colors, pT_max = pT_max)

if __name__ == "__main__":
    ROOT.gROOT.SetBatch()

    parser = ArgumentParser(description = "produce summary plots showing the different uncertainty components in the CDI input file")
    parser.add_argument("--CDI_in", action = "store", dest = "CDI_in")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    parser.add_argument("--refpt", action = "store", dest = "refpt", type = float, default = 325) # 150 for VRTrackJets, 300 for calorimeter jets, 325 for pflow jets
    parser.add_argument("--pT_max", action = "store", dest = "pT_max", type = float, default = 3000)
    parser.add_argument("--plot_all", action = "store_const", const = True, default = False)
    parser.add_argument("--is_VR", action = "store_const", const = True, default = False)
    parser.add_argument('-P', '--PCBT_istrue', default=False, help="Please specify which scale factor scheme: False=Fixed cut, True=Pseudo-continuous")

    args = vars(parser.parse_args())

    plot_CDI_uncertainties(**args)
