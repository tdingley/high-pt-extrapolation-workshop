#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# setup the python environment needed for the CDITools
lsetup "lcgenv -p LCG_94 x86_64-$ALRB_USER_PLATFORM-gcc62-opt Python"
lsetup "lcgenv -p LCG_94 x86_64-$ALRB_USER_PLATFORM-gcc62-opt numpy"
lsetup "lcgenv -p LCG_94 x86_64-$ALRB_USER_PLATFORM-gcc62-opt scipy"
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
