import ROOT
from ROOT import TH1D
from TFileUtils import TFileUtils
from array import array
import math, re
import numpy as np
import uuid

# this class wraps ROOT histograms and makes them appear as "curves"
class Curve(object):

    def __init__(self, hist, name = None, bootstrap_replicas = {}):
        assert isinstance(bootstrap_replicas, dict) # make sure we get a dictionary here!
        print(self)
        self.hist = hist
        self.bootstrap_replicas = bootstrap_replicas

        if name is not None:
            self.name = name
        else:
            self.name = self.hist.GetName()

        self.cached_uncertainty_obj = None

    # re-initializes this curve to the properties of the given curve
    def reinit(self, curve):
        self.hist = curve.hist
        self.bootstrap_replicas = curve.bootstrap_replicas

        if curve.name is not None:
            self.name = curve.name

    def merge_bins(self, merge_start, merge_end, merge_method = None):
        # take care of the default bin merging method
        from MapUtils import MapUtils
        if not merge_method:
            merge_method = MapUtils.merge_bins        

        # need to coherently merge the bins in the nominal histogram as well as in
        # all stored bootstrap replicas
        self.hist = merge_method(self.hist, merge_start, merge_end)

        for rep_name in self.bootstrap_replicas.keys():
            self.bootstrap_replicas[rep_name] = merge_method(self.bootstrap_replicas[rep_name], merge_start, merge_end)

    @classmethod
    def from_array(cls, bin_edges, bin_contents, bin_errors = None, name = None):
        hist = TH1D(name + "_hist", name, len(bin_edges) - 1, array('d', bin_edges))

        if not bin_errors:
            bin_errors = [0.0 for bin in bin_contents]

        # set the values of the newly created histogram
        #print(bin)
        for bin, (bin_content, bin_error) in enumerate(zip(bin_contents, bin_errors)):
            hist.SetBinContent(bin + 1, bin_content)
            hist.SetBinError(bin + 1, bin_error)

        # create the Curve object
        obj = cls(hist = hist, name = name)
        return obj

    @classmethod
    def from_file(cls, file_path, histname, name = None, binrange = None, loose_matching = True, ignore_bootstrap = False):
        
        def _restrict_binrange(hist, binrange):
            # extract the edges of the selected bins
            edges = []
            for bin in binrange + [binrange[-1] + 1]:
                edges.append(hist.GetBinLowEdge(bin))

            # construct a new histogram with only the selected bins
            print("created {}".format(hist.GetName()))
            hist_selected = TH1D(hist.GetName() + str(uuid.uuid4()), hist.GetName() + str(uuid.uuid4()), len(edges) - 1, array('d', edges))
            ROOT.SetOwnership(hist_selected, False)

            # fill the newly created histogram
            for bin in binrange:
                hist_selected.SetBinContent(bin, hist.GetBinContent(bin))
                hist_selected.SetBinError(bin, hist.GetBinError(bin))

            return hist_selected
            
        print("looking for histogram: {} {}".format(file_path, histname))
        print(histname)
        cands = TFileUtils.get_hist_candidates(infile = file_path, histname = histname)
        print("found the following candidates: {}".format(cands))

        if ignore_bootstrap:
            if len(cands) > 1:
                cands = [cand for cand in cands if "_bootstrap_0" in cand]

        if len(cands) == 1:
            print("found unique candidate for histogram, no bootstrap replicas available")

        # load all histograms
        bootstrap_replicas = {}
        nominal_hist = None
        print("CANDIDATES HERE:",cands)

        for cand in cands:
            hist = TFileUtils.get_hist(infile = file_path, histname = cand, loose_matching = False)
            

            if "_bootstrap_0" in cand or len(cands) == 1:
                print(len(cands), cand, "it is here")
                nominal_hist = hist
                print("found nominal: {} {}".format(file_path, cand))


            else:
                # extract the name of the bootstrap object
                print("found not nominal: {} {}".format(file_path, cand))

                be = re.compile(".*(bootstrap_.*)")
                m = be.match(cand)
                bootstrap_name = m.group(1)
                print("extracted bootstrap_name = {}".format(bootstrap_name))
                bootstrap_replicas[bootstrap_name] = hist
        
        # must have found some nominal histogram (whether from bootstraps or not), otherwise 
        # nothing in the following is going to make sense
        assert nominal_hist 

        # check if the loaded histograms need to be restricted to a certain bin range
        if binrange:
            nominal_hist = _restrict_binrange(nominal_hist, binrange)

            for rep_name in bootstrap_replicas.keys():
                bootstrap_replicas[rep_name] = _restrict_binrange(bootstrap_replicas[rep_name], binrange)

        obj = Curve(hist = nominal_hist, name = name, bootstrap_replicas = bootstrap_replicas)
        return obj

    @classmethod
    def from_data(cls, file_path, name = None):
        from configparser import ConfigParser
        # first, read the file and construct a histogram from the stored data
        config = ConfigParser()
        config.read(file_path)

        bin_edges = []
        bin_values = []
        bin_uncs = []
        for section_name in config:
            if "bin" in section_name:
                section = config[section_name]
                bin_edges.append(float(section["low"]))
                bin_values.append(float(section["val"]))
                bin_uncs.append(float(section["unc"]))
        bin_edges.append(float(section["up"]))

        bin_edges = sorted(bin_edges)

        hist = TH1D("eff_hist", "eff_hist", len(bin_edges) - 1, array('d', bin_edges))
        ROOT.SetOwnership(hist, False)

        # now set the bin contents
        for ind, (bin_value, bin_unc) in enumerate(zip(bin_values, bin_uncs)):
            hist.SetBinContent(ind + 1, bin_value)
            hist.SetBinError(ind + 1, bin_unc)
        
        # then, construct a Curve in the usual way
        obj = cls(hist = hist, name = name)
        return obj

    def to_csv(self, outfile_path):
        xvals = self.get_bin_centers()
        lvals = self.get_bin_l_edges()
        rvals = self.get_bin_u_edges()
        yvals = [self.evaluate(xval) for xval in xvals]

        with open(outfile_path, 'w') as outfile:
            for xval, yval, lval, rval in zip(xvals, yvals, lvals, rvals):
                outfile.write("{}, {}, {}, {}\n".format(lval, rval, xval, yval))            

    # mathematical operations that can be performed on a Curve and preserve the associated uncertainties
    # if available, these operations will also propagate the bootstrap replicas correctly
    def __add__(self, other):
        cur_hist = self.hist.Clone()
        cur_replicas = {name: rep.Clone() for name, rep in self.bootstrap_replicas.items()}
        if isinstance(other, Curve):
            cur_hist.Add(other.hist)

            if len(cur_replicas) == len(other.bootstrap_replicas):
                for name in cur_replicas.keys():
                    cur_replicas[name].Add(other.bootstrap_replicas[name])
            else:
                # these curves have different numbers of bootstrap replicas, i.e. they are fundamentally incompatible
                # do not attempt to keep the bootstraps up-to-date, but just mark them as being unavailable
                cur_replicas = {}
                
        elif isinstance(other, (int, float, long)):
            for hist in [cur_hist] + cur_replicas.values():
                for bin in range(hist.GetSize()):
                    hist.AddBinContent(bin, other)

        return Curve(cur_hist, bootstrap_replicas = cur_replicas)

    def __sub__(self, other):
        cur_hist = self.hist.Clone()
        cur_replicas = {name: rep.Clone() for name, rep in self.bootstrap_replicas.items()}
        if isinstance(other, Curve):
            cur_hist.Add(other.hist, -1)

            if len(cur_replicas) == len(other.bootstrap_replicas):
                for name in cur_replicas.keys():
                    cur_replicas[name].Add(other.bootstrap_replicas[name], -1)
            else:
                # these curves have different numbers of bootstrap replicas, i.e. they are fundamentally incompatible
                # do not attempt to keep the bootstraps up-to-date, but just mark them as being unavailable
                cur_replicas = {}

        elif isinstance(other, (int, float, long)):
            for hist in [cur_hist] + cur_replicas.values():
                for bin in range(hist.GetSize()):
                    hist.AddBinContent(bin, -other)

        return Curve(cur_hist, bootstrap_replicas = cur_replicas)

    def __mul__(self, other):
        cur_hist = self.hist.Clone()
        cur_replicas = {name: rep.Clone() for name, rep in self.bootstrap_replicas.items()}
        if isinstance(other, Curve):
            cur_hist.Multiply(other.hist)

            if len(cur_replicas) == len(other.bootstrap_replicas):
                for name in cur_replicas.keys():
                    cur_replicas[name].Multiply(other.bootstrap_replicas[name])
            else:
                # these curves have different numbers of bootstrap replicas, i.e. they are fundamentally incompatible
                # do not attempt to keep the bootstraps up-to-date, but just mark them as being unavailable
                cur_replicas = {}
                
        elif isinstance(other, (int, float, long)):
            for hist in [cur_hist] + cur_replicas.values():
                hist.Scale(other)

        return Curve(cur_hist, bootstrap_replicas = cur_replicas)

    def __truediv__(self, other):
        return self.__div__(other)

    def __div__(self, other):
        cur_hist = self.hist.Clone()
        cur_replicas = {name: rep.Clone() for name, rep in self.bootstrap_replicas.items()}
        if isinstance(other, Curve):
            cur_hist.Divide(other.hist)

            if len(cur_replicas) == len(other.bootstrap_replicas):
                for name in cur_replicas.keys():
                    cur_replicas[name].Divide(other.bootstrap_replicas[name])
            else:
                # these curves have different numbers of bootstrap replicas, i.e. they are fundamentally incompatible
                # do not attempt to keep the bootstraps up-to-date, but just mark them as being unavailable
                cur_replicas = {}

        elif isinstance(other, (int, long, float)):
            for hist in [cur_hist] + cur_replicas.values():
                hist.Scale(1 / other)

        return Curve(cur_hist, bootstrap_replicas = cur_replicas)

    def __neg__(self):
        return self.__mul__(-1)

    # easy-access
    def __getitem__(self, key):
        return self.hist.GetBinContent(key + 1)

    def __setitem__(self, key, value):
        self.hist.SetBinContent(key + 1, value)

    def size(self):
        return self.hist.GetSize()

    # copy over the binning from 'other', but retain the values from 'self'
    def rebin_virtual_from(self, other, ignore_bootstrap_replicas = False):
        print(type(other))
        tmp_eff = other.clone(self.name)

        assert len(self.bootstrap_replicas) == len(tmp_eff.bootstrap_replicas)

        # loop over the bin centers and replace their values and uncertainties correspondingly
        for bin_center in tmp_eff.get_bin_centers():
            print("{}: {} +/- {}".format(bin_center, self.evaluate(bin_center), self.evaluate_uncertainty(bin_center)))
            tmp_eff.set(bin_center, self.evaluate(bin_center))
            tmp_eff.set_uncertainty(bin_center, self.evaluate_uncertainty(bin_center))
            
            # also do the same for the bootstrap replicas
            for rep_name in self.bootstrap_replicas.keys():
                bin = tmp_eff.hist.FindBin(bin_center)
                tmp_eff.bootstrap_replicas[rep_name].SetBinContent(bin, self.bootstrap_replicas[rep_name].GetBinContent(bin))
                tmp_eff.bootstrap_replicas[rep_name].SetBinError(bin, self.bootstrap_replicas[rep_name].GetBinError(bin))
            
        return tmp_eff

    def get_bin_centers(self):
        l_edges = self.get_bin_l_edges()
        u_edges = self.get_bin_u_edges()

        centers = [0.5 * (l_edge + u_edge) for l_edge, u_edge in zip(l_edges, u_edges)]

        return centers

    # returns the lower bin edges of this curve
    def get_bin_l_edges(self):
        l_edges = []
        for bin in range(1, self.size()):
            l_edges.append(self.hist.GetBinLowEdge(bin))
        print(l_edges)
        return l_edges[:-1]

    # returns the upper bin edges of this curve
    def get_bin_u_edges(self):
        l_edges = []
        for bin in range(1, self.size()):
            l_edges.append(self.hist.GetBinLowEdge(bin))

        return l_edges[1:]

    # allows to apply an arbitrary function elementwise to this instance
    # Note: as of now, this will break the propagation of uncertainties!
    def apply(self, func, error_prop = None):
        retval = self.clone()
        for bin in range(retval.hist.GetSize()):
            retval.hist.SetBinContent(bin, func(self.hist.GetBinContent(bin)))

            # if demanded, also update the uncertainty on the nominal curve
            if error_prop:
                retval.hist.SetBinError(bin, error_prop(self.hist.GetBinContent(bin), self.hist.GetBinError(bin)))
            else:
                
                retval.hist.SetBinError(bin, 0.0)
            
            # also apply the function to all the bootstrap replicas
            for name, replica in retval.bootstrap_replicas.items():
                replica.SetBinContent(bin, func(replica.GetBinContent(bin)))

        return retval
    
    # returns the maximum value of this curve in its domain
    def max(self):
        conts = []
        for bin in range(1, self.hist.GetSize()):
            conts.append(self.hist.GetBinContent(bin))
        return max(conts)

    def min(self):
        conts = []
        for bin in range(1, self.hist.GetSize()):
            conts.append(self.hist.GetBinContent(bin))
        return min(conts)

    def get_bin_center(self, bin):
        centers = self.get_bin_centers()
        return centers[bin]

    def get_bin_index(self, x_value):
        return self.hist.FindBin(x_value) - 1

    # return the number of meaningful bins, i.e. excluding over- and underflow bins
    def get_number_bins(self):
        return self.hist.GetSize() - 2

    def evaluate_uncertainty(self, x):
        unc_obj = self.create_uncertainty_obj()
        return unc_obj.evaluate(x)

    def evaluate_uncertainty_bin(self, bin):
        return self.hist.GetBinError(bin)

    def remove_uncertainty(self):
        for bin in range(1, self.hist.GetSize()):
            self.hist.SetBinError(bin, 0.0)

    def evaluate(self, x):
        return self.hist.GetBinContent(self.hist.FindBin(x))

    def evaluate_bin(self, bin):
        return self.hist.GetBinContent(bin)

    def set(self, x, value):
        self.hist.SetBinContent(self.hist.FindBin(x), value)

    def set_bin(self, bin, value):
        self.hist.SetBinContent(bin, value)

    def set_uncertainty(self, x, uncertainty):
        self.hist.SetBinError(self.hist.FindBin(x), uncertainty)

    def set_value_uncertainty(self, x, value, uncertainty):
        self.set(x, value)
        self.set_uncertainty(x, uncertainty)

    def set_value_uncertainty_bin(self, bin, value, uncertainty):
        self.hist.SetBinContent(bin, value)
        self.hist.SetBinError(bin, uncertainty)

    # stops the bootstrap coherence and uses it to evaluate the uncertainties
    def set_uncertainty_from_bootstrap(self):

        # print("+++ set_uncertainty_from_bootstrap +++")
        # print("current SOW uncertainties")
        # self.dump()

        if len(self.bootstrap_replicas) > 0:
            unc = self.create_uncertainty_obj()

            for bin in range(self.hist.GetSize()):
                self.hist.SetBinError(bin, unc.hist.GetBinContent(bin))

        # print("new uncertainties (possibly taken from bootstrap)")
        # self.dump()

        # print("+++ end set_uncertainty_from_bootstrap +++")

        #self.bootstrap_replicas = {}
        #self.cached_uncertainty_obj = None

    # constructs and returns the Curve object that corresponds to the statistical uncertainty inherent to this current Curve
    def create_uncertainty_obj(self, no_caching = False):

        # result is not yet stored in cache, or caching is disabled, therefore compute it
        if not self.cached_uncertainty_obj or no_caching:
            unc_hist = self.hist.Clone()

            # no bootstraps are available for this Curve, use the normal
            # sum-of-weights-squared way of assessing statistical uncertainties
            print(len(self.bootstrap_replicas))
            if len(self.bootstrap_replicas) == 0:
                print("No bootstrap replicas are available, using sum-of-weights-squared as measure for statistical variance")

                for bin in range(unc_hist.GetSize()):
                    cur_stat_unc = self.hist.GetBinError(bin)
                    cur_stat_unc_test = unc_hist.GetBinError(bin)
                    print("Bin {}, statunc: {}", bin, cur_stat_unc_test)
                    print(cur_stat_unc)

                    # if a bin is empty, assign a large uncertainty
                    if cur_stat_unc == 0.0 and self.hist.GetBinContent(bin) == 0.0:
                        cur_stat_unc = 1.0

                    unc_hist.SetBinContent(bin, cur_stat_unc)
                    unc_hist.SetBinError(bin, 0.0)
            else:
                print("Have {} bootstrap replicas available, using per-bin RMS as measure for statistical variance".format(len(self.bootstrap_replicas)))
                # have bootstrap replicas available, thus return the statistical uncertainty as the RMS of the replicas
                for bin in range(unc_hist.GetSize()):
                    # compute the RMS in this bin over all stored replicas
                    num_replicas = float(len(self.bootstrap_replicas))
                    replica_values = [rep.GetBinContent(bin) for rep in self.bootstrap_replicas.values()]

                    # as measure of uncertainty, take the mean squared difference between the *actual* value of the curve
                    # in this bin, and the bootstrap replicas. Note: this is equivalent to just taking the RMS of the 
                    # replicas themselves, in almost all cases. Differences occur in some pathological situations, where
                    # the Poisson fluctuations are cut off because they would drive the efficiency negative. Then, the
                    # current prescription gives more conservative intervals for the statistical uncertainty.
                    curval = self.hist.GetBinContent(bin) # this is the actual value
                    rmsval = np.sqrt(np.mean(np.square(np.array(replica_values) - curval)))

                    # if a bin is empty, assign a large uncertainty                    
                    if rmsval == 0.0 and self.hist.GetBinContent(bin) == 0.0:
                        rmsval = 1.0

                    unc_hist.SetBinContent(bin, rmsval)
                    unc_hist.SetBinError(bin, 0.0)
                    self.hist.SetBinError(bin, rmsval)
                    
            self.cached_uncertainty_obj = Curve(unc_hist)

        return self.cached_uncertainty_obj

    def get_statistical_uncertainty(self):
        from AbsUncertainty import AbsUncertainty
        return AbsUncertainty(self.create_uncertainty_obj()).convert_to_relative(self)

    # to create clones of this instance
    def clone(self, name = None):
        if name is None:
            name = self.name

        hist_clone = self.hist.Clone()
        bootstrap_replicas_clone = {name: rep.Clone() for name, rep in self.bootstrap_replicas.items()}

        return Curve(hist_clone, name, bootstrap_replicas = bootstrap_replicas_clone)

    def dump(self):
        for bin in range(0, self.hist.GetSize()):
            print("bin {}: {} +/- {}".format(bin, self.evaluate_bin(bin), self.evaluate_uncertainty_bin(bin)))
