import sys, re
from argparse import ArgumentParser
from TFileUtils import TFileUtils
from MapUtils import MapUtils

def bm():
    parser = ArgumentParser(description = "merges a consecutive list of bins in all histograms contained in a file")
    parser.add_argument("--in", action = "store", dest = "infile_path")
    parser.add_argument("--out", action = "store", dest = "outfile_path")
    parser.add_argument("--mergebins", action = "store", dest = "mergebins")
    args = vars(parser.parse_args())
    
    infile_path = args["infile_path"]
    outfile_path = args["outfile_path"]
    mergebins = args["mergebins"]

    # extract the numerical range of bins that should be merged
    binrange_regex = re.compile("(.+)-(.+)")
    m = binrange_regex.match(mergebins)
    merge_start = int(m.group(1))
    merge_end = int(m.group(2))

    # perform the bin merging operation on all histograms in this file
    merge_callback = lambda obj: MapUtils.merge_bins(obj, merge_start, merge_end)
    TFileUtils.map(infile_path, outfile_path, "TH1D", merge_callback)

if __name__ == "__main__":
    bm()
