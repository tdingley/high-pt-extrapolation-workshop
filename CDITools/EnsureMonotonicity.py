import os
import ROOT
from argparse import ArgumentParser

from CDIInputImporter import CDIInputImporter
from CDIInputExporter import CDIInputExporter
from RelUncertainty import RelUncertainty

def make_monotonic(curve, pT_threshold):
    """
    Ensure that the passed curve becomes monotonic in the extrapolation region (i.e. above a certain pT threshold)
    """
    retcurve = curve.clone()

    bin_centers = retcurve.get_bin_centers()
    for cur_bin_center, prev_bin_center in zip(bin_centers[1:], bin_centers[:-1]):
        if cur_bin_center < pT_threshold:
            continue

        prev_value = retcurve.evaluate(prev_bin_center)
        if retcurve.evaluate(cur_bin_center) < prev_value:
            retcurve.set(cur_bin_center, prev_value)

    return retcurve

def EnsureMonotonicity(CDI_in, CDI_out, pT_threshold):
    """
    Read in CDI text inputs and make sure that all contained uncertainty components are monotonously increasing with pT.
    """
    # load the CDI text inputs
    calib_name, final_stat, tagger, WP, jetcoll = CDIInputImporter._read_header(CDI_in)
    options = {"WP": WP, "jet_collection": jetcoll, "tagger": tagger}

    available_systematics = CDIInputImporter._read_available_systematics(CDI_in)
    central_value = CDIInputImporter._read_central_values(CDI_in)
    stat_unc = central_value.create_uncertainty_obj()
    stat_uncs_up = [stat_unc]
    stat_uncs_down = [-stat_unc]

    syst_uncs_up = {}
    syst_uncs_down = {}

    # make sure that each uncertainty component is indeed monotonically increasing
    for cur_syst_name in available_systematics:
        cur_syst = RelUncertainty(CDIInputImporter._read_systematics_values(CDI_in, cur_syst_name))

        cur_monotonic_syst = make_monotonic(cur_syst, pT_threshold)

        syst_uncs_up[cur_syst_name] = cur_monotonic_syst
        syst_uncs_down[cur_syst_name] = cur_monotonic_syst

    # write out the processed CDI text inputs
    CDIInputExporter.produce_CDI_input_from_uncertainties(CDI_out, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)

if __name__ == "__main__":
    ROOT.gROOT.SetBatch()

    parser = ArgumentParser(description = "make sure that extrapolation uncertainties increase monotonically")
    parser.add_argument("--CDI_in", action = "store", dest = "CDI_in")
    parser.add_argument("--CDI_out", action = "store", dest = "CDI_out")
    parser.add_argument("--pT_threshold", action = "store", type = float, dest = "pT_threshold", default = 400)
    args = vars(parser.parse_args())

    EnsureMonotonicity(**args)
