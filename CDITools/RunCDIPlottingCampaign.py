import sys, os, glob, re, fnmatch
from argparse import ArgumentParser
import subprocess as sp
from time import sleep
def _run_plcdi(infile, outdir):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "plcdi.py"), "--in", infile, "--outdir", outdir]
    print(" ".join(cmd))
    sp.Popen(cmd)

def _run_plcdi_uncs(infile, outdir, refpt):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "plcdi_uncs.py"), "--refpt", refpt, "--CDI_in", infile, "--outdir", outdir, "--PCBT_istrue", PCBT_istrue]
    print(" ".join(cmd))
    sp.Popen(cmd)

def _run_new_plcdi_uncs(infile, outdir):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "new_plcdi_uncs.py"),"--plot_all", "--CDI_in", infile, "--outdir", outdir]
    print(" ".join(cmd))

    sp.Popen(cmd)

def RunCDIInputPlottingCampaign(indir, refpt):
    # first, get a list of all CDI input files that are available and that should be plotted / visualized
    matches = []
    
    for root, dirnames, filenames in os.walk(indir):
        for filename in fnmatch.filter(filenames, '*EMPFlow_*_jet.txt'):
            print(fnmatch.filter(filenames, 'EMPFlow_*_jet*.txt'))
            matches.append(os.path.join(root, filename))
    print(matches)

    # now, for each found file, generate a series of plots
    for match in matches:
        cur_plotdir = os.path.join(os.path.dirname(match), "plots_" + os.path.splitext(os.path.basename(match))[0])
        print("generating plots for '{}' in '{}'".format(match, cur_plotdir))

        if not os.path.exists(cur_plotdir):
            os.makedirs(cur_plotdir)
        
        _run_plcdi_uncs(match, cur_plotdir, refpt)
        #_run_plcdi(match, cur_plotdir)
        #_run_new_plcdi_uncs(match, cur_plotdir)
if __name__ == "__main__":
    if not os.environ["XTRAP_ROOTDIR"]:
        raise Exception("Error: 'XTRAP_ROOTDIR' not defined. Please do 'source high-pT-extrapolation/setup.sh'")

    parser = ArgumentParser(description = "orchestrates the production of extrapolation CDI text inputs")
    parser.add_argument("--indirs", nargs = '+', action = "store")
    parser.add_argument("--refpt", action = "store", dest = "refpt") 
    parser.add_argument('-P', '--PCBT_istrue', default="False", help="Please specify which scale factor scheme: False=Fixed cut, True=Pseudo-continuous")

    args = vars(parser.parse_args())
    refpt = args["refpt"]
    PCBT_istrue = args["PCBT_istrue"]
    print(PCBT_istrue)
    
    for indir in args["indirs"]:
        print("processing '{}'".format(indir))
        RunCDIInputPlottingCampaign(indir, refpt)
    exit(0)

