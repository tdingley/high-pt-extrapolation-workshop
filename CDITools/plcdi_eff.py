import os
from argparse import ArgumentParser

from CDIInputImporter import CDIInputImporter
from RatioPlotter import RatioPlotter
from ColorPicker import ColorPicker
from EfficiencyLoader import EfficiencyLoader
from RelUncertainty import RelUncertainty
from UncertaintyCombinator import UncertaintyCombinator

def plot_efficiency_CDI_uncertainties(CDI_in, eff_in, WP, outdir):
    cp = ColorPicker()

    tagged_hist_names = {
        "FixedCutBEff_60": "taggedb_FixedCutBEff_60",
        "FixedCutBEff_70": "taggedb_FixedCutBEff_70",
        "FixedCutBEff_77": "taggedb_FixedCutBEff_77",
        "FixedCutBEff_85": "taggedb_FixedCutBEff_85"
        }
    WP_descriptions = {
        "FixedCutBEff_60": "Fixed Cut, #epsilon_{b}^{MC} = 60%",
        "FixedCutBEff_70": "Fixed Cut, #epsilon_{b}^{MC} = 70%",
        "FixedCutBEff_77": "Fixed Cut, #epsilon_{b}^{MC} = 77%",
        "FixedCutBEff_85": "Fixed Cut, #epsilon_{b}^{MC} = 85%"
        }
    tagged_hist = tagged_hist_names[WP]
    WP_description = WP_descriptions[WP]
    nominal_name = ".*[Nn]ominal/nominal"

    # first, load the nominal efficiency curve
    eff_nominal = EfficiencyLoader.from_highpt_xtrap_file(eff_in, tagged_hist, nominal_name = nominal_name, do_rebinning = False)

    # load the uncertainties that are available in this CDI input file
    systs_up = []
    systs_down = []
    available_systematics = CDIInputImporter._read_available_systematics(CDI_in)
    for syst in available_systematics:
        cur_syst = RelUncertainty(CDIInputImporter._read_systematics_values(CDI_in, syst))
        systs_up.append(cur_syst)
        systs_down.append(-cur_syst)

    calib_name, final_state, tagger, WP, jetcoll = CDIInputImporter._read_header(CDI_in)

    # get the total systematic uncertainty envelope by summing the independent variations in quadrature
    total_syst_unc_up = RelUncertainty.from_quadrature_sum(systs_up)
    total_syst_unc_up = total_syst_unc_up.rebin_virtual_from(eff_nominal)
    total_syst_unc_down = -total_syst_unc_up

    # get the individual ones as well to plot
    unc_separate_names = ["modelsyst_combined", "tracksyst_combined", "jetsyst_combined"]
    uncs_separate = []
    uncs_uncs = []
    uncs_labels = ["MOD_*", "TRK_*", "JET_*"]
    uncs_colors = [cp.get_color("green3"), cp.get_color("orange3"), cp.get_color("blue2")]
    for cur_name in unc_separate_names:
        uncs_separate.append(RelUncertainty(CDIInputImporter._read_systematics_values(CDI_in, cur_name)))
        uncs_uncs.append(None)
    
    # also load the statistical uncertainty
    stat_unc_up, stat_unc_down = UncertaintyCombinator.stat(eff_nominal)
    stat_unc_up = stat_unc_up.rebin_virtual_from(eff_nominal)
    
    # combine them to get the total uncertainty
    total_unc_up = RelUncertainty.from_quadrature_sum([total_syst_unc_up, stat_unc_up])
    total_syst_unc_CDI = RelUncertainty.from_quadrature_sum(uncs_separate)

    id_string = "#sqrt{s} = 13 TeV, " + jetcoll + ", " + tagger
    inner_label = "#splitline{" + id_string + "}{" + WP_description + "}"

    xaxis_range = (20, 3000)
    RatioPlotter.create_ratio_plot(curves = [eff_nominal],
                                   rel_uncertainties_up = [total_unc_up],
                                   rel_uncertainties_down = [-total_unc_up], 
                                   labels = ["   stat. + syst."],
                                   marker_colors = [cp.get_color("black")],
                                   uncertainty_fill_colors = [cp.get_color("greenblue1")],
                                   uncertainty_styles = ["5"],
                                   ratio_reference = eff_nominal,
                                   outfile = os.path.join(outdir, "eff"),
                                   inner_label = inner_label,
                                   outer_label = "",
                                   x_label = "p_{T} [GeV]",
                                   y_label = "b efficiency",
                                   y_ratio_label = "ratio",
                                   yaxis_range_abs = (0.0, 1.2),
                                   yaxis_range_rel = (0.75, 1.25),
                                   xaxis_range = xaxis_range,
                                   show_ratio = False,
                                   legpos = 'topright'
                                   )

    RatioPlotter.create_ratio_plot(curves = uncs_separate + [total_syst_unc_CDI],
                                   rel_uncertainties_up = uncs_uncs + [None],
                                   rel_uncertainties_down = uncs_uncs + [None],
                                   labels = uncs_labels + ["total"],
                                   yaxis_range_abs = (0.0, 0.25),
                                   xaxis_range = xaxis_range,
                                   outfile = os.path.join(outdir, "uncs"),
                                   curve_styles = ["lpx", "lpx", "lpx", "lpx"],
                                   x_label = "p_{T} [GeV]",
                                   y_label = "rel. uncertainty",
                                   y_ratio_label = "",
                                   legpos= 'topright',
                                   marker_colors = uncs_colors + [cp.get_color("black")],
                                   inner_label = inner_label,
                                   show_ratio = False)

if __name__ == "__main__":
    parser = ArgumentParser(description = "produce plots showing the tagger efficiency and on top the combined uncertainty taken from the CDI")
    parser.add_argument("--CDI_in", action = "store", dest = "CDI_in")
    parser.add_argument("--eff_in", action = "store", dest = "eff_in")
    parser.add_argument("--WP", action = "store", dest = "WP")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    args = vars(parser.parse_args())

    plot_efficiency_CDI_uncertainties(**args)
