import sys, os, re
from argparse import ArgumentParser
import ROOT

from RatioPlotter import RatioPlotter
from ColorPicker import ColorPicker
from EfficiencyLoader import EfficiencyLoader

def generate_efficiency_comparison_plots(eff_paths, runs, curve_labels, colors, outdir, tagged_hist, inner_label = ""):
    if not os.path.exists(outdir):
        os.path.makedirs(outdir)

    xaxis_range = (20, 500)
    #xaxis_range = (200, 2000)

    # load the efficiency curves
    eff_curves = []
    for cur_eff_path, cur_run in zip(eff_paths, runs):
        cur_eff_curve = EfficiencyLoader.from_highpt_xtrap_file(cur_eff_path, tagged_hist, nominal_name = cur_run, do_rebinning = False, rebin_options = {"sigth": 2.0, "merge_start": 20, "merge_end": 3000})
        eff_curves.append(cur_eff_curve)

    RatioPlotter.create_ratio_plot(curves = eff_curves,
                                   rel_uncertainties_up = [None],
                                   rel_uncertainties_down = [None],
                                   labels = curve_labels,
                                   marker_colors = colors,
                                   uncertainty_fill_colors = colors,
                                   uncertainty_styles = ["5", "5"],
                                   marker_styles = [20, 21],
                                   ratio_reference = eff_curves[0],
                                   inner_label = inner_label,
                                   outer_label = "",
                                   x_label = "p_{T} [GeV]",
                                   y_label = "b efficiency",
                                   y_ratio_label = "ratio",
                                   color_alpha = 0.35,
                                   yaxis_range_abs = (0.0, 1.6),
                                   yaxis_range_rel = (0.9, 1.05),
                                   xaxis_range = xaxis_range,
                                   legpos = "topleft",
                                   outfile = os.path.join(outdir, "effcomp"),
                                   show_ratio = True)

if __name__ == "__main__":
    ROOT.gROOT.SetBatch()
    parser = ArgumentParser(description = "plot absolute jet yields")
    parser.add_argument("--effA", action = "store", dest = "effA_path")
    parser.add_argument("--effB", action = "store", dest = "effB_path")
    parser.add_argument("--labelA", action = "store", dest = "labelA", default = "efficiency A")
    parser.add_argument("--labelB", action = "store", dest = "labelB", default = "efficiency B")
    parser.add_argument("--runA", action = "store", dest = "runA")
    parser.add_argument("--runB", action = "store", dest = "runB")
    parser.add_argument("--out", action = "store", dest = "outdir")
    parser.add_argument("--tagger", action = "store", dest = "tagger", default = "DL1r")
    parser.add_argument("--jetcoll", action = "store", dest = "jet_collection", default = "AntiKtVR30Rmax4Rmin02TrackJets")
    args = vars(parser.parse_args())

    infileA = args["effA_path"]
    infileB = args["effB_path"]

    labelA = args["labelA"]
    labelB = args["labelB"]

    runA = args["runA"]
    if not runA:
        runA = ".*[Nn]ominal\|?/nominal"

    runB = args["runB"]
    if not runB:
        runB = ".*[Nn]ominal\|?/nominal"

    tagger = args["tagger"]
    jet_collection = args["jet_collection"]

    tagged_hists = [
        "taggedb_FixedCutBEff_60", 
        "taggedb_FixedCutBEff_70", 
        "taggedb_FixedCutBEff_77", 
        "taggedb_FixedCutBEff_85"
        ]
    
    tagged_hists_names = [
        "Fixed Cut, #epsilon_{b}^{MC} = 60%",
        "Fixed Cut, #epsilon_{b}^{MC} = 70%",
        "Fixed Cut, #epsilon_{b}^{MC} = 77%",
        "Fixed Cut, #epsilon_{b}^{MC} = 85%"
        ]
    
    wp_outdirs = [
        "FixedCutBEff_60", 
        "FixedCutBEff_70", 
        "FixedCutBEff_77", 
        "FixedCutBEff_85"
        ]

    id_string = "#sqrt{s} = 13 TeV, " + jet_collection + ", " + tagger

    cp = ColorPicker()
    colors = [cp.get_color("green3"), cp.get_color("greenblue3")]

    for tagged_hist, hist_name, wp_outdir in zip(tagged_hists, tagged_hists_names, wp_outdirs):
        cur_outdir = os.path.join(args["outdir"], wp_outdir)
        if not os.path.exists(cur_outdir):
            os.makedirs(cur_outdir)
            
        generate_efficiency_comparison_plots(eff_paths = [infileA, infileB], runs = [runA, runB], curve_labels = [labelA, labelB], 
                                             colors = colors, outdir = cur_outdir, tagged_hist = tagged_hist, inner_label = "#splitline{" + id_string + "}{" + hist_name + "}")

