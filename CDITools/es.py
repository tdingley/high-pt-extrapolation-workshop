import os
from argparse import ArgumentParser

from TFileUtils import TFileUtils
from EfficiencyLoader import EfficiencyLoader
from EfficiencySorter import EfficiencySorter
from Curve import Curve

def SaveEfficiency(input_file_path, histname_probe, histname_tag, outdir, nominal_name):
    dirnames = TFileUtils.get_leaf_dirlist(input_file_path)
    nominal_dirname = [dirname for dirname in dirnames if EfficiencySorter.by_regex(dirname, nominal_name)]

    assert len(nominal_dirname) == 1
    nominal_dirname = nominal_dirname[0]

    probe_curve = Curve.from_file(input_file_path, os.path.join(nominal_dirname, histname_probe))
    tag_curve = Curve.from_file(input_file_path, os.path.join(nominal_dirname, histname_tag))

    probe_curve.to_csv(os.path.join(outdir, "probe.csv"))
    tag_curve.to_csv(os.path.join(outdir, "tag.csv"))

if __name__ == "__main__":
    parser = ArgumentParser(description = "write the efficiency curve to a csv file")
    parser.add_argument("--WP", action = "store", dest = "WP")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    parser.add_argument("files", nargs = '+', action = "store")
    args = vars(parser.parse_args())

    WP = args["WP"]
    outdir = args["outdir"]
    input_file = args["files"]

    # work with a single input file only
    assert len(input_file) == 1
    input_file = input_file[0]

    if not os.path.exists(outdir):
        os.makedirs(outdir)
    
    tagged_hists = {"60": "tagged_FixedCutBEff_60",
                    "70": "tagged_FixedCutBEff_70", 
                    "77": "tagged_FixedCutBEff_77", 
                    "85": "tagged_FixedCutBEff_85"}
    
    tagged_hist = tagged_hists[WP]

    SaveEfficiency(input_file_path = input_file, histname_probe = "truth", histname_tag = tagged_hist, outdir = outdir, nominal_name = ".*[Nn]ominal/nominal")
