import sys, os, re
from argparse import ArgumentParser

import ROOT

from EfficiencyLoader import EfficiencyLoader
from RatioPlotter import RatioPlotter
from ColorPicker import ColorPicker
from SystematicUtils import SystematicUtils
from UncertaintyCombinator import UncertaintyCombinator
from CDIInputImporter import CDIInputImporter
from RelUncertainty import RelUncertainty

def PlotTagProbe(input_files, outdir, tagger, jet_collection, WP, dataset, CDI_overlay):
    tagged_hist_desc = {
        "taggedb_FixedCutBEff_60": "Fixed Cut, #epsilon_{b}^{MC} = 60%",
        "taggedb_FixedCutBEff_70": "Fixed Cut, #epsilon_{b}^{MC} = 70%",
        "taggedb_FixedCutBEff_77": "Fixed Cut, #epsilon_{b}^{MC} = 77%",
        "taggedb_FixedCutBEff_85": "Fixed Cut, #epsilon_{b}^{MC} = 85%"
        }

    tagged_hist_names = {
        "FixedCutBEff_60": "taggedb_FixedCutBEff_60", 
        "FixedCutBEff_70": "taggedb_FixedCutBEff_70", 
        "FixedCutBEff_77": "taggedb_FixedCutBEff_77", 
        "FixedCutBEff_85": "taggedb_FixedCutBEff_85"
        }

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    cp = ColorPicker()
    varcols = [cp.get_color("red3"), cp.get_color("orange3"), cp.get_color("blue3")]

    # if provided, load the CDI input file to plot as an overlay
    if CDI_overlay:
        available_CDI_systematics = CDIInputImporter._read_available_systematics(CDI_overlay)

    for input_file in input_files:
        cur_outdir = os.path.join(outdir, os.path.splitext(os.path.basename(input_file))[0])

        if not os.path.exists(cur_outdir):
            os.makedirs(cur_outdir)

        nominal_name = ".*[Nn]ominal/nominal" # identifier of the nominal run
        tagged_hist = tagged_hist_names[WP]

        # load the ROOT input file, and do not do any fancy processing
        effcoll = EfficiencyLoader.as_efficiency_collection(input_file, tagged_hist = tagged_hist, nominal_name = nominal_name)

        for name, eff in effcoll.items():
            print(name)
            eff.dump()

        if dataset == "ttbar":
            do_rebinning = False
            rebin_options = {}
        elif dataset == "Zprime":
            do_rebinning = False
            rebin_options = {"sigth": 2.0, "merge_start": 0, "merge_end": 5000}

        # also load the same data, but push it through the whole pipeline assessing the actual uncertainties
        eff_curve = EfficiencyLoader.from_highpt_xtrap_file(input_file, tagged_hist = tagged_hist, nominal_name = nominal_name, 
                                                            do_rebinning = do_rebinning, rebin_options = rebin_options)

        # look at all the raw systematic variations that are available now
        available_variations = effcoll.keys()
        common_names = list(set(map(SystematicUtils.get_common_name, available_variations)))

        # # remove all uncertainties that are _not_ related to *TRK_FAKE_RATE_LOOSE* in order to keep only the dominant ones
        # common_names_filtered = []
        # for cur_name in common_names:
        #     if "TRK_FAKE_RATE_LOOSE" in cur_name:
        #         common_names_filtered.append(cur_name)
        # common_names = common_names_filtered

        print("----------------------------------------")
        print("have the following base names available:")
        print(common_names)
        print("----------------------------------------")
        
        # generate a few plots for these, together with the nominal curve
        for cur_name in sorted(common_names):
            if cur_name == "nominal" or cur_name == "Nominal":
                continue

            cur_identifier = SystematicUtils.extract_identifier(cur_name)
            cur_identifier_stripped = cur_identifier.rstrip('_')

            # extract the corresponding 'primitive' efficiency curves
            available_variations = effcoll.keys()
            regex_expr = ".*" + cur_identifier + ".*"
            cur_regex = re.compile(regex_expr)
            found_keys = filter(cur_regex.match, available_variations)
            print("looking for primitive efficiency curves for {}".format(cur_name))
            print("have the following available:")
            print(available_variations)
            print("found the following matches")
            print(found_keys)

            nominal_eff = effcoll["nominal"]
            nominal_eff.create_uncertainty_obj()
            nominal_probe = effcoll["nominal"].probe_curve
            nominal_probe.create_uncertainty_obj()
            nominal_tag = effcoll["nominal"].tag_curve
            nominal_tag.create_uncertainty_obj()
            nominal_untag = nominal_probe - nominal_tag
            nominal_untag.create_uncertainty_obj()
            
            cur_eff_vars = []
            cur_tag_vars = []
            cur_untag_vars = []
            cur_probe_vars = []
            cur_labels = []
            for cur_key in sorted(found_keys):
                cur_labels.append(SystematicUtils.extract_identifier(cur_key))
                
                cur_eff = effcoll[cur_key]
                cur_eff.create_uncertainty_obj()
                
                cur_tag = effcoll[cur_key].tag_curve
                cur_tag.create_uncertainty_obj()

                cur_probe = effcoll[cur_key].probe_curve
                cur_probe.create_uncertainty_obj()

                cur_untag = cur_probe - cur_tag
                cur_untag.create_uncertainty_obj()
                cur_untag_vars.append(cur_untag)
                
                cur_eff_vars.append(cur_eff)
                cur_tag_vars.append(cur_tag)
                cur_probe_vars.append(cur_probe)

            marker_colors = [cp.get_color("black")] + varcols[0:len(cur_eff_vars)]

            # load the fitted envelope from the CDI inputs
            if CDI_overlay:
                print("have cur_identifier = {}".format(cur_identifier_stripped))
                print("have CDI uncertainties: {}".format(available_CDI_systematics))

                if cur_identifier_stripped in available_CDI_systematics:
                    cur_syst_overlay = RelUncertainty(CDIInputImporter._read_systematics_values(CDI_overlay, cur_identifier_stripped))
                    cur_eff_vars.append(nominal_eff * (cur_syst_overlay + 1.0))
                    cur_labels.append("fitted envelope")
                    marker_colors.append(cp.get_color("blue3"))

            # extract the corresponding systematic uncertainty band by looking up the common name in the dict of the
            # fully processed efficiency curve
            common_name = SystematicUtils.get_common_name(cur_name)
            available_uncertainties = eff_curve.uncertainties.keys()
            cur_regex = re.compile(common_name + "_.*symmetrized")
            found_keys = filter(cur_regex.match, available_uncertainties)
            
            effcoll["nominal"].create_uncertainty_obj()
            print("looking for {}".format(common_name))
            print("Have the following available: {}".format(available_uncertainties))
            print("found the following matches:")
            print(found_keys)
            
            if len(found_keys) == 1:
                cur_syst_unc_up, cur_syst_unc_down = UncertaintyCombinator.partial_syst(eff_curve, found_keys[0])
            else:
                raise Exception("Error: found more than one candidate for the processed systematic, this should not have happened!")
            
            id_string = "#sqrt{s} = 13 TeV, " + jet_collection + ", " + tagger
            inner_label = "#splitline{" + id_string + "}{" + tagged_hist_desc[tagged_hist] + "}"

            # ----------------------------------------
            # efficiency
            # ----------------------------------------
            yaxis_range_abs = [nominal_eff.min(), nominal_eff.max()]
            #yaxis_range_abs[1] *= (yaxis_range_abs[1] / yaxis_range_abs[0]) ** (0.8)
            yaxis_range_abs[1] *= 1.5
            yaxis_range_abs[0] *= 0.8

            print(yaxis_range_abs)
            RatioPlotter.create_ratio_plot(curves = [nominal_eff] + cur_eff_vars,
                                           # rel_uncertainties_up = [cur_syst_unc_up] + [None for cur in cur_eff_vars],
                                           # rel_uncertainties_down = [cur_syst_unc_down] + [None for cur in cur_eff_vars],
                                           rel_uncertainties_up = [None],
                                           rel_uncertainties_down = [None],
                                           labels = ["nominal"] + cur_labels,
                                           curve_styles = ["lp", "lp", "lp"],
                                           x_label = "p_{T} [GeV]",
                                           y_label = "efficiency [a.u.]",
                                           y_ratio_label = "",
                                           marker_colors = marker_colors,
                                           show_ratio = True,
                                           xaxis_range = (0, 3000),
                                           #yaxis_range_abs = (1e-3, 1.6),
                                           yaxis_range_abs = yaxis_range_abs,
                                           #yaxis_range_rel = (1.0 - 2.5 * cur_syst_unc_up.max(), 1.0 + 2.5 * cur_syst_unc_up.max()),
                                           yaxis_range_rel = (0.7, 1.3),
                                           inner_label = inner_label,
                                           legpos = 'topleft',
                                           outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + "_eff"),
                                           y_log = True)

            RatioPlotter.create_ratio_plot(curves = [nominal_eff] + cur_eff_vars,
                                           rel_uncertainties_up = [cur_syst_unc_up] + [None for cur in cur_eff_vars],
                                           rel_uncertainties_down = [cur_syst_unc_down] + [None for cur in cur_eff_vars],
                                           labels = ["nominal"] + cur_labels,
                                           curve_styles = ["lp", "lp"],
                                           x_label = "p_{T} [GeV]",
                                           y_label = "efficiency [a.u.]",
                                           y_ratio_label = "",
                                           marker_colors = marker_colors,
                                           show_ratio = True,
                                           xaxis_range = (0, 3000),
                                           yaxis_range_abs = (1e-3, 1.6),
                                           #yaxis_range_rel = (1.0 - 2.5 * cur_syst_unc_up.max(), 1.0 + 2.5 * cur_syst_unc_up.max()),
                                           yaxis_range_rel = (0.7, 1.3),
                                           inner_label = inner_label,
                                           legpos = 'topleft',
                                           outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + "_eff_log"),
                                           x_log = True,
                                           y_log = False)

        #     # ----------------------------------------
        #     # tag + variations
        #     # ----------------------------------------            
        #     cur_curves = [nominal_tag] + cur_tag_vars
        #     RatioPlotter.create_ratio_plot(curves = cur_curves,
        #                                    rel_uncertainties_up = [None],
        #                                    rel_uncertainties_down = [None],
        #                                    labels = ["nominal"] + cur_labels,
        #                                    curve_styles = ["lp", "lp"],
        #                                    x_label = "p_{T} [GeV]",
        #                                    y_label = "tagged b-jet yield",
        #                                    y_ratio_label = "",
        #                                    marker_colors = [cp.get_color("black")] + varcols[0:len(cur_eff_vars)],
        #                                    show_ratio = True,
        #                                    xaxis_range = (0.0, 3000),
        #                                    yaxis_range_abs = (0.1, 100 * nominal_probe.max()),
        #                                    yaxis_range_rel = (0.75, 1.1),
        #                                    inner_label = inner_label,
        #                                    legpos = 'topright',
        #                                    y_log = True,
        #                                    outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + "_tag"))

        #     cur_curves = [nominal_tag] + cur_tag_vars
        #     RatioPlotter.create_ratio_plot(curves = cur_curves,
        #                                    rel_uncertainties_up = [None],
        #                                    rel_uncertainties_down = [None],
        #                                    labels = ["nominal"] + cur_labels,
        #                                    curve_styles = ["lp", "lp"],
        #                                    x_label = "p_{T} [GeV]",
        #                                    y_label = "tagged b-jet yield",
        #                                    y_ratio_label = "",
        #                                    marker_colors = [cp.get_color("black")] + varcols[0:len(cur_eff_vars)],
        #                                    show_ratio = True,
        #                                    xaxis_range = (0.0, 3000),
        #                                    yaxis_range_abs = (0.1, 100 * nominal_probe.max()),
        #                                    yaxis_range_rel = (0.75, 1.1),
        #                                    inner_label = inner_label,
        #                                    legpos = 'topright',
        #                                    y_log = True,
        #                                    x_log = True,
        #                                    outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + "_tag_log"))

        #     # ----------------------------------------
        #     # untag + variations
        #     # ----------------------------------------            
        #     inner_label = "#splitline{" + id_string + "}{" + tagged_hist_desc[tagged_hist] + ", untagged b-jets}"
        #     cur_curves = [nominal_untag] + cur_untag_vars
        #     RatioPlotter.create_ratio_plot(curves = cur_curves,
        #                                    rel_uncertainties_up = [None],
        #                                    rel_uncertainties_down = [None],
        #                                    labels = ["nominal"] + cur_labels,
        #                                    curve_styles = ["lp", "lp"],
        #                                    x_label = "p_{T} [GeV]",
        #                                    y_label = "untagged b-jet yield",
        #                                    y_ratio_label = "",
        #                                    marker_colors = [cp.get_color("black")] + varcols[0:len(cur_eff_vars)],
        #                                    show_ratio = True,
        #                                    xaxis_range = (0.0, 3000),
        #                                    yaxis_range_abs = (0.1, 100 * nominal_probe.max()),
        #                                    yaxis_range_rel = (0.9, 1.1),
        #                                    inner_label = inner_label,
        #                                    legpos = 'topright',
        #                                    y_log = True,
        #                                    outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + "_untag"))

        #     cur_curves = [nominal_untag] + cur_untag_vars
        #     RatioPlotter.create_ratio_plot(curves = cur_curves,
        #                                    rel_uncertainties_up = [None],
        #                                    rel_uncertainties_down = [None],
        #                                    labels = ["nominal"] + cur_labels,
        #                                    curve_styles = ["lp", "lp"],
        #                                    x_label = "p_{T} [GeV]",
        #                                    y_label = "untagged b-jet yield",
        #                                    y_ratio_label = "",
        #                                    marker_colors = [cp.get_color("black")] + varcols[0:len(cur_eff_vars)],
        #                                    show_ratio = True,
        #                                    xaxis_range = (0.0, 3000),
        #                                    yaxis_range_abs = (0.1, 100 * nominal_probe.max()),
        #                                    yaxis_range_rel = (0.9, 1.1),
        #                                    inner_label = inner_label,
        #                                    legpos = 'topright',
        #                                    y_log = True,
        #                                    x_log = True,
        #                                    outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + "_untag_log"))

        #     # ----------------------------------------
        #     # probe + variations
        #     # ----------------------------------------            
        #     inner_label = "#splitline{" + id_string + "}{" + tagged_hist_desc[tagged_hist] + ", truth b-jets}"
        #     cur_curves = [nominal_probe] + cur_probe_vars
        #     RatioPlotter.create_ratio_plot(curves = cur_curves,
        #                                    rel_uncertainties_up = [None],
        #                                    rel_uncertainties_down = [None],
        #                                    labels = ["nominal"] + cur_labels,
        #                                    curve_styles = ["lp", "lp"],
        #                                    x_label = "p_{T} [GeV]",
        #                                    y_label = "truth b-jet yield",
        #                                    y_ratio_label = "",
        #                                    marker_colors = [cp.get_color("black")] + varcols[0:len(cur_eff_vars)],
        #                                    show_ratio = True,
        #                                    xaxis_range = (0.0, 3000),
        #                                    yaxis_range_abs = (0.1, 100 * effcoll["nominal"].probe_curve.max()),
        #                                    yaxis_range_rel = (0.9, 1.1),
        #                                    inner_label = inner_label,
        #                                    legpos = 'topright',
        #                                    y_log = True,
        #                                    outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + "_probe"))

        #     cur_curves = [nominal_probe] + cur_probe_vars
        #     RatioPlotter.create_ratio_plot(curves = cur_curves,
        #                                    rel_uncertainties_up = [None],
        #                                    rel_uncertainties_down = [None],
        #                                    labels = ["nominal"] + cur_labels,
        #                                    curve_styles = ["lp", "lp"],
        #                                    x_label = "p_{T} [GeV]",
        #                                    y_label = "truth b-jet yield",
        #                                    y_ratio_label = "",
        #                                    marker_colors = [cp.get_color("black")] + varcols[0:len(cur_eff_vars)],
        #                                    show_ratio = True,
        #                                    xaxis_range = (0.0, 3000),
        #                                    yaxis_range_abs = (0.1, 100 * effcoll["nominal"].probe_curve.max()),
        #                                    yaxis_range_rel = (0.9, 1.1),
        #                                    inner_label = inner_label,
        #                                    legpos = 'topright',
        #                                    y_log = True,
        #                                    x_log = True,
        #                                    outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + "_probe_log"))

        #     # ----------------------------------------
        #     # tag + untag + probe yield
        #     # ----------------------------------------            
        #     for cur_tag_var, cur_untag_var, cur_probe_var, cur_label in zip(cur_tag_vars, cur_untag_vars, cur_probe_vars, cur_labels):
        #         inner_label = "#splitline{" + id_string + "}{" + tagged_hist_desc[tagged_hist] + ", b-jets, " + cur_label + "}"
        #         RatioPlotter.create_ratio_plot(curves = [cur_tag_var, cur_untag_var, cur_probe_var],
        #                                        ratio_reference = cur_probe_var,
        #                                        rel_uncertainties_up = [None, None, None],
        #                                        rel_uncertainties_down = [None, None, None],
        #                                        labels = ["tagged", "untagged", "all"],
        #                                        curve_styles = ["lp", "lp", "lp"],
        #                                        x_label = "p_{T} [GeV]",
        #                                        y_label = "b-jet yield",
        #                                        y_ratio_label = "",
        #                                        marker_colors = [cp.get_color("red3"), cp.get_color("orange3"), cp.get_color("black")],
        #                                        show_ratio = True,
        #                                        xaxis_range = (0.0, 3000),
        #                                        yaxis_range_abs = (0.1, 100 * nominal_probe.max()),
        #                                        yaxis_range_rel = (0.0, 1.2),
        #                                        inner_label = inner_label,
        #                                        legpos = 'topright',
        #                                        y_log = True,
        #                                        outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + cur_label + "_tag_untag_all"))

        #         RatioPlotter.create_ratio_plot(curves = [cur_tag_var, cur_untag_var, cur_probe_var],
        #                                        ratio_reference = cur_probe_var,
        #                                        rel_uncertainties_up = [None, None, None],
        #                                        rel_uncertainties_down = [None, None, None],
        #                                        labels = ["tagged", "untagged", "all"],
        #                                        curve_styles = ["lp", "lp", "lp"],
        #                                        x_label = "p_{T} [GeV]",
        #                                        y_label = "b-jet yield",
        #                                        y_ratio_label = "",
        #                                        marker_colors = [cp.get_color("red3"), cp.get_color("orange3"), cp.get_color("black")],
        #                                        show_ratio = True,
        #                                        xaxis_range = (0.0, 3000),
        #                                        yaxis_range_abs = (0.1, 100 * nominal_probe.max()),
        #                                        yaxis_range_rel = (0.0, 1.2),
        #                                        inner_label = inner_label,
        #                                        legpos = 'topright',
        #                                        y_log = True,
        #                                        x_log = True,
        #                                        outfile = os.path.join(cur_outdir, cur_identifier + tagged_hist + cur_label + "_tag_untag_all_log"))


        # # ----------------------------------------
        # # nominal
        # # ----------------------------------------            
        # # also plot the nominal efficiency only, without showing the variations on top
        # RatioPlotter.create_ratio_plot(curves = [nominal_eff],
        #                                rel_uncertainties_up = [cur_syst_unc_up],
        #                                rel_uncertainties_down = [cur_syst_unc_down],
        #                                labels = ["nominal"],
        #                                curve_styles = ["lp"],
        #                                x_label = "p_{T} [GeV]",
        #                                y_label = "efficiency [a.u.]",
        #                                y_ratio_label = "",
        #                                marker_colors = [cp.get_color("black")],
        #                                show_ratio = True,
        #                                xaxis_range = (0, 3000),
        #                                yaxis_range_rel = (0.9, 1.1),
        #                                inner_label = inner_label,
        #                                legpos = 'topright',
        #                                outfile = os.path.join(cur_outdir, tagged_hist + "_nominal_eff"))

        # inner_label = "#splitline{" + id_string + "}{" + tagged_hist_desc[tagged_hist] + ", b-jets}"
        # RatioPlotter.create_ratio_plot(curves = [nominal_tag, nominal_untag, nominal_probe],
        #                                ratio_reference = nominal_probe,
        #                                rel_uncertainties_up = [None, None, None],
        #                                rel_uncertainties_down = [None, None, None],
        #                                labels = ["tagged", "untagged", "all"],
        #                                curve_styles = ["lp", "lp", "lp"],
        #                                x_label = "p_{T} [GeV]",
        #                                y_label = "b-jet yield",
        #                                y_ratio_label = "",
        #                                marker_colors = [cp.get_color("red3"), cp.get_color("orange3"), cp.get_color("black")],
        #                                show_ratio = True,
        #                                xaxis_range = (0.0, 3000),
        #                                yaxis_range_abs = (0.1, 100 * nominal_probe.max()),
        #                                yaxis_range_rel = (0.0, 1.2),
        #                                inner_label = inner_label,
        #                                legpos = 'topright',
        #                                y_log = True,
        #                                outfile = os.path.join(cur_outdir, "nominal_" + tagged_hist + "_tag_untag_all"))

        # RatioPlotter.create_ratio_plot(curves = [nominal_tag, nominal_untag, nominal_probe],
        #                                ratio_reference = nominal_probe,
        #                                rel_uncertainties_up = [None, None, None],
        #                                rel_uncertainties_down = [None, None, None],
        #                                labels = ["tagged", "untagged", "all"],
        #                                curve_styles = ["lp", "lp", "lp"],
        #                                x_label = "p_{T} [GeV]",
        #                                y_label = "b-jet yield",
        #                                y_ratio_label = "",
        #                                marker_colors = [cp.get_color("red3"), cp.get_color("orange3"), cp.get_color("black")],
        #                                show_ratio = True,
        #                                xaxis_range = (0.0, 3000),
        #                                yaxis_range_abs = (0.1, 100 * nominal_probe.max()),
        #                                yaxis_range_rel = (0.0, 1.2),
        #                                inner_label = inner_label,
        #                                legpos = 'topright',
        #                                y_log = True,
        #                                x_log = True,
        #                                outfile = os.path.join(cur_outdir, "nominal_" + tagged_hist + "_tag_untag_all_log"))

if __name__ == "__main__":
    ROOT.gROOT.SetBatch()
    parser = ArgumentParser(description = "plot absolute jet yields")
    parser.add_argument("input_files", nargs = '+', action = "store")
    parser.add_argument("--out", action = "store", dest = "outdir")
    parser.add_argument("--tagger", action = "store", dest = "tagger", default = "DL1r")
    parser.add_argument("--jetcoll", action = "store", dest = "jet_collection", default = "AntiKtVR30Rmax4Rmin02TrackJets")
    parser.add_argument("--WP", action = "store", dest = "WP", default = "FixedCutBEff_77")
    parser.add_argument("--dataset", action = "store", dest = "dataset", default = "Zprime")
    parser.add_argument("--CDI_overlay", action = "store", dest = "CDI_overlay", default = None)
    args = vars(parser.parse_args())

    PlotTagProbe(**args)
