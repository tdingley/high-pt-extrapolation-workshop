import sys, os, glob
from argparse import ArgumentParser
import subprocess as sp

def _hadd_files(outfile, folders, dryrun = False):
    source = [os.path.join(folder, '*.root') for folder in folders]
    print("--------------------------")
    print("destination:\n{}".format(outfile))
    print("source:\n{}".format('\n'.join(source)))
    print("--------------------------")

    if not dryrun:
        files_to_hadd = []
        for folder in folders:
            files_to_hadd += glob.glob(os.path.join(folder, '*.root'))    
        sp.check_output(["hadd", outfile] + files_to_hadd)

def _get_folders(candidate_dirs, systematic_type, tagger, jet_type, run):
    return [matched_dir for matched_dir in candidate_dirs if 
            '.' + systematic_type + '.' in matched_dir and 
            '_' + tagger + '_' in matched_dir and '_' + jet_type in matched_dir and '_' +
            run in matched_dir]

def _get_available_runs(dirs, available_types = ["b_jet", "c_jet", "l_jet"]):
    runs = []
    for cur_dir in dirs:
        for jet_type in available_types:
            # whatever comes after the tagger name must be the run identifier
            split = cur_dir.split(jet_type + '_')
            if len(split) > 1:
                runs.append(split[-1])

    return list(set(runs))

def _check_match(path, keywords):
    for kw in keywords:
        if kw in path:
            return kw

def _get_dataset(path, supported_datasets = ["800030", "Zprime", "ttbar", "500567", "500568", "522021", "512953"]):
    #if _check_match(path, supported_datasets) == "500567":
    #    print("Zprime")
    #    return "Zprime"
    #else:
    print(_check_match(path, supported_datasets))
    #return _check_match(path, supported_datasets)
    return "Zprime"
    
def _get_jet_collection(path, supported_jet_collections = ["AntiKt4EMTopoJets", "AntiKt4EMPFlowJets", "AntiKtVR30Rmax4Rmin02TrackJets", "EMPFlow", "EMPFlowSlim"]):
    if _check_match(path, supported_jet_collections):
        return _check_match(path, supported_jet_collections)
    else:
        return "EMPFlow"

def _get_tagger(path, supported_taggers = ["DL1dv01", "GN120220509", "GN2v00"]):
    return _check_match(path, supported_taggers)

def _get_jet_type(path, available_types = ["b_jet", "c_jet", "l_jet"]):
    return _check_match(path, available_types)


def _get_folders(candidate_dirs, systematic_type, tagger, jet_type, run):
    return [matched_dir for matched_dir in candidate_dirs if
            '.' + systematic_type + '.' in matched_dir and
            '_' + tagger + '_' in matched_dir and '_' + jet_type in matched_dir and '_' +
            run in matched_dir]



def CombineEfficiencies(workdir, dryrun = False):
    print("now processing '{}'".format(workdir))
    outdir = os.path.join(workdir, "combined")

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    supported_taggers = ["GN120220509", "DL1dv01", "GN2v00"]
    supported_systtypes = ["JUNC_noretagging", "JUNC_retagging", "TUNC_NBLS", "TUNC_BLS", "MUNC_fastsim", "MUNC_fullsim", "TUNC_NBLS_nrc", "TUNC_NBLS_rc", "TUNC_BLS_nrc", "TUNC_BLS_rc", "MUNC_FSR", "MUNC_PS", "MUNC_QS"]

    # first, need to get lists of directories whose ROOT files should be merged together
    candidate_dirs = glob.glob(os.path.join(workdir, 'user.*'))
    # determine which taggers, runs and systematic types are available
    available_runs = _get_available_runs(candidate_dirs)
    print("have the following runs available:")
    print('\n'.join(available_runs))

    # process all runs
    for cur_run in available_runs:
        run_outdir = os.path.join(outdir, cur_run)
        print(run_outdir, "run_outdir")
        if not os.path.exists(run_outdir):
            os.makedirs(run_outdir)

        # start hadding
        for tagger in supported_taggers:
            for systtype in supported_systtypes:
                jet_type = _get_jet_type(candidate_dirs[0])
                to_hadd = _get_folders(candidate_dirs, systtype, tagger,  jet_type, cur_run)
                print(to_hadd)

                if len(to_hadd) > 0:
                    jet_type = _get_jet_type(to_hadd[0])
                    dataset = _get_dataset(to_hadd[0])
                    jet_collection = _get_jet_collection(to_hadd[0])
                    outfile = os.path.join(run_outdir, "{}_{}_{}_{}_{}.root".format(tagger, systtype, dataset, jet_collection, jet_type))
                    if not os.path.exists(outfile):
                        _hadd_files(outfile, to_hadd, dryrun)

if __name__ == "__main__":
    parser = ArgumentParser(description = "orchestrates the production root files, used to produce CDI output files")
    parser.add_argument("workdirs", nargs = '+', action = "store")
    parser.add_argument("--dryrun", action = "store_const", const = True, default = False)
    args = vars(parser.parse_args())

    for workdir in args["workdirs"]:
        CombineEfficiencies(workdir, args["dryrun"])
