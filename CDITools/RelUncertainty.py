from Curve import Curve
from MapUtils import MapUtils
import math
#from time import sleep

class RelUncertainty(Curve):
    
    def __init__(self, curve, name = None):
        super(RelUncertainty, self).__init__(curve.hist, name, curve.bootstrap_replicas)

        # evalute the statistical uncertainty on this systematic variation
        self.set_uncertainty_from_bootstrap()

    def clone(self, name = None):
        return RelUncertainty(super(RelUncertainty, self).clone(name = name))
        
    def save(self, storage_loc):
        if not isinstance(storage_loc, str):
            self.hist.Write()
        else:
            raise NotImplementedError

    # loads an existing RelUncertainty from a histogram
    @classmethod
    def from_histogram(cls, file_path, histname, name = None, binrange = None):
        curve = Curve.from_file(file_path, histname, binrange = binrange, loose_matching = False)
        obj = cls(curve, name)
        return obj

    # constructs a new uncertainty object from the relative difference
    # between a nominal and a varied curve
    @classmethod
    def from_variation(cls, nom_curve, var_curve, name = None):
        if name is None:
            name = var_curve.name
        rel_unc = (var_curve - nom_curve) / nom_curve

        obj = cls(curve = rel_unc, name = name)

        return obj

    # constructs a new uncertainty object from the relative difference
    # between a nominal and a varied curve. Performs rebinning to ensure a certain
    # significance of the resulting uncertainty.
    @classmethod
    def from_variation_significance_rebinning(cls, nom_curve, var_curve, name = None, merge_start = 20, merge_end = 1000, sigth = 0.0, smooth_uncertainty = False):

        # implements a single step of the iterative bin merging procedure
        def merge_step(cur_nom_curve, cur_var_curve, merge_start, merge_end, sigth = 0.0, direction = 'left'):

            # break immediately if there is nothing more to merge
            if cur_nom_curve.get_number_bins() == 1 or cur_var_curve.get_number_bins() == 1:
                return cur_nom_curve, cur_var_curve, 0

            # find the indices of the bins in the active region and make sure it is allowed
            active_bins = range(max(0, cur_var_curve.get_bin_index(merge_start)), 
                                min(cur_var_curve.get_bin_index(merge_end) + 1, cur_var_curve.get_number_bins()))

            print("active_bins = {}".format(active_bins))

            # also stop if there is just a single bin left to consider
            if len(active_bins) < 2:
                return cur_nom_curve, cur_var_curve, 0                

            if direction == 'right':
                active_bins.reverse()

            # this is the uncertainty assessed from the current nominal and varied curves
            cur_unc = RelUncertainty.from_variation(cur_nom_curve, cur_var_curve)

            print("cur_unc")
            cur_unc.dump()

            # merge two bins if *both* have too low a significance
            perform_merging = lambda x_value, x_value_neighbour: cur_unc.unc_significance(x_value) < sigth and cur_unc.unc_significance(x_value_neighbour) < sigth

            # go through the list of bins in the correct order and perform one merging step
            for bin, neighbour in zip(active_bins[:-1], active_bins[1:]):
                bin_center = cur_nom_curve.get_bin_center(bin)
                neighbour_center = cur_nom_curve.get_bin_center(neighbour)
                print("checking whether bin {} (centered at {}) should be merged with {} (centered at {})".format(bin, bin_center, neighbour, neighbour_center))

                # check if these bins should be merged
                if perform_merging(bin_center, neighbour_center):
                    print("yes, have sig(bin) = {}, sig(neighbour) = {}".format(cur_unc.unc_significance(bin_center), cur_unc.unc_significance(neighbour_center)))
                    # if so, merge these bins in both the nominal and the varied efficiency curve ...
                    cur_nom_curve.merge_bins(bin + 1, neighbour + 1)
                    cur_var_curve.merge_bins(bin + 1, neighbour + 1)

                    # ... and then return them
                    return cur_nom_curve, cur_var_curve, 1

            # nothing happened, return the original curves
            return cur_nom_curve, cur_var_curve, 0

        if name is None:
            name = var_curve.name

        # the naive uncertainty without any rebinning
        initial_unc = RelUncertainty.from_variation(nom_curve, var_curve)
            
        # set up the local objects on which operations will be performed
        cur_nom_curve = nom_curve.clone()
        cur_var_curve = var_curve.clone()

        while True: 
            # need to merge bins in the 'truth' and 'tagged' histograms in a synchronized way, updating the changed efficiency along the way
            print("+++++++++++++ merge step +++++++++++++++")
            cur_nom_curve, cur_var_cuve, bins_merged = merge_step(cur_nom_curve, cur_var_curve, merge_start = merge_start, merge_end = merge_end, sigth = sigth, direction = 'left')
            print("+++++++++++++ ++++++++++++++++ +++++++++++++++")

            if bins_merged == 0:
                break

        # rebinning complete, can now simply re-compute the final uncertainty from the properly
        # rebinned tag- and probe curves
        obj = cls.from_variation(cur_nom_curve, cur_var_curve)

        # copy back the original binning
        retval = obj.rebin_virtual_from(initial_unc)

        print("rebinning complete:")
        print("initial uncertainty:")
        initial_unc.dump()

        print("final uncertainty:")
        retval.dump()

        if smooth_uncertainty:
            raise NotImplementedError("Error: smoothing of the rebinned uncertainty not available yet.")

        return retval

    # computes the upper edge of the relative uncertainty band
    @classmethod
    def upper_envelope(cls, eff_nominal, var_list):
        unc_list = []
        for cur_var in var_list:
            unc_list.append(RelUncertainty.from_variation(eff_nominal, cur_var))

        return RelUncertainty.from_positive_quadrature_sum(unc_list)

    # computes the lower edge of the relative uncertainty band
    @classmethod
    def lower_envelope(cls, eff_nominal, var_list):
        unc_list = []
        for cur_var in var_list:
            unc_list.append(RelUncertainty.from_variation(eff_nominal, cur_var))

        return RelUncertainty.from_negative_quadrature_sum(unc_list)

    # this sums in quadrature all positive relative uncertainties and returns a new
    # uncertainty object
    @classmethod
    def from_positive_quadrature_sum(cls, unc_list):
        pos_uncs = []
        for unc in unc_list:
            cur_pos_unc = unc.apply(lambda y: max(y, 0.0), error_prop = lambda y, y_err: y_err)
            pos_uncs.append(cur_pos_unc)

        return RelUncertainty.from_quadrature_sum(pos_uncs)

    # this sums in quadrature all negative relative uncertainties and returns a new
    # uncertainty object
    @classmethod
    def from_negative_quadrature_sum(cls, unc_list):
        neg_uncs = []
        for unc in unc_list:
            cur_neg_unc = unc.apply(lambda y: min(y, 0.0), error_prop = lambda y, y_err: y_err)
            neg_uncs.append(cur_neg_unc)

        return RelUncertainty.from_quadrature_sum(neg_uncs)

    # constructs a new uncertainty object from the quadratic sum of the 
    # passed uncertainty objects
    @classmethod
    def from_quadrature_sum(cls, unc_list):
        unc_list = [el for el in unc_list if el]
     
        if len(unc_list) > 0:
            unc = unc_list[0]
            #print("VALUE OF THE UNCERTAINTY BEFORE: " + str(cls(unc).evaluate(1000)))
            #display(unc)
            #print(cls(unc).evaluate(1000))
            tot_unc_sq = unc.apply(lambda y: y ** 2, error_prop = lambda y, y_err: 2 * y * y_err)
            #print(cls(unc))
            #display(tot_unc_sq)
            #print("IN RELUNCERTAINTIY HERE **************************************")
            for unc in unc_list[1:]:
                tot_unc_sq = tot_unc_sq + unc.apply(lambda y: y ** 2, error_prop = lambda y, y_err: 2 * y * y_err)
 
            tot_unc = tot_unc_sq.apply(lambda x: math.sqrt(x), error_prop = lambda y, y_err: y_err / (2 * math.sqrt(abs(y)) + 1e-6))
            #print("VALUE OF THE UNCERTAINTY AFTER:")
            #display(tot_unc)
            #print(cls(tot_unc_sq))
            #print("VALUE OF THE UNCERTAINTY AFTER: " + str(cls(tot_unc).evaluate(1000)))



            return cls(tot_unc)
        else:
            return None

    # returns the significance of this uncertainty, i.e. the
    # number of sigmas it is away from zero (taking into account
    # the limited statistical uncertainty)
    def unc_significance(self, x_value):

        print("unc = {}".format(self.evaluate(x_value)))
        print("unc err = {}".format(self.evaluate_uncertainty(x_value)))

        sig = abs(self.evaluate(x_value)) / abs(self.evaluate_uncertainty(x_value))
        print("sig = {}".format(sig))

        return sig

    def __add__(self, other):
        return RelUncertainty(super(RelUncertainty, self).__add__(other))

    def __sub__(self, other):
        return RelUncertainty(super(RelUncertainty, self).__sub__(other))

    def __mul__(self, other):
        return RelUncertainty(super(RelUncertainty, self).__mul__(other))

    def __truediv__(self, other):
        return RelUncertainty(super(RelUncertainty, self).__truediv__(other))

    def __div__(self, other):
        return RelUncertainty(super(RelUncertainty, self).__div__(other))

    def __neg__(self):
        return RelUncertainty(super(RelUncertainty, self).__neg__())
    
    # can convert a relative uncertainty to an absolute one, if the normalization
    # is available
    def convert_to_absolute(self, normalization):
        from AbsUncertainty import AbsUncertainty
        return AbsUncertainty(self * normalization)

    # returns the mirrored version of this uncertainty object, e.g. used
    # when symmetrizing an uncertainty
    def invert(self):
        return -self

    def dump(self):
        print("--------------------")
        print("RelUncertainty:")
        super(RelUncertainty, self).dump()
        print("--------------------")
        
