import re
from UncertaintyCombinator import UncertaintyCombinator

class CDIInputExporter:

    # nuisances ... a dictionary of the form {"uncertainty or uncertainty group name (regex)": "official nuisance name"}
    # eff_curve_nominal ... the actual, nominal efficiency curve (will usually come from a fullsim sample)
    # eff_curves_aux ... any additional efficiency curves that come from different samples and might need to be exported as well
    @staticmethod
    def produce_CDI_input(eff_curve_nominal, nuisances, outpath, eff_curves_aux = [], options = {}, store_stat_unc = True):
        options.setdefault("tagger", "DL1dv01")
        options.setdefault("jet_collection", "AntiKt4EMPFlowJets")
        options.setdefault("WP", "FixedCutBEff_70")
        options.setdefault("jet_type", "bottom")

        eff_curves = [eff_curve_nominal] + eff_curves_aux
        
        syst_uncs_up = {}
        syst_uncs_down = {}

        stat_uncs_up = []
        stat_uncs_down = []
        
        # ... as well as the relative systematic uncertainties corresponding to the requested variations
        print("*****************************************************")
        print(nuisances.items())
        print("*****************************************************")

        for nuisance, nuisance_name in nuisances.items():
            print("trying to match '{}'".format(nuisance))
            
            for eff_curve in eff_curves:
                available_variations = eff_curve.uncertainties.keys()

                cur_regex = re.compile(nuisance)
                print(available_variations)
                found_keys = filter(cur_regex.match, available_variations)
                print(found_keys, "found_keys")

                if len(found_keys) > 0:                  
                    if len(found_keys) == 1:

                        print("found the following matching entries: {}".format(" ".join(found_keys)))
                        print(len(found_keys))
                        if len(eff_curve.uncertainties[found_keys[0]]) > 1:
                            # have more than one variation in this group; compute and return their envelope
                            syst_unc_up, syst_unc_down = UncertaintyCombinator.partial_syst(eff_curve, found_keys[0])
                        elif len(eff_curve.uncertainties[found_keys[0]]) == 1:
                            # have only a single variation here, use it directly
                            syst_unc_up = syst_unc_down = eff_curve.uncertainties[found_keys[0]][0]
                            #print(syst_unc_up.values())
                        else:
                            raise Exception("Error: have no variations here!")
                        
                        syst_uncs_up[nuisance_name] = syst_unc_up
                        syst_uncs_down[nuisance_name] = syst_unc_down
                        print(syst_unc_up.evaluate(1000)) # print value of systematic variation at 1000 GeV
                        print("staty",syst_unc_up.get_statistical_uncertainty().evaluate(1000))

                        if store_stat_unc:                        
                            syst_uncs_up["STAT_" + nuisance_name] = syst_unc_up.create_uncertainty_obj()
                            syst_uncs_down["STAT_" + nuisance_name] = syst_unc_down.create_uncertainty_obj()
                    else:
                        print("found the following matching entries: {}".format(" ".join(found_keys)))

                        raise Exception("Warning: found more than one systematic, is your regex too inclusive?")

        # get the statistical uncertainty from the main "nominal" efficiency curve
        stat_unc_up, stat_unc_down = UncertaintyCombinator.stat(eff_curve_nominal)
        print("statunc",stat_unc_up.evaluate(1000))

        stat_uncs_up.append(stat_unc_up)
        stat_uncs_down.append(stat_unc_down)

        CDIInputExporter.produce_CDI_input_from_uncertainties(outpath, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)

    @staticmethod
    def produce_CDI_input_from_uncertainties(outpath, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options = {}):
        options.setdefault("tagger", "MV2c10")
        options.setdefault("jet_collection", "AntiKt4EMTopoJets")
        options.setdefault("WP", "FixedCutBEff_70")
        options.setdefault("jet_type", "bottom")

        # have everything, now write the CDI input txt file
        CDIInputWriter.write(outpath, 
                             header = ["Run2MCcalib", options["jet_type"], options["tagger"], options["WP"], options["jet_collection"]],
                             bin_name = "pt",
                             bin_header = ["0<abseta<2.5"],
                             stat_uncs_up = stat_uncs_up,
                             stat_uncs_down = stat_uncs_down,
                             syst_uncs_up = syst_uncs_up,
                             syst_uncs_down = syst_uncs_down)

class CDIInputWriter:

    # serializes all the information needed for the high-pt extrapolation and puts it into a text file
    # stat_uncs_up/down ... statistical uncertainties of all the participating efficiency curves
    # stys_uncs_up/down ... dictionary of systematic variations
    @staticmethod
    def write(outpath, header, bin_name, bin_header, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down):
        with open(outpath, "w") as outfile:
            
            # first, write the header
            CDIInputWriter._write_header(outfile, header)

            # write out the contribution from each systematic variation for each bin
            CDIInputWriter._write_bins(outfile, bin_name, bin_header, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down)

            # finally, finish the file
            CDIInputWriter._write_footer(outfile)
            
    @staticmethod
    def _write_header(outfile, header):
        outfile.write("Analysis(" + ",".join(header) + "){")
        outfile.write('\n')

    @staticmethod
    def _write_footer(outfile):
        outfile.write("}")
        outfile.write('\n')
      
    # assumes that all passed Curves follow the same binning as 'stat_unc_up'
    @staticmethod
    def _write_bins(outfile, bin_name, bin_header, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down):
        # determine the uniformly finest binning
        bin_l_edges = []
        bin_u_edges = []
        for unc in stat_uncs_up + syst_uncs_up.values():
            bin_l_edges += unc.get_bin_l_edges()
            bin_u_edges += unc.get_bin_u_edges()

        bin_l_edges = list(sorted(set(bin_l_edges)))
        bin_u_edges = list(sorted(set(bin_u_edges)))
        bin_centers = [0.5 * (l_edge + u_edge) for l_edge, u_edge in zip(bin_l_edges, bin_u_edges)]

        print("chosen commensurate binning: lower edges:")
        print(bin_l_edges)
        print("chosen commensurate binning: upper edges:")
        print(bin_u_edges)
        print("chosen commensurate binning: bin centers:")
        print(bin_centers)

        for bin_center, bin_l_edge, bin_u_edge in zip(bin_centers, bin_l_edges, bin_u_edges):
            stat_uncs_up_val = [stat_unc_up.evaluate(bin_center) for stat_unc_up in stat_uncs_up]
            stat_uncs_down_val = [stat_unc_down.evaluate(bin_center) for stat_unc_down in stat_uncs_down]

            # by default, take the actual statistical uncertainty to be the maximum of the encountered values
            stat_unc_up_val = max(stat_uncs_up_val)
            stat_unc_down_val = min(stat_uncs_down_val)

            syst_unc_up_vals = {syst_name: unc_up.evaluate(bin_center) for syst_name, unc_up in syst_uncs_up.items()}
            syst_unc_down_vals = {syst_name: unc_down.evaluate(bin_center) for syst_name, unc_down in syst_uncs_down.items()}
        
            bin_name_header = "{:.1f}<{}<{:.1f}".format(bin_l_edge, bin_name, bin_u_edge)
            header = ",".join([bin_name_header] + bin_header)

            CDIInputWriter._write_bin(outfile, header = header, 
                                      stat_unc_up = stat_unc_up_val, 
                                      stat_unc_down = stat_unc_down_val, 
                                      syst_uncs_up = syst_unc_up_vals, 
                                      syst_uncs_down = syst_unc_down_vals)

    @staticmethod
    def _write_bin(outfile, header, stat_unc_up, stat_unc_down, syst_uncs_up, syst_uncs_down):
        outfile.write('\t')
        outfile.write("bin(" + header + ")")
        outfile.write('\n\t{\n')
        
        #print("stat_unc_up / down = {} / {}".format(stat_unc_up, stat_unc_down))

        # put the central value and its statistical uncertainty
        outfile.write('\t')
        outfile.write('\t')
        #outfile.write("central_value({:.2f},{:.3f})".format(1.0, stat_unc_up))
        outfile.write('\n')

        # put the individual systematic variations
        for syst in syst_uncs_up.keys():
            print("syst_unc_up / down [{}] = {} / {}".format(syst, syst_uncs_up[syst], syst_uncs_down[syst]))

            cur_syst_unc = 100 * syst_uncs_up[syst]

            outfile.write('\t')
            outfile.write('\t')
            outfile.write("sys({},{:.2f}%)".format(syst, cur_syst_unc))
            outfile.write('\n')
        outfile.write('\t}\n')
