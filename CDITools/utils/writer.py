# Define the variable to append
variable_to_append = "TRK_BIAS_Z0_WM"
# Define the input and output file paths
input_file1_path = '/home/dingleyt/QT/high-pT-extrapolation/high-pT-extrapolation/TDD_EfficiencyProducer/Sreelakshmi/with_QS_bjet/DL1r/FixedCutBEff_77/DL1r_TUNC_NBLS_Zprime_AntiKt4EMPFlowJets_b_jet.txt'
input_file2_path = '/home/dingleyt/QT/high-pT-extrapolation/high-pT-extrapolation/TDD_EfficiencyProducer/mc21CDI/bottom/CDIjet/DL1dv01/FixedCutBEff_60/MCcalibCDI_DL1dv01_extrap_FixedCutBEff_60_EMPFlow_retagging_b_jet.txt'
output_file_path = "output_file.txt"  # Replace with the desired output file path

# Initialize variables to store data
bin_data = {}
current_bin = None

# Read the first input file and store the data
with open(input_file1_path, "r") as file1:
    for line in file1:
        if line.startswith(" bin("):
            current_bin = line.strip()
            bin_data[current_bin] = []
        elif current_bin or variable_to_append in line:
            value_line = next(file1).strip()
            print(line, current_bin, value_line)
            bin_data[current_bin].append(value_line)

# Read the second input file and append the data
with open(input_file2_path, "r") as file2, open(output_file_path, "w") as output_file:
    for line in file2:
        output_file.write(line)
        if line.startswith("bin("):
            current_bin = line.strip()
            if current_bin in bin_data:
                for value_line in bin_data[current_bin]:
                    output_file.write(value_line + "\n")
