import ROOT
import numpy as np
def check_match(string, kw):
    if kw in string:
        return True
    else:
        print("not found in given string")
# Define the paths to the input and output ROOT files
input_file_path ="eff.root"  # Replace with the path to your input ROOT file

output_file_path = 'unc.txt'  # Replace with the desired path for the output ROOT file

# Define the name of the nominal histogram
default_histogram_name = 'effnominal'

# Open the input ROOT file
input_file = ROOT.TFile(input_file_path)

tagged_histograms = ['tagged_FixedCutBEff_60_bootstrap_0','tagged_FixedCutBEff_70_bootstrap_0','tagged_FixedCutBEff_77_bootstrap_0','tagged_FixedCutBEff_85_bootstrap_0']
# Loop through the N directories and X various names
directories = input_file.GetListOfKeys()
print(directories.GetName())
binning = np.array([0.0, 85.0, 140.0, 250.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1250.0, 1400.0, 1550.0, 1750.0, 2000.0, 2250.0, 2500.0, 2750.0, 3000.0], dtype='float64')

names = []
for variation_key in directories:
    names.append(variation_key.GetName())
    print(variation_key.GetName())
    if check_match(variation_key.GetName(), "nominal"):
        print("nominal found")
        print(variation_key.GetName())
    

names.remove("eff_nominal")
names.insert(0, "eff_nominal")
print(names)
name_hists=[]
for name in names:
    directory = input_file.GetDirectory(name)
    hists = []
    for tag in tagged_histograms:
        # here we want to append effiencies for each of the tagged histograms, to be used as comparison with each variation
       hists.append(directory.Get(tag))
    if check_match(name, "nominal"):
        print("storing nominal histograms to be used to calculate variations from varied histograms")
        nominal_effs = hists
    else:
        name_hists.append(hists)

    # number of bins consistent across all histograms:
nbin = len(binning)
print("number of bins: ", nbin)
    # now, we compare this nominal case with each variation
for j in range(len(hists)):
    outfile = "DL1dv01_"+tagged_histograms[j].split('_')[2]+".txt"
    with open(outfile, 'w') as file:

        for i in range(nbin):
            file.write("Jet pT: "+ str(binning[i]))

            for t in range(len(name_hists)):

                #print(hists[j].GetBinContent(i))
                cur_unc = (name_hists[t][j].GetBinContent(i+1) - nominal_effs[j].GetBinContent(i+1))/nominal_effs[j].GetBinContent(i+1)
                file.write(names[t+1].strip("eff_") + ": " + str(cur_unc)+ "\n")
                #print(cur_unc)
    #directory = input_file.GetDirectory(str(name)+"/nominal/")
    # Create a new directory in the output file
    #output_directory = output_file.mkdir("eff"+directory_name)

    # Access the tagged histogram in the current directory
    #for tag_name in tagged_histograms:
    #    tagged_histogram = directory.Get(tag_name)

        # Access the default histogram in the current directory
    #    default_histogram = directory.Get(default_histogram_name)

    #    if tagged_histogram and default_histogram:
            # Calculate the ratio histogram
    #        ratio_histogram = tagged_histogram.Clone()
    #        ratio_histogram.Divide(default_histogram)

            # Write the ratio histogram to the output directory
    #        output_directory.cd()
    #        ratio_histogram.Write()
    
    #with open(output_file_path, 'w') as file:
    #    file.write(blaa)
    

# Close the input and output ROOT files
input_file.Close()
file.Close()