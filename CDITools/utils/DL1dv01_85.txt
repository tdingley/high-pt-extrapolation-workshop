Jet pT: 0.0TRK_BIAS_QOVERP_SAGITTA_WM: 9.20125242196e-06
TRK_EFF_LOOSE_GLOBAL: -0.00163125060794
TRK_EFF_LOOSE_IBL: -0.000903037201978
TRK_EFF_LOOSE_PHYSMODEL: -0.00102396794809
TRK_EFF_LOOSE_PP0: -0.00123691121843
TRK_EFF_LOOSE_TIDE: -0.00102922580662
TRK_FAKE_RATE_LOOSE: -3.9433938951e-05
TRK_RES_D0_DEAD: -2.23458987388e-05
TRK_RES_Z0_DEAD: -0.000756007102315
TRK_RES_Z0_MEAS: 0.00215046413746
TRK_RES_D0_MEAS: 0.00259606764761
TRK_BIAS_D0_WM: -2.6289292634e-05
TRK_FAKE_RATE_LOOSE_TIDE: -0.00702370374629
Jet pT: 85.0TRK_BIAS_QOVERP_SAGITTA_WM: -7.451802804e-05
TRK_EFF_LOOSE_GLOBAL: -0.001241967134
TRK_EFF_LOOSE_IBL: -0.00111422194308
TRK_EFF_LOOSE_PHYSMODEL: -0.0011177704206
TRK_EFF_LOOSE_PP0: -0.0014619727406
TRK_EFF_LOOSE_TIDE: -0.00142293948781
TRK_FAKE_RATE_LOOSE: -3.19362977314e-05
TRK_RES_D0_DEAD: -1.06454325771e-05
TRK_RES_Z0_DEAD: 0.000481439756278
TRK_RES_Z0_MEAS: 0.00207585935254
TRK_RES_D0_MEAS: 0.00203682609976
TRK_BIAS_D0_WM: 0.000102905848246
TRK_FAKE_RATE_LOOSE_TIDE: 0.000237917866144
Jet pT: 140.0TRK_BIAS_QOVERP_SAGITTA_WM: 0.000120900233653
TRK_EFF_LOOSE_GLOBAL: -0.00151650945256
TRK_EFF_LOOSE_IBL: -0.001287850315
TRK_EFF_LOOSE_PHYSMODEL: -0.0014166353465
TRK_EFF_LOOSE_PP0: -0.00160849876077
TRK_EFF_LOOSE_TIDE: -0.00204741917425
TRK_FAKE_RATE_LOOSE: -0.00015243942504
TRK_RES_D0_DEAD: 2.62826594898e-05
TRK_RES_Z0_DEAD: -9.11591174551e-05
TRK_RES_Z0_MEAS: 0.00255204623645
TRK_RES_D0_MEAS: 0.00255730276835
TRK_BIAS_D0_WM: 0.000157695956939
TRK_FAKE_RATE_LOOSE_TIDE: 0.000635311241851
Jet pT: 250.0TRK_BIAS_QOVERP_SAGITTA_WM: -4.68297434432e-05
TRK_EFF_LOOSE_GLOBAL: -0.00150089327736
TRK_EFF_LOOSE_IBL: -0.00131357430358
TRK_EFF_LOOSE_PHYSMODEL: -0.00152430814908
TRK_EFF_LOOSE_PP0: -0.00188958014794
TRK_EFF_LOOSE_TIDE: -0.00225719363396
TRK_FAKE_RATE_LOOSE: -0.000196684922462
TRK_RES_D0_DEAD: 9.36594868868e-06
TRK_RES_Z0_DEAD: -0.000599530546603
TRK_RES_Z0_MEAS: 0.00271846660688
TRK_RES_D0_MEAS: 0.00230402337741
TRK_BIAS_D0_WM: 0.000250539127421
TRK_FAKE_RATE_LOOSE_TIDE: 0.0019576635416
Jet pT: 400.0TRK_BIAS_QOVERP_SAGITTA_WM: 1.84490271829e-05
TRK_EFF_LOOSE_GLOBAL: -0.00147961198006
TRK_EFF_LOOSE_IBL: -0.00135784840065
TRK_EFF_LOOSE_PHYSMODEL: -0.00153126925617
TRK_EFF_LOOSE_PP0: -0.00185228232915
TRK_EFF_LOOSE_TIDE: -0.00207367065535
TRK_FAKE_RATE_LOOSE: -0.00023614754794
TRK_RES_D0_DEAD: 0.000110694163097
TRK_RES_Z0_DEAD: 0.00129854274612
TRK_RES_Z0_MEAS: 0.00220650365106
TRK_RES_D0_MEAS: 0.00228029975979
TRK_BIAS_D0_WM: 0.000261976185996
TRK_FAKE_RATE_LOOSE_TIDE: 0.00210084786643
Jet pT: 500.0TRK_BIAS_QOVERP_SAGITTA_WM: -0.000221506837178
TRK_EFF_LOOSE_GLOBAL: -0.00111491774713
TRK_EFF_LOOSE_IBL: -0.00104477391535
TRK_EFF_LOOSE_PHYSMODEL: -0.00134380814554
TRK_EFF_LOOSE_PP0: -0.00167606840131
TRK_EFF_LOOSE_TIDE: -0.00219291768806
TRK_FAKE_RATE_LOOSE: -0.000465164358073
TRK_RES_D0_DEAD: -0.000276883546472
TRK_RES_Z0_DEAD: 0.00106207511441
TRK_RES_Z0_MEAS: 0.002407040964
TRK_RES_D0_MEAS: 0.00204893824389
TRK_BIAS_D0_WM: -1.10753418589e-05
TRK_FAKE_RATE_LOOSE_TIDE: 0.00165775462925
Jet pT: 600.0TRK_BIAS_QOVERP_SAGITTA_WM: -0.000271989209853
TRK_EFF_LOOSE_GLOBAL: -0.000998535729322
TRK_EFF_LOOSE_IBL: -0.00103206864561
TRK_EFF_LOOSE_PHYSMODEL: -0.00111403799652
TRK_EFF_LOOSE_PP0: -0.00128542845752
TRK_EFF_LOOSE_TIDE: -0.00237338529693
TRK_FAKE_RATE_LOOSE: -0.000432202032095
TRK_RES_D0_DEAD: 0.000346506801593
TRK_RES_Z0_DEAD: 0.000497005622076
TRK_RES_Z0_MEAS: 0.0014642706777
TRK_RES_D0_MEAS: 0.00261929334968
TRK_BIAS_D0_WM: 0.000376313838289
TRK_FAKE_RATE_LOOSE_TIDE: 0.00244825307248
Jet pT: 700.0TRK_BIAS_QOVERP_SAGITTA_WM: 7.78094779354e-05
TRK_EFF_LOOSE_GLOBAL: -0.00091518766905
TRK_EFF_LOOSE_IBL: -0.00084108340435
TRK_EFF_LOOSE_PHYSMODEL: -0.000948534588165
TRK_EFF_LOOSE_PP0: -0.00167846159546
TRK_EFF_LOOSE_TIDE: -0.00207491941161
TRK_FAKE_RATE_LOOSE: -0.000674348808774
TRK_RES_D0_DEAD: 0.000155618955871
TRK_RES_Z0_DEAD: -0.00128920663131
TRK_RES_Z0_MEAS: 0.00210826633073
TRK_RES_D0_MEAS: 0.00237133647041
TRK_BIAS_D0_WM: 0.000459446441143
TRK_FAKE_RATE_LOOSE_TIDE: 0.00195535695913
Jet pT: 800.0TRK_BIAS_QOVERP_SAGITTA_WM: 0.000109328346955
TRK_EFF_LOOSE_GLOBAL: -0.000893476490636
TRK_EFF_LOOSE_IBL: -0.000957565521609
TRK_EFF_LOOSE_PHYSMODEL: -0.000629580480743
TRK_EFF_LOOSE_PP0: -0.000950025635612
TRK_EFF_LOOSE_TIDE: -0.00191890098622
TRK_FAKE_RATE_LOOSE: -0.000848237174654
TRK_RES_D0_DEAD: 0.00014702777694
TRK_RES_Z0_DEAD: 0.00224769053978
TRK_RES_Z0_MEAS: 0.000863316946648
TRK_RES_D0_MEAS: 0.0031969116627
TRK_BIAS_D0_WM: -0.000297825496879
TRK_FAKE_RATE_LOOSE_TIDE: 0.0018118860236
Jet pT: 900.0TRK_BIAS_QOVERP_SAGITTA_WM: 0.000135600049591
TRK_EFF_LOOSE_GLOBAL: -0.000693497396479
TRK_EFF_LOOSE_IBL: -0.000693497396479
TRK_EFF_LOOSE_PHYSMODEL: -0.000844594594595
TRK_EFF_LOOSE_PP0: -0.00111579469378
TRK_EFF_LOOSE_TIDE: -0.00184416067444
TRK_FAKE_RATE_LOOSE: -0.000794228861889
TRK_RES_D0_DEAD: -0.000185965782296
TRK_RES_Z0_DEAD: -0.000761074995264
TRK_RES_Z0_MEAS: 0.00140636622861
TRK_RES_D0_MEAS: 0.00305681254649
TRK_BIAS_D0_WM: -0.000178217208034
TRK_FAKE_RATE_LOOSE_TIDE: 0.00201092242504
Jet pT: 1000.0TRK_BIAS_QOVERP_SAGITTA_WM: -0.000254764899766
TRK_EFF_LOOSE_GLOBAL: -0.000804101714886
TRK_EFF_LOOSE_IBL: -0.000772256102416
TRK_EFF_LOOSE_PHYSMODEL: -0.000589143830709
TRK_EFF_LOOSE_PP0: -0.000967310478799
TRK_EFF_LOOSE_TIDE: -0.00117430695986
TRK_FAKE_RATE_LOOSE: -0.000457780679267
TRK_RES_D0_DEAD: 0.000246803496648
TRK_RES_Z0_DEAD: 0.000100336794549
TRK_RES_Z0_MEAS: 0.00108673152556
TRK_RES_D0_MEAS: 0.00308902440966
TRK_BIAS_D0_WM: -0.000406031559002
TRK_FAKE_RATE_LOOSE_TIDE: 0.00388983125551
Jet pT: 1100.0TRK_BIAS_QOVERP_SAGITTA_WM: 0.000422211666931
TRK_EFF_LOOSE_GLOBAL: -0.000563874792019
TRK_EFF_LOOSE_IBL: -0.000491654375307
TRK_EFF_LOOSE_PHYSMODEL: -0.000541653125339
TRK_EFF_LOOSE_PP0: -0.000733315000458
TRK_EFF_LOOSE_TIDE: -0.00114997125072
TRK_FAKE_RATE_LOOSE: -0.000727759583788
TRK_RES_D0_DEAD: 8.33312500521e-05
TRK_RES_Z0_DEAD: -0.0002724463075
TRK_RES_Z0_MEAS: 0.00108608395901
TRK_RES_D0_MEAS: 0.00334158312709
TRK_BIAS_D0_WM: 0.000116663750073
TRK_FAKE_RATE_LOOSE_TIDE: 0.0021810820944
Jet pT: 1250.0TRK_BIAS_QOVERP_SAGITTA_WM: -0.00010669859721
TRK_EFF_LOOSE_GLOBAL: -0.000216361044342
TRK_EFF_LOOSE_IBL: -2.66746493023e-05
TRK_EFF_LOOSE_PHYSMODEL: -0.000216361044342
TRK_EFF_LOOSE_PP0: -0.000352698140777
TRK_EFF_LOOSE_TIDE: -0.000489035237212
TRK_FAKE_RATE_LOOSE: -0.000334915041242
TRK_RES_D0_DEAD: -5.63131485273e-05
TRK_RES_Z0_DEAD: -0.00223756075999
TRK_RES_Z0_MEAS: 0.00134558786481
TRK_RES_D0_MEAS: 0.00446355798328
TRK_BIAS_D0_WM: -0.000533492986049
TRK_FAKE_RATE_LOOSE_TIDE: 0.00628384301227
Jet pT: 1400.0TRK_BIAS_QOVERP_SAGITTA_WM: 0.00014333924348
TRK_EFF_LOOSE_GLOBAL: -0.00021189279471
TRK_EFF_LOOSE_IBL: -0.000292910627982
TRK_EFF_LOOSE_PHYSMODEL: -0.000345883826659
TRK_EFF_LOOSE_PP0: -0.000401973095847
TRK_EFF_LOOSE_TIDE: -0.000236821358794
TRK_FAKE_RATE_LOOSE: -0.000196312442158
TRK_RES_D0_DEAD: -0.00021189279471
TRK_RES_Z0_DEAD: -0.00444369709964
TRK_RES_Z0_MEAS: 0.000850687249351
TRK_RES_D0_MEAS: 0.00370500783692
TRK_BIAS_D0_WM: -0.000785249768632
TRK_FAKE_RATE_LOOSE_TIDE: 0.00430313027122
Jet pT: 1550.0TRK_BIAS_QOVERP_SAGITTA_WM: 4.97485212253e-05
TRK_EFF_LOOSE_GLOBAL: 0.000104471894573
TRK_EFF_LOOSE_IBL: 7.46227818392e-06
TRK_EFF_LOOSE_PHYSMODEL: -1.989940849e-05
TRK_EFF_LOOSE_PP0: -1.49245563674e-05
TRK_EFF_LOOSE_TIDE: -0.000121883877002
TRK_FAKE_RATE_LOOSE: 6.46730775927e-05
TRK_RES_D0_DEAD: 0.000151732989737
TRK_RES_Z0_DEAD: -0.0002553064559
TRK_RES_Z0_MEAS: 0.00161682693982
TRK_RES_D0_MEAS: 0.00466889871699
TRK_BIAS_D0_WM: 0.000213918641268
TRK_FAKE_RATE_LOOSE_TIDE: 0.00262821509153
Jet pT: 1750.0TRK_BIAS_QOVERP_SAGITTA_WM: 0.000221242330448
TRK_EFF_LOOSE_GLOBAL: 0.000102954351793
TRK_EFF_LOOSE_IBL: 7.44776161905e-05
TRK_EFF_LOOSE_PHYSMODEL: 1.09525906161e-05
TRK_EFF_LOOSE_PP0: -2.40956993558e-05
TRK_EFF_LOOSE_TIDE: 0.000330768236611
TRK_FAKE_RATE_LOOSE: 0.000107335388039
TRK_RES_D0_DEAD: 0.000440294142774
TRK_RES_Z0_DEAD: -0.00174865568804
TRK_RES_Z0_MEAS: 0.00219708967762
TRK_RES_D0_MEAS: 0.00597792395835
TRK_BIAS_D0_WM: 6.13345074511e-05
TRK_FAKE_RATE_LOOSE_TIDE: 0.00333157753359
Jet pT: 2000.0TRK_BIAS_QOVERP_SAGITTA_WM: 0.000208405696422
TRK_EFF_LOOSE_GLOBAL: 9.17977472337e-05
TRK_EFF_LOOSE_IBL: 0.000156304272317
TRK_EFF_LOOSE_PHYSMODEL: 0.000225772837791
TRK_EFF_LOOSE_PP0: 0.00012156998958
TRK_EFF_LOOSE_TIDE: 0.000543343422815
TRK_FAKE_RATE_LOOSE: 0.000218329777204
TRK_RES_D0_DEAD: 1.73671413685e-05
TRK_RES_Z0_DEAD: -0.00239024315465
TRK_RES_Z0_MEAS: 0.00367190988935
TRK_RES_D0_MEAS: 0.00603384111547
TRK_BIAS_D0_WM: 0.000146380191535
TRK_FAKE_RATE_LOOSE_TIDE: 0.00507505768025
Jet pT: 2250.0TRK_BIAS_QOVERP_SAGITTA_WM: -0.000233561673099
TRK_EFF_LOOSE_GLOBAL: 0.000333252631129
TRK_EFF_LOOSE_IBL: 0.000176595411368
TRK_EFF_LOOSE_PHYSMODEL: 0.000350342509649
TRK_EFF_LOOSE_PP0: 0.000313314439523
TRK_EFF_LOOSE_TIDE: 0.00116211173932
TRK_FAKE_RATE_LOOSE: 0.000430095276073
TRK_RES_D0_DEAD: 0.00021647179458
TRK_RES_Z0_DEAD: 0.0027929360577
TRK_RES_Z0_MEAS: 0.00420980674196
TRK_RES_D0_MEAS: 0.00626059216429
TRK_BIAS_D0_WM: -0.000546876112622
TRK_FAKE_RATE_LOOSE_TIDE: 0.00648465521952
Jet pT: 2500.0TRK_BIAS_QOVERP_SAGITTA_WM: 9.77948937576e-05
TRK_EFF_LOOSE_GLOBAL: 0.000394551812746
TRK_EFF_LOOSE_IBL: 0.000225939926957
TRK_EFF_LOOSE_PHYSMODEL: 0.000337223771578
TRK_EFF_LOOSE_PP0: 0.000438390903051
TRK_EFF_LOOSE_TIDE: 0.00104202145418
TRK_FAKE_RATE_LOOSE: 0.000964459986713
TRK_RES_D0_DEAD: 0.000290012443557
TRK_RES_Z0_DEAD: -0.00141468487709
TRK_RES_Z0_MEAS: 0.00498416734392
TRK_RES_D0_MEAS: 0.00831593820712
TRK_BIAS_D0_WM: 0.000104539369189
TRK_FAKE_RATE_LOOSE_TIDE: 0.00730097619581
Jet pT: 2750.0TRK_BIAS_QOVERP_SAGITTA_WM: 0.000111976708845
TRK_EFF_LOOSE_GLOBAL: 0.000547886039704
TRK_EFF_LOOSE_IBL: 0.000551885207877
TRK_EFF_LOOSE_PHYSMODEL: 0.000355925967399
TRK_EFF_LOOSE_PP0: 0.000367923471918
TRK_EFF_LOOSE_TIDE: 0.00139970886056
TRK_FAKE_RATE_LOOSE: 0.000959800361525
TRK_RES_D0_DEAD: -8.39825316335e-05
TRK_RES_Z0_DEAD: 0.00121750628171
TRK_RES_Z0_MEAS: 0.00561083294675
TRK_RES_D0_MEAS: 0.00790635547806
TRK_BIAS_D0_WM: 0.000331930958361
TRK_FAKE_RATE_LOOSE_TIDE: 0.00362745118329
Jet pT: 3000.0TRK_BIAS_QOVERP_SAGITTA_WM: 0.000752776865725
TRK_EFF_LOOSE_GLOBAL: 0.000342067299167
TRK_EFF_LOOSE_IBL: 0.000386684772971
TRK_EFF_LOOSE_PHYSMODEL: 0.000459903191522
TRK_EFF_LOOSE_PP0: 0.000495368363008
TRK_EFF_LOOSE_TIDE: 0.00183160450156
TRK_FAKE_RATE_LOOSE: 0.00128246636243
TRK_RES_D0_DEAD: 0.000252832351558
TRK_RES_Z0_DEAD: -0.00115790320328
TRK_RES_Z0_MEAS: 0.00724519132316
TRK_RES_D0_MEAS: 0.0114266494451
TRK_BIAS_D0_WM: 0.000426726095616
TRK_FAKE_RATE_LOOSE_TIDE: 0.00671747064922
