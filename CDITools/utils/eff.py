import ROOT

# Define the paths to the input and output ROOT files
input_file_path ="/home/dingleyt/QT/high-pT-extrapolation/high-pT-extrapolation/TDD_EfficiencyProducer/mc20CDI/bottom/combined/01/DL1dv01_TUNC_NBLS_Zprime_EMPFlow_b_jet.root"  # Replace with the path to your input ROOT file

output_file_path = 'eff.root'  # Replace with the desired path for the output ROOT file

# Define the name of the default histogram
default_histogram_name = 'truth_bootstrap_0'

# Open the input ROOT file
input_file = ROOT.TFile(input_file_path)

# Create a new output ROOT file
output_file = ROOT.TFile(output_file_path, 'RECREATE')
tagged_histograms = ['tagged_FixedCutBEff_60_bootstrap_0','tagged_FixedCutBEff_70_bootstrap_0','tagged_FixedCutBEff_77_bootstrap_0','tagged_FixedCutBEff_85_bootstrap_0']
pcbt_hist = ['tagged_PCBTBEff_100-85_bootstrap_0','tagged_PCBTBEff_100-85_bootstrap_0','tagged_PCBTBEff_100-85_bootstrap_0','tagged_PCBTBEff_100-85_bootstrap_0','tagged_PCBTBEff_100-85_bootstrap_0']
# Loop through the N directories and X various names
antikt_dir =input_file.GetDirectory('AntiKt4EMPFlowJets')
directories = antikt_dir.GetListOfKeys()
for directory_key in directories:
    directory_name = directory_key.GetName()
    print(directory_name)
    directory = input_file.GetDirectory("AntiKt4EMPFlowJets/"+str(directory_name)+"/nominal/")
    # Create a new directory in the output file
    output_directory = output_file.mkdir("eff_"+directory_name)

    # Access the tagged histogram in the current directory
    for tag_name in tagged_histograms:
        tagged_histogram = directory.Get(tag_name)

        # Access the default histogram in the current directory
        default_histogram = directory.Get(default_histogram_name)

        if tagged_histogram and default_histogram:
            # Calculate the ratio histogram
            ratio_histogram = tagged_histogram.Clone()
            ratio_histogram.Divide(default_histogram)

            # Write the ratio histogram to the output directory
            output_directory.cd()
            ratio_histogram.Write()
# Close the input and output ROOT files
input_file.Close()
output_file.Close()





