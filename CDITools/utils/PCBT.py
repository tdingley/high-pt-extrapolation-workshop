import ROOT

# define root file name
root_file = "/home/dingleyt/QT/high-pT-extrapolation/high-pT-extrapolation/TDD_EfficiencyProducer/mc20CDI/bottom/combined/01/DL1dv01_TUNC_NBLS_Zprime_EMPFlow_b_jet.root"
histogram_name = "AntiKt4EMPFlowJets/nominal/nominal/truth_bootstrap_0"

# Open the ROOT file
root_file = ROOT.TFile(root_file)

# Access the histogram
histogram = root_file.Get(histogram_name)

# Check if the histogram exists
if histogram:
    # Create a canvas for plotting (optional)
    canvas = ROOT.TCanvas('canvas', 'Histogram Canvas', 800, 600)
    histogram.Draw()
    
    # Save the canvas as a PNG file
    canvas.SaveAs('histogram.png')

    # Close the ROOT file
    root_file.Close()

    # Optionally, display the canvas (you may need to close it manually)
    canvas.Update()
    canvas.Draw()

else:
    print("Histogram f'{histogram_name}' not found in the ROOT file.")

# Run the script with 'python script_name.py'