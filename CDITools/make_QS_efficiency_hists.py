import ROOT
from ROOT import TH1F, TDirectory
from ROOT import TFile, TH1F, TCanvas, TString
from argparse import ArgumentParser

def make_QS_efficiency_hists():
    parser = ArgumentParser(description = "produces efficiency input hists for QS uncertainty")
    parser.add_argument("--in_QS", action = "store", dest = "in_QS")
    parser.add_argument("--in_FSR", action = "store", dest = "in_FSR")
    # parser.add_argument("--out", action = "store", dest = "out")
    args = vars(parser.parse_args())
    filepath_QS = args["in_QS"]
    filepath_FSR = args["in_FSR"]
    output_file_path = args["in_QS"]
    file_QS = TFile.Open(filepath_QS, "READ")
    file_FSR = TFile.Open(filepath_FSR, "READ")
    
    nom = file_FSR.GetDirectory("AntiKt4EMPFlowJets_BTagging201903/Nominal/nominal/")
    QS = file_QS.GetDirectory("AntiKt4EMPFlowJets_BTagging201903/Nominal/MC_QS/")
    #truthb_nominal_bootstrap_0
    # nomdir = TDirectory.Copy(file_FSR, "AntiKt4EMPFlowJets_BTagging201903/Nominal/nominal")
    # nom_dir = TDirectory.CloneObject(nom, True)
    outFile = TFile.Open( output_file_path, "RECREATE")
    outFile.cd()
    dir1 = outFile.mkdir("AntiKt4EMPFlowJets_BTagging201903")
    dir1.cd()
    dir2_nom = dir1.mkdir("Nominal")
    # dir2_QS = dir1.mkdir("MC_QS")
    # dir2_nom.cd()

    dir3_nom = dir2_nom.mkdir("nominal")
    dir3_QS = dir2_nom.mkdir("MC_QS")
    # dir2_QS.mkdir("nominal")
    for key in nom.GetListOfKeys():
        hist = key.ReadObj()
        print(hist)
        dir3_nom.WriteTObject(hist)
    for key in QS.GetListOfKeys():
        hist = key.ReadObj()
        print(hist)
        dir3_QS.WriteTObject(hist)
    # outFile.WriteObject(QS, "AntiKt4EMPFlowJets_BTagging201903")
    outFile.Write()
#    outFile.Delete();
    print(nom)

if __name__ == "__main__":
    make_QS_efficiency_hists()
