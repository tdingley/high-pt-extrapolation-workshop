import sys, os, glob, re, fnmatch
from argparse import ArgumentParser
import subprocess as sp

from CombineEfficiencies import _get_dataset, _get_jet_collection, _get_tagger

def _run_pl_tagprobe(infile, outdir, tagger, jetcoll, WP, dataset):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "pl_tagprobe.py"), "--out", outdir, "--tagger", tagger, "--jetcoll", jetcoll, "--WP", WP, "--dataset", dataset, infile]
    print(" ".join(cmd))
    sp.Popen(cmd)

def RunUncertaintiesPlottingCampaign(indirs, outdir):
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    for indir in indirs:
        # first, make a list of all ROOT files that are available
        infiles = glob.glob(os.path.join(indir, "*MUNC*.root"))
        supported_WPs = ["FixedCutBEff_60", "FixedCutBEff_70", "FixedCutBEff_77", "FixedCutBEff_85"]
        
        for infile in infiles:
            for WP in supported_WPs:
                _run_pl_tagprobe(infile, outdir, tagger = _get_tagger(infile), 
                                 jetcoll = _get_jet_collection(infile), WP = WP, 
                                 dataset = _get_dataset(infile))
                
if __name__ == "__main__":
    if not os.environ["XTRAP_ROOTDIR"]:
        raise Exception("Error: 'XTRAP_ROOTDIR' not defined. Please do 'source high-pT-extrapolation/setup.sh'")

    parser = ArgumentParser(description = "generates some plots containing information before the CDI-building stage")
    parser.add_argument("indirs", nargs = '+', action = "store")
    parser.add_argument("--out", action = "store", dest = "outdir")
    args = vars(parser.parse_args())

    RunUncertaintiesPlottingCampaign(**args)
