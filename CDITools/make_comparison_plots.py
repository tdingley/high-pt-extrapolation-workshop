from RelUncertainty import RelUncertainty
from UncertaintyCombinator import UncertaintyCombinator
from EfficiencyCurve import EfficiencyCurve
from RatioPlotter import RatioPlotter
from ColorPicker import ColorPicker
from EfficiencyLoader import EfficiencyLoader

import ROOT

import sys, os
from argparse import ArgumentParser

def syst_str_rep(raw):
     subdirs = raw.split(':')[-1]
     return subdirs.replace('/', '_')

def main():
    ROOT.gROOT.SetBatch(True)
    
    parser = ArgumentParser(description = "compares the extracted efficiency curve with any other efficiency curve obtained from a different analysis")
    # the ROOT file containing the high-pt histograms
    parser.add_argument("--hpteff", action = "store", dest = "eff")
    parser.add_argument("--nomeff", action = "store", dest = "nom")

    plot_variations = False
    
    # the ROOT file containing efficiency histograms from the PDF data-based calibration
    parser.add_argument("--pdfeff", action = "store", dest = "alteff")
    parser.add_argument("--inclpdfeff", action = "store", dest = "inclpdfeff")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    args = vars(parser.parse_args())

    eff_path = args["eff"]
    nom_path = args["nom"]
    alteff_path = args["alteff"]
    alteff_incl_path = args["inclpdfeff"]
    outdir = args["outdir"]

    # check if output directory exists and create it if it doesn't
    if not os.path.exists(outdir):
         os.makedirs(outdir)
    
    # working points to look at
    tagged_hists = [
        "taggedb_FixedCutBEff_60", 
        "taggedb_FixedCutBEff_70", 
        "taggedb_FixedCutBEff_77", 
        "taggedb_FixedCutBEff_85"
        ]

    # output directories for individual working points
    wp_outdirs = [
         "FixedCutBEff_60", 
         "FixedCutBEff_70", 
         "FixedCutBEff_77", 
         "FixedCutBEff_85"
         ]

    tagged_hists_names = [
        "Fixed Cut, #epsilon_{b}^{MC} = 60%",
        "Fixed Cut, #epsilon_{b}^{MC} = 70%",
        "Fixed Cut, #epsilon_{b}^{MC} = 77%",
        "Fixed Cut, #epsilon_{b}^{MC} = 85%"
        ]

    # histogram names from the PDF calibration
    PDF_data_eff_hist_names = [
        "e_b_60_Postfit",
        "e_b_70_Postfit",
        "e_b_77_Postfit",
        "e_b_85_Postfit"
        ]

    PDF_data_syst_unc_up_hist_names = [
        "sf_b_60_syst_Error_high_rel",
        "sf_b_70_syst_Error_high_rel",
        "sf_b_77_syst_Error_high_rel",
        "sf_b_85_syst_Error_high_rel"
        ]

    PDF_data_syst_unc_down_hist_names = [
        "sf_b_60_syst_Error_low_rel",
        "sf_b_70_syst_Error_low_rel",
        "sf_b_77_syst_Error_low_rel",
        "sf_b_85_syst_Error_low_rel"
        ]

    PDF_MC_eff_inclusive_hist_names = [
        "h_effMC_ttbar_b_60wp",
        "h_effMC_ttbar_b_70wp",
        "h_effMC_ttbar_b_77wp",
        "h_effMC_ttbar_b_85wp"
        ]

    PDF_MC_eff_hist_names = [
        "h_effMC_ttbar_b_60wp_c_nominal",
        "h_effMC_ttbar_b_70wp_c_nominal",
        "h_effMC_ttbar_b_77wp_c_nominal",
        "h_effMC_ttbar_b_85wp_c_nominal"
        ]
    
    for tagged_hist, tagged_hist_name, PDF_data_eff_hist_name, PDF_MC_eff_hist_name, PDF_MC_eff_inclusive_hist_name, PDF_data_syst_unc_up_hist_name, PDF_data_syst_unc_down_hist_name, wp_outdir in zip(tagged_hists, tagged_hists_names, PDF_data_eff_hist_names, PDF_MC_eff_hist_names, PDF_MC_eff_inclusive_hist_names, PDF_data_syst_unc_up_hist_names, PDF_data_syst_unc_down_hist_names, wp_outdirs):
         
         # prepare the output directory for the current working point
         cur_outdir = os.path.join(outdir, wp_outdir)
         if not os.path.exists(cur_outdir):
              os.makedirs(cur_outdir)
         
         # create the efficiency curve for a certain working point
         eff_unc = EfficiencyLoader.from_highpt_xtrap_file(eff_path, tagged_hist, binrange = range(1, 10))
         eff_nominal = EfficiencyLoader.from_highpt_xtrap_file(nom_path, tagged_hist, binrange = range(1, 10))
         
         # get the systematic uncertainty from the fastsim samples...
         total_syst_unc_up, total_syst_unc_down = UncertaintyCombinator.syst(eff_unc)

         # and the statistical uncertainty (as well as the nominal curve) from the nominal fullsim ones
         total_stat_unc_up, total_stat_unc_down = UncertaintyCombinator.stat(eff_nominal)
         
         # compute the total stat. + syst. uncertainty band
         total_unc_up_MC = RelUncertainty.from_quadrature_sum([total_syst_unc_up, total_stat_unc_up])
         total_unc_down_MC = -RelUncertainty.from_quadrature_sum([total_syst_unc_down, total_stat_unc_down])
         
         total_uncs_up = [total_unc_up_MC]
         total_uncs_down = [total_unc_down_MC]
         
         # load other data to show in the same plot
         #eff_alt_data = EfficiencyCurve.from_histogram(alteff_path, PDF_data_eff_hist_name)
         eff_alt_data = EfficiencyCurve.from_histogram(alteff_path, os.path.join("results_comulative", PDF_data_eff_hist_name))
         stat_unc_up_data, stat_unc_down_data = UncertaintyCombinator.stat(eff_alt_data)

         # syst_unc_up_data = RelUncertainty.from_histogram(alteff_path, PDF_data_syst_unc_up_hist_name)
         # syst_unc_down_data = RelUncertainty.from_histogram(alteff_path, PDF_data_syst_unc_down_hist_name)
         syst_unc_up_data = RelUncertainty.from_histogram(alteff_path, os.path.join("results_comulative", PDF_data_syst_unc_up_hist_name))
         syst_unc_down_data = RelUncertainty.from_histogram(alteff_path, os.path.join("results_comulative", PDF_data_syst_unc_down_hist_name))

         total_unc_up_data = RelUncertainty.from_quadrature_sum([syst_unc_up_data, stat_unc_up_data])
         total_unc_down_data = RelUncertainty.from_quadrature_sum([syst_unc_down_data, stat_unc_down_data])

         eff_alt_MC = EfficiencyCurve.from_histogram(alteff_path, os.path.join("results_comulative", PDF_MC_eff_hist_name))
         eff_alt_MC_inclusive = EfficiencyCurve.from_histogram(alteff_incl_path, os.path.join("MCeff", PDF_MC_eff_inclusive_hist_name))
         #eff_alt_MC_inclusive = EfficiencyCurve.from_histogram(alteff_path, PDF_MC_eff_hist_name)

         cp = ColorPicker()
         hpt_color = cp.get_color("black")
         var_color_data = cp.get_color("orange3")
         var_color_MC = cp.get_color("red2")

         # prepare plot labels
         jet_collection = "AntiKt4EMTopoJets"
         tagger = "MV2c10"
         id_string = "#sqrt{s} = 13 TeV, " + jet_collection + ", " + tagger
         hist_name = tagged_hist_name
         inner_label = "#splitline{" + id_string + "}{" + hist_name + "}"

         # also prepare some (partial) systematic uncertainties, coming only from jets / tracks / modeling
         syst_groups = ["TRK_*", "JET_*", "MC_*", "PDF_*"]
         unc_colors = [cp.get_color("greenblue1"), cp.get_color("blue2"), cp.get_color("red1"), cp.get_color("orange1"), cp.get_color("green3")]

         for syst_group in syst_groups:
              group_variations = eff_unc.variations[syst_group]
              group_syst_envelope_up, group_syst_envelope_down = UncertaintyCombinator.from_group(syst_group, nominal_curve = eff_unc, group_variations = group_variations)
              
              total_uncs_up.append(RelUncertainty.from_quadrature_sum([group_syst_envelope_up, total_stat_unc_up]))
              total_uncs_down.append(-RelUncertainty.from_quadrature_sum([group_syst_envelope_down, total_stat_unc_down]))
              
         for cur_unc_up, cur_unc_down, name, suffix, unc_color in zip(total_uncs_up, total_uncs_down, ["stat. + syst."] + syst_groups, ["total", "trk", "jet", "mc", "pdf"], unc_colors):           

              # RatioPlotter.create_ratio_plot(curves = [eff_nominal, eff_alt_MC_inclusive],
              #                                rel_uncertainties_up = [cur_unc_up, None],
              #                                rel_uncertainties_down = [cur_unc_down, None], 
              #                                labels = ["high p_{T} extrap. (" + name + ")",#", events with exactly 2 jets with p_{T} > 20 GeV, |#eta| < 2.5)",
              #                                          "PDF b-calibration (stat. only, MC16d, 21.2.29)"],
              #                                marker_colors = [hpt_color, cp.get_color("red2")],
              #                                uncertainty_fill_colors = [unc_color, None],
              #                                uncertainty_styles = ["5", None],
              #                                ratio_reference = eff_nominal,
              #                                outfile = os.path.join(cur_outdir, tagged_hist + "_" + suffix),
              #                                inner_label = inner_label,
              #                                outer_label = "t#bar{t}, di-lepton",
              #                                x_label = "p_{T} [GeV]",
              #                                y_label = "b efficiency",
              #                                y_ratio_label = "ratio to high p_{T} eff.")

              # RatioPlotter.create_ratio_plot(curves = [eff_nominal, eff_alt_data, eff_alt_MC_inclusive],
              #                                rel_uncertainties_up = [cur_unc_up, total_unc_up_data, None],
              #                                rel_uncertainties_down = [cur_unc_down, -total_unc_down_data, None], 
              #                                labels = ["high p_{T} extrap. (" + name + ")",#", events with exactly 2 jets with p_{T} > 20 GeV, |#eta| < 2.5)",
              #                                          "PDF b-calibration (stat. + syst., data17, 21.2.29)", "PDF b-calibration (stat. only, MC16d, 21.2.29)"],
              #                                marker_colors = [hpt_color, var_color_data, cp.get_color("red2")],
              #                                uncertainty_fill_colors = [unc_color, var_color_data, None],
              #                                uncertainty_styles = ["5", "2", None],
              #                                ratio_reference = eff_nominal,
              #                                outfile = os.path.join(cur_outdir, tagged_hist + "_with_data_" + suffix),
              #                                inner_label = inner_label,
              #                                outer_label = "t#bar{t}, di-lepton",
              #                                x_label = "p_{T} [GeV]",
              #                                y_label = "b efficiency",
              #                                y_ratio_label = "ratio to high p_{T} eff.")

              RatioPlotter.create_ratio_plot(curves = [eff_nominal, eff_alt_MC, eff_alt_MC_inclusive],
                                             rel_uncertainties_up = [cur_unc_up, None, None],
                                             rel_uncertainties_down = [cur_unc_down, None, None], 
                                             labels = ["high p_{T} extrap. (" + name + ")",#", events with exactly 2 jets with p_{T} > 20 GeV, |#eta| < 2.5)",
                                                       "PDF b-calibration (stat. only, MC16d, signal region)", "PDF b-calibration (stat. only, MC16d, inclusive)"],
                                             marker_colors = [hpt_color, var_color_MC, cp.get_color("blue2")],
                                             uncertainty_fill_colors = [unc_color, None, None],
                                             uncertainty_styles = ["5", None, None],
                                             ratio_reference = eff_nominal,
                                             outfile = os.path.join(cur_outdir, tagged_hist + "_" + suffix),
                                             inner_label = inner_label,
                                             outer_label = "t#bar{t}, di-lepton",
                                             x_label = "p_{T} [GeV]",
                                             y_label = "b efficiency",
                                             y_ratio_label = "ratio to high p_{T} eff.")

              RatioPlotter.create_ratio_plot(curves = [eff_nominal, eff_alt_data, eff_alt_MC, eff_alt_MC_inclusive],
                                             rel_uncertainties_up = [cur_unc_up, total_unc_up_data, None, None],
                                             rel_uncertainties_down = [cur_unc_down, -total_unc_down_data, None, None], 
                                             labels = ["high p_{T} extrap. (" + name + ")",#", events with exactly 2 jets with p_{T} > 20 GeV, |#eta| < 2.5)",
                                                       "PDF b-calibration (stat. + syst., data17)", "PDF b-calibration (stat. only, MC16d, signal region)", "PDF b-calibration (stat. only, MC16d, inclusive)"],
                                             marker_colors = [hpt_color, var_color_data, var_color_MC, cp.get_color("blue2")],
                                             uncertainty_fill_colors = [unc_color, var_color_data, None, None],
                                             uncertainty_styles = ["5", "2", None, None],
                                             ratio_reference = eff_nominal,
                                             outfile = os.path.join(cur_outdir, tagged_hist + "_with_data_" + suffix),
                                             inner_label = inner_label,
                                             outer_label = "t#bar{t}, di-lepton",
                                             x_label = "p_{T} [GeV]",
                                             y_label = "b efficiency",
                                             y_ratio_label = "ratio to high p_{T} eff.")
              
         if plot_variations:
              var_outdir = os.path.join(cur_outdir, "variations")
              if not os.path.exists(var_outdir):
                   os.makedirs(var_outdir)

              # plot the individual variations separately as well
              for cur_varname, cur_var in eff_unc.variations.items():
                  
                   var_shortname = syst_str_rep(cur_varname)
                   
                   RatioPlotter.create_ratio_plot(curves = [eff_unc] + cur_var,
                                                  rel_uncertainties_up = [total_unc_up_MC] + [None for var in cur_var],
                                                  rel_uncertainties_down = [total_unc_down_MC] + [None for var in cur_var],
                                                  curve_styles = ["pZ"] + ["lp" for var in cur_var],
                                                  labels = ["high p_{T} (stat. + syst.)"] + [var_shortname] + ["" for var in cur_var[:-1]],
                                                  marker_colors = [hpt_color] + [cp.get_color("blue2") for var in cur_var],
                                                  uncertainty_fill_colors = [cp.get_color("greenblue1")] + [None for var in cur_var],
                                                  ratio_reference = eff_unc,
                                                  outfile = os.path.join(var_outdir, tagged_hist + "_" + var_shortname),
                                                  inner_label = inner_label,
                                                  outer_label = "t#bar{t}, di-lepton",
                                                  x_label = "p_{T} [GeV]",
                                                  y_label = "b efficiency",
                                                  y_ratio_label = "ratio to high p_{T} eff.")
                   
                
if __name__ == "__main__":
    main()
