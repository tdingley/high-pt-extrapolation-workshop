import re
from enum import Enum

class StrEnum(str, Enum):
    pass

class EfficiencyCurveOrigin(StrEnum):
    MC_weights = "MC_FSR*"
    tracks = "TRK_*"
    jets = "JET_*"
    pdf = "PDF_*"
    nominal = "nominal"
    ps = "PS_*"
    #orig = "JET_*_orig"
    #prop = "JET_*_prop"

class VariationType(Enum):
    up = "UP"
    down = "DOWN"
    to_symmetrize = "both"    

class EfficiencySorter:

    @staticmethod
    def by_regex(syst_name, regex):
        cur_re = re.compile(regex)
        if cur_re.match(syst_name):
            return True
        else:
            return False

    @staticmethod
    def by_source(syst_name):
        # nominal_re = re.compile(".*Nominal/MCWeight_nominal")
        MC_weights_re = re.compile(".*/Nominal/MC_.*")
        # tracks_re = re.compile(".*TRK_.*/MCWeight_nominal.*")
        # jets_re = re.compile(".*JET_.*/MCWeight_nominal.*")

        nominal_re = re.compile(".*[Nn]ominal/nominal")
        MC_weights_re = re.compile(".*MC_.*/nominal.*")
        tracks_re = re.compile(".*TRK_.*/nominal.*")
        jets_re = re.compile(".*JET_.*/nominal.*")
        #jet_test_re = re.compile(".*BJES_.*/nominal.*")
        pdf_re = re.compile(".*Nominal/PDF_.*")
        ps_test_re = re.compile(".*herwig7/nominal.*")
        #jets_orig_re = re.compile("JET*_orig/nominal.*")
        #jets_prop_re = re.compile("JET*_prop/nominal.*")


        if MC_weights_re.match(syst_name):
            return EfficiencyCurveOrigin.MC_weights
        if tracks_re.match(syst_name):
            return EfficiencyCurveOrigin.tracks
        elif jets_re.match(syst_name):
            return EfficiencyCurveOrigin.jets
        elif pdf_re.match(syst_name):
            return EfficiencyCurveOrigin.pdf
        elif ps_test_re.match(syst_name):
            return EfficiencyCurveOrigin.ps
        #elif jets_orig_re.match(syst_name):
        #    return EfficiencyCurveOrigin.orig
        #elif jets_prop_re.match(syst_name):
        #    return EfficiencyCurveOrigin.prop
        elif nominal_re.match(syst_name):
            return EfficiencyCurveOrigin.nominal
        else:
            raise NotImplementedError("<" + syst_name + "> is from no known source, are you sure this is not the nominal case?")

    @staticmethod
    def by_type(syst_name):
        up_re = re.compile(".*(up|UP).*$")
        down_re = re.compile(".*(down|DOWN).*$")

        if up_re.match(syst_name):
            return VariationType.up
        elif down_re.match(syst_name):
            return VariationType.down
        else:
            # this includes any variation that is not explicitely called UP / DOWN, but also PDF variations!
            return VariationType.to_symmetrize
