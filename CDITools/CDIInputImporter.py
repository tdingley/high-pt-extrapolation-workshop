import re

from Curve import Curve

class CDIInputImporter:

    # extracts the information from the header line
    @staticmethod
    def _read_header(infile_path):
        header_re = re.compile("Analysis\((.+),\s*(.+),\s*(.+),\s*(.+),\s*(.+)\)")

        with open(infile_path, 'r') as infile:
            for line in infile:
                m = header_re.match(line)
                if m:
                    calib_name = m.group(1)
                    final_state = m.group(2)
                    tagger = m.group(3)
                    WP = m.group(4)
                    jetcoll = m.group(5)
                    break

        return calib_name, final_state, tagger, WP, jetcoll

    # reads in the list of available systematics
    @staticmethod
    def _read_available_systematics(infile_path):
        available_systematics = []

        bin_header_re = re.compile(".*bin\(.*")
        bin_sys_re = re.compile(".*sys\((.+),(.+)%\).*")

        bin_active = False
        with open(infile_path, 'r') as infile:
            for line in infile:
                if bin_active:
                    m = bin_sys_re.match(line)
                    if m:
                        available_systematics.append(m.group(1))

                    m = bin_header_re.match(line)
                    if m:
                        break

                if not bin_active:
                    m = bin_header_re.match(line)
                    if m:
                        bin_active = True

        return available_systematics

    @staticmethod
    def _read_central_values(infile_path):        
        bin_header_re = re.compile(".*bin\((.+)<pt<(.+),.*\)")
        central_value_re = re.compile(".*central_value\((.+),(.+)\)")

        bin_edges = []
        bin_values = []
        bin_errors = []

        bin_active = False
        with open(infile_path, 'r') as infile:
            for line in infile:
                if bin_active:
                    m = central_value_re.match(line)
                    if m:
                        bin_values.append(float(m.group(1)))
                        bin_errors.append(float(m.group(2)))

                    m = bin_header_re.match(line)
                    if m:
                        bin_active = False

                if not bin_active:
                    m = bin_header_re.match(line)
                    if m:
                        bin_edges.append(float(m.group(1)))
                        bin_edges.append(float(m.group(2)))
                        bin_active = True

        # get rid of all duplicates
        bin_edges = sorted(list(set(bin_edges)))

        return Curve.from_array(bin_edges, bin_values, bin_errors, "central_value")

    @staticmethod
    def _read_systematics_values(infile_path, systematics_name):
        bin_header_re = re.compile(".*bin\((.+)<pt<(.+),.*\)")

        # make sure all special characters contained in the systematics name are escaped!
        systematics_name = systematics_name.replace('*', '\*')
        central_value_re = re.compile(".*sys\(" + systematics_name + ",(.+)%\)")

        bin_edges = []
        bin_values = []
        bin_errors = []

        bin_active = False
        with open(infile_path, 'r') as infile:
            for line in infile:
                if bin_active:
                    m = central_value_re.match(line)
                    if m:
                        bin_values.append(float(m.group(1)) / 100.0)
                        bin_errors.append(0.0)

                    m = bin_header_re.match(line)
                    if m:
                        bin_active = False

                if not bin_active:
                    m = bin_header_re.match(line)
                    if m:
                        bin_edges.append(float(m.group(1)))
                        bin_edges.append(float(m.group(2)))
                        bin_active = True

        # get rid of all duplicates
        bin_edges = sorted(list(set(bin_edges)))

        if len(bin_values) == 0:
            raise Exception("Error: systematic '{}' not found".format(systematics_name))

        return Curve.from_array(bin_edges, bin_values, bin_errors, systematics_name)
        
