import os, re
import ROOT
import numpy as np
from argparse import ArgumentParser

from CDIInputImporter import CDIInputImporter
from CDIInputExporter import CDIInputExporter
from RelUncertainty import RelUncertainty

def make_max_envelope(curve, pT_range, zero_below_threshold = True):
    retcurve = curve.clone()

    # first, find the maximum value that this uncertainty component ever attains
    bin_centers = retcurve.get_bin_centers()
    curvevals = [retcurve.evaluate(cur_bin_center) for cur_bin_center in bin_centers if cur_bin_center > pT_range[0] and cur_bin_center < pT_range[1]]
    maxval = curvevals[np.argmax(np.abs(curvevals))]

    # then, generate the new curve
    for cur_bin_center in bin_centers:
        
        if cur_bin_center <= pT_range[0] and zero_below_threshold:
            retcurve.set(cur_bin_center, 0.0)

        if cur_bin_center > pT_range[0]:
            #retcurve.set(cur_bin_center, maxval)
            retcurve.set(cur_bin_center, abs(maxval)) ## temporary: make double sure that nothing fishy can happen with the sign
    
    return retcurve

def MakeMaxEnvelope(CDI_in, CDI_out, pT_low, pT_high, select_regex, veto_regex):
    # load the CDI text inputs
    calib_name, final_stat, tagger, WP, jetcoll = CDIInputImporter._read_header(CDI_in)
    options = {"WP": WP, "jet_collection": jetcoll, "tagger": tagger}

    # make sure to use the right jet collection name
    options["jet_collection"] = "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903"

    pT_range = (pT_low, pT_high)

    available_systematics = CDIInputImporter._read_available_systematics(CDI_in)
    central_value = CDIInputImporter._read_central_values(CDI_in)
    stat_unc = central_value.create_uncertainty_obj()
    stat_uncs_up = [stat_unc]
    stat_uncs_down = [-stat_unc]

    syst_uncs_up = {}
    syst_uncs_down = {}

    if isinstance(select_regex, str):
        select_regex = re.compile(select_regex)

    if isinstance(veto_regex, str):
        veto_regex = re.compile(veto_regex)

    # make sure that each uncertainty component attains its maximum value above the pT threshold
    for cur_syst_name in available_systematics:

        cur_syst = RelUncertainty(CDIInputImporter._read_systematics_values(CDI_in, cur_syst_name))

        if select_regex.match(cur_syst_name) and not veto_regex.match(cur_syst_name):
            cur_processed_syst = make_max_envelope(cur_syst, pT_range)
        else:
            cur_processed_syst = cur_syst

        syst_uncs_up[cur_syst_name] = cur_processed_syst
        syst_uncs_down[cur_syst_name] = cur_processed_syst
        
    # write out the processed CDI text inputs
    CDIInputExporter.produce_CDI_input_from_uncertainties(CDI_out, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)

if __name__ == "__main__":
    ROOT.gROOT.SetBatch()

    parser = ArgumentParser(description = "create max-type extrapolation uncertainties")
    parser.add_argument("--CDI_in", action = "store", dest = "CDI_in")
    parser.add_argument("--CDI_out", action = "store", dest = "CDI_out")
    parser.add_argument("--pT_low", action = "store", type = float, dest = "pT_low", default = 400)
    parser.add_argument("--pT_high", action = "store", type = float, dest = "pT_high", default = 3000)
    parser.add_argument("--select_regex", action = "store", default = ".*")
    parser.add_argument("--veto_regex", action = "store", default = "a^")
    args = vars(parser.parse_args())

    MakeMaxEnvelope(**args)

