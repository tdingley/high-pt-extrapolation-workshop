from TFileUtils import TFileUtils
from RelUncertainty import RelUncertainty
from UncertaintyCombinator import UncertaintyCombinator
from EfficiencyCurve import EfficiencyCurve
from RatioPlotter import RatioPlotter
from EfficiencyLoader import EfficiencyLoader
from EfficiencySorter import EfficiencySorter, EfficiencyCurveOrigin, VariationType
from SystematicUtils import SystematicUtils
from VariationGrouper import VariationGrouper
from ColorPicker import ColorPicker

import ROOT

import sys, os
from argparse import ArgumentParser

# Converts a (sub)directory path into a short string representation by concatenating the individual
# directory names. This allows to uniquely represent a certain (combination of) systematic variations
def syst_str_rep(raw):
     subdirs = raw.split(':')[1]
     return subdirs.replace('/', '_')

# This generates the typical "extrapolation" plots, showing the extrapolated efficiency curve
def generate_extrapolation_plots(fullsim_path, fastsim_path, tagged_hist, output_dir, inner_label = "", outer_label = "", plot_variations = False, 
                                 fullsim_syst_groups = ["TRK_*", "JET_*", "PDF_*"], fastsim_syst_groups = ["MC_*"], xaxis_range = (20, 3000)):

     # load the two efficiency curves together with their associated variations
     eff_fullsim = EfficiencyLoader.from_highpt_xtrap_file(fullsim_path, tagged_hist, nominal_name = ".*[Nn]ominal/nominal")

     # ... as well as the systematic uncertainty from the fullsim samples (for TRK, JET and PDF uncertainties)
     fullsim_syst_unc_up, fullsim_syst_unc_down = UncertaintyCombinator.syst(eff_fullsim)

     # get the statistical uncertainty on the nominal efficiency curve as well (will always be taken from the nominal - fullsim - sample)
     fullsim_stat_unc_up, fullsim_stat_unc_down = UncertaintyCombinator.stat(eff_fullsim)

     if fastsim_path:
          eff_fastsim = EfficiencyLoader.from_highpt_xtrap_file(fastsim_path, tagged_hist, nominal_name = ".*[Nn]ominal/nominal")

          # get the systematic uncertainty from the fastsim samples (for modeling uncertainties)...
          fastsim_syst_unc_up, fastsim_syst_unc_down = UncertaintyCombinator.syst(eff_fastsim)

          # compute the total uncertainty band, combining all systematic and statistical uncertainties taken from fastsim AND fullsim samples
          total_unc_up = RelUncertainty.from_quadrature_sum([fullsim_stat_unc_up, fullsim_syst_unc_up, fastsim_syst_unc_up])
          total_unc_down = -RelUncertainty.from_quadrature_sum([fullsim_stat_unc_down, fullsim_syst_unc_down, fastsim_syst_unc_down])
     else:
          eff_fastsim = None

          # omit the fastsim component, if not explicitly specified
          total_unc_up = RelUncertainty.from_quadrature_sum([fullsim_stat_unc_up, fullsim_syst_unc_up])
          total_unc_down = -RelUncertainty.from_quadrature_sum([fullsim_stat_unc_down, fullsim_syst_unc_down])

     uncs_up = [total_unc_up]
     uncs_down = [total_unc_down]
     
     for category, cur_eff_curve in zip([fullsim_syst_groups, fastsim_syst_groups], [eff_fullsim, eff_fastsim]):           # this iterates over fastsim / fullsim
          for syst_group in category: # this iterates over the individual groups within that sample / category
               group_uncertainties = cur_eff_curve.uncertainties[syst_group]
               group_syst_envelope_up, group_syst_envelope_down = UncertaintyCombinator.from_group(syst_group, nominal_curve = cur_eff_curve, group_uncertainties = group_uncertainties)
               
               # will always plot with the fullsim curve as the nominal one, hence combine with the statistical uncertainty coming from that curve
               group_total_envelope_up = RelUncertainty.from_quadrature_sum([group_syst_envelope_up, fullsim_stat_unc_up])
               group_total_envelope_down = -RelUncertainty.from_quadrature_sum([group_syst_envelope_down, fullsim_stat_unc_down])

               uncs_up.append(group_total_envelope_up)
               uncs_down.append(group_total_envelope_down)

     # can now start plotting!
     cp = ColorPicker()
     hpt_color = cp.get_color("black")
     var_color_data = cp.get_color("orange3")
     var_color_MC = cp.get_color("red2")

     unc_colors = [cp.get_color("greenblue1"), cp.get_color("blue2"), cp.get_color("red1"), cp.get_color("orange1"), cp.get_color("green3")]

     for cur_unc_up, cur_unc_down, cur_syst_name, cur_unc_color in zip(uncs_up, uncs_down, ["syst."] + fullsim_syst_groups + fastsim_syst_groups, unc_colors):

          cur_file_suffix = cur_syst_name.replace('_*', '').replace('.', '')
     
          RatioPlotter.create_ratio_plot(curves = [eff_fullsim],
                                         rel_uncertainties_up = [cur_unc_up],
                                         rel_uncertainties_down = [cur_unc_down], 
                                         labels = ["high p_{T} extrap. (stat. + " + cur_syst_name + ")"],
                                         marker_colors = [hpt_color],
                                         uncertainty_fill_colors = [cur_unc_color],
                                         uncertainty_styles = ["5"],
                                         ratio_reference = eff_fullsim,
                                         outfile = os.path.join(output_dir, tagged_hist + "_" + cur_file_suffix),
                                         inner_label = inner_label,
                                         outer_label = outer_label,
                                         x_label = "p_{T} [GeV]",
                                         y_label = "b efficiency",
                                         y_ratio_label = "ratio to high p_{T} eff.",
                                         xaxis_range = xaxis_range)

     # if also requested to plot the individual variations, do it here
     if plot_variations:
          var_outdir = os.path.join(output_dir, "variations")
          if not os.path.exists(var_outdir):
               os.makedirs(var_outdir)
          
          # plot the individual variations separately as well
          for eff_nominal in [eff_fullsim, eff_fastsim]:
               if eff_nominal:
                    for cur_varname, cur_unc in eff_nominal.uncertainties.items():                      
                         if not "_raw" in cur_varname and not "_symmetrized" in cur_varname:
                              continue

                         # small subtlety here: want to show the statistical uncertainties on the variations as they have been computed
                         # but if compute the variation as below, would again fold in the statistical uncertainties of the nominal curve
                         # to avoid this double-counting, first zero out the statistical uncertainties in the nominal curve
                         eff_nominal_zero_stat = eff_nominal.clone()
                         eff_nominal_zero_stat.remove_uncertainty()
                         cur_var = [eff_nominal_zero_stat + unc.convert_to_absolute(eff_nominal_zero_stat) for unc in cur_unc]

                         var_shortname = syst_str_rep(cur_varname)

                         RatioPlotter.create_ratio_plot(curves = [eff_nominal] + cur_var,
                                                        rel_uncertainties_up = [total_unc_up] + [None for var in cur_var],
                                                        rel_uncertainties_down = [total_unc_down] + [None for var in cur_var],
                                                        curve_styles = ["pZ"] + ["lp" for var in cur_var],
                                                        labels = ["high p_{T} (stat. + syst.)"] + [var_shortname] + ["" for var in cur_var[:-1]],
                                                        marker_colors = [hpt_color] + [cp.get_color("blue2") for var in cur_var],
                                                        uncertainty_fill_colors = [cp.get_color("greenblue1")] + [None for var in cur_var],
                                                        ratio_reference = eff_nominal,
                                                        outfile = os.path.join(var_outdir, tagged_hist + "_" + var_shortname),
                                                        inner_label = inner_label,
                                                        outer_label = outer_label,
                                                        x_label = "p_{T} [GeV]",
                                                        y_label = "b efficiency",
                                                        y_ratio_label = "ratio to high p_{T} eff.",
                                                        xaxis_range = xaxis_range)

     
# This generates the plots comparing the efficiency from the high-pt extrapolation to the efficiency from
# the PDF b-calibration
def generate_validation_plots(fullsim_path, fastsim_path, PDF_efficiency_file, tagged_hist, output_dir, PDF_efficiency_file_inclusive = "", inner_label = "", outer_label = ""):
     # the corresponding histograms for the data-based results of the PDF b-calibration...
     PDF_data_eff_hist_names = {
          "tagged_FixedCutBEff_60": "e_b_60_Postfit",
          "tagged_FixedCutBEff_70": "e_b_70_Postfit",
          "tagged_FixedCutBEff_77": "e_b_77_Postfit",
          "tagged_FixedCutBEff_85": "e_b_85_Postfit"
          }

     # ... and their systematic uncertainties
     PDF_data_syst_unc_up_hist_names = {
          "tagged_FixedCutBEff_60": "sf_b_60_syst_Error_high_rel",
          "tagged_FixedCutBEff_70": "sf_b_70_syst_Error_high_rel",
          "tagged_FixedCutBEff_77": "sf_b_77_syst_Error_high_rel",
          "tagged_FixedCutBEff_85": "sf_b_85_syst_Error_high_rel"
          }

     PDF_data_syst_unc_down_hist_names = {
          "tagged_FixedCutBEff_60": "sf_b_60_syst_Error_low_rel",
          "tagged_FixedCutBEff_70": "sf_b_70_syst_Error_low_rel",
          "tagged_FixedCutBEff_77": "sf_b_77_syst_Error_low_rel",
          "tagged_FixedCutBEff_85": "sf_b_85_syst_Error_low_rel"
          }

     # the corresponding histograms for the (inclusive) MC results...
     PDF_MC_eff_inclusive_hist_names = {
          "tagged_FixedCutBEff_60": "h_effMC_ttbar_b_60wp",
          "tagged_FixedCutBEff_70": "h_effMC_ttbar_b_70wp",
          "tagged_FixedCutBEff_77": "h_effMC_ttbar_b_77wp",
          "tagged_FixedCutBEff_85": "h_effMC_ttbar_b_85wp"
          }

     # ... and those for the efficiencies in the ttbar signal region only
     PDF_MC_eff_SR_hist_names = {
          "tagged_FixedCutBEff_60": "h_effMC_ttbar_b_60wp_c_nominal",
          "tagged_FixedCutBEff_70": "h_effMC_ttbar_b_70wp_c_nominal",
          "tagged_FixedCutBEff_77": "h_effMC_ttbar_b_77wp_c_nominal",
          "tagged_FixedCutBEff_85": "h_effMC_ttbar_b_85wp_c_nominal"
          }

     # do very similar things compared to before: first, load the efficiency curves (but only take the pT-bins that are going to be needed for the comparison)
     eff_fullsim = EfficiencyLoader.from_highpt_xtrap_file(fullsim_path, tagged_hist, nominal_name = ".*Nominal/nominal", binrange = range(1, 10))

     fullsim_syst_unc_up, fullsim_syst_unc_down = UncertaintyCombinator.syst(eff_fullsim)
     fullsim_stat_unc_up, fullsim_stat_unc_down = UncertaintyCombinator.stat(eff_fullsim)

     if fastsim_path:
          eff_fastsim = EfficiencyLoader.from_highpt_xtrap_file(fastsim_path, tagged_hist, nominal_name = ".*Nominal/nominal", binrange = range(1, 10))     
     
          # get the combined uncertainty band
          fastsim_syst_unc_up, fastsim_syst_unc_down = UncertaintyCombinator.syst(eff_fastsim)

          total_unc_up = RelUncertainty.from_quadrature_sum([fullsim_stat_unc_up, fullsim_syst_unc_up, fastsim_syst_unc_up])
          total_unc_down = -RelUncertainty.from_quadrature_sum([fullsim_stat_unc_down, fullsim_syst_unc_down, fastsim_syst_unc_down])
     else:
          total_unc_up = RelUncertainty.from_quadrature_sum([fullsim_stat_unc_up, fullsim_syst_unc_up])
          total_unc_down = -RelUncertainty.from_quadrature_sum([fullsim_stat_unc_down, fullsim_syst_unc_down])

     # now, also load the efficiencies from the PDF b-calibration for data
     PDF_data_eff = EfficiencyCurve.from_histogram(PDF_efficiency_file, os.path.join("results_comulative", PDF_data_eff_hist_names[tagged_hist]))
     PDF_data_eff_stat_unc_up, PDF_data_eff_stat_unc_down = UncertaintyCombinator.stat(PDF_data_eff)
     PDF_data_eff_syst_unc_up = RelUncertainty.from_histogram(PDF_efficiency_file, os.path.join("results_comulative", PDF_data_syst_unc_up_hist_names[tagged_hist]))
     PDF_data_eff_syst_unc_down = -RelUncertainty.from_histogram(PDF_efficiency_file, os.path.join("results_comulative", PDF_data_syst_unc_down_hist_names[tagged_hist]))

     PDF_data_eff_total_unc_up = RelUncertainty.from_quadrature_sum([PDF_data_eff_syst_unc_up, PDF_data_eff_stat_unc_up])
     PDF_data_eff_total_unc_down = -RelUncertainty.from_quadrature_sum([PDF_data_eff_syst_unc_down, PDF_data_eff_stat_unc_down])

     # as well as the MC efficiencies with their associated statistical uncertainties
     PDF_MC_eff_SR = EfficiencyCurve.from_histogram(PDF_efficiency_file, os.path.join("results_comulative", PDF_MC_eff_SR_hist_names[tagged_hist]))
     
     # and also the inclusive efficiencies (if available)
     if PDF_efficiency_file_inclusive:
          PDF_MC_eff_inclusive = EfficiencyCurve.from_histogram(PDF_efficiency_file_inclusive, os.path.join("MCeff", PDF_MC_eff_inclusive_hist_names[tagged_hist]))
     else:
          PDF_MC_eff_inclusive = None

     cp = ColorPicker()
     hpt_color = cp.get_color("black")
     unc_color = cp.get_color("greenblue1")
     var_color_data = cp.get_color("orange3")
     var_color_MC = cp.get_color("red2")

     # plot only with the combined uncertainty band, not the breakdown into individual components, but show it with and without the data-based points
     RatioPlotter.create_ratio_plot(curves = [eff_fullsim, PDF_MC_eff_SR, PDF_MC_eff_inclusive],
                                    rel_uncertainties_up = [total_unc_up, None, None],
                                    rel_uncertainties_down = [total_unc_down, None, None], 
                                    labels = ["high p_{T} extrap. (stat. + syst.)",
                                              "PDF b-calibration (stat. only, MC16d, signal region)", "PDF b-calibration (stat. only, MC16d, inclusive)"],
                                    marker_colors = [hpt_color, var_color_MC, cp.get_color("blue2")],
                                    uncertainty_fill_colors = [unc_color, None, None],
                                    uncertainty_styles = ["5", None, None],
                                    ratio_reference = eff_fullsim,
                                    outfile = os.path.join(output_dir, tagged_hist + "_validation_MC_only"),
                                    inner_label = inner_label,
                                    outer_label = outer_label,
                                    x_label = "p_{T} [GeV]",
                                    y_label = "b efficiency",
                                    y_ratio_label = "ratio to high p_{T} eff.",
                                    xaxis_range = (20, 600))

     RatioPlotter.create_ratio_plot(curves = [eff_fullsim, PDF_data_eff, PDF_MC_eff_SR, PDF_MC_eff_inclusive],
                                    rel_uncertainties_up = [total_unc_up, PDF_data_eff_total_unc_up, None, None],
                                    rel_uncertainties_down = [total_unc_down, PDF_data_eff_total_unc_down, None, None], 
                                    labels = ["high p_{T} extrap. (stat. + syst.)",
                                              "PDF b-calibration (stat. + syst., data17)", "PDF b-calibration (stat. only, MC16d, signal region)", "PDF b-calibration (stat. only, MC16d, inclusive)"],
                                    marker_colors = [hpt_color, var_color_data, var_color_MC, cp.get_color("blue2")],
                                    uncertainty_fill_colors = [unc_color, var_color_data, None, None],
                                    uncertainty_styles = ["5", "2", None, None],
                                    ratio_reference = eff_fullsim,
                                    outfile = os.path.join(output_dir, tagged_hist + "_validation_with_data"),
                                    inner_label = inner_label,
                                    outer_label = outer_label,
                                    x_label = "p_{T} [GeV]",
                                    y_label = "b efficiency",
                                    y_ratio_label = "ratio to high p_{T} eff.",
                                    xaxis_range = (20, 600))

def main():    
     ROOT.gROOT.SetBatch(True)

     parser = ArgumentParser(description = "plot efficiency curves")
     parser.add_argument("--outdir", action = "store", dest = "outdir")
     parser.add_argument("--tagger", action = "store", dest = "tagger")
     parser.add_argument("--jetcoll", action = "store", dest = "jetcoll")
     parser.add_argument("--ds", action = "store", dest = "ds")
     parser.add_argument("--plot_variations", action = "store_const", const = True, default = False)
     parser.add_argument("--hpt_fullsim", action = "store", dest = "hpt_fullsim_file")
     parser.add_argument("--hpt_fastsim", action = "store", dest = "hpt_fastsim_file")
     parser.add_argument("--PDF_SR", action = "store", dest = "PDF_SR_file")
     parser.add_argument("--PDF_inc", action = "store", dest = "PDF_inc_file")
     args = vars(parser.parse_args())

     args.setdefault("dataset", "")
     args.setdefault("PDF_SR_file", "")
     args.setdefault("PDF_inc_file", "")

     outdir_root = args["outdir"]
     jet_collection = args["jetcoll"]
     tagger = args["tagger"]
     dataset = args["ds"]
     plot_variations = args["plot_variations"]
     hpt_fullsim_file = args["hpt_fullsim_file"]
     hpt_fastsim_file = args["hpt_fastsim_file"]
     PDF_SR_file = args["PDF_SR_file"]
     PDF_inc_file = args["PDF_inc_file"]

     # attempt to guess whether this file comes from a ttbar or a Zprime run
     if "Z" in dataset and not "t" in dataset:
          dataset = "Zprime"
          outer_label = "Z'"
     elif "t" in dataset and not "Z" in dataset:
          dataset = "ttbar"
          outer_label = "t#bar{t}, di-lepton"
     else:
          print("Error: could not guess which datset this file comes from!")

     if dataset == "Zprime":
          if args["hpt_fullsim_file"] and args["hpt_fastsim_file"] in args:
               print("Error: something is fishy: do you really have separate fullsim and fastsim samples for Zprime?")
          if not "hpt_fullsim_file" in args and "hpt_fastsim_file" in args:
               print("Warning: passed a Zprime file generated from a fastsim dataset, will continue!")
               hpt_fullsim_file = hpt_fastsim_file
               hpt_fastsim_file = ""

          # these are the uncertainty groups that can be plotted individually for Zprime
          #fullsim_syst_groups = ["TRK_*", "JET_*"]
          fullsim_syst_groups = ["TRK_*"]
          fastsim_syst_groups = []

          # also set an axis range that works well
          xaxis_range = (20, 3000)

     elif dataset == "ttbar":
          #fullsim_syst_groups = ["TRK_*", "JET_*", "PDF_*"]
          #fullsim_syst_groups = ["PDF_*"]
          fullsim_syst_groups = []
          fastsim_syst_groups = ["MC_*"]
          xaxis_range = (20, 3000)

     # these are the names of the contained histograms
     truth_hist = "truth"
     tagged_hists = [
          # "tagged_FixedCutBEff_60", 
          # "tagged_FixedCutBEff_70", 
          "tagged_FixedCutBEff_77", 
          # "tagged_FixedCutBEff_85"
          ]

     tagged_hists_names = [
          "Fixed Cut, #epsilon_{b}^{MC} = 60%",
          "Fixed Cut, #epsilon_{b}^{MC} = 70%",
          "Fixed Cut, #epsilon_{b}^{MC} = 77%",
          "Fixed Cut, #epsilon_{b}^{MC} = 85%"
          ]

     wp_outdirs = [
          "FixedCutBEff_60", 
          "FixedCutBEff_70", 
          "FixedCutBEff_77", 
          "FixedCutBEff_85"
          ]

     id_string = "#sqrt{s} = 13 TeV, " + jet_collection + ", " + tagger
     
     for tagged_hist, hist_name, wp_outdir in zip(tagged_hists, tagged_hists_names, wp_outdirs):
          cur_outdir = os.path.join(outdir_root, wp_outdir)

          cur_highpt_outdir = os.path.join(cur_outdir, "highpt")
          cur_lowpt_outdir = os.path.join(cur_outdir, "lowpt")

          if not os.path.exists(cur_highpt_outdir):
               os.makedirs(cur_highpt_outdir)

          if not os.path.exists(cur_lowpt_outdir):
               os.makedirs(cur_lowpt_outdir)
          
          print("====================================")
          print(tagged_hist)

          # generate the high-pt extrapolation plots as well as the validation plots at low pt
          generate_extrapolation_plots(fullsim_path = hpt_fullsim_file,
                                       fastsim_path = hpt_fastsim_file,
                                       tagged_hist = tagged_hist,
                                       output_dir = cur_highpt_outdir,
                                       inner_label = "#splitline{" + id_string + "}{" + hist_name + "}",
                                       outer_label = outer_label,
                                       plot_variations = plot_variations,
                                       fullsim_syst_groups = fullsim_syst_groups,
                                       fastsim_syst_groups = fastsim_syst_groups,
                                       xaxis_range = xaxis_range)

          # generate the high-pt extrapolation plots as well as the validation plots at low pt
          generate_extrapolation_plots(fullsim_path = hpt_fullsim_file,
                                       fastsim_path = hpt_fastsim_file,
                                       tagged_hist = tagged_hist,
                                       output_dir = cur_lowpt_outdir,
                                       inner_label = "#splitline{" + id_string + "}{" + hist_name + "}",
                                       outer_label = outer_label,
                                       plot_variations = False,
                                       fullsim_syst_groups = fullsim_syst_groups,
                                       fastsim_syst_groups = fastsim_syst_groups,
                                       xaxis_range = (20, 600))

          # generate validation plots only for ttbar and only if the data-based results were given
          if dataset == "ttbar" and PDF_SR_file and PDF_inc_file:
               generate_validation_plots(fullsim_path = hpt_fullsim_file,
                                         fastsim_path = hpt_fastsim_file,
                                         PDF_efficiency_file = PDF_SR_file,
                                         PDF_efficiency_file_inclusive = PDF_inc_file,
                                         tagged_hist = tagged_hist,
                                         output_dir = cur_outdir,
                                         inner_label = "#splitline{" + id_string + "}{" + hist_name + "}",
                                         outer_label = outer_label)

          print("====================================")

if __name__ == "__main__":
     main()
