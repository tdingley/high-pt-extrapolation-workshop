import ROOT
import sys, os
from argparse import ArgumentParser

from RelUncertainty import RelUncertainty
from EfficiencyLoader import EfficiencyLoader
from UncertaintyCombinator import UncertaintyCombinator
from ColorPicker import ColorPicker
from RatioPlotter import RatioPlotter

def generate_jet_uncertainty_comparison_plots(retagging_path, noretagging_path, tagged_hist, output_dir, inner_label = ""):
    eff_retagging = EfficiencyLoader.from_highpt_xtrap_file(retagging_path, tagged_hist, nominal_name = ".*[Nn]ominal/nominal", do_rebinning = False, rebin_options = {"sigth": 2.0, "merge_start": 20, "merge_end": 3000})
    eff_noretagging = EfficiencyLoader.from_highpt_xtrap_file(noretagging_path, tagged_hist, nominal_name = ".*[Nn]ominal/nominal", do_rebinning = False, rebin_options = {"sigth": 2.0, "merge_start": 20, "merge_end": 3000})
    xaxis_range = (20, 3000)

    # get the combined statistical and systematic uncertainty band for each of them
    retagging_syst_unc_up, retagging_syst_unc_down = UncertaintyCombinator.syst(eff_retagging)
    retagging_stat_unc_up, retagging_stat_unc_down = UncertaintyCombinator.stat(eff_retagging)
    retagging_total_unc_up = RelUncertainty.from_quadrature_sum([retagging_syst_unc_up, retagging_stat_unc_up])
    retagging_total_unc_down = -RelUncertainty.from_quadrature_sum([retagging_syst_unc_down, retagging_stat_unc_down])

    noretagging_syst_unc_up, noretagging_syst_unc_down = UncertaintyCombinator.syst(eff_noretagging)
    noretagging_stat_unc_up, noretagging_stat_unc_down = UncertaintyCombinator.stat(eff_retagging)
    noretagging_total_unc_up = RelUncertainty.from_quadrature_sum([noretagging_syst_unc_up, noretagging_stat_unc_up])
    noretagging_total_unc_down = -RelUncertainty.from_quadrature_sum([noretagging_syst_unc_down, noretagging_stat_unc_down])

    # prepare the colors for the plots
    cp = ColorPicker()
    retagging_marker_color = cp.get_color("black")
    noretagging_marker_color = cp.get_color("black")
    retagging_color = cp.get_color("orange3")
    noretagging_color = cp.get_color("red2")

    # plot the two efficiency curves and their respective uncertainties
    RatioPlotter.create_ratio_plot(curves = [eff_noretagging, eff_retagging],
                                   rel_uncertainties_up = [noretagging_total_unc_up, retagging_total_unc_up],
                                   rel_uncertainties_down = [noretagging_total_unc_down, retagging_total_unc_down], 
                                   labels = ["b-efficiency (stat. + jet syst., tag uncalibrated jets)", "b-efficiency (stat. + jet syst., tag calibrated jets)"],
                                   marker_colors = [noretagging_marker_color, retagging_marker_color],
                                   uncertainty_fill_colors = [noretagging_color, retagging_color],
                                   uncertainty_styles = ["5", "5"],
                                   marker_styles = [20, 21],
                                   ratio_reference = eff_noretagging,
                                   outfile = os.path.join(output_dir, tagged_hist),
                                   inner_label = inner_label,
                                   outer_label = "",
                                   x_label = "p_{T} [GeV]",
                                   y_label = "b efficiency",
                                   y_ratio_label = "ratio to uncalibrated",
                                   color_alpha = 0.35,
                                   yaxis_range_rel = (0.3, 1.7),
                                   xaxis_range = xaxis_range,
                                   legpos = "topleft")

def main():
    ROOT.gROOT.SetBatch(True)

    parser = ArgumentParser(description = "try to factorize the jet-related uncertainties on the b-tagging efficiency")
    parser.add_argument("--hpt_jet_retagging", action = "store", dest = "hpt_retagging")
    parser.add_argument("--hpt_jet_noretagging", action = "store", dest = "hpt_noretagging")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    parser.add_argument("--tagger", action = "store", dest = "tagger")
    args = vars(parser.parse_args())

    infile_retagging = args["hpt_retagging"]
    infile_noretagging = args["hpt_noretagging"]
    outdir_root = args["outdir"]
    tagger = args["tagger"]

    truth_hist = "truth"
    tagged_hists = [
        "tagged_FixedCutBEff_60", 
        "tagged_FixedCutBEff_70", 
        "tagged_FixedCutBEff_77", 
        "tagged_FixedCutBEff_85"
        ]
    
    tagged_hists_names = [
        "Fixed Cut, #epsilon_{b}^{MC} = 60%",
        "Fixed Cut, #epsilon_{b}^{MC} = 70%",
        "Fixed Cut, #epsilon_{b}^{MC} = 77%",
        "Fixed Cut, #epsilon_{b}^{MC} = 85%"
        ]
    
    wp_outdirs = [
        "FixedCutBEff_60", 
        "FixedCutBEff_70", 
        "FixedCutBEff_77", 
        "FixedCutBEff_85"
        ]
    
    jet_collection = "AntiKt4EMPFlowJets"
    id_string = "#sqrt{s} = 13 TeV, " + jet_collection + ", " + tagger
    
    for tagged_hist, hist_name, wp_outdir in zip(tagged_hists, tagged_hists_names, wp_outdirs):
        cur_outdir = os.path.join(outdir_root, wp_outdir)
        
        if not os.path.exists(cur_outdir):
            os.makedirs(cur_outdir)

        generate_jet_uncertainty_comparison_plots(infile_retagging, infile_noretagging, tagged_hist, 
                                                  cur_outdir, inner_label = "#splitline{" + id_string + "}{" + hist_name + "}")

if __name__ == "__main__":
    main()
