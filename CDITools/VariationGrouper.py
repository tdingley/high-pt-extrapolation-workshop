from EfficiencySorter import EfficiencySorter, EfficiencyCurveOrigin
from AbsUncertainty import AbsUncertainty
from RelUncertainty import RelUncertainty

# contains methods that combine the individual systematic variations into groups and apply different symmetrization prescriptions
class VariationGrouper:
    
    # never symmetrize up / down variations, always symmtrize single variations
    @staticmethod
    def symmetrize_singles(efficiency_curve):
        for common_name, eff_vars in list(efficiency_curve.variations.items()):
            print("currently grouping " + common_name + " with " + str(len(eff_vars)) + " entries")

            origid = EfficiencySorter.by_source(common_name)

            if origid == EfficiencyCurveOrigin.jets:
                var_cat = "JET_*"
            elif origid == EfficiencyCurveOrigin.tracks:
                var_cat = "TRK_*"
            elif origid == EfficiencyCurveOrigin.MC_weights:
                var_cat = "MC_*"
            elif origid == EfficiencyCurveOrigin.pdf:
                var_cat = "PDF_*"
            elif origid == EfficiencyCurveOrigin.orig:
                var_cat = "JET_*_orig"
            elif origid == EfficiencyCurveOrigin.prop:
                var_cat = "JET_*_prop"
            #elif origid == EfficiencyCurveOrigin.jet_test:
            #    var_cat = "BJES_*"
            elif origid == EfficiencyCurveOrigin.ps:
                var_cat = "PS_*"
            else:
                raise Exception("came across a systematic variation that should not be here")

            if len(eff_vars) > 1:
                # have up / down variations, just add them to the group
                efficiency_curve.add_variation_to_category(var_cat, eff_vars[0])
                efficiency_curve.add_variation_to_category(var_cat, eff_vars[1])
            else:
                # have only a single variation, need to symmetrize
                var = eff_vars[0]
                efficiency_curve.add_variation_to_category(var_cat, efficiency_curve + (var - efficiency_curve))
                efficiency_curve.add_variation_to_category(var_cat, efficiency_curve - (var - efficiency_curve))

    # always symmetrize according to the following prescription
    # -> one-sided variations get symmetrized as +/- (varied - nominal)
    # -> two-sided variations:
    #        * use +/- 0.5 * (up - down) if up- and down-variations go in opposite directions
    #        * use +/- max(up, down) if up- and down-variations go in the same directions, or one of them is compatible with zero
    @staticmethod
    def symmetrize_always(efficiency_curve):
        for common_name, eff_vars in list(efficiency_curve.uncertainties.items()):
            if "_raw" in common_name:
                continue
            print(list(efficiency_curve.uncertainties.items()))
            #print(eff_vars[1].evaluate_uncertainty(500))
            
            print("currently grouping " + common_name + " with " + str(len(eff_vars)) + " entries")

            origid = EfficiencySorter.by_source(common_name)
            if origid == EfficiencyCurveOrigin.jets:
                var_cat = "JET_*"
            elif origid == EfficiencyCurveOrigin.tracks:
                var_cat = "TRK_*"
            elif origid == EfficiencyCurveOrigin.MC_weights:
                var_cat = "MC_*"
            elif origid == EfficiencyCurveOrigin.ps:
                var_cat = "PS_*"

            else:
                raise Exception("came across a systematic variation that should not be here")
            
            if len(eff_vars) > 1:
                if len(eff_vars) == 2:
                    unc_a = eff_vars[0]
                    unc_b = eff_vars[1]
                    print("unc_a here:", unc_a.evaluate_uncertainty(500))

                    mean_unc = (unc_a - unc_b) * 0.5# this also propagates all uncertainties
                    print("---")

                    print(mean_unc.evaluate_uncertainty_bin(5))
                    print("create final stat unc")
                    mean_unc.create_uncertainty_obj(no_caching = True)
                    print("---")

                    unc_symm = unc_a.clone()

                    for pos in range(unc_a.size()):
                        if unc_a.evaluate_bin(pos) * unc_b.evaluate_bin(pos) > 0:
                            # the variations go in the same direction here, always take the max()
                            if abs(unc_a.evaluate_bin(pos)) < abs(unc_b.evaluate_bin(pos)):
                                unc_symm.set_value_uncertainty_bin(pos, value = unc_b.evaluate_bin(pos), 
                                                                   uncertainty = unc_b.evaluate_uncertainty_bin(pos))
                            print("bin {}: taking max(up, down) = max({}, {})".format(pos, unc_a.evaluate_bin(pos), unc_b.evaluate_bin(pos)))
                            print("with uncertainties ({}, {}) -> {}".format(unc_a.evaluate_uncertainty_bin(pos), unc_b.evaluate_uncertainty_bin(pos), unc_symm.evaluate_uncertainty_bin(pos)))
                        else:
                            # the variations go in opposite directions here, take the mean
                            unc_symm.set_value_uncertainty_bin(pos, value = mean_unc.evaluate_bin(pos), 
                                                               uncertainty = mean_unc.evaluate_uncertainty_bin(pos))
                            print("bin {}: taking mean(up, down) = mean({}, {})".format(pos, unc_a.evaluate_bin(pos), unc_b.evaluate_bin(pos)))
                            print("with uncertainties ({}, {}) -> {}".format(unc_a.evaluate_uncertainty_bin(pos), unc_b.evaluate_uncertainty_bin(pos), unc_symm.evaluate_uncertainty_bin(pos)))

                    # the two variations change the nominal curve in different directions, use their symmetrized difference
                    efficiency_curve.add_uncertainty_to_category(var_cat, unc_symm)
                    efficiency_curve.add_uncertainty_to_category(var_cat, -unc_symm)

                    # also save the symmetrized version separately
                    efficiency_curve.add_uncertainty_to_category(common_name + "_symmetrized", unc_symm)
                    efficiency_curve.add_uncertainty_to_category(common_name + "_symmetrized", -unc_symm)
                else:
                    print("Error: have found {} variations.".format(len(eff_vars)))
            else:
                # have only a single variation, need to symmetrize
                unc = eff_vars[0]

                efficiency_curve.add_uncertainty_to_category(var_cat, unc)
                efficiency_curve.add_uncertainty_to_category(var_cat, -unc)

                efficiency_curve.add_uncertainty_to_category(common_name + "_symmetrized", unc)
                efficiency_curve.add_uncertainty_to_category(common_name + "_symmetrized", -unc)

                print("... single variation, symmetrizing")
