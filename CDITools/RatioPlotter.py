import ROOT
from ROOT import TH1F, TCanvas, TPad, TLegend, TGraphAsymmErrors, TMultiGraph, TGaxis, TLatex, TGraph, TGraphErrors
from ColorPicker import ColorPicker
import os

class RatioPlotter:

    @staticmethod
    def create_hist_plot(curves, labels, colors, inner_label, outfile, y_range = (0.0, 1.0), x_range = None, x_label = "p_{T} [GeV]", y_label = "extrapolation uncertainty"):

        if len(colors) != len(curves):
            cp = ColorPicker()
            default_color = cp.get_color("black")
            colors = [default_color for curve in curves]

        if len(labels) != len(curves):
            labels = ["" for curve in curves]

        canv = TCanvas("canv_ratio_plot", "canv", 10, 10, 2400, 1450)
        ROOT.SetOwnership(canv, False)

        canv.SetRightMargin(0.05)
        canv.SetTopMargin(0.05)
        canv.SetBottomMargin(0.15)
        canv.SetLeftMargin(0.12)
        canv.cd()

        label_column_x = 0.16

        leg = TLegend(label_column_x, 0.73 - 0.05 * len(curves), label_column_x + 0.15, 0.73)
        ROOT.SetOwnership(leg, False)

        leg.SetBorderSize(0)
        leg.SetTextFont(42)
        leg.SetTextSize(0.04)

        # plot the actual histograms
        first = True
        for curve, label, color in zip(curves, labels, colors):

            cur_hist = curve.hist.Clone()
            ROOT.SetOwnership(cur_hist, False)
            cur_hist.SetLineColor(color)
            cur_hist.SetLineWidth(3)
            cur_hist.SetLineStyle(1)
            cur_hist.SetTitle("")

            if y_range:
                cur_hist.SetMinimum(y_range[0])
                cur_hist.SetMaximum(y_range[1])

            if x_range:
                cur_hist.GetXaxis().SetRangeUser(x_range[0], x_range[1])

            x_axis = cur_hist.GetXaxis()
            x_axis.SetTitle(x_label)
            x_axis.SetTitleFont(42)
            x_axis.SetTitleSize(0.05)
            x_axis.SetTitleOffset(1.2)
            x_axis.SetLabelSize(0.05)
            x_axis.SetTickLength(0.02)

            y_axis = cur_hist.GetYaxis()
            y_axis.SetTitle(y_label)
            y_axis.SetTitleFont(42)
            y_axis.SetTitleSize(0.05)
            y_axis.SetTitleOffset(1.05)
            y_axis.SetLabelSize(0.05)
            y_axis.SetTickLength(0.02)

            if first:
                cur_hist.Draw("hist")
                first = False
            else:
                cur_hist.Draw("hist same")

            if label:
                leg.AddEntry(cur_hist, label, 'l')

        # draw the plot labels
        if inner_label:
            RatioPlotter._drawText(label_column_x, 0.78, inner_label, font = 42, fontsize = 0.04, alignment = 11)

        leg.Draw()

        # draw the ATLAS label
        RatioPlotter._drawATLASLabel(label_column_x, 0.88, "Work In Progress", fontsize = 0.05)

        ROOT.gPad.SetTicks()

        ROOT.gPad.Update()
        ROOT.gPad.RedrawAxis()

        canv.SaveAs(outfile + ".pdf")
        canv.SaveAs(outfile + ".png")
        canv.SaveAs(outfile + ".root")


    # General function to create a ratio plot:
    # Creates a plot showing the individual objects in the list of "curves" (which have their respective statistical uncertainty already associated), together with their assigned total uncertainties (as given by the lists "rel_uncertainties_up" and "rel_uncertainties_down")
    # Also adds a ratio pane, where the ratios w.r.t. the curve "ratio_reference" are plotted
    @staticmethod
    def create_ratio_plot(curves, rel_uncertainties_up, rel_uncertainties_down, outfile, x_label, y_label, y_ratio_label, labels = [], marker_colors = [], uncertainty_fill_colors = [], uncertainty_styles = [], curve_styles = [], marker_styles = [], inner_label = "", outer_label = "", ratio_reference = None, xaxis_range = (20, 600), yaxis_range_abs = (0, 1.4), yaxis_range_rel = (0.9, 1.1), color_alpha = 1.0, show_ratio = True, legpos = 'topleft', y_log = False, x_log = False):

        eps = 1e-5
        
        # if no explicit ratio reference curve is given, use the first curve as default option
        if not ratio_reference:
            ratio_reference = curves[0]

        # if no explicit choice of marker_colors is given, use the internal default
        if len(marker_colors) != len(curves):
            cp = ColorPicker()
            default_color = cp.get_color("black")
            marker_colors = [default_color for curve in curves]

        if len(uncertainty_fill_colors) != len(curves):
            cp = ColorPicker()
            default_color = cp.get_color("greenblue1")
            uncertainty_fill_colors = [default_color for curve in curves]

        # if no labels are given, don't put any
        if len(labels) != len(curves):
            labels = [None for curve in curves]

        # same for the total uncertainty band: don't put them if not specified for all curves
        if len(rel_uncertainties_up) != len(curves):
            rel_uncertainties_up = [None for curve in curves]

        if len(rel_uncertainties_down) != len(curves):
            rel_uncertainties_down = [None for curve in curves]

        if len(uncertainty_styles) != len(curves):
            uncertainty_styles = ["5" for curve in curves]

        if len(curve_styles) != len(curves):
            curve_styles = ["" for curve in curves]

        if len(marker_styles) != len(curves):
            marker_styles = [20 for curve in curves]

        # axis range of the absolute...
        # abs_y_min = 0
        # abs_y_max = 1.2
        abs_y_min = yaxis_range_abs[0]
        abs_y_max = yaxis_range_abs[1]

        # ... and relative plot
        # rel_y_min = 0.9
        # rel_y_max = 1.1

        rel_y_min = yaxis_range_rel[0]
        rel_y_max = yaxis_range_rel[1]

        # ticklengths of the plots
        ticklength_y = 0.01;
        ticklength_x = 0.02;
        
        # some general settings
        ROOT.gStyle.SetLineScalePS(3)
        ROOT.gStyle.SetOptStat(0)

        # -------------------------------------------
        # create the global layout of the plot
        # -------------------------------------------
        if show_ratio:
            canv = TCanvas("canv_ratio_plot", "canv", 10, 10, 2400, 2200)
        else:
            canv = TCanvas("canv_ratio_plot", "canv", 10, 10, 2400, int(2200 * (1 - 0.33)))
        ROOT.SetOwnership(canv, False)

        if show_ratio:
            pad_abs = TPad("abs_pad", "abs_pad", 0, 0.33, 1, 1)
        else:
            pad_abs = TPad("abs_pad", "abs_pad", 0, 0, 1, 1)

        if show_ratio:
            pad_abs.SetBottomMargin(0.02)
        else:
            pad_abs.SetBottomMargin(0.11)

        pad_abs.SetRightMargin(0.05)
        pad_abs.SetTopMargin(0.05)
        pad_abs.SetLeftMargin(0.15)

        pad_abs.SetFillColor(ROOT.kWhite)
        pad_abs.SetFrameFillStyle(0)
        pad_abs.Draw()
        canv.cd()

        if show_ratio:
            pad_rel = TPad("rel_pad", "rel_pad", 0, 0, 1, 0.33)
            pad_rel.SetTopMargin(0.02)
            pad_rel.SetBottomMargin(0.3)
            pad_rel.SetRightMargin(0.05)
            pad_rel.SetLeftMargin(0.15)
            pad_rel.SetFillColor(ROOT.kWhite)
            pad_rel.SetFrameFillStyle(0)
            pad_rel.Draw()

        # -------------------------------------------
        # fill the "absolute" part of the plot
        # -------------------------------------------
        # prepare the legend
        if legpos == "bottomleft":
            leg_abs = TLegend(0.12, 0.19, 0.22, 0.35)
        elif legpos == "topright":
            #leg_abs = TLegend(0.37, 0.74, 0.52, 0.87)
            leg_abs = TLegend(0.55, 0.6, 0.62, 0.77)
        elif legpos == "topleft":
            leg_abs = TLegend(0.18, 0.73 - 0.05 * len(curves), 0.30, 0.73)

        ROOT.SetOwnership(leg_abs, False)
        leg_abs.SetBorderSize(0)
        leg_abs.SetTextFont(42)
        leg_abs.SetTextSize(0.04)

        # prepare the actual graphs
        mg_abs = TMultiGraph()
        ROOT.SetOwnership(mg_abs, False)
        i=0
        for curve, marker_color, uncertainty_color, uncertainty_style, curve_style, marker_style, label, rel_uncertainty_up, rel_uncertainty_down in zip(curves, marker_colors, uncertainty_fill_colors, uncertainty_styles, curve_styles, marker_styles, labels, rel_uncertainties_up, rel_uncertainties_down):
            if not curve:
                continue
            i=i+1
            # this plots the points and the statistical uncertainty
            if y_log:
                # in case demand a logarithmic y-axis, make sure that everything is nonnegative
                RatioPlotter._set_nonnegative(curve.hist)

            cur_graph = TGraphAsymmErrors(curve.hist)
            cur_graph.SetLineColor(marker_color)
            cur_graph.SetMarkerStyle(marker_style)
            cur_graph.SetMarkerSize(3.5)
            cur_graph.SetLineWidth(1)
            cur_graph.SetMarkerColor(marker_color)
                
            if rel_uncertainty_up and rel_uncertainty_down:
                # abs_uncertainty_up = rel_uncertainty_up.convert_to_absolute(curve)
                # abs_uncertainty_down = rel_uncertainty_down.convert_to_absolute(curve)
                cur_graph_unc = cur_graph.Clone()
                cur_graph_unc = RatioPlotter._set_uncertainties(cur_graph_unc,
                                                                uncertainty_up = rel_uncertainty_up,
                                                                uncertainty_down = rel_uncertainty_down,
                                                                limit_up = 1.0,
                                                                limit_down = 0.0
                                                                )
                if i%2==0:
                    cur_graph_unc.SetFillStyle(3345)
                    cur_graph_unc.SetLineWidth(1)
                    ROOT.gStyle.SetHatchesLineWidth(1)
                    # ROOT.gStyle.SetHatchesLineColor(uncertainty_color)


                else:
                    cur_graph_unc.SetFillStyle(3354)
                    cur_graph_unc.SetLineWidth(1)
                    ROOT.gStyle.SetHatchesLineWidth(1)
                    # ROOT.gStyle.SetHatchesLineColor(uncertainty_color)
                    
                cur_graph_unc.SetLineColor(marker_color)
                cur_graph_unc.SetMarkerColor(marker_color)
                cur_graph_unc.SetFillColor(marker_color)
                cur_graph_unc.SetFillColorAlpha(marker_color, color_alpha)
                mg_abs.Add(cur_graph_unc, uncertainty_style if uncertainty_style else "")

                # make dummy graph just for the legend, with the correct line color etc
                cur_graph_unc_legend = TGraphAsymmErrors(cur_graph_unc)
                ROOT.SetOwnership(cur_graph_unc_legend, False)                
                
                cur_graph_unc_legend.SetLineColor(marker_color)
                cur_graph_unc_legend.SetMarkerColor(marker_color)

                if label:
                    leg_abs.AddEntry(cur_graph_unc_legend, label, "lpfe")
            elif label:
                leg_abs.AddEntry(cur_graph, label, "lpfe")

            mg_abs.Add(cur_graph, curve_style)

        if show_ratio:
            # -------------------------------------------
            # fill the "relative" part of the plot
            # -------------------------------------------
            mg_rel = TMultiGraph()
            ROOT.SetOwnership(mg_rel, False)
            for curve, marker_color, uncertainty_color, uncertainty_style, curve_style, marker_style, label, rel_uncertainty_up, rel_uncertainty_down in zip(curves, marker_colors, uncertainty_fill_colors, uncertainty_styles, curve_styles, marker_styles, labels, rel_uncertainties_up, rel_uncertainties_down):
                # get the current relative statistical uncertainty to be shown in the ratio plot
                rel_stat_unc_up = curve.get_statistical_uncertainty()
                rel_stat_unc_down = -rel_stat_unc_up

                # compute the actual ratio curve
                graph_ratio = TGraphAsymmErrors((curve / ratio_reference).hist)

                # since this will scale the visible uncertainties as well, copy over the true, relative statistical uncertainty again
                graph_ratio = RatioPlotter._set_uncertainties(graph_ratio,
                                                              uncertainty_up = rel_stat_unc_up,
                                                              uncertainty_down = rel_stat_unc_down)
                graph_ratio.SetLineColor(marker_color)
                graph_ratio.SetMarkerStyle(marker_style)
                graph_ratio.SetMarkerSize(3.5)
                graph_ratio.SetLineWidth(1)
                graph_ratio.SetMarkerColor(marker_color)

                if rel_uncertainty_up and rel_uncertainty_down:
                    # prepare the graph showing the assigned total relative uncertainty band
                    graph_unc_rel = graph_ratio.Clone()
                    graph_unc_rel = RatioPlotter._set_uncertainties(graph_unc_rel,
                                                                    uncertainty_up = rel_uncertainty_up,
                                                                    uncertainty_down = rel_uncertainty_down)

                    cur_graph_unc.SetFillStyle(3344)
                    cur_graph_unc.SetLineWidth(1)
                    ROOT.gStyle.SetHatchesLineWidth(1)                    
                    
                    graph_unc_rel.SetLineColor(uncertainty_color)
                    graph_unc_rel.SetFillColor(uncertainty_color)
                    graph_unc_rel.SetFillColorAlpha(uncertainty_color, color_alpha)
                    graph_unc_rel.SetMarkerColor(uncertainty_color)

                    mg_rel.Add(graph_unc_rel, uncertainty_style if uncertainty_style else "")

                mg_rel.Add(graph_ratio, curve_style)

        # -------------------------------------------
        # put all components of the graph together
        # -------------------------------------------
        # first, draw the pad with the absolute curves
        pad_abs.cd()
        mg_abs.Draw("alp")
        mg_abs.GetYaxis().SetRangeUser(abs_y_min, abs_y_max)
        mg_abs.GetXaxis().SetLimits(xaxis_range[0], xaxis_range[1])
        mg_abs.GetYaxis().SetTickLength(ticklength_y)
        mg_abs.GetYaxis().SetTitleOffset(0.62 * 2)
        #mg_abs.GetXaxis().SetTickLength(2.5 * ticklength_x / (abs_y_max - abs_y_min))
        mg_abs.GetXaxis().SetTickLength(ticklength_x)
        mg_abs.GetXaxis().SetTitle(x_label)
        mg_abs.GetYaxis().SetTitle(y_label)

        # adjust axis titles fonts and sizes
        mg_abs.GetYaxis().SetTitleFont(42)
        mg_abs.GetYaxis().SetTitleSize(0.05)
        mg_abs.GetYaxis().SetLabelSize(0.05)

        mg_abs.GetXaxis().SetTitleFont(42)
        mg_abs.GetXaxis().SetLabelFont(42)

        if y_log:
            pad_abs.SetLogy()
            pad_abs.Draw()

        if show_ratio:
            #mg_abs.GetXaxis().SetTitleSize(0.035)
            mg_abs.GetXaxis().SetLabelSize(0)
        else:
            mg_abs.GetXaxis().SetLabelSize(0.05)
            mg_abs.GetXaxis().SetTitleSize(0.05)

        ROOT.gPad.SetTicks()
        mg_abs.Draw("alp")

        if x_log:
            pad_abs.SetLogx()

        leg_abs.Draw()

        # draw the plot labels:
        if inner_label:
            inner_label_obj = TLatex()
            inner_label_obj.SetTextAlign(11)
            inner_label_obj.SetTextFont(42)
            inner_label_obj.SetTextSize(0.04)

            if legpos == "bottomleft":
                inner_label_obj.DrawLatexNDC(0.12, 0.15, inner_label)
            elif legpos == "topright":
                #inner_label_obj.DrawLatexNDC(0.37, 0.65, inner_label)
                inner_label_obj.DrawLatexNDC(0.45, 0.81, inner_label)
            elif legpos == "topleft":
                inner_label_obj.DrawLatexNDC(0.18, 0.78, inner_label)

        # draw the outer label
        if outer_label:
            plot_label = TLatex()
            plot_label.SetTextAlign(11)
            plot_label.SetTextFont(42)
            plot_label.SetTextSize(0.035)
            plot_label.DrawLatexNDC(0.12, 0.79, outer_label)

        # draw the ATLAS label
        RatioPlotter._drawATLASLabel(0.18, 0.88, "Work In Progress", fontsize = 0.05)

        if show_ratio:
            # then draw the second pad with all relative curves
            pad_rel.cd()
            mg_rel.Draw("alp")
            mg_rel.GetYaxis().SetRangeUser(rel_y_min + eps, rel_y_max - eps)
            mg_rel.GetXaxis().SetLimits(xaxis_range[0], xaxis_range[1])
            #mg_rel.GetYaxis().SetTickLength(ticklength_y)
            mg_rel.GetYaxis().SetNdivisions(504)
            #mg_rel.GetXaxis().SetTickLength(ticklength_x / (rel_y_max - rel_y_min))
            mg_rel.GetXaxis().SetTitle(x_label)
            mg_rel.GetYaxis().SetTitle(y_ratio_label)
            mg_rel.GetYaxis().SetTitleOffset(0.62)
            mg_rel.GetXaxis().SetTitleOffset(1.1)

            # adjust axis titles fonts and sizes
            mg_rel.GetYaxis().SetTitleFont(42)
            mg_rel.GetYaxis().SetTitleSize(0.05 * 2)

            mg_rel.GetXaxis().SetTitleFont(42)
            mg_rel.GetXaxis().SetTitleSize(0.05 * 2)

            mg_rel.GetYaxis().SetLabelFont(42)
            mg_rel.GetYaxis().SetLabelSize(0.05 * 2)

            mg_rel.GetXaxis().SetLabelFont(42)
            mg_rel.GetXaxis().SetLabelSize(0.05 * 2)
            ROOT.gPad.SetTicks()        
            mg_rel.Draw("alp")

            if x_log:
                pad_rel.SetLogx()

        canv.cd()

        # redraw the bounding box on top of everything else
        if show_ratio:
            pad_rel.cd()
            ROOT.gPad.Update()
            ROOT.gPad.RedrawAxis()

        pad_abs.cd()
        ROOT.gPad.Update()
        ROOT.gPad.RedrawAxis()

        # save the plot
        canv.SaveAs(outfile + ".pdf")
        canv.SaveAs(outfile + ".png")
        canv.SaveAs(outfile + ".root")

    # helper function to copy uncertainties, taking into account physical limits on the quantity plotted:
    # limit_up ... maximum value of the quantity whose uncertainties are set in this function
    # limit_down ... minimium value of the quantity whose uncertainties are set in this function
    #   --> these two parameters mainly concern *physical limits*, i.e. hard boundaries that cannot possibly be crossed
    @staticmethod
    def _set_uncertainties(graph, uncertainty_up, uncertainty_down, limit_up = None, limit_down = None):
        for ind in range(graph.GetN()):
            unc_up = uncertainty_up.hist.GetBinContent(ind + 1) if uncertainty_up else 0.0
            unc_down = uncertainty_down.hist.GetBinContent(ind + 1) if uncertainty_down else 0.0
            
            # clip the values at the physical limits, if given
            if limit_up:
                point_x, point_y = ROOT.Double(), ROOT.Double()
                graph.GetPoint(ind, point_x, point_y)
                unc_up = min(unc_up, abs(limit_up - point_y))

            if limit_down:
                point_x, point_y = ROOT.Double(), ROOT.Double()
                graph.GetPoint(ind, point_x, point_y)
                unc_up = min(unc_down, abs(limit_down - point_y))

            graph.SetPointEYhigh(ind, unc_up)
            graph.SetPointEYlow(ind, -unc_down)
        return graph
    
    @staticmethod
    def _set_nonnegative(hist):
        eps = 1e-6

        for ind in range(hist.GetSize()):
            if(hist.GetBinContent(ind) <= 0.0):
                hist.SetBinContent(ind, eps)

    @staticmethod
    def _drawText(x, y, text, color = ROOT.kBlack, fontsize = 0.05, font = 42, doNDC = True, alignment = 12):
        tex = ROOT.TLatex()
        if doNDC:
            tex.SetNDC()
        ROOT.SetOwnership(tex, False)
        tex.SetTextAlign(alignment)
        tex.SetTextSize(fontsize)
        tex.SetTextFont(font)
        tex.SetTextColor(color)
        tex.DrawLatex(x, y, text)        

    @staticmethod
    def _drawATLASLabel(x, y, text, doNDC = True, fontsize = 0.07):
        RatioPlotter._drawText(x, y, "ATLAS", fontsize = fontsize, font = 72, doNDC = doNDC, alignment = 11)
        RatioPlotter._drawText(x + 0.11, y, text, fontsize = fontsize, font = 42, doNDC = doNDC, alignment = 11)

