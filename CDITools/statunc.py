import ROOT
from math import sqrt
# Open the ROOT file containing the histogram
file = ROOT.TFile.Open("/home/dingleyt/QT/FSR/mc20/combinedc/01/DenmarkFSR_DL1dv01False_MUNC_FSR_800030_EMPFlow_c_jet.root")
# Retrieve the TH1D histogram from the file
histogram_nominal_truth = file.Get("/AntiKt4EMPFlowJets/nominal/nominal/truth_bootstrap_0")
histogram_nominal_tagged = file.Get("/AntiKt4EMPFlowJets/nominal/nominal/tagged_FixedCutBEff_60_bootstrap_0")

eff_nom = histogram_nominal_tagged.Clone()
eff_nom.Divide(histogram_nominal_truth)

# systematic variation:
histogram_varied_truth = file.Get("/AntiKt4EMPFlowJets/MC_FSR_1_05/nominal/truth_bootstrap_0")
histogram_varied_tagged = file.Get("/AntiKt4EMPFlowJets/MC_FSR_1_05/nominal/tagged_FixedCutBEff_60_bootstrap_0")

eff_var = histogram_varied_tagged.Clone()
eff_var.Divide(histogram_varied_truth)
# Check if the histogram is valid
if eff_nom:
    # Specify the bin number for which you want to get the bin error
    num_bins = eff_nom.GetNbinsX()

    for bin_number in range(num_bins):
        if bin_number == 0:
            continue
        #bin_number = 6  # Replace with your desired bin number
        print(bin_number)
        # Get the bin error for the specified bin
        bin_error = eff_nom.GetBinError(bin_number)
        #print(histogram_tagged.GetBinContent(bin_number))
        #print(histogram_nominal.GetBinContent(bin_number))
        error_nom = eff_nom.GetBinContent(bin_number) * sqrt( (1 / histogram_nominal_tagged.GetBinContent(bin_number) + 1 / histogram_nominal_truth.GetBinContent(bin_number)))
        error_var = eff_var.GetBinContent(bin_number) * sqrt( (1 / histogram_varied_tagged.GetBinContent(bin_number) + 1 / histogram_varied_truth.GetBinContent(bin_number)))
        overall_error = ((eff_var.GetBinContent(bin_number)) / eff_nom.GetBinContent(bin_number)) * sqrt( error_nom**2 + error_var**2 )
        print("bin:"+str(bin_number) + "error_nominal:" + str(error_nom))
        print("bin:"+str(bin_number) + "error_varied:" + str(error_var))
        print("bin:"+str(bin_number) + "error_overall:" + str(overall_error))

        # Print the bin error
        print(bin_error)
else:
    print("Failed to retrieve the histogram.")

# Close the ROOT file
file.Close()
