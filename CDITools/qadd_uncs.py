import os
from argparse import ArgumentParser

from CDIInputImporter import CDIInputImporter
from CDIInputExporter import CDIInputExporter
from RelUncertainty import RelUncertainty

def QuadraticallyCombineUncertainties(infile, outfile):
    uncs = []
    available_uncertainties = CDIInputImporter._read_available_systematics(infile)

    central_value = CDIInputImporter._read_central_values(infile)
    stat_unc = central_value.create_uncertainty_obj()
    stat_uncs_up = [stat_unc]
    stat_uncs_down = [-stat_unc]
    
    for cur_uncertainty in available_uncertainties:
        cur_syst = RelUncertainty(CDIInputImporter._read_systematics_values(infile, cur_uncertainty))
        uncs.append(cur_syst)

    unc_combined = RelUncertainty.from_quadrature_sum(uncs)
    syst_uncs_up = {"combined": unc_combined}
    syst_uncs_down = {"combined": -unc_combined}

    calib_name, final_stat, tagger, WP, jetcoll = CDIInputImporter._read_header(infile)
    options = {"WP": WP, "jet_collection": jetcoll, "tagger": tagger}

    CDIInputExporter.produce_CDI_input_from_uncertainties(outfile, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)

if __name__ == "__main__":
    parser = ArgumentParser(description = "quadratically sum the uncertainties found in the txt input files")
    parser.add_argument("--infile", action = "store")
    parser.add_argument("--outfile", action = "store")
    args = vars(parser.parse_args())

    QuadraticallyCombineUncertainties(**args)
