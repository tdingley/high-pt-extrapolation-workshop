from argparse import ArgumentParser

from CDIInputImporter import CDIInputImporter
from CDIInputExporter import CDIInputExporter
from RelUncertainty import RelUncertainty

from scipy.interpolate import interp1d

def rebin(incurve, new_binning_curve, mode = 'hist'):
    retcurve = new_binning_curve.clone()

    # first, extract the current curve
    centers = incurve.get_bin_centers()
    values = [incurve.evaluate(xval) for xval in centers]

    # extract the new binning
    ob_lower_edges = new_binning_curve.get_bin_l_edges()
    ob_upper_edges = new_binning_curve.get_bin_u_edges()
    ob_centers = new_binning_curve.get_bin_centers()

    print("using the following new bin centers")
    print(ob_centers)

    if mode == 'interp':
        # make it continuous by subjecting it to a 1st order spline
        interp = interp1d(centers, values, kind = "linear", fill_value = "extrapolate")
        
        # evaluate the spline on the new bin edges, in a conservative way
        for cur_bin_center, cur_bin_l_edge, cur_bin_u_edge in zip(ob_centers, ob_lower_edges, ob_upper_edges):
            print("setting: {} = {}".format(cur_bin_center, interp(cur_bin_center)))
            retcurve.set(cur_bin_center, max(interp(cur_bin_l_edge), interp(cur_bin_u_edge), interp(cur_bin_center)))
    elif mode == 'hist':
        # just keep it flat within the original bin edges
        for cur_bin_center in ob_centers:
            retcurve.set(cur_bin_center, incurve.evaluate(cur_bin_center))

    return retcurve

def rebin_CDI(infile, outfile, convert_to_binning_from):
    # first, load the binning that this CDI input file should be rebinned to
    new_binning_curve = CDIInputImporter._read_central_values(convert_to_binning_from)

    # then read in the systematics from the original file that are to be rebinned
    available_uncertainties = CDIInputImporter._read_available_systematics(infile)

    # also fetch the metadata
    calib_name, final_stat, tagger, WP, jetcoll = CDIInputImporter._read_header(infile)
    options = {"WP": WP, "jet_collection": jetcoll, "tagger": tagger}
    central_value = CDIInputImporter._read_central_values(infile)
    stat_unc = rebin(central_value.create_uncertainty_obj(), new_binning_curve)

    stat_uncs_up = [stat_unc]
    stat_uncs_down = [-stat_unc]

    syst_uncs_up = {}
    syst_uncs_down = {}
    
    # iterate over all uncertainties and add them in their rebinned form
    for cur_uncertainty in available_uncertainties:
        cur_curve = RelUncertainty(CDIInputImporter._read_systematics_values(infile, cur_uncertainty))
        rebinned_curve = rebin(cur_curve, new_binning_curve)
        syst_uncs_up[cur_uncertainty] = rebinned_curve
        syst_uncs_down[cur_uncertainty] = rebinned_curve

    # export the modified CDI input file
    CDIInputExporter.produce_CDI_input_from_uncertainties(outfile, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)

if __name__ == "__main__":
    parser = ArgumentParser(description = "retain the uncertainty components found in this CDI, but evaluate them on a new binning")
    parser.add_argument("--in", action = "store", dest = "infile")
    parser.add_argument("--out", action = "store", dest = "outfile")
    parser.add_argument("--convert_to_binning_from", action = "store", default = None)
    args = vars(parser.parse_args())

    rebin_CDI(**args)
