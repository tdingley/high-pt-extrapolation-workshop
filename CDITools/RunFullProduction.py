import sys, os, glob, re, fnmatch
from argparse import ArgumentParser
import subprocess as sp

#import all_effprod_condor_quick.py
#import all_effprod_social.py
#import CombineEfficiencies.py
#import RunCDIInputProduction.py
#import RunCDIPlottingCampaign.py

def run_all(datadir, histdir, flavour, syst_type):
    pythondir = os.environ["XTRAP_ROOTDIR"]
    print(os.environ["XTRAP_ROOTDIR"], "xtrap folder")
    cmd = ["python", os.path.join(pythondir, "all_effprod_social.py"), datadir, "-o", histdir, "-F", flavour, "-S", syst_type]
    print(cmd)
    joined_cmd = " ".join(cmd)
    print(joined_cmd)

    print(" ".join(cmd))

    sp.Popen(cmd)
    #cmd2 = 

    #cmd = ["python", os.path.join(pythondir, "RunCDIInputProductionCampaign.py"), "--refpt", refpt, "--CDI_in", infile, "--outdir", outdir, "--PCBT_istrue", PCBT_istrue]


def _run_plcdi_uncs(infile, outdir, refpt):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "plcdi_uncs.py"), "--refpt", refpt, "--CDI_in", infile, "--outdir", outdir, "--PCBT_istrue", PCBT_istrue]
    print(" ".join(cmd))
    sp.Popen(cmd)


if __name__ == "__main__":
    if not os.environ["XTRAP_ROOTDIR"]:
        raise Exception("Error: 'XTRAP_ROOTDIR' not defined. Please do 'source high-pT-extrapolation/setup.sh'")

    parser = ArgumentParser(description = "orchestrates the production of extrapolation CDI text inputs")
    parser.add_argument("--indata")
    parser.add_argument('-S', '--syst_type', default="Tracking", help="Please specifiy type from: Tracking, Jet, Modelling")
    parser.add_argument('-F', '--flav_jet', default="b", help="Please specify jet flavour of interest: b, c or l")
   
    parser.add_argument("--indir")
    parser.add_argument("--outdir")
    parser.add_argument("--refpt", action = "store", dest = "refpt") 
    parser.add_argument('-P', '--PCBT_istrue', default="False", help="Please specify which scale factor scheme: False=Fixed cut, True=Pseudo-continuous")

    args = vars(parser.parse_args())
    refpt = args["refpt"]
    datadir = args["indata"]
    histdir = args["indir"]
    print(datadir)
    cdidir = args['outdir']
    flavour = args['flav_jet']
    syst_type = args['syst_type']
    PCBT_istrue = args["PCBT_istrue"]
    print(PCBT_istrue)
    print(syst_type)
    
    #for indir in args["indirs"]:
    #    print("processing '{}'".format(indir))
    #    RunCDIInputPlottingCampaign(indir, refpt)
    run_all(datadir, histdir, flavour, syst_type)
    exit(0)
