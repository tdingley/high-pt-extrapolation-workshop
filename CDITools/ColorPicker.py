import ROOT as r

class ColorPicker:

    def __init__(self):
        self.colors = {}

        self.colors["black"] = r.kBlack
        self.colors["gray"] = r.kGray + 1
        self.colors["atlasgray1"] = r.TColor.GetColor(167, 176, 183)
        self.colors["atlasgray2"] = r.TColor.GetColor(86, 90, 91)

        self.colors["green1"] = r.TColor.GetColor(206, 219, 175)
        self.colors["green2"] = r.TColor.GetColor(185, 207, 150)
        self.colors["green3"] = r.TColor.GetColor(105, 145, 59)
        self.colors["atlasgreen1"] = r.TColor.GetColor(176, 204, 126)

        self.colors["greenyellow1"] = r.TColor.GetColor(227, 229, 151)
        self.colors["greenyellow2"] = r.TColor.GetColor(219, 222, 114)
        self.colors["greenyellow3"] = r.TColor.GetColor(170, 179, 0)

        self.colors["greenblue1"] = r.TColor.GetColor(188, 210, 195)
        self.colors["greenblue2"] = r.TColor.GetColor(123, 162, 150)
        self.colors["greenblue3"] = r.TColor.GetColor(0, 119, 112)

        self.colors["orange1"] = r.TColor.GetColor(243, 222, 116)
        self.colors["orange2"] = r.TColor.GetColor(245, 207, 71)
        self.colors["orange3"] = r.TColor.GetColor(207, 122, 48)
        self.colors["atlasorange1"] = r.TColor.GetColor(222, 215, 126)

        self.colors["blue1"] = r.TColor.GetColor(161, 196, 208)
        self.colors["blue2"] = r.TColor.GetColor(95, 155, 175)
        self.colors["blue3"] = r.TColor.GetColor(68, 104, 125)
        self.colors["atlasblue1"] = r.TColor.GetColor(25, 25, 209)
        self.colors["atlasblue2"] = r.TColor.GetColor(51, 153, 153)
        self.colors["atlasblue3"] = r.TColor.GetColor(102, 102, 102)

        self.colors["red1"] = r.TColor.GetColor(235, 196, 203)
        self.colors["red2"] = r.TColor.GetColor(190, 15, 52)
        self.colors["red3"] = r.TColor.GetColor(135, 36, 52)

    def get_color(self, name):
        return self.colors[name]
