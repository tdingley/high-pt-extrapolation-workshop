from EfficiencyCurve import EfficiencyCurve
from EfficiencyPlotter import EfficiencyPlotter
import os

def main():
    infilepath = "/afs/cern.ch/work/s/ssindhu/private/pflow_jets/high-pT-extrapolation/All_outputs/hists/combined/01/DL1r_TUNC_BLS_Zprime_AntiKt4EMPFlowJets.root"
    output_dir = "/afs/cern.ch/work/s/ssindhu/private/pflow_jets/high-pT-extrapolation/All_outputs/efficiency"

    ec = EfficiencyCurve.from_histogram(infilepath,"truth_nominal_bootstrap_0")
    
    # also, combine the variations in quadrature and plot it as well
    unc_total_up = ec.get_combined_uncertainty_up()
    unc_total_down = ec.get_combined_uncertainty_down()
    print("combined rel. uncertainty:")
    
    EfficiencyPlotter.plot_uncertainty_ratio(ec, 
                                             rel_uncertainty_up = unc_total_up, 
                                             rel_uncertainty_down = unc_total_down.invert(), # which prescription to use here? 
                                             outfile = os.path.join(output_dir, "reloaded.pdf"))
    

if __name__ == "__main__":
     main()
