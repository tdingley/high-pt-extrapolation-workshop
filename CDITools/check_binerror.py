import ROOT

# Open the ROOT file containing the histogram
file = ROOT.TFile.Open("/home/dingleyt/QT/FSR/mc20/combinedb/01/DenmarkFSR_DL1dv01False_MUNC_FSR_800030_EMPFlow_b_jet.root")

# Retrieve the TH1D histogram from the file
histogram = file.Get("/AntiKt4EMPFlowJets/nominal/nominal/tagged_FixedCutBEff_85_bootstrap_0")

# Check if the histogram is valid
if histogram:
    # Specify the bin number for which you want to get the bin error
    bin_number = 6  # Replace with your desired bin number

    # Get the bin error for the specified bin
    bin_error = histogram.GetBinError(bin_number)

    # Print the bin error
    print(bin_error)
else:
    print("Failed to retrieve the histogram.")

# Close the ROOT file
file.Close()
