#!/bin/bash
python ee.py --outdir ~/BTagPlotsNew/AntiKt4EMTopoJets/MV2c10/Zprime/ --tagger MV2c10 --jetcoll AntiKt4EMTopoJets --plot_variations ~/data/2018_12_20_Zprime/combined_MV2c10.root
python ee.py --outdir ~/BTagPlotsNew/AntiKt4EMTopoJets/DL1r/Zprime/ --tagger DL1r --jetcoll AntiKt4EMTopoJets --plot_variations ~/data/2018_12_20_Zprime/combined_DL1r.root
python ee.py --outdir ~/BTagPlotsNew/AntiKt4EMTopoJets/MV2c10/Zprime/ --tagger MV2c10 --jetcoll AntiKt4EMTopoJets --plot_variations ~/data/2018_12_21_ttbar/combined_MV2c10.root
python ee.py --outdir ~/BTagPlotsNew/AntiKt4EMTopoJets/DL1r/ttbar/ --tagger DL1r --jetcoll AntiKt4EMTopoJets --plot_variations ~/data/2018_12_21_ttbar/combined_DL1r.root