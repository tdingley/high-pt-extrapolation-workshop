from EfficiencyCurve import EfficiencyCurve
from EfficiencySorter import EfficiencySorter, EfficiencyCurveOrigin, VariationType
from RelUncertainty import RelUncertainty
from UncertaintyCombinator import UncertaintyCombinator
from VariationGrouper import VariationGrouper
from SystematicUtils import SystematicUtils
from TFileUtils import TFileUtils

import os, sys

class EfficiencyLoader:

    # reads all available EfficiencyCurve objects from the information
    # 0in the given ROOT file. Do not attempt to do any processing
    # whatsoever
    @staticmethod
    def as_efficiency_collection(infile_path, tagged_hist, nominal_name = "", binrange = None):
        truth_hist = "truth"
        retdict = {}

        # read the entire list of available leaf directories (each leaf directory contains all the information to build efficiency curves
        # for different working points)
        dirnames = TFileUtils.get_leaf_dirlist(infile_path)
        jet_collection_name = TFileUtils.get_dirlist(infile_path)[0]
        print("found the following leaf directories in this file:")
        for dirname in dirnames:
            print(dirname)

        # ---------------------------------
        # load the nominal efficiency curve
        # ---------------------------------
        if not nominal_name:
            # if no dedicated name for the nominal efficiency curve is given, just take the default choice
            nominal_dirname = [dirname for dirname in dirnames if EfficiencySorter.by_source(dirname) == EfficiencyCurveOrigin.nominal]
        else:
            # otherwise, look for the name that was passed
            nominal_dirname = [dirname for dirname in dirnames if EfficiencySorter.by_regex(dirname, nominal_name)]

        if len(nominal_dirname) > 1:
            print("Error: found multiple candidates for the nominal run -- you might want to check your tree names")
            sys.exit()
        elif len(nominal_dirname) == 0:
            print("Error: found no candidate for the nominal run -- you might want to check your tree names")
            sys.exit()
        else:
            nominal_dirname = nominal_dirname[0]
            print("found the following nominal run: {}".format(nominal_dirname))
        
            # this object will be associated with *all* systematic variations
            eff_nominal = EfficiencyCurve.from_file(file_path = infile_path, 
                                                   histname_probe = os.path.join(nominal_dirname, truth_hist),
                                                   histname_tag = os.path.join(nominal_dirname, tagged_hist),
                                                   name = "nominal",
                                                   binrange = binrange)
            eff_nominal.create_uncertainty_obj()
            retdict["nominal"] = eff_nominal

            for dirname in dirnames:
                if not nominal_dirname in dirname:
                    eff_var = EfficiencyCurve.from_file(file_path = infile_path,
                                                        histname_probe = os.path.join(dirname, truth_hist),
                                                        histname_tag = os.path.join(dirname, tagged_hist),
                                                        name = dirname,
                                                        binrange = binrangse)
                    eff_var.create_uncertainty_obj()
                    retdict[dirname] = eff_var

        return retdict
        

    # reads and constructs an EfficiencyCurve object from the information
    # found in the given ROOT file. Also looks for and attaches all 
    # systematic variations to it.
    # tagged hist ... name of the histograms corresponding to the chosen WP
    # bin_range ... range of the bins that should be loaded and used 
    #               (useful when comparing to efficiency curves binned in a different way)
    @staticmethod
    def from_highpt_xtrap_file(infile_path, tagged_hist, binrange = None, nominal_name = "", do_rebinning = True, do_smoothing = False, rebin_options = {"sigth": 0.0, "merge_start": 20, "merge_end": 1400}):
        truth_hist = "truth"

        # read the entire list of available leaf directories (each leaf directory contains all the information to build efficiency curves
        # for different working points)
        dirnames = TFileUtils.get_leaf_dirlist(infile_path)
        jet_collection_name = TFileUtils.get_dirlist(infile_path)[0]
        print("found the following leaf directories in this file:")
        for dirname in dirnames:
            print(dirname)
        print(nominal_name)
        print("**********************")
        
        # ---------------------------------
        # load the nominal efficiency curve
        # ---------------------------------
        if not nominal_name:
            # if no dedicated name for the nominal efficiency curve is given, just take the default choice
            nominal_dirname = [dirname for dirname in dirnames if EfficiencySorter.by_source(dirname) == EfficiencyCurveOrigin.nominal]
        else:
            # otherwise, look for the name that was passed
            nominal_dirname = [dirname for dirname in dirnames if EfficiencySorter.by_regex(dirname, nominal_name)]

        if len(nominal_dirname) > 1:
            print("Error: found multiple candidates for the nominal run -- you might want to check your tree names")
            sys.exit()
        elif len(nominal_dirname) == 0:
            print("Error: found no candidate for the nominal run -- you might want to check your tree names")
            sys.exit()
        else:
            nominal_dirname = nominal_dirname[0]
            print("found the following nominal run: {}".format(nominal_dirname))
            print("histogram_probe name: {}".format(os.path.join(nominal_dirname,tagged_hist)))
            # this object will be associated with *all* systematic variations
            eff_global = EfficiencyCurve.from_file(file_path = infile_path, 
                                                   histname_probe = os.path.join(nominal_dirname, truth_hist),
                                                   histname_tag = os.path.join(nominal_dirname, tagged_hist),
                                                   name = "nominal",
                                                   binrange = binrange)

            for dirname in dirnames:
                if not nominal_dirname in dirname:
                    eff_var = EfficiencyCurve.from_file(file_path = infile_path,
                                                        histname_probe = os.path.join(dirname, truth_hist),
                                                        histname_tag = os.path.join(dirname, tagged_hist),
                                                        name = dirname,
                                                        binrange = binrange)
                    
                    common_name = SystematicUtils.get_common_name(dirname)

                    # use a default significance threshold of 2 sigma here for the rebinning!
                    if do_rebinning:
                        unc = RelUncertainty.from_variation_significance_rebinning(eff_global, eff_var, name = eff_var.name, 
                                                                                   sigth = rebin_options["sigth"], merge_start = rebin_options["merge_start"], 
                                                                                   merge_end = rebin_options["merge_end"], smooth_uncertainty = do_smoothing)
                    else:
                        unc = RelUncertainty.from_variation(eff_global, eff_var, name = eff_var.name)

                    # store the variation in the dict under this common name
                    eff_global.add_uncertainty_to_category(common_name, unc)

                    # also store it under its own name
                    eff_global.add_uncertainty_to_category(dirname + "_raw", unc)

            # show the built categories
            print("have the following common-name categories")
            for name, arr in eff_global.uncertainties.items():
                print("{}: {}".format(name, len(arr)))

            # create variation groups
            VariationGrouper.symmetrize_always(eff_global)

        return eff_global
