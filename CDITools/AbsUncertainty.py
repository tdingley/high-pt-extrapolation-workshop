from Curve import Curve

class AbsUncertainty(Curve):

    def __init__(self, curve, name = None):
        super(AbsUncertainty, self).__init__(curve.hist, name, curve.bootstrap_replicas)

    # constructs a new AbsUncertainty object from the total
    # difference between a nominal and a varied efficiency curve
    @classmethod
    def from_variation(cls, nom_curve, var_curve):
        abs_unc = nom_curve - var_curve
        obj = cls(curve = abs_unc)
        return obj

    def convert_to_relative(self, normalization):
        from RelUncertainty import RelUncertainty
        rel_curve = self / normalization
        return RelUncertainty(curve = rel_curve)
