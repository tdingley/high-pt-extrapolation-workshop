#!/bin/bash

LOCAL_DIR="/home/windischhofer/BTagPlotsSymmetrized/*"
REMOTE_DIR="/eos/user/p/phwindis/www/plots/MCBTagEff/"
REMOTE_HOST="lxplus"

# first, delete the old plots that might still be around
ssh $REMOTE_HOST "rm -r $REMOTE_DIR/*"

# copy the new plots
scp -r $LOCAL_DIR ${REMOTE_HOST}:${REMOTE_DIR}
