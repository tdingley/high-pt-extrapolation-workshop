import os, re
from argparse import ArgumentParser

import ROOT

from CDIInputImporter import CDIInputImporter
from RatioPlotter import RatioPlotter
from ColorPicker import ColorPicker
from RelUncertainty import RelUncertainty

from plcdi_uncs import get_CDI_extrapolation_uncertainty

def plot_CDI_uncertainties_comparison(outdir, infiles, unc_labels, refpt):

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    WP_descriptions = {
        "FixedCutBEff_60": "Fixed Cut, #epsilon_{b}^{MC} = 60%",
        "FixedCutBEff_70": "Fixed Cut, #epsilon_{b}^{MC} = 70%",
        "FixedCutBEff_77": "Fixed Cut, #epsilon_{b}^{MC} = 77%",
        "FixedCutBEff_85": "Fixed Cut, #epsilon_{b}^{MC} = 85%"
        }

    assert(len(unc_labels) == len(infiles))

    cp = ColorPicker()
    suggested_colors = [cp.get_color("black"), cp.get_color("orange3"), cp.get_color("blue2"), cp.get_color("green2")]

    # for each of the specified files, load the total combined uncertainty and plot them
    total_uncs = []
    uncs_colors = []

    for ind, infile in enumerate(infiles):
        uncs_colors.append(suggested_colors[ind])

        # also get some metadata
        calib_name, final_state, tagger, WP, jetcoll = CDIInputImporter._read_header(infile)

        total_unc = get_CDI_extrapolation_uncertainty(infile, ".*", refpt = refpt)
        total_uncs.append(total_unc)

    xaxis_range = (refpt, 3000)
    maxunc = max([cur_unc.max() for cur_unc in total_uncs if cur_unc is not None])
    yaxis_range = (0, maxunc * 1.4)

    WP_description = WP_descriptions[WP]
    id_string = "#sqrt{s} = 13 TeV, " + jetcoll + ", " + tagger
    inner_label = "#splitline{" + id_string + "}{" + WP_description + "}"

    outfile = os.path.join(outdir, "comp")

    # plot it
    RatioPlotter.create_hist_plot(curves = total_uncs,
                                  labels = unc_labels,
                                  colors = uncs_colors,
                                  inner_label = inner_label,
                                  outfile = outfile,
                                  y_range = yaxis_range,
                                  x_range = xaxis_range,
                                  x_label = "p_{T} [GeV]",
                                  y_label = "extrap. unc.")    
    
if __name__ == "__main__":
    ROOT.gROOT.SetBatch()

    parser = ArgumentParser(description = "produce summary plots showing the different uncertainty components in the CDI input file")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    parser.add_argument("--labels", nargs = '+', action = "store", dest = "unc_labels")
    parser.add_argument("--infiles", nargs = '+', action = "store")
    parser.add_argument("--refpt", type = float, action = "store", default = 325)
    args = vars(parser.parse_args())

    plot_CDI_uncertainties_comparison(**args)
