import sys, os, glob, re
from argparse import ArgumentParser
import subprocess as sp

from CombineEfficiencies import _get_dataset, _get_jet_collection, _get_tagger, _get_jet_type

def _run_ccdi(outfile, infiles):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "ccdi.py"), "--batch"] + infiles + [outfile]
    print(" ".join(cmd))
    sp.check_output(cmd)

def _run_effdiff2cdi(outfile, file_a, file_b, WP, tagger, jetcoll):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "effdiff2cdi.py"), "--infile_nominal", file_a, "--infile_alternative", file_b, "--outfile", outfile, "--WP", WP, "--tagger", tagger, "--jetcoll", jetcoll]
    print(" ".join(cmd))
    sp.check_output(cmd)

def _run_pcdi(outfile, WP = "FixedCutBEff_77", ds = "Zprime", tagger = "DL1dv01", jet_type = "b_jet", jet_collection = "AntiKt4EMPFlowJets", input_files = [], PCBT_istrue = "False"):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    #STAT: cmd = ["python", os.path.join(pythondir, "pcdi.py"), "--store_stat_unc", "--out", outfile, "--WP", WP, "--jet_type", jet_type, "--ds", ds, "--tagger", tagger, "--jet_collection", jet_collection, "--PCBT_istrue", PCBT_istrue] + input_files
    cmd = ["python", os.path.join(pythondir, "pcdi.py"), "--out", outfile, "--WP", WP, "--jet_type", jet_type, "--ds", ds, "--tagger", tagger, "--jet_collection", jet_collection, "--PCBT_istrue", PCBT_istrue] + input_files
    print(" ".join(cmd))
    sp.check_output(cmd) 

def _run_eunc(outfile, infile, unc_name = "MC_FRAG", convert_to_binning_from = None, force_monotonicity = False, crossover = False, anchor_pt = None):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "eunc.py"), "--unc", unc_name, infile, outfile] + (["--convert_to_binning_from", convert_to_binning_from] if convert_to_binning_from is not None else []) + (["--force_monotonicity"] if force_monotonicity else []) + (["--crossover"] if crossover else []) + (["--anchor", str(anchor_pt)] if anchor_pt is not None else [])
    print(" ".join(cmd))
    logoutput = sp.check_output(cmd)
    with open(os.path.join(os.path.dirname(outfile), "eunc_{}.log".format(unc_name)), "w") as outfile:
        outfile.write(logoutput)

def _run_scdi(outfile, infile, strip_regex = "STAT_.*"):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "scdi.py"), "--infile", infile, "--outfile", outfile, "--strip", strip_regex]
    print(" ".join(cmd))
    sp.check_output(cmd)

def _run_ensure_monotonicity(outfile, infile, pT_threshold):
    pythondir = os.path.join(os.environ["XTRAP_ROOTDIR"], "CDITools")
    cmd = ["python", os.path.join(pythondir, "EnsureMonotonicity.py"), "--CDI_in", infile, "--CDI_out", outfile, "--pT_threshold", str(pT_threshold)]
    sp.check_output(cmd)

def _get_bhad_file(repo_path, tagger, WP):
    candidates = glob.glob(os.path.join(repo_path, tagger+"_"+ '*' ,'*'+ WP + '*'))
    assert len(candidates) == 1
    bhad_file = candidates[0]
    return bhad_file

def RunCDIInputProductionCampaign(indir, outdir, no_retagging = False, do_merge = True, add_long_lived = False, long_lived_repo = "", PCBT_istrue = "False"):
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    # the taggers and working points that are currently supported by the extrapolation
    if PCBT_istrue == "True":
        supported_WPs = ["PCBTBEff_100_85","PCBTBEff_85_77","PCBTBEff_77_70","PCBTBEff_70_60","PCBTBEff_60_0"]
    else:
        supported_WPs = ["FixedCutBEff_60", "FixedCutBEff_70", "FixedCutBEff_77", "FixedCutBEff_85"]
    to_extrapolate = ["MC_FRAG", "MC_FSR"]

    candidate_input_dirs = [""]
    input_files = []

    # get a list of all input files that should be used for the CDI building
    for candidate_dir in candidate_input_dirs:
        cur_path = os.path.join(indir, candidate_dir)
        print("looking for input files in '{}'".format(cur_path))

        if os.path.isdir(cur_path):
            input_files += glob.glob(os.path.join(cur_path, '*.root'))

    print("using the following input files:")
    print('\n'.join(input_files))

    # split them into sublists, one for each tagger
    input_files_per_tagger = {}
    for input_file in input_files:
        tagger_name = _get_tagger(input_file)
        
        if tagger_name not in input_files_per_tagger:
            input_files_per_tagger[tagger_name] = []

        input_files_per_tagger[tagger_name].append(input_file)

    # start generating the CDI input files
    for tagger, infiles in input_files_per_tagger.items():
        tagger_outdir = os.path.join(outdir, tagger)


        for cur_WP in supported_WPs:
            cur_outdir = os.path.join(tagger_outdir, cur_WP)
            if not os.path.exists(cur_outdir):
                os.makedirs(cur_outdir)
                

            # convert each input ROOT file to its corresponding CDI input txt file
            for infile in infiles:
                outname = os.path.splitext(os.path.basename(infile))[0] + '.txt'
                pcdi_outfile = os.path.join(cur_outdir, outname)
                if not os.path.exists(pcdi_outfile):
                    _run_pcdi(outfile = pcdi_outfile,
                              WP = cur_WP,
                              ds = _get_dataset(infile),
                              tagger = tagger,
                              jet_type = _get_jet_type(infile),
                              jet_collection = _get_jet_collection(infile),
                              input_files = [infile])
                else:
                    print("file '{}' exists already, not regenerating it".format(pcdi_outfile))

            # if retagging was enabled for JUNC, also extract the resulting difference in tagger efficiency
            retagging_regex = re.compile('.*JUNC_retagging.*')
            retagging_files = filter(retagging_regex.match, infiles)
            reference_regex = re.compile('.*TUNC_NBLS.*')
            reference_files = filter(reference_regex.match, infiles)

            if len(retagging_files) == 1 and len(reference_files) == 1:
                retagging_file = retagging_files[0]
                reference_file = reference_files[0]
                print("found the following summary file for retagging: '{}'".format(retagging_file))
                print("found the following reference efficiency summary file: '{}'".format(reference_file))
                
                print("converting efficiency difference into uncertainty...")
                effdiff2cdi_outfile = os.path.join(cur_outdir, "effdiff.txt")
                if not os.path.exists(effdiff2cdi_outfile):
                    """
                    _run_effdiff2cdi(outfile = effdiff2cdi_outfile,
                                     file_a = retagging_file,
                                     file_b = reference_file,
                                     WP = cur_WP,
                                     tagger = tagger,
                                     jetcoll = _get_jet_collection(retagging_file))
                    """
            # merge the individual files and get the final CDI inputs
            required = ["*TUNC_NBLS_*", "*TUNC_BLS_*", "*JUNC_retagging*", "*effdiff*", "*MUNC_fastsim*", "*MUNC_fullsim*"]
            suffix = "retagging"

            print("collecting the following files: {}".format(" ".join(required)))

            # merge files and do additional post-processing, if required
            if do_merge:
                
                anchor_pts_fastsim = {"FixedCutBEff_60": 200,
                                      "FixedCutBEff_70": 200,
                                      "FixedCutBEff_77": 200,
                                      "FixedCutBEff_85": 200}
                
                anchor_pts_fullsim = {"FixedCutBEff_60": 210,
                                      "FixedCutBEff_70": 150,
                                      "FixedCutBEff_77": 150,
                                      "FixedCutBEff_85": 150}

                to_merge = []
                for cur in ["*TUNC_NBLS_*", "*TUNC_BLS_*", "*JUNC_retagging*", "*effdiff*", "*MUNC_FSR*", "*MUNC_PS*", "*MUNC_QS*"]:
                    to_merge += glob.glob(os.path.join(cur_outdir, cur))

                outfile = os.path.join(cur_outdir, "MCcalibCDI_{}_extrap_{}_{}_{}_{}.txt".format(tagger, cur_WP, _get_jet_collection(to_merge[0]), suffix, _get_jet_type(to_merge[0])))
                _run_ccdi(outfile, to_merge)

                     # strip away any STAT_* leftovers
                     # _run_scdi(outfile = outfile, infile = outfile, strip_regex = "STAT_.*")

                     # add in the ad-hoc uncertainty components components from long-lived b-hadrons
                """
                if add_long_lived:
                    outfile_bhad_unc = os.path.join(cur_outdir, "MCcalibCDI_{}_extrap_{}_{}_{}_{}_bhad_unc.txt".format(tagger, cur_WP, _get_jet_collection(to_merge[0]), suffix, get_jet_type(to_merge[0])))
                    long_lived_file = _get_bhad_file(long_lived_repo, tagger, cur_WP)
                    to_merge = [outfile, long_lived_file]
                    _run_ccdi(outfile_bhad_unc, to_merge)

                    outfile = outfile_bhad_unc # now this becomes the final output file
                """

if __name__ == "__main__":
    if not os.environ["XTRAP_ROOTDIR"]:
        raise Exception("Error: 'XTRAP_ROOTDIR' not defined. Please do 'source high-pT-extrapolation/setup.sh'")

    parser = ArgumentParser(description = "orchestrates the production of extrapolation CDI text inputs")
    parser.add_argument("--indir", action = "store", dest = "indir")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    parser.add_argument("--no_retagging", action = "store_const", const = True, default = False)
    parser.add_argument("--no_merge", action = "store_const", const = False, default = True, dest = "do_merge")
    parser.add_argument("--add_long_lived", action = "store_const", const = True, default = False, dest = "add_long_lived")
    parser.add_argument("--long_lived_repo", action = "store", dest = "long_lived_repo")
    parser.add_argument('-P', '--PCBT_istrue', default=False, help="Please specify which scale factor scheme: False=Fixed cut, True=Pseudo-continuous")

    args = vars(parser.parse_args())

    RunCDIInputProductionCampaign(**args)
