import ROOT
from ROOT import TFile
import os, re

class TFileUtils:

    # applies the operation 'callback' to all objects of type 'obj_type' 
    # that can be found in the file
    # callback must have the signature obj_type = callback(obj_type)
    # selector ... a function (or lambda) returning a boolean, selects
    #              whether the map operation should be applied to the histogram,
    #              or whether it should just be copied through
    #              default: apply it to all histograms
    @staticmethod
    def map(infile, outfile, obj_type, callback, selector = lambda: True):
        need_to_close_infile = False
        if isinstance(infile, str):
            infile = TFile(infile, 'READ')
            need_to_close_infile = True

        need_to_close_outfile = False
        if isinstance(outfile, str):
            outfile = TFile(outfile, 'RECREATE')
            need_to_close_outfile = True

        dirnames = TFileUtils.get_leaf_dirlist(infile)

        for dirname in dirnames:
            # strip away the file path and keep only the directory structure internal to that file
            strip = re.compile(".*:/(.+)")
            m = strip.match(dirname)
            dirname_short = m.group(1)
            print("currently in directory '{}'".format(dirname_short))

            # create this subdirectory also in the output file
            out_cur_sub = outfile.mkdir(dirname_short)

            infile.cd(dirname_short)
            in_cur_sub = infile.CurrentDirectory()
            objnames = TFileUtils.get_objectlist(in_cur_sub, "TH1D")

            print("processed the following objects:")
            for objname in objnames:
                print(objname)
            
                obj = infile.Get(os.path.join(dirname_short, objname))
                obj.SetDirectory(0)

                # process the histogram here
                if selector(dirname):
                    obj_processed = callback(obj)
                else:
                    obj_processed = obj.Clone()

                outfile.cd(dirname_short)
                obj_processed.Write()
            
        # only close the files if they were opened internally
        # (otherwise it is the job of the caller to manage them)
        if need_to_close_outfile:
            outfile.Close()

        if need_to_close_infile:
            infile.Close()

    @staticmethod
    def get_objectlist(cur_dir, obj_type):
        need_to_close = False
        if isinstance(cur_dir, str):
            cur_dir = TFile.Open(cur_dir)
            need_to_close = True

        obj_list = []

        for cur_key in cur_dir.GetListOfKeys():
            if cur_key.GetClassName() == obj_type:
                obj_list.append(cur_key.GetName())

        if need_to_close:
            cur_dir.Close()
        
        return obj_list

    # return all subdirectories contained in this TFile
    @staticmethod
    def get_dirlist(infile):
        # if this is the path, open the file
        need_to_close = False
        if isinstance(infile, str):
            infile = TFile(infile)
            need_to_close = True

        dirlist = TFileUtils.get_objectlist(infile, "TDirectoryFile")

        if need_to_close:
            infile.Close()
                
        return dirlist

    @staticmethod
    def get_leaf_dirlist(curdir):
        dirlist = []
        leafdirlist = []

        need_to_close = False
        if isinstance(curdir, str):
            curdir = TFile(curdir)
            need_to_close = True

        for cur_key in curdir.GetListOfKeys():
            if cur_key.GetClassName() == "TDirectoryFile":
                dirlist.append(cur_key.GetName())

        cur_path = curdir.GetPath()
        if len(dirlist) == 0:
            # are already in a leaf directory, just return the current name
            leafdirlist.append(cur_path)
        else:
            # still need to go through all the subdirectories to find their leaves
            for subdir in dirlist:
                curdir.cd(subdir)
                subdir = curdir.CurrentDirectory()
                leafdirlist += TFileUtils.get_leaf_dirlist(subdir)
                curdir.cd(cur_path)
                curdir = curdir.CurrentDirectory()
                
        if need_to_close:
            curdir.Close()

        return leafdirlist

    @staticmethod
    def get_hist(infile, histname, loose_matching = True):
        # if this is the path, open the file
        need_to_close = False
        if isinstance(infile, str):
            infile = TFile(infile)
            need_to_close = True

        if loose_matching:

            cand_hists = TFileUtils.get_hist_candidates(infile, histname)

            if len(cand_hists) != 1:
                print("[loose_matching] Warning: histogram specification did not yield a unique candidate!")
                print(histname + ": " + str(cand_hists))
            
            histname = cand_hists[0]
            infile.cd("/")

        hist = infile.Get(histname).Clone()
        hist.SetDirectory(0) # remove the directory this histogram is associated with

        if need_to_close:
            infile.Close()

        return hist

    @staticmethod
    def get_hist_candidates(infile, histname):
        need_to_close = False
        if isinstance(infile, str):
            infile = TFile(infile)
            need_to_close = True

        # first, get a list of all the files in the directory of the histogram
        dirname = os.path.dirname(histname)
        histbasename = os.path.basename(histname)
        infile.cd(dirname)
        curdir = infile.CurrentDirectory()

        histlist = TFileUtils.get_objectlist(curdir, "TH1D")

        cand_hists = [cur_hist for cur_hist in histlist if histbasename in cur_hist]
        cand_hist_paths = [os.path.join(dirname, cand_hist) for cand_hist in cand_hists]

        if need_to_close:
            infile.Close()

        return cand_hist_paths
