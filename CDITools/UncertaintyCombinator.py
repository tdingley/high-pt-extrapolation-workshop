from RelUncertainty import RelUncertainty
from EfficiencySorter import EfficiencyCurveOrigin
import math

class UncertaintyCombinator:

    # combines the individual variations within a certain variation group
    @staticmethod
    def from_group(group_name, nominal_curve, group_uncertainties):
        if group_name == "PDF_*":
            # Note regarding the combination of PDF uncertainties: the NNPDF prescription calls for the computation of the
            # standard deviation over the entire sample of PDF replicas. This involves dividing the sum of squared differences
            # by the number of used variations, then symmetrizing this uncertainty band.
            # Since in VariationGrouper, the PDF variations are already added in a symmetrized way, calling upper_envelope and
            # lower_envelope below directly yields the symmetrized uncertainty band. However, the actual number of variations is
            # half the number of symmetrized variations, hence the additional factor of 2.0 in the normalization.
            number_variations = len(group_uncertainties)
            envelope_up = RelUncertainty.from_positive_quadrature_sum(group_uncertainties) / math.sqrt(number_variations / 2.0)
            envelope_down = -RelUncertainty.from_negative_quadrature_sum(group_uncertainties) / math.sqrt(number_variations / 2.0)
        else:
            # add additional selectivity here if needed
            envelope_up = RelUncertainty.from_positive_quadrature_sum(group_uncertainties)
            envelope_down = -RelUncertainty.from_negative_quadrature_sum(group_uncertainties)

        return envelope_up, envelope_down

    # TODO: call this method from within syst()
    @staticmethod
    def partial_syst(efficiency_curve, group_name):
        group_uncertainties = efficiency_curve.uncertainties[group_name]
        group_syst_envelope_up, group_syst_envelope_down = UncertaintyCombinator.from_group(group_name, nominal_curve = efficiency_curve, group_uncertainties = group_uncertainties)

        return group_syst_envelope_up, group_syst_envelope_down        

    # computes the total systematic uncertainty band associated with this efficiency curve
    @staticmethod
    def syst(efficiency_curve):
        # get the total uncertainty envelope that takes into account all systematic variations
        group_syst_envelopes_up = []
        group_syst_envelopes_down = []

        # iterate only over the actual uncertainty groups that have been defined
        group_names = list(map(lambda x: x[0:], EfficiencyCurveOrigin))
        for group_name in group_names:
            if group_name in efficiency_curve.uncertainties:
                group_uncertainties = efficiency_curve.uncertainties[group_name]
                group_syst_envelope_up, group_syst_envelope_down = UncertaintyCombinator.from_group(group_name, nominal_curve = efficiency_curve, group_uncertainties = group_uncertainties)

                group_syst_envelopes_up.append(group_syst_envelope_up)
                group_syst_envelopes_down.append(group_syst_envelope_down)
            
        # the total systematic uncertainty is the combination of the uncertainties of the individual groups
        total_syst_envelope_up = RelUncertainty.from_quadrature_sum(group_syst_envelopes_up)
        total_syst_envelope_down = -RelUncertainty.from_quadrature_sum(group_syst_envelopes_down) if group_syst_envelopes_down else None

        return total_syst_envelope_up, total_syst_envelope_down

    # computes the statistical uncertainty associated with this efficiency curve
    @staticmethod
    def stat(efficiency_curve):
        # also get the statistical uncertainty associated with the nominal curve (this is symmetric by default, thus make it so)
        stat_envelope_up = efficiency_curve.get_statistical_uncertainty()
        stat_envelope_down = -stat_envelope_up

        return stat_envelope_up, stat_envelope_down

    # computes the total uncertainty band, combining statistical and systematic uncertainties
    @staticmethod
    def stat_syst(efficiency_curve):
        # fetch the total systematic and statistical uncertainties
        total_syst_envelope_up, total_syst_envelope_down = UncertaintyCombinator.syst(efficiency_curve)
        total_stat_envelope_up, total_stat_envelope_down = UncertaintyCombinator.stat(efficiency_curve)

        # the total uncertainty envelope is the quadratic sum of stat + syst
        total_envelope_up = RelUncertainty.from_quadrature_sum([total_syst_envelope_up, stat_envelope_up])
        total_envelope_down = -RelUncertainty.from_quadrature_sum([total_syst_envelope_down, stat_envelope_down])

        return total_envelope_up, total_envelope_down
