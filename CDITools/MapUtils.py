from Curve import Curve

import numpy as np
from array import array
import re
import ROOT

# contains a number of convenient callback functions that process histograms in various ways
class MapUtils:

    @staticmethod
    def merge(hist, merge_start, merge_end, merge_callback):
        # first, get the original bin edges
        l_edges_old = []
        for bin in range(1, hist.GetSize()):
            l_edges_old.append(hist.GetBinLowEdge(bin))

        to_merge_contents = [hist.GetBinContent(bin) for bin in range(merge_start, merge_end + 1)]
        to_merge_errors = [hist.GetBinError(bin) for bin in range(merge_start, merge_end + 1)]

        merged_bin_value, merged_bin_error = merge_callback(to_merge_contents, to_merge_errors)

        # now get the new bin edges after merging the requested bins
        l_edges_new = l_edges_old[:merge_start] + l_edges_old[merge_end:]
        l_edges_new_arr = array('d', l_edges_new)

        hist_processed = hist.Clone()
        hist_processed.SetBins(len(l_edges_new) - 1, l_edges_new_arr)

        print("original bin edges: '{}'".format(l_edges_old))
        print("new bin edges: '{}'".format(l_edges_new))

        # now set the bin values and uncertainties
        for bin in range(1, merge_start):
            print("{} -> {}".format(bin, bin))
            hist_processed.SetBinContent(bin, hist.GetBinContent(bin))
            hist_processed.SetBinError(bin, hist.GetBinError(bin))

        print("{} -> {}".format("merged", merge_start))
        hist_processed.SetBinContent(merge_start, merged_bin_value)
        hist_processed.SetBinError(merge_start, merged_bin_error)

        for bin in range(merge_start + 1, hist_processed.GetSize()):
            print("{} -> {}".format(bin + merge_end - merge_start, bin))
            hist_processed.SetBinContent(bin, hist.GetBinContent(bin + merge_end - merge_start))
            hist_processed.SetBinError(bin, hist.GetBinError(bin + merge_end - merge_start))
            
        return hist_processed

    # merges histogram bins in the range between merge_start and merge_end
    @staticmethod
    def merge_bins(hist, merge_start, merge_end):

        def merge_callback(to_merge_contents, to_merge_errors):
            merged_bin_value = sum(to_merge_contents)
            merged_bin_error = np.sqrt(sum([val**2 for val in to_merge_errors]))

            return merged_bin_value, merged_bin_error

        return MapUtils.merge(hist, merge_start, merge_end, merge_callback)
        
    @staticmethod
    def average_bins(hist, merge_start, merge_end):

        def merge_callback(to_merge_contents, to_merge_errors):
            normval = sum([1 / unc**2 for unc in to_merge_errors])

            merged_bin_value = 1 / normval * sum([val / unc**2 for val, unc in zip(to_merge_contents, to_merge_errors)])
            merged_bin_error = np.sqrt(1 / normval)

            return merged_bin_value, merged_bin_error

        return MapUtils.merge(hist, merge_start, merge_end, merge_callback)
    
