import sys, os
from argparse import ArgumentParser

from CDIInputExporter import CDIInputExporter
from EfficiencyLoader import EfficiencyLoader

def EfficiencyDifferenceToCDI(infile_nominal, infile_alternative, WP, outfile, tagger = "DL1r", jetcoll = "AntiKt4EMTopoJets"):
    print("using infile_nominal = {}".format(infile_nominal))
    print("using infile_alternative = {}".format(infile_alternative))
    print("using outfile = {}".format(outfile))

    tagged_hist_names = {
        "FixedCutBEff_60": "tagged_FixedCutBEff_60",
        "FixedCutBEff_70": "tagged_FixedCutBEff_70",
        "FixedCutBEff_77": "tagged_FixedCutBEff_77",
        "FixedCutBEff_85": "tagged_FixedCutBEff_85"
        }
    tagged_hist = tagged_hist_names[WP]
    nominal_name = ".*[Nn]ominal/nominal"

    # first, load the two efficiency curves (Note: rebinning does not lead to any changes of the *nominal* efficiency curve, so do not use it to save time!)
    eff_nominal = EfficiencyLoader.from_highpt_xtrap_file(infile_nominal, tagged_hist, nominal_name = nominal_name, do_rebinning = False)

    eff_alternative = EfficiencyLoader.from_highpt_xtrap_file(infile_alternative, tagged_hist, nominal_name = nominal_name, do_rebinning = False)

    # compute the relative difference between the two
    unc_diff = (eff_alternative - eff_nominal) / eff_nominal
    unc_diff = unc_diff.apply(lambda x: abs(x)) # CDI only supports symmetric uncertainties up to now!
    unc_diff.dump()

    syst_uncs_up = syst_uncs_down = {"eff_diff": unc_diff}
    stat_uncs_up = [eff_nominal.create_uncertainty_obj(), eff_alternative.create_uncertainty_obj()]
    stat_uncs_down = [-eff_nominal.create_uncertainty_obj(), -eff_alternative.create_uncertainty_obj()]

    # dump this into the requested output file
    options = {"WP": WP, "jet_collection": jetcoll, "tagger": tagger}
    print(options)
    CDIInputExporter.produce_CDI_input_from_uncertainties(outfile, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)

if __name__ == "__main__":
    parser = ArgumentParser(description = "computes the relative mismatch between two b-efficiency curves and dumps it into a CDI input txt file")
    parser.add_argument("--infile_nominal", action = "store", dest = "infile_nominal")
    parser.add_argument("--infile_alternative", action = "store", dest = "infile_alternative")
    parser.add_argument("--outfile", action = "store", dest = "outfile")
    parser.add_argument("--WP", action = "store", dest = "WP")
    parser.add_argument("--tagger", action = "store", dest = "tagger", default = "DL1r")
    parser.add_argument("--jetcoll", action = "store", dest = "jetcoll", default = "AntiKt4EMTopoJets")
    args = vars(parser.parse_args())

    EfficiencyDifferenceToCDI(**args)
