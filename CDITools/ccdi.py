from argparse import ArgumentParser

from CDIInputImporter import CDIInputImporter
from CDIInputExporter import CDIInputExporter
from RelUncertainty import RelUncertainty

def _PromptList(inlist, prompt = "", batchmode = False):
    if batchmode:
        return inlist[0] # assume by default that the first option is the correct one

    while True:
        print(prompt)
        for index, entry in enumerate(inlist):
            print("[{}]: {}".format(index, entry))

        answer = int(input(''))
        if answer in range(len(inlist)):
            break

        print("invalid choice")

    return inlist[answer]

def InteractiveMerging(input_file_paths, output_file_path, batchmode):

    # first, get a list of the uncertainties that are available across all files
    available_uncertainties = {} # of the form {uncertainty_name: [files that have this uncertainty available]}
    
    for input_file_path in input_file_paths:
        cur_available_uncertainties = CDIInputImporter._read_available_systematics(input_file_path)
        
        for cur_available_uncertainty in cur_available_uncertainties:
            if not cur_available_uncertainty in available_uncertainties:
                available_uncertainties[cur_available_uncertainty] = []

            available_uncertainties[cur_available_uncertainty].append(input_file_path)

    # now, build the merged CDI file in an interactive fashion

    # first, ask which file to take the statistical uncertainty from
    cv_file = _PromptList(inlist = input_file_paths, prompt = "From which file to take the central value?", batchmode = batchmode)
    # cv_file = input_file_paths[0]
    print("taking central value from '{}'".format(cv_file))
    central_value = CDIInputImporter._read_central_values(cv_file)
    calib_name, final_stat, tagger, WP, jetcoll = CDIInputImporter._read_header(cv_file)
    options = {"WP": WP, "jet_collection": jetcoll, "tagger": tagger, "jet_type": final_stat}

    stat_unc = central_value.create_uncertainty_obj()
    stat_uncs_up = [stat_unc]
    stat_uncs_down = [-stat_unc]

    # let the user decide which file to get the various systematic variations from
    syst_uncs_up = {}
    syst_uncs_down = {}

    for cur_uncertainty, available_in_files in available_uncertainties.items():
        if len(available_in_files) == 1:
            selected_file = available_in_files[0]
            print("'{}' only available in '{}'".format(cur_uncertainty, selected_file))
        else:
            selected_file = _PromptList(inlist = available_in_files, prompt = "From which file to take '{}'?".format(cur_uncertainty), batchmode = batchmode)        

        cur_syst = RelUncertainty(CDIInputImporter._read_systematics_values(selected_file, cur_uncertainty))
        syst_uncs_up[cur_uncertainty] = cur_syst
        syst_uncs_down[cur_uncertainty] = cur_syst

    # dump the combined CDI input file to disk
    CDIInputExporter.produce_CDI_input_from_uncertainties(output_file_path, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)
        

if __name__ == "__main__":
    parser = ArgumentParser(description = "interactively combine uncertainties taken from different CDI input files")
    parser.add_argument("files", nargs = '+', action = "store")
    parser.add_argument("--batch", action = "store_const", const = True, default = False)
    args = vars(parser.parse_args())

    input_files = args["files"][:-1]
    output_file = args["files"][-1]

    InteractiveMerging(input_files, output_file, batchmode = args["batch"])
