from argparse import ArgumentParser

from CDIInputImporter import CDIInputImporter
from CDIInputExporter import CDIInputExporter
from RelUncertainty import RelUncertainty

import numpy as np
from scipy.interpolate import interp1d

def _PromptList(inlist, prompt = "", choicelist = None):
    while True:
        print(prompt)
        for index, entry in enumerate(inlist):
            print("[{}]: {}".format(index, entry))

        answer = int(input(''))

        if answer in range(len(inlist)):
            break

        print("invalid choice")

    if choicelist:
        return choicelist[answer]
    else:
        return inlist[answer]

def extrapolate(xvals, yvals, yerrs, xvals_extrapolated, mode = "monotonic"):
    # at the moment, use a simple linear fit for the extrapolation
    from scipy.optimize import curve_fit

    # this is the anchor for the fitted (and extrapolated) envelope
    x0 = xvals[0]
    y0 = yvals[0]

    def smooth_heaviside(x):
        return 1.0 / (1 + np.exp(-x))

    if mode == "crossover":
        # fit a linear function that connects to the overlap bin
        def extrap_func(x, a1, a2, x1):
            # implement some penalty to avoid the second segment being shifted all the way to the right
            if x1 > 0.75:
                x1 = 0.75
            return smooth_heaviside(1000 * x1 - x) * (a1 * np.log(x / x0) + y0) + smooth_heaviside(x - 1000 * x1) * (a2 * np.log(x / (1000 * x1)) + a1 * np.log(1000 * x1 / x0) + y0)
    elif mode == "monotonic":
        # fit a linear function that connects to the overlap bin
        def extrap_func(x, a):
            return a * np.log(x / x0) + y0
    else:
        raise Exception("Mode not supported!")
    
    print(" using xvals = {}".format(xvals))
    print(" using yvals = {}".format(yvals))
    print(" using yerrs = {}".format(yerrs))
        
    print(" extrapolating to xvals = {}".format(xvals_extrapolated))

    opt, cov = curve_fit(extrap_func, xvals, yvals, sigma = yerrs, method = 'dogbox')
    print("fit resulted in parameters: {}".format(opt.tolist()))

    retval = extrap_func(np.array(xvals_extrapolated), *opt.tolist())
    fitted_func = lambda arg: extrap_func(arg, *opt.tolist())

    return retval, fitted_func

def get_chisq(xvals, yvals, yerrs, func, fitpars = 3):
    if not isinstance(xvals, np.ndarray):
        xvals = np.array(xvals)
    if not isinstance(yvals, np.ndarray):
        yvals = np.array(yvals)
    if not isinstance(yerrs, np.ndarray):
        yerrs = np.array(yerrs)

    yvals_fitted = func(xvals)
    
    binned_chisq = np.square((yvals_fitted - yvals) / yerrs)
    print("xvals = {}".format(xvals))
    print("yvals = {}".format(yvals))
    print("yerrs = {}".format(yerrs))
    print("binned chisq = {}".format(binned_chisq))
    chisq = np.sum(binned_chisq)
    red_chisq = chisq / (len(yerrs) - 1 - fitpars)
    
    return red_chisq

def InteractiveExtrapolation(input_file_path, output_file_path, active_unc_name = "", sig_threshold = None, anchor_pt = 350.0,
                             convert_to_binning_from = None, force_monotonicity = False, mode = "monotonic"):
    available_uncertainties = CDIInputImporter._read_available_systematics(input_file_path)

    # if given, load the final binning that should be used for the output
    if convert_to_binning_from is not None:
        ob_curve = CDIInputImporter._read_central_values(convert_to_binning_from)
        ob_lower_edges = ob_curve.get_bin_l_edges()
        ob_upper_edges = ob_curve.get_bin_u_edges()
        ob_centers = ob_curve.get_bin_centers()
        print("using the following output bin centers: {}".format(ob_centers))

    if not active_unc_name:
        # first, ask, which uncertainty should be extrapolated
        active_unc_name = _PromptList(available_uncertainties, prompt = "Which uncertainty should be extrapolated?")
    
    # remove this uncertainty from the above list
    available_uncertainties.remove(active_unc_name)
    inactive_uncertainties = available_uncertainties

    # load the active uncertainty and process it
    active_syst = RelUncertainty(CDIInputImporter._read_systematics_values(input_file_path, active_unc_name))

    # also load the stat. uncertainties on this systematic
    active_syst_unc = RelUncertainty(CDIInputImporter._read_systematics_values(input_file_path, "STAT_" + active_unc_name))

    # make a list of the available binning and ask which bins are reliable
    lower_edges = active_syst.get_bin_l_edges()
    upper_edges = active_syst.get_bin_u_edges()
    centers = active_syst.get_bin_centers()
    values = [active_syst.evaluate(xval) for xval in centers]
    errors = [active_syst_unc.evaluate(xval) for xval in centers]

    if anchor_pt is not None and sig_threshold is None:
        # get the bin number that corresponds to the chosen anchor pT:
        fit_start = max(np.flatnonzero(anchor_pt >= np.array(lower_edges)))
    elif anchor_pt is None and sig_threshold is not None:
        # find the bin where to start the extrapolation
        sigs = np.array(values) / np.array(errors) # these are the significances
        fit_start = np.flatnonzero(sigs < sig_threshold)[0] # this is the number of the first bin where the significance is not sufficient
        
        # anchor the extrapolation at the bin that comes immediately before
        fit_start -= 1
    else:
        raise Exception("Error: need to use either anchor_pt or sig_threshold!")

    print("starting at bin_center = {}".format(centers[fit_start]))

    # extract the values that will be fed into the extrapolation
    xvals = centers[fit_start:]
    yvals = [active_syst.evaluate(xval) for xval in xvals]
    yerrs = [active_syst_unc.evaluate(xval) for xval in xvals]

    # do not use bins with zero uncertainty and stat. uncertainty of 100% for the fitting
    xvals_cleaned = []
    yvals_cleaned = []
    yerrs_cleaned = []
    for xval, yval, yerr in zip(xvals, yvals, yerrs):
        if yval == 0 and yerr == 1.0:
            continue
        else:
            xvals_cleaned.append(xval)
            yvals_cleaned.append(yval)
            yerrs_cleaned.append(yerr)

    print("using the following values as input to extrapolation")
    print(xvals_cleaned)
    print(yvals_cleaned)

    # now perform the extrapolation
    xvals_extrapolated = centers[fit_start:]
    yvals_extrapolated, extrap_func = extrapolate(xvals_cleaned, yvals_cleaned, yerrs_cleaned, xvals_extrapolated, mode = mode)

    if mode == "monotonic":
        fitpars = 1
    elif mode == "crossover":
        fitpars = 3

    red_chisq = get_chisq(xvals_cleaned, yvals_cleaned, yerrs_cleaned, extrap_func, fitpars = fitpars)
    print("got red. chisq = {}".format(red_chisq))

    if force_monotonicity:
        # ensure monotonicity of the fitted uncertainty envelope
        yvals_extrapolated = np.abs(yvals_extrapolated)
        yvals_extrapolated = np.maximum.accumulate(yvals_extrapolated)

    print("got the following extrapolated values")
    print(yvals_extrapolated)

    # generate the combined curve
    xvals_combined = np.concatenate([np.array(centers[:fit_start]), np.array(xvals_extrapolated)])
    yvals_combined = np.concatenate([np.array(values[:fit_start]), np.array(yvals_extrapolated)])
    yvals_combined = np.abs(yvals_combined)
    print("got the following combined values")
    print("x = {}".format(xvals_combined))
    print("y = {}".format(yvals_combined))

    if convert_to_binning_from is not None:
        # use a linear spline to evaluate it at arbitrary positions
        interp = interp1d(xvals_combined, yvals_combined, kind = "linear", fill_value = "extrapolate")
        active_syst = ob_curve

        print("new centers: {}".format(ob_centers))
        print("available centers: {}".format(active_syst.get_bin_centers()))

        for cur_bin_center, cur_bin_l_edge, cur_bin_u_edge in zip(ob_centers, ob_lower_edges, ob_upper_edges):
            print("setting: {} = {}".format(cur_bin_center, interp(cur_bin_center)))
            active_syst.set(cur_bin_center, max(interp(cur_bin_l_edge), interp(cur_bin_u_edge), interp(cur_bin_center)))

    else:
        for xval, yval in zip(xvals_combined, yvals_combined):
            active_syst.set(xval, yval)

    # copy all inactive uncertainties over to the output file without modifications
    central_value = CDIInputImporter._read_central_values(input_file_path)
    calib_name, final_stat, tagger, WP, jetcoll = CDIInputImporter._read_header(input_file_path)
    options = {"WP": WP, "jet_collection": jetcoll, "tagger": tagger}

    if convert_to_binning_from is not None:
        stat_unc = ob_curve.create_uncertainty_obj()
    else:        
        stat_unc = central_value.create_uncertainty_obj()
        
    stat_uncs_up = [stat_unc]
    stat_uncs_down = [-stat_unc]

    syst_uncs_up = {}
    syst_uncs_down = {}

    # for cur_uncertainty in inactive_uncertainties:
    #     cur_syst = RelUncertainty(CDIInputImporter._read_systematics_values(input_file_path, cur_uncertainty))
    #     syst_uncs_up[cur_uncertainty] = cur_syst
    #     syst_uncs_down[cur_uncertainty] = cur_syst

    # add back the extrapolated values
    syst_uncs_up[active_unc_name] = active_syst
    syst_uncs_down[active_unc_name] = active_syst

    # dump the combined CDI input file to disk
    CDIInputExporter.produce_CDI_input_from_uncertainties(output_file_path, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)

if __name__ == "__main__":
    parser = ArgumentParser(description = "interactively extrapolate (and smooth) the uncertainty that ends up in the CDI input file")
    parser.add_argument("files", nargs = '+', action = "store")
    parser.add_argument("--unc", action = "store", dest = "unc")
    parser.add_argument("--convert_to_binning_from", action = "store", default = None)
    parser.add_argument("--force_monotonicity", action = "store_const", const = True, default = False)
    parser.add_argument("--crossover", action = "store_const", const = True, default = False)
    parser.add_argument("--anchor", action = "store", dest = "anchor", default = 200)
    args = vars(parser.parse_args())

    assert len(args["files"]) == 2 # expect exactly 2 files to work with, one input and one output file

    input_file = args["files"][0]
    output_file = args["files"][1]
    unc = args["unc"]
    convert_to_binning_from = args["convert_to_binning_from"]
    force_monotonicity = args["force_monotonicity"]
    mode = "crossover" if args["crossover"] else "monotonic"
    anchor_pt = float(args["anchor"])

    InteractiveExtrapolation(input_file, output_file, active_unc_name = unc, convert_to_binning_from = convert_to_binning_from, force_monotonicity = force_monotonicity, mode = mode, anchor_pt = anchor_pt)
