import sys, os
import numpy as np
from argparse import ArgumentParser
import ROOT

from CDIInputImporter import CDIInputImporter
from RatioPlotter import RatioPlotter
from ColorPicker import ColorPicker
from RelUncertainty import RelUncertainty
from AbsUncertainty import AbsUncertainty

def plot_CDI_input():
    parser = ArgumentParser(description = "produces plots from a CDI input txt-file")
    parser.add_argument("--in", action = "store", dest = "in")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    args = vars(parser.parse_args())

    input_file_path = args["in"]
    output_dir = args["outdir"]

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    cp = ColorPicker()

    # first, read the available header information
    calib_name, final_state, tagger, WP, jetcoll = CDIInputImporter._read_header(input_file_path)

    central_values = CDIInputImporter._read_central_values(input_file_path)

    xaxis_range = (20, 3000)
    RatioPlotter.create_ratio_plot(curves = [central_values],
                                   rel_uncertainties_up = [None],
                                   rel_uncertainties_down = [None], 
                                   labels = ["central value"],
                                   marker_colors = [cp.get_color("black")],
                                   uncertainty_fill_colors = [None],
                                   uncertainty_styles = [None],
                                   ratio_reference = central_values,
                                   outfile = os.path.join(output_dir, "central_value"),
                                   inner_label = "{}, {}, {}".format(calib_name, final_state, tagger),
                                   outer_label = os.path.splitext(os.path.basename(input_file_path))[0],
                                   x_label = "p_{T} [GeV]",
                                   y_label = "b scale factor",
                                   y_ratio_label = "ratio",
                                   yaxis_range_abs = (0.1, 1.9),
                                   yaxis_range_rel = (0.75, 1.25),
                                   xaxis_range = xaxis_range,
                                   show_ratio = True)

    available_systematics = CDIInputImporter._read_available_systematics(input_file_path)
    #available_systematics.append("TRK_*")

    for syst in available_systematics:
        # do not look at statistics just now
        if "STAT_" in syst:
            continue

        cur_syst = RelUncertainty(CDIInputImporter._read_systematics_values(input_file_path, syst))
        
        # determine a suitable axis range
        xaxis_range = (20, 3000)
        cur_unc_vals = [cur_syst.evaluate(x_value) for x_value in np.linspace(xaxis_range[0], xaxis_range[1], 20)]
        cur_unc_max = max(abs(max(cur_unc_vals)), 0.01)
        margin_factor = 1.2
        
        RatioPlotter.create_ratio_plot(curves = [central_values],
                                       rel_uncertainties_up = [-cur_syst],
                                       rel_uncertainties_down = [cur_syst], 
                                       labels = ["Overall: jet, track and PS"],
                                       marker_colors = [cp.get_color("black")],
                                       uncertainty_fill_colors = [cp.get_color("greenblue1")],
                                       uncertainty_styles = ["5"],
                                       ratio_reference = central_values,
                                       outfile = os.path.join(output_dir, syst),
                                       inner_label = "{}, {}, {}".format(calib_name, final_state, tagger),
                                       x_label = "p_{T} [GeV]",
                                       y_label = "SF_{b}",
                                       y_ratio_label = "ratio",
                                       yaxis_range_abs = (0.4, 1.6),
                                       yaxis_range_rel = (0.7, 1.3),
                                       xaxis_range = xaxis_range,
                                       show_ratio = False)

        RatioPlotter.create_ratio_plot(curves = [central_values],
                                       rel_uncertainties_up = [-cur_syst],
                                       rel_uncertainties_down = [cur_syst], 
                                       labels = [syst],
                                       marker_colors = [cp.get_color("black")],
                                       uncertainty_fill_colors = [cp.get_color("greenblue1")],
                                       uncertainty_styles = ["5"],
                                       ratio_reference = central_values,
                                       outfile = os.path.join(output_dir, syst + "_cropped"),
                                       inner_label = "{}, {}, {}".format(calib_name, final_state, tagger),
                                       outer_label = os.path.splitext(os.path.basename(input_file_path))[0],
                                       x_label = "p_{T} [GeV]",
                                       y_label = "SF_{b}",
                                       y_ratio_label = "ratio",
                                       yaxis_range_abs = (0.9, 1.1),
                                       yaxis_range_rel = (0.9, 1.1),
                                       xaxis_range = xaxis_range,
                                       show_ratio = False)

        if "STAT_" + syst in available_systematics:
            # also generate a version showing the statistical uncertainties on the systematics as well
            cur_syst_unc = AbsUncertainty(CDIInputImporter._read_systematics_values(input_file_path, "STAT_" + syst))
            RatioPlotter.create_ratio_plot(curves = [central_values, central_values + cur_syst, central_values - cur_syst],
                                           rel_uncertainties_up = [-cur_syst, -cur_syst_unc.convert_to_relative(cur_syst), -cur_syst_unc.convert_to_relative(cur_syst)],
                                           rel_uncertainties_down = [cur_syst, cur_syst_unc.convert_to_relative(cur_syst), cur_syst_unc.convert_to_relative(cur_syst)], 
                                           labels = [syst + " (+ stat. unc.)", "", ""],
                                           marker_colors = [cp.get_color("greenblue1"), cp.get_color("greenblue3"), cp.get_color("greenblue3")],
                                           uncertainty_fill_colors = [cp.get_color("greenblue1"), cp.get_color("black"), cp.get_color("black")],
                                           uncertainty_styles = ["5", "", ""],
                                           curve_styles = ["", "PZ", "PZ"],
                                           ratio_reference = central_values,
                                           outfile = os.path.join(output_dir, syst + "_with_stat"),
                                           inner_label = "{}, {}, {}".format(calib_name, final_state, tagger),
                                           outer_label = os.path.splitext(os.path.basename(input_file_path))[0],
                                           x_label = "p_{T} [GeV]",
                                           y_label = "SF_{b}",
                                           y_ratio_label = "ratio",
                                           yaxis_range_abs = (1 - cur_unc_max * margin_factor, 1 + cur_unc_max * margin_factor),
                                           yaxis_range_rel = (0.2, 1.8),
                                           xaxis_range = xaxis_range,
                                           show_ratio = False)

        # determine a suitable axis range
        xaxis_range = (20, 550)
        cur_unc_vals = [cur_syst.evaluate(x_value) for x_value in np.linspace(xaxis_range[0], xaxis_range[1], 20)]
        cur_unc_max = max(abs(max(cur_unc_vals)), 0.01)
        margin_factor = 1.7

        RatioPlotter.create_ratio_plot(curves = [central_values],
                                       rel_uncertainties_up = [-cur_syst],
                                       rel_uncertainties_down = [cur_syst], 
                                       labels = [syst],
                                       marker_colors = [cp.get_color("black")],
                                       uncertainty_fill_colors = [cp.get_color("greenblue1")],
                                       uncertainty_styles = ["5"],
                                       ratio_reference = central_values,
                                       outfile = os.path.join(output_dir, syst + "_lowpt"),
                                       inner_label = "{}, {}, {}".format(calib_name, final_state, tagger),
                                       outer_label = os.path.splitext(os.path.basename(input_file_path))[0],
                                       x_label = "p_{T} [GeV]",
                                       y_label = "b SF",
                                       y_ratio_label = "ratio",
                                       yaxis_range_abs = (1 - cur_unc_max * margin_factor, 1 + cur_unc_max * margin_factor),
                                       yaxis_range_rel = (0.9, 1.1),
                                       xaxis_range = xaxis_range,
                                       show_ratio = False)

if __name__ == "__main__":
    ROOT.gROOT.SetBatch()
    plot_CDI_input()
