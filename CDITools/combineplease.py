import os
import glob
import subprocess
import uproot

# Path to the directory containing ROOT files
root_files_dir = "/home/dingleyt/QT/high-pT-extrapolation/high-pT-extrapolation/TDD_EfficiencyProducer/all_jet/user.tdingley.800030.h5.JUNC_retagging._DL1dv01_b_jet_01"

# Output directory for hadded ROOT files
output_dir = "/home/dingleyt/QT/high-pT-extrapolation/high-pT-extrapolation/TDD_EfficiencyProducer/all_jet/combined/01"

# Get a list of all ROOT files in the root_files_dir
root_files = glob.glob(os.path.join(root_files_dir, "*.root"))

# Dictionary to store files for each variable
variable_files = {}

# Loop through root files
for root_file_path in root_files:
    with uproot.open(root_file_path) as root_file:
        # Extract the variable name from the ROOT file metadata
        try:
            variable_name = root_file["AntiKt4EMPFlowJets"].decode() # Replace with the correct key
            variable_files.setdefault(variable_name, []).append(root_file_path)
        except KeyError:
            print(f"Variable name not found in {root_file_path}. Skipping.")

# Loop through variable_files and create hadded ROOT files
for variable, files in variable_files.items():
    if len(files) > 1:
        # Create a new hadded ROOT file for the current variable
        hadded_file_path = os.path.join(output_dir, f"hadded_{variable}.root")
        
        # Use uproot to hadd files and store them in the new hadded ROOT file
        with uproot.recreate(hadded_file_path) as output_file:
            for file_path in files:
                with uproot.open(file_path) as input_file:
                    # Loop through keys in the input file and copy them to the output file
                    for key in input_file.keys():
                        obj = input_file[key]
                        output_file[key] = obj
        
        print(f"Hadded {len(files)} files for variable {variable} into {hadded_file_path}")
