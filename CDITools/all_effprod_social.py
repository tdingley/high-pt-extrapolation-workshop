#!/usr/bin/env python3
import sys, os, glob
import ROOT
from ROOT import TFile, TH1D
from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
from collections import defaultdict
import re

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('indirectory', action = "store")
    parser.add_argument('-o', '--out_dir', default='roc1d.h5',
                        help='output hist file')
    parser.add_argument('-m', '--dr-matching', default=0.2, type=float,
                        help='default %(default)s')
    parser.add_argument('-S', '--syst_type', default="Tracking", help="Please specifiy type from: tracking, jet, modelling")
    parser.add_argument('-F', '--flav_jet', default="b", help="Please specify jet flavour of interest: b, c or l")
    parser.add_argument('-C', '--CDI-file', 
                        default="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root",
                        help="Input CDI file to extract tagger cut values")
    return parser.parse_args()

def _check_match(path, keywords):
    for kw in keywords:
        if kw in path:
            return kw

def get_supported_systtypes(type):
    types = [modelling_systtypes,tracking_systtypes,jet_systtypes]
    type_check = ['modelling', 'tracking', 'jet']
    return types[type_check.index(type)]

def _get_syst_type(path, available_types):
    if _check_match(path, available_types) == "nominal_test":
        return "Nominal"
    if _check_match(path, available_types) == "TRK_EFF_LOOSE_GLOBAL_new":
        return "TRK_new_EFF_LOOSE_GLOBAL"
    else:
        return _check_match(path, available_types)

modelling_systtypes = ['pythia8', 'herwig7']
tracking_systtypes = ["nominal","TRK_BIAS_Z0_WM","TRK_FAKE_RATE_LOOSE_TIDE","TRK_EFF_LOOSE_IBL","TRK_BIAS_D0_WM","TRK_EFF_LOOSE_PHYSMODEL","TRK_RES_D0_DEAD","TRK_BIAS_QOVERP_SAGITTA_WM","TRK_EFF_LOOSE_PP0","TRK_RES_D0_MEAS","TRK_EFF_LOOSE_TIDE","TRK_RES_Z0_DEAD","TRK_RES_Z0_MEAS","TRK_EFF_LOOSE_GLOBAL","TRK_FAKE_RATE_LOOSE"]
jet_systtypes = ["nominal", "JET_JESUnc_mc20vsmc21_MC21_PreRec_DOWN","JET_JESUnc_mc20vsmc21_MC21_PreRec_UP","JET_JERUnc_mc20vsmc21_MC21_PreRec_DOWN","JET_JERUnc_mc20vsmc21_MC21_PreRec_UP", "JET_GroupedNP_1_UP","JET_GroupedNP_1_DOWN", "JET_GroupedNP_2_UP","JET_GroupedNP_2_DOWN", "JET_GroupedNP_3_DOWN", "JET_GroupedNP_3_UP", "JET_EtaIntercalibration_NonClosure_PreRec_UP","JET_EtaIntercalibration_NonClosure_PreRec_DOWN", "JET_InSitu_NonClosure_PreRec_DOWN","JET_InSitu_NonClosure_PreRec_UP", "JET_JESUnc_mc20vsmc21_MC20_PreRec_DOWN", "JET_JESUnc_mc20vsmc21_MC20_PreRec_UP", "JET_JESUnc_Noise_PreRec_DOWN","JET_JESUnc_Noise_PreRec_UP", "JET_JESUnc_VertexingAlg_PreRec_DOWN","JET_JESUnc_VertexingAlg_PreRec_UP", "JET_JER_DataVsMC_MC16_DOWN", "JET_JER_DataVsMC_MC16_UP", "JET_JER_EffectiveNP_1_UP", "JET_JER_EffectiveNP_1_DOWN", "JET_JER_EffectiveNP_2_DOWN","JET_JER_EffectiveNP_2_UP", "JET_JER_EffectiveNP_3_DOWN","JET_JER_EffectiveNP_3_UP", "JET_JER_EffectiveNP_4_UP", "JET_JER_EffectiveNP_4_DOWN","JET_JER_EffectiveNP_5_DOWN","JET_JER_EffectiveNP_5_UP","JET_JER_EffectiveNP_6_DOWN","JET_JER_EffectiveNP_6_UP","JET_JER_EffectiveNP_7restTerm_DOWN","JET_JER_EffectiveNP_7restTerm_UP","JET_JERUnc_mc20vsmc21_MC20_PreRec_DOWN","JET_JERUnc_mc20vsmc21_MC20_PreRec_UP", "JET_JERUnc_Noise_PreRec_DOWN", "JET_JERUnc_Noise_PreRec_UP"]

xtrap_rootdir = os.environ["XTRAP_ROOTDIR"]
print(xtrap_rootdir + '\n')

args = get_args()
path = args.out_dir
print(path)
print('\n')

print(str(args.indirectory))
print('\n')

# Define input parameters for the loop
candidate_dirs = glob.glob(os.path.join(args.indirectory, '*output.h5'))
print(candidate_dirs)
print('\n')
flavour = args.flav_jet

commands_file = 'command_file'+str(flavour)+str(args.indirectory).split('/')[-1]+'.txt'
# Check if the file exists
if os.path.exists(commands_file):
    # Delete the file
    os.remove(commands_file)
    print(f"The file '{commands_file}' has been deleted.")
else:
    print(f"The file '{commands_file}' does not exist.")
i = 0
syst = []
with open(commands_file, 'a') as c:

    # Start the for loop
    for candidate_dir in candidate_dirs:
        
        print(len(candidate_dirs))
        print(candidate_dir)
        type = args.syst_type
        CDI_File = args.CDI_file
        supported_systtypes = get_supported_systtypes(type)
        print(len(supported_systtypes))
        syst.append(re.findall(r'\w+_\w+', str(candidate_dir).split('/')[-1]))

        c.write(str(candidate_dir).split('/')[-1] + '\n')

        syst_name = _get_syst_type(candidate_dir, supported_systtypes)
        #print(syst_name)
        # Define the HTCondor job description file for this iteration
        #os.cd("submit")
        #print(supported_systtypes)
        i += 1

job_file = 'Eff_prod_' + str(args.indirectory).split('/')[-1] + '.submit'

print(syst)
# Submit the job for this iteration
with open(job_file, 'w') as f:
        f.write('executable = EffProd.sh\n')
        f.write('arguments = ' + str(args.indirectory)+'$(syst)' + ' ' + str(args.out_dir)+' '+str(type)+' ' +str(flavour)+'\n')
        f.write('output = output/output_' +'$(syst)'+ '.out\n')
        f.write('error = error/error_' + '$(syst)' + '.err\n')
        f.write('log = log/log.'+ '$(syst)' +'.log'+'\n')
        f.write('initialdir = ' + str(os.getcwd()) + '\n')
        f.write('queue syst from ' +str(commands_file)+'\n')
os.system('condor_submit ' + job_file)
