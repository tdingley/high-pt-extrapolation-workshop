from RelUncertainty import RelUncertainty
from AbsUncertainty import AbsUncertainty
from Curve import Curve
from MapUtils import MapUtils
from ROOT import TFile
import os

class EfficiencyCurve(Curve):

    # Note: the passed histogram will be owned by this curve,
    #       i.e. make sure to pass a clone
    def __init__(self, curve, name = None, tag_curve = None, probe_curve = None):
        super(EfficiencyCurve, self).__init__(curve.hist, name, curve.bootstrap_replicas)
        self.uncertainties = {}

        # also store the two curves separately
        self.tag_curve = tag_curve
        self.probe_curve = probe_curve

    @classmethod
    def from_file(cls, file_path, histname_probe, histname_tag, name = None, binrange = None):
        probe_curve = Curve.from_file(file_path, histname_probe, binrange = binrange)
        tag_curve = Curve.from_file(file_path, histname_tag, binrange = binrange)


        # ensure that all jet yields remain positive
        probe_curve = probe_curve.apply(lambda x: max(x, 0.0))
        tag_curve = tag_curve.apply(lambda x: max(x, 0.0))

        obj = cls.from_probe_tag(probe_curve = probe_curve, tag_curve = tag_curve, name = name)
        return obj

    # this just loads the efficiency histogram from a ROOT and uses it directly
    @classmethod
    def from_histogram(cls, file_path, histname, name = None, binrange = None):
        eff_curve = Curve.from_file(file_path, histname, binrange = binrange, loose_matching = False)
        obj = cls(eff_curve, name = name)
        return obj

    # this reads the efficiency histogram from a text file (ini style)
    @classmethod
    def from_data(cls, file_path, name = None):
        read_curve = Curve.from_data(file_path, name)
        obj = cls(curve = read_curve, name = name)
        return obj

    @classmethod
    def from_probe_tag(cls, probe_curve, tag_curve, name = None):
        obj = cls(tag_curve / probe_curve, name = name, probe_curve = probe_curve, tag_curve = tag_curve)
        print("STATistUNC",obj.get_statistical_uncertainty())
        return obj

    def clone(self, name = None):
        # first, clone everything that has been inherited
        cloneobj = EfficiencyCurve(super(EfficiencyCurve, self).clone(), name)

        # then, manually copy over the tag and probe curves
        if self.tag_curve:
            cloneobj.tag_curve = self.tag_curve.clone()
        if self.probe_curve:
            cloneobj.probe_curve = self.probe_curve.clone()

        return cloneobj

    def merge_bins(self, merge_start, merge_end):
        # need to do it in two steps: first, merge the bins in the stored
        # 'tag' and 'probe' curves, then recompute (and re-initialize) the
        # efficiency stored in this object
        self.tag_curve.merge_bins(merge_start, merge_end)
        self.probe_curve.merge_bins(merge_start, merge_end)

        self.reinit(self.tag_curve / self.probe_curve)

    def __add__(self, other):
        return EfficiencyCurve(super(EfficiencyCurve, self).__add__(other))

    def __sub__(self, other):
        return EfficiencyCurve(super(EfficiencyCurve, self).__sub__(other))

    def __mul__(self, other):
        return EfficiencyCurve(super(EfficiencyCurve, self).__mul__(other))

    def __truediv__(self, other):
        return EfficiencyCurve(super(EfficiencyCurve, self).__truediv__(other))

    def __div__(self, other):
        return EfficiencyCurve(super(EfficiencyCurve, self).__div__(other))

    def __neg__(self):
        return EfficiencyCurve(super(EfficiencyCurve, self).__neg__())

    def add_uncertainty_to_category(self, category_name, unc):
        print("adding to {}".format(category_name))

        if category_name not in self.uncertainties:
            self.uncertainties[category_name] = []

        self.uncertainties[category_name].append(unc)        

    def get_statistical_uncertainty(self):
        return AbsUncertainty(self.create_uncertainty_obj()).convert_to_relative(self)

    def all_variations(self):
        retval = []
        for key, val in self.variations.iteritems():
            retval += val
        return retval

    def dump(self):
        print("--------------------")
        print("'EfficiencyCurve '" + self.name + "':")
        super(EfficiencyCurve, self).dump()
        print("--------------------")
