from argparse import ArgumentParser
import re

from CDIInputImporter import CDIInputImporter
from CDIInputExporter import CDIInputExporter
from RelUncertainty import RelUncertainty

def StripUncertainty(infile, outfile, strip_unc = ".*", pass_unc = ".*"):
    print("using PASS = {}".format(pass_unc))
    print("using STRIP = {}".format(strip_unc))

    strip_unc = re.compile(strip_unc)
    pass_unc = re.compile(pass_unc)

    # read metadata
    cur_available_uncertainties = CDIInputImporter._read_available_systematics(infile)
    central_value = CDIInputImporter._read_central_values(infile)
    calib_name, final_stat, tagger, WP, jetcoll, jet_type = CDIInputImporter._read_header(infile)
    options = {"WP": WP, "jet_collection": jetcoll, "tagger": tagger, "jet_type": jet_type}

    stat_unc = central_value.create_uncertainty_obj()
    stat_uncs_up = [stat_unc]
    stat_uncs_down = [-stat_unc]

    # let the user decide which file to get the various systematic variations from
    syst_uncs_up = {}
    syst_uncs_down = {}
    
    passed_uncertainties = [cur_unc for cur_unc in cur_available_uncertainties if not strip_unc.match(cur_unc) and pass_unc.match(cur_unc)]

    print("passing the following uncertainties: {}".format(" ".join(passed_uncertainties)))

    for cur_unc_name in passed_uncertainties:
        cur_syst = RelUncertainty(CDIInputImporter._read_systematics_values(infile, cur_unc_name))
        syst_uncs_up[cur_unc_name] = cur_syst
        syst_uncs_down[cur_unc_name] = cur_syst

    # dump the combined CDI input file to disk
    CDIInputExporter.produce_CDI_input_from_uncertainties(outfile, stat_uncs_up, stat_uncs_down, syst_uncs_up, syst_uncs_down, options)


if __name__ == "__main__":
    parser = ArgumentParser(description = "strip away certain uncertainties")
    parser.add_argument("--infile", action = "store")
    parser.add_argument("--outfile", action = "store")
    parser.add_argument("--strip", action = "store", default = ".*", dest = "strip_unc")
    parser.add_argument("--pass", action = "store", default = ".*", dest = "pass_unc")
    args = vars(parser.parse_args())

    StripUncertainty(**args)
