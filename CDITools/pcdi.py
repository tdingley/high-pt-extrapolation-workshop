import sys, os
from argparse import ArgumentParser

from CDIInputExporter import CDIInputExporter
from EfficiencyLoader import EfficiencyLoader

def write_CDI_input():
    parser = ArgumentParser(description = "produces CDI input txt-files")
    parser.add_argument("input_files", nargs = '+', action = "store")
    parser.add_argument("--out", action = "store", dest = "out")
    parser.add_argument("--WP", action = "store", dest = "WP")
    parser.add_argument("--ds", action = "store", dest = "ds")
    parser.add_argument("--tagger", action = "store", dest = "tagger", default = "DL1r")
    parser.add_argument("--jet_collection", action = "store", dest = "jet_collection", default = "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903")
    parser.add_argument("--store_stat_unc", action = "store_const", const = True, default = False)
    parser.add_argument("--jet_type", action = "store", dest = "jet_type")
    parser.add_argument('-P', '--PCBT_istrue', default=False, help="Please specify which scale factor scheme: False=Fixed cut, True=Pseudo-continuous")

    args = vars(parser.parse_args())

    nominal_name = ".*[Nn]ominal/nominal" # identifier of the nominal run
    print(nominal_name)
    input_file_paths = args["input_files"]
    output_file_path = args["out"]
    WP = args["WP"]
    dataset = args["ds"]
    tagger = args["tagger"]
    jet_collection = args["jet_collection"]
    store_stat_unc = args["store_stat_unc"]
    jet = args["jet_type"]
    PCBT = args["PCBT_istrue"]
    if PCBT == "True":
        print('running pcbt!')
        tagged_hist_names = {
            "PCBTBEff_100_85": "tagged_PCBTBEff_100-85",
            "PCBTBEff_85_77": "tagged_PCBTBEff_85-77",
            "PCBTBEff_77_70": "tagged_PCBTBEff_77-70",
            "PCBTBEff_70_60": "tagged_PCBTBEff_70-60",
            "PCBTBEff_60_0": "tagged_PCBTBEff_60-0"
        }
    else:
        tagged_hist_names = {
            "FixedCutBEff_60": "tagged_FixedCutBEff_60",
            "FixedCutBEff_70": "tagged_FixedCutBEff_70",
            "FixedCutBEff_77": "tagged_FixedCutBEff_77",
            "FixedCutBEff_85": "tagged_FixedCutBEff_85"
        }

    
    tagged_hist = tagged_hist_names[WP]

    jet_type_name ={
        "b_jet": "bottom",
        "c_jet": "charm",
        "l_jet": "light"
    }

    jet_type = jet_type_name[jet]
    # generally valid definition for track uncertainties
    track_nuisances = {
        ".*TRK_RES_Z0_MEAS.*_symmetrized":                "TRK_RES_Z0_MEAS",
        ".*TRK_RES_D0_MEAS.*_symmetrized":                "TRK_RES_D0_MEAS",
        ".*TRK_EFF_LOOSE_TIDE.*_symmetrized":             "TRK_EFF_LOOSE_TIDE",
        ".*TRK_EFF_LOOSE_GLOBAL.*_symmetrized":           "TRK_EFF_LOOSE_GLOBAL",
        ".*TRK_EFF_LOOSE_IBL.*_symmetrized":              "TRK_EFF_LOOSE_IBL",
        ".*TRK_EFF_LOOSE_PP0.*_symmetrized":              "TRK_EFF_LOOSE_PP0",
        ".*TRK_EFF_LOOSE_PHYSMODEL.*_symmetrized":        "TRK_EFF_LOOSE_PHYSMODEL",
        ".*TRK_EFF_LARGED0_GLOBAL.*_symmetrized":         "TRK_EFF_LARGED0_GLOBAL",
        ".*/TRK_FAKE_RATE_LOOSE/nominal_symmetrized":           "TRK_FAKE_RATE_LOOSE",
        ".*TRK_FAKE_RATE_LOOSE_TIDE.*_symmetrized":     "TRK_FAKE_RATE_LOOSE_TIDE",
        ".*TRK_BIAS_D0_WM.*_symmetrized":                 "TRK_BIAS_D0_WM",
        ".*TRK_BIAS_Z0_WM.*_symmetrized":                 "TRK_BIAS_Z0_WM",
        ".*TRK_BIAS_QOVERP_SAGITTA_WM.*_symmetrized":     "TRK_BIAS_QOVERP_SAGITTA_WM",
        ".*TRK_RES_D0_DEAD.*_symmetrized":                "TRK_RES_D0_DEAD",
        ".*TRK_RES_Z0_DEAD.*_symmetrized":                "TRK_RES_Z0_DEAD"

        }
    
    jet_nuisances_separate = {
        ".*JET_GroupedNP_1.*_symmetrized": "JET_GroupedNP_1",
        ".*JET_GroupedNP_2.*_symmetrized": "JET_GroupedNP_2",
        ".*JET_GroupedNP_3.*_symmetrized": "JET_GroupedNP_3",
        ".*JET_EtaIntercalibration_NonClosure_PreRec.*_symmetrized": "JET_EtaIntercalibration_NonClosure_PreRec",
        ".*JET_InSitu_NonClosure_PreRec.*_symmetrized": "JET_InSitu_NonClosure_PreRec",
        ".*JET_JESUnc_mc20vsmc21_MC20_PreRec.*_symmetrized": "JET_JESUnc_mc20vsmc21_MC20_PreRec",
        ".*JET_JESUnc_mc20vsmc21_MC21_PreRec.*_symmetrized": "JET_JESUnc_mc20vsmc21_MC21_PreRec",
        ".*JET_JESUnc_Noise_PreRec.*_symmetrized": "JET_JESUnc_Noise_PreRec",
        ".*JET_JESUnc_VertexingAlg_PreRec.*_symmetrized": "JET_JESUnc_VertexingAlg_PreRec",
        ".*JET_JER_DataVsMC_MC16.*_symmetrized": "JET_JER_DataVsMC_MC16",
        ".*JET_JER_EffectiveNP_1.*_symmetrized": "JET_JER_EffectiveNP_1",
        ".*JET_JER_EffectiveNP_2.*_symmetrized": "JET_JER_EffectiveNP_2",
        ".*JET_JER_EffectiveNP_3.*_symmetrized": "JET_JER_EffectiveNP_3",
        ".*JET_JER_EffectiveNP_4.*_symmetrized": "JET_JER_EffectiveNP_4",
        ".*JET_JER_EffectiveNP_5.*_symmetrized": "JET_JER_EffectiveNP_5",
        ".*JET_JER_EffectiveNP_6.*_symmetrized": "JET_JER_EffectiveNP_6",
        ".*JET_JER_EffectiveNP_7restTerm.*_symmetrized": "JET_JER_EffectiveNP_7restTerm",
        ".*JET_JERUnc_mc20vsmc21_MC20_PreRec.*_symmetrized": "JET_JERUnc_mc20vsmc21_MC20_PreRec",
        ".*JET_JERUnc_mc20vsmc21_MC21_PreRec.*_symmetrized": "JET_JERUnc_mc20vsmc21_MC21_PreRec",
        ".*JET_JERUnc_Noise_PreRec.*_symmetrized": "JET_JERUnc_Noise_PreRec"
    }
    
    
    # available only for ttbar samples
    mod_nuisances = {
       # ".*MC_FSR_1_2.*_symmetrized": "MC_FSR_1_2",
       ".*MC_FSR_1_05.*_symmetrized": "MC_FSR",
       # ".*MC_FSR_1_175.*_symmetrized": "MC_FSR_1_175",
       # ".*MC_FSR_1_15.*_symmetrized": "MC_FSR_1_15",
        #".*MC_FSR_1_125.*_symmetrized": "MC_FSR_1_125",
        #".*MC_FSR_1_0625.*_symmetrized": "MC_FSR_1_0625",
        #".*MC_FSR_1_075.*_symmetrized": "MC_FSR_1_075",
       # ".*MC_FSR_1_0875.*_symmetrized": "MC_FSR_1_0875",
       #".*MC_FSR*.*_symmetrized": "MC_FSR",
        ".*MC_FRAG.*_raw": "MC_FRAG",
        ".*MC_QS.*_symmetrized": "MC_QS",
        ".*herwig7.*_symmetrized": "MC_PS"
        }

    # merge the individual dictionaries
    all_nuisances = {}
    all_nuisances.update(track_nuisances)
    all_nuisances.update(jet_nuisances_separate)
    all_nuisances.update(mod_nuisances)

    # load the efficiency curves from the input files
    eff_curves = []
    for input_file_path in input_file_paths:
        if dataset == "ttbar":
            do_rebinning = False
            rebin_options = {}
        elif dataset == "Zprime":
            #merge_end = 1600 # have enough statistics to extend the range in which bin merging is performed
            do_rebinning = False
            rebin_options = {}
            #rebin_options = {"sigth": 2.0, "merge_start": 0, "merge_end": 5000}
        elif dataset == "800030":
            do_rebinning = False
            rebin_options = {}

        print("currently on {}".format(input_file_path))
        print("using rebin_options = {}".format(rebin_options))

        eff = EfficiencyLoader.from_highpt_xtrap_file(input_file_path, tagged_hist, nominal_name = nominal_name, do_rebinning = do_rebinning,
                                                      rebin_options = rebin_options)
        eff_curves.append(eff)
        #print(eff)

    CDIInputExporter.produce_CDI_input(eff_curve_nominal = eff_curves[0], nuisances = all_nuisances, outpath = output_file_path, eff_curves_aux = eff_curves[1:], 
                                       options = {"WP": WP, "jet_collection": jet_collection, "tagger": tagger, "jet_type": jet_type}, store_stat_unc = store_stat_unc)

if __name__ == "__main__":
    write_CDI_input()
