from RelUncertainty import RelUncertainty
from UncertaintyCombinator import UncertaintyCombinator
from EfficiencyCurve import EfficiencyCurve
from RatioPlotter import RatioPlotter
from ColorPicker import ColorPicker
from EfficiencyLoader import EfficiencyLoader

import ROOT

import sys, os
from argparse import ArgumentParser

def main():
    ROOT.gROOT.SetBatch(True)

    parser = ArgumentParser(description = "compares different efficiency curves on a stats-only basis")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    args = vars(parser.parse_args())
    outdir = args["outdir"]

    cp = ColorPicker()

    # check if output directory exists and create it if it doesn't
    if not os.path.exists(outdir):
         os.makedirs(outdir)

    # working points to look at
    tagged_hists = [
        "taggedb_FixedCutBEff_60", 
        "taggedb_FixedCutBEff_70", 
        "taggedb_FixedCutBEff_77", 
        "taggedb_FixedCutBEff_85"
        ]

    tagged_hists_names = [
        "Fixed Cut, #epsilon_{b}^{MC} = 60%",
        "Fixed Cut, #epsilon_{b}^{MC} = 70%",
        "Fixed Cut, #epsilon_{b}^{MC} = 77%",
        "Fixed Cut, #epsilon_{b}^{MC} = 85%"
        ]

    # output directories for individual working points
    wp_outfiles = [
         "FixedCutBEff_60", 
         "FixedCutBEff_70", 
         "FixedCutBEff_77", 
         "FixedCutBEff_85"
         ]

    PDF_MC_eff_hist_names = [
        "h_effMC_ttbar_b_60wp_c_nominal",
        "h_effMC_ttbar_b_70wp_c_nominal",
        "h_effMC_ttbar_b_77wp_c_nominal",
        "h_effMC_ttbar_b_85wp_c_nominal"
        ]

    PDF_MC_eff_inclusive_hist_names = [
        "h_effMC_ttbar_b_60wp",
        "h_effMC_ttbar_b_70wp",
        "h_effMC_ttbar_b_77wp",
        "h_effMC_ttbar_b_85wp"
        ]

    PDF_MC_eff_hist_PowHW7_names = [
        "h_effMC_ttbar_b_60wp_c_FTAG2_ttbar_PowHW7",
        "h_effMC_ttbar_b_70wp_c_FTAG2_ttbar_PowHW7",
        "h_effMC_ttbar_b_77wp_c_FTAG2_ttbar_PowHW7",
        "h_effMC_ttbar_b_85wp_c_FTAG2_ttbar_PowHW7"
        ]

    # results from the high-pT extrapolation framework
    #path_nominal_fullsim_alljets = "/data/atlas/atlasdata/windischhofer/2019_02_01_ttbar_dil_nominal_fullsim/MV2c10_alljets.root"
    # path_nominal_fullsim_twohardcentraljets = "/data/atlas/atlasdata/windischhofer/2019_02_01_ttbar_dil_nominal_fullsim/MV2c10_twohardcentraljets.root"
    # path_nominal_fullsim_twohardcentraljets = "/data/atlas/atlasdata/windischhofer/2019_02_05_ttbar_dil_nominal_fullsim_FTAG2/MV2c10_FTAG2_twohardjets_repeat.root"
    #path_nominal_fullsim_twohardcentraljets = "/data/atlas/atlasdata/windischhofer/2019_02_01_ttbar_dil_nominal_fullsim/MV2c10_twohardcentraljets_reweighted_to_FTAG2.root"

    # when running on the PDF b-calibration NTuples
    # path_nominal_fullsim_twohardcentraljets = "/data/atlas/atlasdata/windischhofer/user.jgeisen.410472.PhPy8EG.DAOD_FTAG2.e6348_s3126_r10201_p3703.AT21.2.53.0_v1_output.root_MV2c10_hists_twohardcentraljets/7121c6b4-45b5-404b-8c3b-37193107334f.root"
    # path_nominal_fullsim_alljets = "/data/atlas/atlasdata/windischhofer/user.jgeisen.410472.PhPy8EG.DAOD_FTAG2.e6348_s3126_r10201_p3703.AT21.2.53.0_v1_output.root_MV2c10_hists_alljets/7121c6b4-45b5-404b-8c3b-37193107334f.root"

    # when running on the reweighted FTAG1 samples
    path_nominal_fullsim_twohardcentraljets = "/data/atlas/atlasdata/windischhofer/2019_02_01_ttbar_dil_nominal_fullsim/user.phwindis.2019_02_01_ttbar_dil_nominal_fullsim.410472.AntiKt4EMTopoJets.TUNC._AntiKt4EMTopoJets_TUNC_hists_MV2c10_twohardcentraljets_reweighted_to_FTAG2_PDF_harsh_JVT/7121c6b4-45b5-404b-8c3b-37193107334f.root"
    path_nominal_fullsim_alljets = "/data/atlas/atlasdata/windischhofer/user.jgeisen.410472.PhPy8EG.DAOD_FTAG2.e6348_s3126_r10201_p3703.AT21.2.53.0_v1_output.root_MV2c10_hists_alljets/7121c6b4-45b5-404b-8c3b-37193107334f.root"

    # path_nominal_fastsim_alljets = "/data/atlas/atlasdata/windischhofer/2019_01_16_ttbar_dil/combined_MV2c10_alljets.root"
    # path_nominal_fastsim_twohardcentraljets = "/data/atlas/atlasdata/windischhofer/2019_01_16_ttbar_dil/combined_MV2c10_twohardcentraljets.root"

    path_PDF_signal_region = "/home/windischhofer/BTagPlotsSymmetrized/dil_PDF_validation/NEW_FinalFitPlots_r21_d17_MV2c10_FixedCutBEff_emu_OS_J2.root"
    path_PDF_inclusive_region = "/home/windischhofer/BTagPlotsSymmetrized/dil_PDF_validation/NEW_INCLUSIVE_FitPlot_r21_d17_MV2c10_FixedCutBEff_emu_OS_J2_fconst_nominal.root"

    # path_PDF_signal_region = "/home/windischhofer/BTagPlotsSymmetrized/dil_PDF_validation/NEW_FinalFitPlots_r21_d1516_MV2c10_FixedCutBEff_emu_OS_J2.root"
    # path_PDF_inclusive_region = "/home/windischhofer/BTagPlotsSymmetrized/dil_PDF_validation/NEW_INCLUSIVE_FitPlot_r21_d1516_MV2c10_FixedCutBEff_emu_OS_J2_fconst_nominal.root"

    for tagged_hist, tagged_hist_name, wp_outfile, PDF_MC_eff_hist_name, PDF_MC_eff_inclusive_hist_name in zip(tagged_hists, tagged_hists_names, wp_outfiles, PDF_MC_eff_hist_names, PDF_MC_eff_inclusive_hist_names):
        jet_collection = "AntiKt4EMTopoJets"
        tagger = "MV2c10"
        id_string = "#sqrt{s} = 13 TeV, " + jet_collection + ", " + tagger
        hist_name = tagged_hist_name
        inner_label = "#splitline{" + id_string + "}{" + hist_name + "}"

        # load the nominal efficiency curve (fullsim), using all events...
        eff_nominal_fullsim_alljets = EfficiencyLoader.from_highpt_xtrap_file(path_nominal_fullsim_alljets, tagged_hist, binrange = range(1, 10))

        # ... and only events with exactly two jets
        eff_nominal_fullsim_twohardcentraljets = EfficiencyLoader.from_highpt_xtrap_file(path_nominal_fullsim_twohardcentraljets, tagged_hist, binrange = range(1, 10))

        # eff_nominal_fastsim_alljets = EfficiencyLoader.from_highpt_xtrap_file(path_nominal_fastsim_alljets, tagged_hist, binrange = range(1, 10))
        # eff_nominal_fastsim_twohardcentraljets = EfficiencyLoader.from_highpt_xtrap_file(path_nominal_fastsim_twohardcentraljets, tagged_hist, binrange = range(1, 10))

        # load the new (more recent) PDF b-calibration results
        eff_PDF_inclusive_region = EfficiencyCurve.from_histogram(path_PDF_inclusive_region, os.path.join("MCeff", PDF_MC_eff_inclusive_hist_name))
        eff_PDF_signal_region = EfficiencyCurve.from_histogram(path_PDF_signal_region, os.path.join("results_comulative", PDF_MC_eff_hist_name))
        
        # load (manually) some efficiency curves and plot them for sake of comparison (stat. only)
        RatioPlotter.create_ratio_plot(curves = [eff_nominal_fullsim_alljets,
                                                 eff_PDF_inclusive_region, eff_PDF_signal_region],
                                       labels = ["high p_{T} extrap.",
                                                 "PDF b-calib. MC eff. (inclusive)", "PDF b-calib. MC eff. (signal region)"],
                                       marker_colors = [cp.get_color("black"), cp.get_color("blue2"),
                                                        cp.get_color("red2")],
                                       rel_uncertainties_up = [None],
                                       rel_uncertainties_down = [None],
                                       ratio_reference = eff_nominal_fullsim_alljets,
                                       outfile = os.path.join(outdir, tagged_hist + "_PhPy8_fullsim_comparison_alljets"),
                                       inner_label = inner_label,
                                       outer_label = "t#bar{t}, di-lepton",
                                       x_label = "p_{T} [GeV]",
                                       y_label = "b efficiency",
                                       y_ratio_label = "ratio to high-p_{T} eff.")

        RatioPlotter.create_ratio_plot(curves = [eff_nominal_fullsim_twohardcentraljets,
                                                 eff_PDF_inclusive_region, eff_PDF_signal_region],
                                       labels = ["high p_{T} extrap. (only events with exactly two jets with p_{T} > 20 GeV and |#eta| < 2.5)",
                                                 "PDF b-calib. MC eff. (inclusive)", "PDF b-calib. MC eff. (signal region)"],
                                       marker_colors = [cp.get_color("black"),
                                                        cp.get_color("blue2"), cp.get_color("red2")],
                                       rel_uncertainties_up = [None],
                                       rel_uncertainties_down = [None],
                                       ratio_reference = eff_nominal_fullsim_twohardcentraljets,
                                       outfile = os.path.join(outdir, tagged_hist + "_PhPy8_fullsim_comparison_twohardcentraljets"),
                                       inner_label = inner_label,
                                       outer_label = "t#bar{t}, di-lepton",
                                       x_label = "p_{T} [GeV]",
                                       y_label = "b efficiency",
                                       y_ratio_label = "ratio to high-p_{T} eff.")

        # RatioPlotter.create_ratio_plot(curves = [eff_nominal_fastsim_alljets, eff_nominal_fastsim_twohardcentraljets,
        #                                          eff_PDF_inclusive_region, eff_PDF_signal_region],
        #                                labels = ["high p_{T} extrap. (fastsim, all jets)", "high p_{T} extrap. (fastsim, only events with exactly two jets with p_{T} > 20 GeV and |#eta| < 2.5)",
        #                                          "PDF b-calib. MC eff. (inclusive)", "PDF b-calib. MC eff. (signal region)"],
        #                                marker_colors = [cp.get_color("orange1"), cp.get_color("red1"),
        #                                                 cp.get_color("gray"), cp.get_color("black")],
        #                                rel_uncertainties_up = [None],
        #                                rel_uncertainties_down = [None],
        #                                ratio_reference = eff_PDF_signal_region,
        #                                outfile = os.path.join(outdir, tagged_hist + "_PhPy8_fastsim_comparison"),
        #                                inner_label = inner_label,
        #                                outer_label = "t#bar{t}, di-lepton",
        #                                x_label = "p_{T} [GeV]",
        #                                y_label = "b efficiency",
        #                                y_ratio_label = "ratio to PDF eff.")

        # # plot the alternative efficiency curves on PhHW7
        # RatioPlotter.create_ratio_plot(curves = eff_nominal_fastsim_alljets.variations[PhHW7_key_alljets] + eff_nominal_fastsim_twohardcentraljets.variations[PhHW7_key_twohardcentraljets] + [eff_PDF_inclusive_region_PhHW7],
        #                                labels = ["high p_{T} (PhHW7, all jets)", "high p_{T} (PhHW7, exactly two jets with p_{T} > 20 GeV and |#eta| < 2.5)",
        #                                          "PDF b-calib. MC eff. (inclusive)"],
        #                                marker_colors = [cp.get_color("orange3"), cp.get_color("red3"),
        #                                                 cp.get_color("black")],
        #                                rel_uncertainties_up = [None],
        #                                rel_uncertainties_down = [None],
        #                                ratio_reference = eff_PDF_inclusive_region,
        #                                outfile = os.path.join(outdir, tagged_hist + "_PhHW7_comparison"),
        #                                inner_label = inner_label,
        #                                outer_label = "t#bar{t}, di-lepton",
        #                                x_label = "p_{T} [GeV]",
        #                                y_label = "b efficiency",
        #                                y_ratio_label = "ratio to inclusive PDF eff.")

if __name__ == "__main__":
    main()
