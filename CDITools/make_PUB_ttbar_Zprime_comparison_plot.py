import sys, os, re
from argparse import ArgumentParser
import ROOT
from EfficiencyLoader import EfficiencyLoader
from RatioPlotter import RatioPlotter

def create_ratio_plot(curves_upper, curves_lower, outfile, x_label, y_label, y_ratio_label, labels = [], marker_colors_upper = [], marker_colors_lower = [], curve_styles_upper = [], curve_styles_lower = [], marker_styles_upper = [], marker_styles_lower = [], inner_label = "", xaxis_range = (20, 600), yaxis_range_abs = (0, 1.4), yaxis_range_rel = (0.9, 1.1)):

    eps = 1e-5
        
    # axis range of the absolute...
    abs_y_min = yaxis_range_abs[0]
    abs_y_max = yaxis_range_abs[1]
    
    # ... and relative plot
    rel_y_min = yaxis_range_rel[0]
    rel_y_max = yaxis_range_rel[1]
    
    # ticklengths of the plots
    ticklength_y = 0.01;
    ticklength_x = 0.02;
    
    # some general settings
    ROOT.gStyle.SetLineScalePS(3)
    ROOT.gStyle.SetOptStat(0)

    # -------------------------------------------
    # create the global layout of the plot
    # -------------------------------------------
    canv = ROOT.TCanvas("canv_ratio_plot", "canv", 10, 10, 2400, 2200)
    ROOT.SetOwnership(canv, False)
        
    pad_abs = ROOT.TPad("abs_pad", "abs_pad", 0, 0.33, 1, 1)
    ROOT.SetOwnership(pad_abs, False)

    pad_abs.SetBottomMargin(0.02)
    pad_abs.SetRightMargin(0.05)
    pad_abs.SetTopMargin(0.05)
    pad_abs.SetLeftMargin(0.15)
    
    pad_abs.SetFillColor(ROOT.kWhite)
    pad_abs.SetFrameFillStyle(0)
    pad_abs.Draw()
    canv.cd()

    pad_rel = ROOT.TPad("rel_pad", "rel_pad", 0, 0, 1, 0.33)
    ROOT.SetOwnership(pad_rel, False)
    pad_rel.SetTopMargin(0.02)
    pad_rel.SetBottomMargin(0.3)
    pad_rel.SetRightMargin(0.05)
    pad_rel.SetLeftMargin(0.15)
    pad_rel.SetFillColor(ROOT.kWhite)
    pad_rel.SetFrameFillStyle(0)
    pad_rel.Draw()

    # -------------------------------------------
    # fill the "absolute" part of the plot
    # -------------------------------------------
    # prepare the legend
    leg_abs = ROOT.TLegend(0.31, 0.73 - 0.05 * len(curves_upper), 0.93, 0.73)
    ROOT.SetOwnership(leg_abs, False)
    leg_abs.SetNColumns(2)
    leg_abs.SetBorderSize(0)
    leg_abs.SetTextFont(42)
    leg_abs.SetTextSize(0.04)

    RatioPlotter._drawText(0.18, 0.71, "Z'#rightarrow q#bar{q}:", font = 42, fontsize = 0.03, alignment = 11)
    RatioPlotter._drawText(0.18, 0.775, "t#bar{t}#rightarrow q#bar{q} l#nu l#nu:", font = 42, fontsize = 0.03, alignment = 11)
    
    # prepare the actual graphs
    mg_abs = ROOT.TMultiGraph()
    ROOT.SetOwnership(mg_abs, False)

    for curve, marker_color, curve_style, marker_style, label in zip(curves_upper, marker_colors_upper, curve_styles_upper, marker_styles_upper, labels):

        if not curve:
            continue

        # this plots the points and the statistical uncertainty
        cur_graph = ROOT.TGraphAsymmErrors(curve.hist)
        cur_graph.SetLineColor(marker_color)
        cur_graph.SetMarkerStyle(marker_style)
        cur_graph.SetMarkerSize(3.5)
        cur_graph.SetLineWidth(2)
        cur_graph.SetMarkerColor(marker_color)
        
        if label:
            leg_abs.AddEntry(cur_graph, label, "lpfe")
            
        mg_abs.Add(cur_graph, curve_style)
        
    # -------------------------------------------
    # fill the "relative" part of the plot
    # -------------------------------------------
    mg_rel = ROOT.TMultiGraph()
    ROOT.SetOwnership(mg_rel, False)
    for curve, marker_color, curve_style, marker_style in zip(curves_lower, marker_colors_lower, curve_styles_lower, marker_styles_lower):

        # get the current relative statistical uncertainty to be shown in the ratio plot
        rel_stat_unc_up = curve.get_statistical_uncertainty()
        rel_stat_unc_down = -rel_stat_unc_up
            
        # compute the actual ratio curve
        graph_ratio = ROOT.TGraphAsymmErrors(curve.hist)
            
        graph_ratio.SetLineColor(marker_color)
        graph_ratio.SetMarkerStyle(marker_style)
        graph_ratio.SetMarkerSize(3.5)
        graph_ratio.SetLineWidth(2)
        graph_ratio.SetMarkerColor(marker_color)
        
        mg_rel.Add(graph_ratio, curve_style)

    # -------------------------------------------
    # put all components of the graph together
    # -------------------------------------------
    # first, draw the pad with the absolute curves
    pad_abs.cd()
    mg_abs.Draw("alp")
    mg_abs.GetYaxis().SetRangeUser(abs_y_min, abs_y_max)
    mg_abs.GetXaxis().SetLimits(xaxis_range[0], xaxis_range[1])
    mg_abs.GetYaxis().SetTickLength(ticklength_y)
    mg_abs.GetYaxis().SetTitleOffset(0.62 * 2)
    #mg_abs.GetXaxis().SetTickLength(2.5 * ticklength_x / (abs_y_max - abs_y_min))
    mg_abs.GetXaxis().SetTickLength(ticklength_x)
    mg_abs.GetXaxis().SetTitle(x_label)
    mg_abs.GetYaxis().SetTitle(y_label)
    
    # adjust axis titles fonts and sizes
    mg_abs.GetYaxis().SetTitleFont(42)
    mg_abs.GetYaxis().SetTitleSize(0.05)
    mg_abs.GetYaxis().SetLabelSize(0.05)
    
    mg_abs.GetXaxis().SetTitleFont(42)
    mg_abs.GetXaxis().SetLabelFont(42)
    
    mg_abs.GetXaxis().SetLabelSize(0)
        
    ROOT.gPad.SetTicks()
    mg_abs.Draw("alp")

    leg_abs.Draw()
        
    # draw the plot labels:
    if inner_label:
        inner_label_obj = ROOT.TLatex()
        inner_label_obj.SetTextAlign(11)
        inner_label_obj.SetTextFont(42)
        inner_label_obj.SetTextSize(0.04)

        inner_label_obj.DrawLatexNDC(0.18, 0.78, inner_label)
            
    # draw the ATLAS label
    RatioPlotter._drawATLASLabel(0.18, 0.88, "Simulation Preliminary", fontsize = 0.05)
    
    # then draw the second pad with all relative curves
    pad_rel.cd()
    mg_rel.Draw("alp")
    mg_rel.GetYaxis().SetRangeUser(rel_y_min + eps, rel_y_max - eps)
    mg_rel.GetXaxis().SetLimits(xaxis_range[0], xaxis_range[1])
    #mg_rel.GetYaxis().SetTickLength(ticklength_y)
    mg_rel.GetYaxis().SetNdivisions(504)
    #mg_rel.GetXaxis().SetTickLength(ticklength_x / (rel_y_max - rel_y_min))
    mg_rel.GetXaxis().SetTitle(x_label)
    mg_rel.GetYaxis().SetTitle(y_ratio_label)
    mg_rel.GetYaxis().SetTitleOffset(0.62)
    mg_rel.GetXaxis().SetTitleOffset(1.1)
    
    # adjust axis titles fonts and sizes
    mg_rel.GetYaxis().SetTitleFont(42)
    mg_rel.GetYaxis().SetTitleSize(0.05 * 2)
    
    mg_rel.GetXaxis().SetTitleFont(42)
    mg_rel.GetXaxis().SetTitleSize(0.05 * 2)
    
    mg_rel.GetYaxis().SetLabelFont(42)
    mg_rel.GetYaxis().SetLabelSize(0.05 * 2)
        
    mg_rel.GetXaxis().SetLabelFont(42)
    mg_rel.GetXaxis().SetLabelSize(0.05 * 2)
    ROOT.gPad.SetTicks()        
    mg_rel.Draw("alp")
    
    canv.cd()

    # redraw the bounding box on top of everything else
    pad_rel.cd()
    ROOT.gPad.Update()
    ROOT.gPad.RedrawAxis()
        
    pad_abs.cd()
    ROOT.gPad.Update()
    ROOT.gPad.RedrawAxis()

    pad_rel.cd()
    ref_line = ROOT.TLine(20, 1.0, 500, 1.0)
    ROOT.SetOwnership(ref_line, False)
    ref_line.SetLineColor(ROOT.kBlack)
    ref_line.SetLineWidth(1)
    ref_line.SetLineStyle(7)
    ref_line.Draw()
    
    # save the plot
    canv.SaveAs(outfile + ".pdf")
  

def make_PUB_ttbar_Zprime_comparison_plot(ttbar_path, Zprime_path, outdir, tagger = "DL1r", jetcoll = "partice flow jets"):

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    xaxis_range = (20, 500)
    #WPs = ["FixedCutBEff_60", "FixedCutBEff_70", "FixedCutBEff_77", "FixedCutBEff_85"]
    WPs = ["FixedCutBEff_77"]

    WP_label = {
        "FixedCutBEff_60": "Fixed Cut, #epsilon_{b}^{MC} = 60%",
        "FixedCutBEff_70": "Fixed Cut, #epsilon_{b}^{MC} = 70%",
        "FixedCutBEff_77": "Fixed Cut, #epsilon_{b}^{MC} = 77%",
        "FixedCutBEff_85": "Fixed Cut, #epsilon_{b}^{MC} = 85%"
    }
    
    for cur_WP in WPs:
        cur_tagged_hist = "taggedb_{}".format(cur_WP)

        cur_outdir = os.path.join(outdir, cur_WP)
        if not os.path.exists(cur_outdir):
            os.makedirs(cur_outdir)

        ttbar_nominal_curve = EfficiencyLoader.from_highpt_xtrap_file(ttbar_path, cur_tagged_hist, nominal_name = ".*nominal.*", do_rebinning = False, rebin_options = {"sigth": 2.0, "merge_start": 20, "merge_end": 3000})
        ttbar_alternative_curve = EfficiencyLoader.from_highpt_xtrap_file(ttbar_path, cur_tagged_hist, nominal_name = ".*MC_FRAG.*", do_rebinning = False, rebin_options = {"sigth": 2.0, "merge_start": 20, "merge_end": 3000})

        Zprime_nominal_curve = EfficiencyLoader.from_highpt_xtrap_file(Zprime_path, cur_tagged_hist, nominal_name = ".*nominal.*", do_rebinning = False, rebin_options = {"sigth": 2.0, "merge_start": 20, "merge_end": 3000})
        Zprime_alternative_curve = EfficiencyLoader.from_highpt_xtrap_file(Zprime_path, cur_tagged_hist, nominal_name = ".*MC_FRAG.*", do_rebinning = False, rebin_options = {"sigth": 2.0, "merge_start": 20, "merge_end": 3000})

        curves_upper = [ttbar_nominal_curve, ttbar_alternative_curve, Zprime_nominal_curve, Zprime_alternative_curve]
        colors_upper = [ROOT.kRed - 3, ROOT.kOrange - 3, ROOT.kViolet + 2, ROOT.kAzure + 5]
        labels_upper = ["Powheg + Pythia 8", "Powheg + Herwig 7", "MadGraph + Pythia 8", "MadGraph + Herwig 7"]
        marker_styles_upper = [8, 8, 8, 8]
        curve_styles_upper = ["p", "p", "p", "p"]

        curves_lower = [ttbar_alternative_curve / ttbar_nominal_curve, Zprime_alternative_curve / Zprime_nominal_curve]
        colors_lower = [ROOT.kOrange - 3, ROOT.kAzure + 5]
        marker_styles_lower = [8, 8]
        curve_styles_lower = ["p", "p"]

        id_string = "#sqrt{s} = 13 TeV, " + jetcoll + ", " + tagger
        inner_label = "#splitline{" + id_string + "}{" + WP_label[cur_WP] + "}"

        outfile = os.path.join(cur_outdir, "unccomp")

        create_ratio_plot(curves_upper, curves_lower, outfile, x_label = "p_{T} [GeV]", y_label = "efficiency", y_ratio_label = "Herwig 7 / Pythia 8", labels = labels_upper, 
                          marker_colors_upper = colors_upper, marker_styles_upper = marker_styles_upper, curve_styles_upper = curve_styles_upper, 
                          marker_colors_lower = colors_lower, marker_styles_lower = marker_styles_lower, curve_styles_lower = curve_styles_lower,
                          inner_label = inner_label, xaxis_range = (20, 500), yaxis_range_abs = (0.55, 1.1), yaxis_range_rel = (0.9, 1.05))

if __name__ == "__main__":
    ROOT.gROOT.SetBatch()
    parser = ArgumentParser(description = "make special plot for PUB note to compare uncertainty from ttbar and Zprime")
    parser.add_argument("--ttbar", action = "store", dest = "ttbar_path")
    parser.add_argument("--zprime", action = "store", dest = "Zprime_path")
    parser.add_argument("--outdir", action = "store", dest = "outdir")
    args = vars(parser.parse_args())
    
    make_PUB_ttbar_Zprime_comparison_plot(**args)
