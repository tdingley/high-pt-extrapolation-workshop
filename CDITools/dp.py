import ROOT
from ROOT import TFile, TCanvas

import sys, os
from argparse import ArgumentParser
from TFileUtils import TFileUtils
from EfficiencyCurve import EfficiencyCurve
from EfficiencyPlotter import EfficiencyPlotter

def generate_plot(infile_path, outfile_path):
    eff = EfficiencyCurve.from_data(infile_path)

    EfficiencyPlotter.plot_uncertainty_ratio(curve = eff,
                                             rel_uncertainty_up = None,
                                             rel_uncertainty_down = None,
                                             outfile = outfile_path,
                                             outer_label = "",
                                             inner_label = "PDF b-calibration"
                                             )    
def main():
    ROOT.gROOT.SetBatch(True)
    
    parser = ArgumentParser(description = "generates EfficiencyCurve objects from csv files and plots them")
    parser.add_argument("files", nargs = '+', action = "store")
    args = vars(parser.parse_args())
    
    file_paths = args["files"]
    infile_paths = file_paths[:-1]
    outfile_path = file_paths[-1]

    print("using input files: " + " ".join(infile_paths))
    print("using output file: " + outfile_path)
    
    for infile_path in infile_paths:
        generate_plot(infile_path, outfile_path)

if __name__ == "__main__":
    main()
