#!/usr/bin/env python3
import sys, os, glob
import ROOT
from ROOT import TFile, TH1D
from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
from collections import defaultdict

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('indirectory', action = "store")
    parser.add_argument('-o', '--out-file', default='roc1d.h5',
                        help='output hist file')
    parser.add_argument('-m', '--dr-matching', default=0.2, type=float,
                        help='default %(default)s')
    return parser.parse_args()

def is_offline_matched(name):
    return name.startswith('OfflineMatched')
    # return False             

def get_cut(tagger, wp):
    wps = [60, 70, 77, 85]
    cuts_DL1dv00 = [4.884, 3.494, 2.443,0.930]
    cuts_DL1r = [4.565, 3.245, 2.195, 0.665]
    if tagger == "DL1dv00":

        cut = cuts_DL1dv00[wps.index(wp)]

        print(cut)
    else:
        cut = cuts_DL1r[wps.index(wp)]
    return cut
supported_systtypes = ["nominal","TRK_EFF_LOOSE_IBL", "TRK_EFF_LOOSE_GLOBAL","TRK_RES_D0_MEAS_UP", "TRK_RES_D0_MEAS_DOWN", "TRK_RES_D0_MEAS", "TRK_BIAS_Z0_WM","TRK_BIAS_D0_WM", "TRK_RES_Z0_MEAS_UP", "TRK_RES_Z0_MEAS_DOWN", "TRK_RES_Z0_MEAS","TRK_BIAS_QOVERP_SAGITTA_WM", "TRK_FAKE_RATE_LOOSE_TIDE", "TRK_FAKE_RATE_LOOSE",  "TRK_EFF_LOOSE_TIDE", "TRK_RES_Z0_DEAD", "TRK_RES_D0_DEAD", "TRK_EFF_LOOSE_PHYSMODEL", "TRK_EFF_LOOSE_PP0", "MC_FRAG"]
edges = np.array([0.0, 85.0, 140.0, 250.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1250.0, 1400.0, 1550.0, 1750.0, 2000.0, 2250.0, 2500.0, 2750.0, 3000.0], dtype='float64')

def _check_match(path, keywords):
    for kw in keywords:
        if kw in path:
            return kw

def _declare_hists(name):
    h1 = TH1D(name , name, len(edges) - 1, edges)
    return h1
def _get_syst_type(path, available_types):
    if _check_match(path, available_types) == "T145733":
        return "Nominal"
    if _check_match(path, available_types) == "TRK_EFF_LOOSE_GLOBAL_new":
        return "TRK_new_EFF_LOOSE_GLOBAL"
    else:
        return _check_match(path, available_types)

def _get_path_name(path):
    return(path.split("/")[-2].split(".")[0])+"."+(path.split("/")[-2].split(".")[1])+"."+(path.split("/")[-2].split(".")[2])+"."+(path.split("/")[-2].split(".")[5])+".TUNC_NBLS."
        
def _get_file_name(path):
    return (path.split("/"))[-1].split(".h5")[0]

#user.ssindhu.2019_10_01_Zprime.427081.AntiKt4EMPFlowJets.TUNC_NBLS.0_hists_DL1r_10/
#/eos/home-s/ssindhu/tdd/samples/user.ssindhu.800030.e7954_s3681_r13144_r13146_p4974.tdd.EMPFlowSlim.TRK_EFF_LOOSE_IBL_output.h5/user.ssindhu.28807643._000003.output.h5

#user.ssindhu.800030.e7954_s3681_r13144_r13146_p4974.tdd.EMPFlowSlim.2022-03-15T2101.2022-04-21-T145733_output
# ef find_between(s, start, end):
# return (s.split(start))[1].split(end)[0]

if __name__ == '__main__':
    args = get_args()
    path = args.out_file
    taggers = [
        'DL1r',
        'DL1dv00'
    ]
    jet_type = "AntiKt4EMPFlowJets"
    dr = args.dr_matching
    label = 5
    effs= []
    # h1   = ROOT.TH1D( "h1", "Test random", 20, edges )
    # input_file_2 = File("/home/sreelakshmi/PhD/tdd/nominal_1_grid.h5", 'r')
    # input_file_2 = File("/home/sreelakshmi/PhD/tdd/nominal_1_grid.h5", 'r')
    candidate_dirs = glob.glob(os.path.join(args.indirectory, '*output.h5'))
    print(candidate_dirs)
    for inputdir in candidate_dirs:
        print(_get_syst_type(inputdir, supported_systtypes), "getsysttype")
        candidate_files = glob.glob(os.path.join(inputdir, '*output.h5'))
        print(candidate_files, "cand")
        for input_file in candidate_files:
            with File(input_file, 'r') as h5file:
                for tagger in taggers:
                    print(tagger, "tagger")
                    syst_name = _get_syst_type(input_file, supported_systtypes)
                    print(syst_name, "syst_name")
                    outPathName =path+"/"+ _get_path_name(input_file)+"_"+tagger+"_b_jet_01"
                    print(outPathName, "outpathName")
                    if not os.path.exists(outPathName):
                        os.mkdir(outPathName)
                    outFileName =outPathName+"/"+ _get_file_name(input_file)+"_"+tagger+".root"
                    if not os.path.exists(outFileName):
                        outHistFile = TFile.Open(outFileName , "RECREATE")
                        outHistFile.cd()
                        jet_dir = outHistFile.mkdir(jet_type)
                        jet_dir.cd()
                        syst_dir = jet_dir.mkdir(syst_name)
                        #syst_dir.mkdir("nominal").cd()
                        print(outFileName)
                        jets = h5file['jets']
                        if is_offline_matched(tagger):
                            valid = jets['OfflineMatchedHadronConeExclTruthLabelID'] == label
                            valid &= jets['deltaRToOfflineJet'] < dr_match
                        else:
                            valid = jets['HadronConeExclTruthLabelID'] == label
                        truth_tagged = jets[valid]
                        flav = {f:truth_tagged[f'{tagger}_p{f}'] for f in 'cub'}
                        pt_all = truth_tagged['pt']*0.001
                        h2 = _declare_hists("truth_bootstrap_0")
                        for j in pt_all:
                            h2.Fill(j)
                        h2.Write()
                        fc = 0.018
                        discrim = np.log(flav['b'] / (fc * flav['c'] + (1-fc) * flav['u']))
                        for wp in [60, 70, 77, 85]:
                            cut = get_cut(tagger, wp)
                            in_wp = discrim > cut
                            tagged = truth_tagged[in_wp]
                            pt_tagged = tagged['pt'] * 0.001
                            # bincenter = 0.5 * (bin_edges[1:] + bin_edges[:-1])
                            h1 = _declare_hists("tagged_FixedCutBEff_"+str(wp)+"_bootstrap_0")
                            for i in pt_tagged:
                                h1.Fill(i)
                                
                            h1.Write()
                            # hist1.Divide(np.histogram(truth_tagged['pt']))                                                                                                   
                            # print(eff, bin_edges)                 
                            var = "pt"
                            var_type = "pt"
                            effs.append(h1/h2)
                            print(wp, tagger, len(effs))
                            print((h2/h1).GetBinContent(2), h1.GetBinContent(2), h2.GetBinContent(2), h1.GetBinError(2), h2.GetBinError(2))
                        outHistFile.Close()
    # ratioplotter_2(3, effs[0], effs[8], effs[16], '60', 'DL1r', var_type)
    # ratioplotter_2(3, effs[1], effs[9], effs[17], '70', 'DL1r', var_type)
    # ratioplotter_2(3, effs[2], effs[10], effs[18], '77', 'DL1r', var_type)
    # ratioplotter_2(3, effs[3], effs[11], effs[19], '85', 'DL1r', var_type)
    effs.clear()

            
