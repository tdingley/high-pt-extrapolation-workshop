import re

class SystematicUtils:

    # get the systematic name from a path (possibly a path internal to a ROOT file)
    @staticmethod
    def extract_identifier(path):
        regex = re.compile(".*(MC_.*?|TRK_.*?|JET_.*?)(\/|$)")
        m = regex.match(path)
        return m.group(1)
    
    # get the common name for a particular systematic variation
    @staticmethod
    def get_common_name(name):
        retval = name.replace('UP', '')
        #retval = retval.replace('UP', '')
        #retval = retval.replace('down', '')
        retval = retval.replace('DOWN', '')
        retval = retval.replace('.-1', '')
        retval = retval.replace('.1', '')
        return retval
