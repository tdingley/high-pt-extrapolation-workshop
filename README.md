# high-pT extrapolation

This repository contains all the code required to assess b-tagging uncertainties from simulation at p_T > 1 TeV.

The branch release22 is underconstruction and currently can be used to estimate only the extrapolation uncertaintiy from tracking, jet and parton shower systematics.
It is considerably different from the master branch and uses the Training-dataset-dumper (TDD) for NTuple production. 

Before NTuple production, download a data file to retag. Using Rucio:
```
setupATLAS
lsetup rucio
voms-proxy-init -voms atlas -valid 96:0 # A valid grid-proxy is required, this creates one valid for 96hrs
rucio download --nrandom 1 mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3681_r13144_p5627
```
To clone the repository:
```
git clone --recursive -b release22 https://:@gitlab.cern.ch:8443/atlas-ftag-calibration/high-pT-extrapolation.git
```
# Setting up
The training dataset dumper (TDD), and this repository, work with .h5 files that interface well with python (and by extension Flavour Tagging tools). To produce NTuples we first use the TDD. This framework is still a work in progress and uses a fork of the TDD, to run this framework clone the TDD from this fork: https://gitlab.cern.ch/tdingley/training-dataset-dumper.
Setting up the TDD:
```
mkdir build run && cd build
setupATLAS
asetup Athena,23.0.12
cmake ../training-dataset-dumper
make
source x*/setup.sh
cd ..
```
# NTuple production
Now that the directory is setup, we move to NTuple production. NTuple production with the TDD involves a process called retagging. Retagging allows variations on certain parameters, track impact parameter resolution for example, and re-runs all low-level taggers (JetFitter, SV1, IP2/3D etc) which are used as inputs to the high-level taggers (GN1, DL1d). The output NTuples are decorated with the new tagger weights as a result of this variation. These retagged NTuples can then be compared with the nominal case (with no variation) to assess the changes in tagger efficiency for that particular systematic variation. For a more detailed account of this High-pT-extrapolation procedure please read: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2021-003/. Some elements of the procedure have changed since but the premise remains the same. 
## MC20 vs MC21
**Bug**: running MC21 samples through this R22 branch will return error codes regarding RunNumber for the track biasing tool. 

**Temporary fix**: comment out any lines within `BTagTrainingPreprocessing/src/TrackSystematicsAlg.[cxx/h]`. This enables these samples to be run for all systematics other than `TRK_BIAS*`. 
## Tracking systematics:
```
ca-dump-retag $DATA_DIR/FILE -c configs/EMPFlow.json -m 100 -t $TRK_SYS
```
The `-c` flag specifies the config file for the TDD to use, `TRK_SYS` specifies the track systematic to run over. (List of available sysyematics listed here: https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/InDetTrackSystematicsTools/InDetTrackSystematics.h)
To run with no systematics, simply remove the -t flag. To run with the whole dataset, remove the `-m` flag (this sets to run with 100 events as a test).
With this working, it is easier to run this NTuple production on the grid.
To get familiar with grid submissions using the TDD, follow: https://training-dataset-dumper.docs.cern.ch/grid. Running:
```
grid-submit -c configs/EMPFlow.json -t $TRK_SYS -a retag
```
submits a test job, once sucessful (check status here: https://bigpanda.cern.ch/user) remove the `-a` tag and this will produce NTuples for that specific systematic variation. To run the nominal (no systematic) version remove the `-T` tag. 
## Jet systematics:
Jet systematics are implemented in the same way as tracking and uses the same TDD fork. For jet systematics,
```
grid-submit -c configs/EMPFlowSys.json -J $JET_SYS -S $SIGMA -a retag
```
where `JET_SYS` specifies the jet systematic to run over, `SIGMA` is the number of standard deviations (should be 1.0 or -1.0 for up/down variations respectively). 
Use `grid-submit -h` for more information. Create an output directory and download these h5 output files with names (`user.[NAME].DSID.[SYSTEMATIC].[CONFIG]_output.h5`) into a directory of your choice.

## Parton Shower
Parton shower systematics are run using dedicated Z' samples. In MC20 these are `500567` and `500568`, in MC21 these are `512953` and `522021`. Within `BTagTrainingPreprocessing/grid/inputs/single-b-tag.txt` comment out all other samples and submit each parton shower separately with:
```
grid-submit -c configs/EMPFlow.json retag
```
Make sure to run using the dryrunning mode, using the `-d` tag and once satisfied with the output DS name, submit the relevant test jobs before full grid submission. 
## Final state radiation (FSR)
To quantify the uncertainty involved with the choice of factorisation / renormalisation scale, we vary both scales to perform a scale-variation envelope uncertainty. This uses AnalysisTop as the training-dataset-dumper cannot dump MC weights.
To assess this uncertainty, please follow the README instructions within the FSR folder. 
# Efficiency root histogram production
To apply the tagger discriminants and produce truth/tagged root histograms from these retagged files, please follow the README inside the TDD_EfficiencyProducer folder.

# CDI file and plot production
Combining histograms, uncertainty calculation and plotting. This follows the instructions in the README inside CDITools folder.


