# HTCondor Efficiency Production
Due to the large quantity of data to be processed, running each systematic's tagged histogram production in parallel on the batch system is vastly more efficient.
To run, simply call:
```
python EfficiencyProducer_condor.py DATADIR/INPUT_H5_FILES -o OUTPUT_DIR -S SYST_TYPE
```
`DATADIR/INPUT_H5_FILES` is the directory containing all TDD output .h5 files from the grid retagging,
`OUTPUT_DIR` is the directory in which you want to store the histograms and SYST_TYPE is the type of systematic variation: tracking, jet or modelling - this decorates the output file appropriately to be used seemlessly with CDITools.
An optional argument: `-C CDI_FILE_PATH` specifies the CDI file to use. Default: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root - please specify full path or have the CDI stored locally!

# Local Production
To test code or if you're running a small sample, this production can also be run locally:
```
python EfficiencyProducer.py DATADIR/INPUT_H5_FILES -o OUTPUT_DIR -S SYST_TYPE
```