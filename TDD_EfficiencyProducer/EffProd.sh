#!/bin/bash
# setup environment on the batch system
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
lsetup "views LCG_98python3 x86_64-centos7-gcc9-opt"

# set root directory, in which to run the effiency production
export XTRAP_ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# execute python command
exec python $XTRAP_ROOTDIR/condor_effProd.py ${1} -o ${2} -S ${3}