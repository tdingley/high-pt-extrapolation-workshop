#!/usr/bin/env python3
import sys, os, glob
import ROOT
from ROOT import TFile, TH1D
from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
from collections import defaultdict

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('indirectory', action = "store")
    parser.add_argument('-o', '--out-file', default='roc1d.h5',
                        help='output hist file')
    parser.add_argument('-m', '--dr-matching', default=0.2, type=float,
                        help='default %(default)s')
    parser.add_argument('-C', '--CDI-file', 
                        default="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root",
                        help="Input CDI file to extract tagger cut values")
    parser.add_argument('-S', '--syst_type', default="Tracking", help="Please specifiy type from: Tracking, Jet, Modelling")
    return parser.parse_args()

def is_offline_matched(name):
    return name.startswith('OfflineMatched')
    # return False             

def get_cut(tagger, wp):
    wps = [60, 70, 77, 85]
    cuts_DL1dv00 = [4.884, 3.494, 2.443, 0.930]
    #cuts_DL1r = [4.565, 3.245, 2.195, 0.665] #Need to change this to GN1 when the time comes
    if tagger == "DL1dv00":

        cut = cuts_DL1dv00[wps.index(wp)]

        print(cut)
    else:
        cut = cuts_DL1r[wps.index(wp)]
    return cut
def get_cutvalue(tagger, WP, CDI_File, jet_collection):
    # open input CDI file, containing all taggers, WPs and respective discriminant cut values:
    CDI_File = ROOT.TFile.Open(CDI_File)

    # navigate to specified tagger and WP directory:
    dir = CDI_File.GetDirectory("/"+str(tagger) +"/"+ str (jet_collection) +"/"+ "FixedCutBEff_" + str(WP)+"/")
    dir.ls
    print("Getting cut value from: " +"/"+str(tagger) +"/"+ str (jet_collection) +"/"+ "FixedCutBEff_" + str(WP)+"/")
    # retrieve TVector cutvalue object from WP directory:
    cut_value = dir.Get("cutvalue")

    # print the cutvalue
    print(cut_value[0])

    return cut_value[0]

# array of all systematics to run over, the function get_supported_systtypes will retrieve the correct array
modelling_systtypes = ['pythia8', 'herwig7']
tracking_systtypes  = ["nominal_test","TRK_RES_D0_MEAS_UP","TRK_RES_D0_MEAS_DOWN","TRK_RES_D0_MEAS","TRK_RES_Z0_MEAS_UP","TRK_RES_Z0_MEAS_DOWN","TRK_RES_Z0_MEAS","TRK_RES_D0_DEAD","TRK_RES_Z0_DEAD","TRK_BIAS_D0_WM","TRK_BIAS_Z0_WM","TRK_BIAS_QOVERP_SAGITTA_WM","TRK_EFF_LOOSE_GLOBAL","TRK_EFF_LOOSE_IBL","TRK_EFF_LOOSE_PP0","TRK_EFF_LOOSE_PHYSMODEL","TRK_EFF_TIGHT_GLOBAL","TRK_EFF_TIGHT_IBL","TRK_EFF_TIGHT_PP0","TRK_EFF_TIGHT_PHYSMODEL","TRK_EFF_LARGED0_GLOBAL","TRK_EFF_LOOSE_TIDE","TRK_FAKE_RATE_TIGHT_TIDE","TRK_FAKE_RATE_LOOSE_TIDE","TRK_FAKE_RATE_LOOSE_ROBUST","TRK_FAKE_RATE_LOOSE","TRK_FAKE_RATE_TIGHT"]
jet_systtyypes      = ['BJES_Response_UP', 'nominal', 'BJES_Response_DOWN']
def get_supported_systtypes(type):
    types = [modelling_systtypes,tracking_systtypes,jet_systtyypes]
    type_check = ['modelling', 'tracking', 'jet']
    return types[type_check.index(type)]

# bin edges for output histogram
edges = np.array([0.0, 85.0, 140.0, 250.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1250.0, 1400.0, 1550.0, 1750.0, 2000.0, 2250.0, 2500.0, 2750.0, 3000.0], dtype='float64')

def _check_match(path, keywords):
    for kw in keywords:
        if kw in path:
            return kw
            print(kw)

def _declare_hists(name):
    h1 = TH1D(name , name, len(edges) - 1, edges)
    return h1
def _get_syst_type(path, available_types):
    # insert user-specific requirements if using non-standard TDD grid-submit output naming conventions. Please ensure the nominal case returns "Nominal" or "nominal"
    if _check_match(path, available_types) == "nominal_test":
        return "Nominal"
    if _check_match(path, available_types) == "TRK_EFF_LOOSE_GLOBAL_new":
        return "TRK_new_EFF_LOOSE_GLOBAL"
    else:
        return _check_match(path, available_types)

def _get_path_name(path, type):
    if type.lower() == "tracking":
        return(path.split("/")[-2].split(".")[0])+"."+(path.split("/")[-2].split(".")[1])+"."+(path.split("/")[-2].split(".")[2])+"."+("h5")+".TUNC_NBLS."
    elif type.lower() == "modelling":
        return(path.split("/")[-2].split(".")[0])+"."+(path.split("/")[-2].split(".")[1])+"."+(path.split("/")[-2].split(".")[2])+"."+("h5")+".MUNC_PS."
    elif type.lower() == "jet":
        return(path.split("/")[-2].split(".")[0])+"."+(path.split("/")[-2].split(".")[1])+"."+(path.split("/")[-2].split(".")[2])+"."+("h5")+".JUNC_retagging."
    else:
        print("Type not specified correctly, only tracking, modelling or jet available")
        sys.exit()
def _get_file_name(path):
    return (path.split("/"))[-1].split(".h5")[0]

if __name__ == '__main__':
    xtrap_rootdir = os.environ["XTRAP_ROOTDIR"]
    #xtrap_rootdir.cd('/TDD_EfficiencyProducer')

    args = get_args()
    path = args.out_file
    CDI_File = args.CDI_file
    type = args.syst_type
    supported_systtypes = get_supported_systtypes(type)
    path = os.path.join(xtrap_rootdir, path)
    taggers = [
       'GN120220509',
        'DL1dv00'
    ]
    
    jet_collection = "AntiKt4EMPFlowJets"
    dr = args.dr_matching
    label = 5
    effs= []
    candidate_dirs = glob.glob(os.path.join(args.indirectory, '*output.h5'))
    print(candidate_dirs)
    for inputdir in candidate_dirs:
        print(_get_syst_type(inputdir, supported_systtypes), "getsysttype")
        candidate_files = glob.glob(os.path.join(inputdir, '*output.h5'))
        print(candidate_files, "cand")
        for input_file in candidate_files:
            with File(input_file, 'r') as h5file:
                for tagger in taggers:
                    print(tagger, "tagger")
                    syst_name = _get_syst_type(input_file, supported_systtypes)
                    print(syst_name, "syst_name")
                    outPathName =path+ _get_path_name(input_file,type)+"_"+tagger+"_b_jet_01"
                    print(outPathName, "outpathName")
                    print("*************")
                    if not os.path.exists(outPathName):
                        #outPathName.mkdir()
                        os.mkdir(outPathName)
                    outFileName =outPathName+"/"+ _get_file_name(input_file)+"_"+tagger+".root"
                    if not os.path.exists(outFileName):
                        outHistFile = TFile.Open(outFileName , "RECREATE")
                        outHistFile.cd()
                        jet_dir = outHistFile.mkdir(jet_collection)
                        jet_dir.cd()
                        syst_dir = jet_dir.mkdir(syst_name)
                        syst_dir.mkdir("nominal").cd()
                        print(outFileName)
                        jets = h5file['jets']
                        if is_offline_matched(tagger):
                            valid = jets['OfflineMatchedHadronConeExclTruthLabelID'] == label
                            valid &= jets['deltaRToOfflineJet'] < dr_match
                        else:
                            valid = jets['HadronConeExclTruthLabelID'] == label
                        truth_tagged = jets[valid]
                        print(truth_tagged)
                        flav = {f:truth_tagged[f'{tagger}_p{f}'] for f in 'buc'}
                        pt_all = truth_tagged['pt']*0.001
                        h2 = _declare_hists("truth_bootstrap_0")
                        for j in pt_all:
                            h2.Fill(j)
                        h2.Write()
                        fc = 0.018
                        discrim = np.log(flav['b'] / (fc * flav['c'] + (1-fc) * flav['u']))
                        for wp in [60, 70, 77, 85]:
                            cut = get_cutvalue(tagger, wp, CDI_File, jet_collection)
                            syst_dir.cd("nominal")
                            in_wp = discrim > cut
                            tagged = truth_tagged[in_wp]
                            cutvalue = np.percentile(discrim, (100 - wp))
                            print(cutvalue, cut)
                            pt_tagged = tagged['pt'] * 0.001
                            h1 = _declare_hists("tagged_FixedCutBEff_"+str(wp)+"_bootstrap_0")
                            for i in pt_tagged:
                                h1.Fill(i)
                                
                            h1.Write()
           
                            var = "pt"
                            var_type = "pt"
                            effs.append(h1/h2)
                            print(wp, tagger, len(effs))
                            print((h2/h1).GetBinContent(2), h1.GetBinContent(2), h2.GetBinContent(2), h1.GetBinError(2), h2.GetBinError(2))
    effs.clear()

            
