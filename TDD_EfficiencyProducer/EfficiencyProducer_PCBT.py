#!/usr/bin/env python3
import sys, os, glob
import ROOT
from ROOT import TFile, TH1D
from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
from collections import defaultdict

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('indirectory', action = "store")
    parser.add_argument('-o', '--out_dir', default='roc1d.h5',
                        help='output hist file')
    parser.add_argument('-m', '--dr-matching', default=0.2, type=float,
                        help='default %(default)s')
    parser.add_argument('-S', '--syst_type', default="Tracking", help="Please specifiy type from: tracking, jet, modelling")
    parser.add_argument('-F', '--flav_jet', default="b", help="Please specify jet flavour of interest: b, c or l")
    parser.add_argument('-P', '--PCBT_istrue', default=False, help="Please specify which scale factor scheme: False=Fixed cut, True=Pseudo-continuous")

    return parser.parse_args()

def _check_match(path, keywords):
    for kw in keywords:
        if kw in path:
            return kw
def get_supported_systtypes(type):
    types = [modelling_systtypes,tracking_systtypes,jet_systtypes]
    type_check = ['modelling', 'tracking', 'jet']
    return types[type_check.index(type)]

def _get_syst_type(path, available_types):
    if _check_match(path, available_types) == "nominal_test":
        return "Nominal"
    if _check_match(path, available_types) == "TRK_EFF_LOOSE_GLOBAL_new":
        return "TRK_new_EFF_LOOSE_GLOBAL"
    else:
        return _check_match(path, available_types)

#supported_systtypes = ["nominal", "TRK_BIAS_D0_WM", "TRK_EFF_LOOSE_IBL", "TRK_EFF_LOOSE_GLOBAL","TRK_RES_D0_MEAS_UP", "TRK_RES_D0_MEAS_DOWN", "TRK_RES_D0_MEAS", "TRK_BIAS_Z0_WM","TRK_BIAS_D0_WM", "TRK_RES_Z0_MEAS_UP", "TRK_RES_Z0_MEAS_DOWN", "TRK_RES_Z0_MEAS","TRK_BIAS_QOVERP_SAGITTA_WM", "TRK_FAKE_RATE_LOOSE_TIDE", "TRK_FAKE_RATE_LOOSE",  "TRK_EFF_LOOSE_TIDE", "TRK_RES_Z0_DEAD", "TRK_RES_D0_DEAD", "TRK_EFF_LOOSE_PHYSMODEL", "TRK_EFF_LOOSE_PP0", "MC_FRAG"]
#supported_systtypes = ['BJES_Response_UP', 'nominal', 'BJES_Response_DOWN']
#supported_systtypes = ['nominal','TRK_RES_D0_MEAS','TRK_RES_Z0_MEAS',"TRK_RES_D0_DEAD","TRK_RES_Z0_DEAD","TRK_BIAS_D0_WM","TRK_BIAS_QOVERP_SAGITTA_WM","TRK_EFF_LOOSE_GLOBAL","TRK_EFF_LOOSE_IBL","TRK_EFF_LOOSE_PP0","TRK_EFF_LOOSE_PHYSMODEL","TRK_EFF_LARGED0_GLOBAL","TRK_EFF_LOOSE_TIDE","TRK_FAKE_RATE_LOOSE","TRK_FAKE_RATE_LOOSE_ROBUST","TRK_FAKE_RAKE_LOOSE_TIDE"]
#supported_systtypes = ['pythia8','herwig7']
modelling_systtypes = ['pythia8', 'herwig7']
#tracking_systtypes  = ["nominal","TRK_RES_D0_MEAS_UP","TRK_RES_D0_MEAS_DOWN","TRK_RES_D0_MEAS","TRK_RES_Z0_MEAS_UP","TRK_RES_Z0_MEAS_DOWN","TRK_RES_Z0_MEAS","TRK_RES_D0_DEAD","TRK_RES_Z0_DEAD","TRK_BIAS_D0_WM","TRK_BIAS_QOVERP_SAGITTA_WM","TRK_EFF_LOOSE_GLOBAL","TRK_EFF_LOOSE_IBL","TRK_EFF_LOOSE_PP0","TRK_EFF_LOOSE_PHYSMODEL","TRK_EFF_TIGHT_GLOBAL","TRK_EFF_TIGHT_IBL","TRK_EFF_TIGHT_PP0","TRK_EFF_TIGHT_PHYSMODEL","TRK_EFF_LARGED0_GLOBAL","TRK_EFF_LOOSE_TIDE","TRK_FAKE_RATE_TIGHT_TIDE","TRK_FAKE_RATE_LOOSE_TIDE","TRK_FAKE_RATE_LOOSE_ROBUST","TRK_FAKE_RATE_LOOSE","TRK_FAKE_RATE_TIGHT"]
jet_systtypes = ["nominal","JET_EffectiveNP_7_DOWN","JET_JER_EffectiveNP_6_DOWN","JET_EtaIntercalibration_NonClosure_PreRec_DOWN","JET_Pileup_OffsetNPV_DOWN","JET_Pileup_PtTerm_DOWN","JET_JERUnc_Noise_PreRec_DOWN","JET_GroupedNP_2_DOWN","JET_JESUnc_VertexingAlg_PreRec_DOWN","JET_SingleParticle_HighPt_DOWN","JET_Pileup_OffsetMu_DOWN","JET_JER_EffectiveNP_4_DOWN","JET_EffectiveNP_3_DOWN","JET_JER_EffectiveNP_7restTerm_DOWN","JET_JER_EffectiveNP_2_DOWN","JET_EffectiveNP_4_DOWN","JET_JER_EffectiveNP_5_DOWN","JET_JERUnc_mc20vsmc21_MCTYPE_PreRec_DOWN","JET_Flavor_Response_DOWN","JET_GroupedNP_1_DOWN","JET_JESUnc_mc20vsmc21_MCTYPE_PreRec_DOWN","JET_PunchThrough_MC16_DOWN","JET_JER_EffectiveNP_1_DOWN","JET_JESUnc_Noise_PreRec_DOWN","JET_EffectiveNP_6_DOWN","JET_InSitu_NonClosure_PreRec_DOWN","JET_JER_DataVsMC_MC16_DOWN","JET_EtaIntercalibration_TotalStat_DOWN","JET_JER_EffectiveNP_3_DOWN","JET_EffectiveNP_8restTerm_DOWN","JET_BJES_Response_DOWN","JET_EffectiveNP_2_DOWN","JET_EtaIntercalibration_Modelling_DOWN","JET_Flavor_Composition_DOWN","JET_EffectiveNP_1_DOWN","JET_GroupedNP_3_DOWN","JET_EffectiveNP_5_DOWN","JET_Pileup_RhoTopology_DOWN","JET_JER_EffectiveNP_1_UP","JET_JER_EffectiveNP_7restTerm_UP","JET_EtaIntercalibration_NonClosure_PreRec_UP","JET_EffectiveNP_2_UP","JET_EffectiveNP_7_UP","JET_GroupedNP_1_UP","JET_JESUnc_VertexingAlg_PreRec_UP","JET_GroupedNP_2_UP","JET_JERUnc_Noise_PreRec_UP","JET_EffectiveNP_5_UP","JET_SingleParticle_HighPt_UP","JET_EtaIntercalibration_Modelling_UP","JET_JER_EffectiveNP_3_UP","JET_EffectiveNP_3_UP","JET_JER_EffectiveNP_2_UP","JET_PunchThrough_MC16_UP","JET_EffectiveNP_4_UP","JET_InSitu_NonClosure_PreRec_UP","JET_EffectiveNP_6_UP","JET_JESUnc_mc20vsmc21_MCTYPE_PreRec_UP","JET_JESUnc_Noise_PreRec_UP","JET_JER_DataVsMC_MC16_UP","JET_EffectiveNP_1_UP","JET_Pileup_OffsetNPV_UP","JET_BJES_Response_UP","JET_JER_EffectiveNP_4_UP","JET_Flavor_Composition_UP","JET_EffectiveNP_8restTerm_UP","JET_EtaIntercalibration_TotalStat_UP","JET_JERUnc_mc20vsmc21_MCTYPE_PreRec_UP","JET_JER_EffectiveNP_6_UP","JET_GroupedNP_3_UP","JET_JER_EffectiveNP_5_UP","JET_Pileup_PtTerm_UP","JET_Flavor_Response_UP","JET_Pileup_RhoTopology_UP","JET_Pileup_OffsetMu_UP"]
#jet_systtypes=  ["nominal", "JET_GroupedNP_1_UP", "JET_GroupedNP_2_UP", "JET_GroupedNP_3_UP","JET_GroupedNP_1_DOWN", "JET_GroupedNP_2_DOWN", "JET_GroupedNP_3_DOWN"]
#jet_systtypes = ["JET_JER_EffectiveNP_6_DOWN","JET_EtaIntercalibration_NonClosure_PreRec_DOWN","nominal","JET_Pileup_OffsetNPV_DOWN","JET_Pileup_PtTerm_DOWN","JET_JERUnc_Noise_PreRec_DOWN","JET_GroupedNP_2_DOWN","JET_JESUnc_VertexingAlg_PreRec_DOWN","JET_EffectiveNP_7_DOWN","JET_SingleParticle_HighPt_DOWN","JET_Pileup_OffsetMu_DOWN","JET_JER_EffectiveNP_4_DOWN","JET_EffectiveNP_3_DOWN","JET_JER_EffectiveNP_7restTerm_DOWN","JET_JER_EffectiveNP_2_DOWN","JET_EffectiveNP_4_DOWN","JET_JER_EffectiveNP_5_DOWN","JET_JERUnc_mc20vsmc21_MCTYPE_PreRec_DOWN","JET_Flavor_Response_DOWN","JET_GroupedNP_1_DOWN","JET_JESUnc_mc20vsmc21_MCTYPE_PreRec_DOWN","JET_PunchThrough_MC16_DOWN","JET_JER_EffectiveNP_1_DOWN","JET_EffectiveNP_7_UP","JET_JESUnc_Noise_PreRec_DOWN","JET_EffectiveNP_6_DOWN","JET_InSitu_NonClosure_PreRec_DOWN","JET_JER_DataVsMC_MC16_DOWN","JET_EtaIntercalibration_TotalStat_DOWN","JET_JER_EffectiveNP_3_DOWN","JET_EffectiveNP_8restTerm_DOWN","JET_BJES_Response_DOWN","JET_EffectiveNP_2_DOWN","JET_EtaIntercalibration_Modelling_DOWN","JET_Flavor_Composition_DOWN","JET_EffectiveNP_1_DOWN","JET_GroupedNP_3_DOWN","JET_EffectiveNP_5_DOWN","JET_Pileup_RhoTopology_DOWN","JET_JER_EffectiveNP_1_UP","JET_JER_EffectiveNP_7restTerm_UP","JET_EtaIntercalibration_NonClosure_PreRec_UP","JET_EffectiveNP_2_UP","JET_GroupedNP_1_UP","JET_JESUnc_VertexingAlg_PreRec_UP","JET_GroupedNP_2_UP","JET_JERUnc_Noise_PreRec_UP","JET_EffectiveNP_5_UP","JET_SingleParticle_HighPt_UP","JET_EtaIntercalibration_Modelling_UP","JET_JER_EffectiveNP_3_UP","JET_EffectiveNP_3_UP","JET_JER_EffectiveNP_2_UP","JET_PunchThrough_MC16_UP","JET_EffectiveNP_4_UP","JET_InSitu_NonClosure_PreRec_UP","JET_EffectiveNP_6_UP","JET_JESUnc_mc20vsmc21_MCTYPE_PreRec_UP","JET_JESUnc_Noise_PreRec_UP","JET_JER_DataVsMC_MC16_UP","JET_EffectiveNP_1_UP","JET_Pileup_OffsetNPV_UP","JET_BJES_Response_UP","JET_JER_EffectiveNP_4_UP","JET_Flavor_Composition_UP","JET_EffectiveNP_8restTerm_UP","JET_EtaIntercalibration_TotalStat_UP","JET_JERUnc_mc20vsmc21_MCTYPE_PreRec_UP","JET_JER_EffectiveNP_6_UP","JET_GroupedNP_3_UP","JET_JER_EffectiveNP_5_UP","JET_Pileup_PtTerm_UP","JET_Flavor_Response_UP","JET_Pileup_RhoTopology_UP","JET_Pileup_OffsetMu_UP"]
#jet_systtypes = ["nominal", "JET_EffectiveNP_7_DOWN", "JET_EffectiveNP_7_UP"]
tracking_systtypes = ["nominal","TRK_BIAS_Z0_WM","TRK_EFF_LOOSE_IBL","TRK_FAKE_RATE_LOOSE_ROBUST","TRK_BIAS_D0_WM","TRK_EFF_LOOSE_PHYSMODEL","TRK_RES_D0_DEAD","TRK_BIAS_QOVERP_SAGITTA_WM","TRK_EFF_LOOSE_PP0","TRK_RES_D0_MEAS","TRK_BIAS_Z0_WM","TRK_EFF_LOOSE_TIDE","TRK_RES_Z0_DEAD","TRK_EFF_LARGED0_GLOBAL","TRK_RES_Z0_MEAS","TRK_EFF_LOOSE_GLOBAL","TRK_FAKE_RATE_LOOSE"]
#tracking_systtypes = ["nominal"]
xtrap_rootdir = os.environ["XTRAP_ROOTDIR"]
print(xtrap_rootdir + '\n')

args = get_args()
path = args.out_dir
print(path)
print('\n')

print(str(args.indirectory))
print('\n')

# Define input parameters for the loop
candidate_dirs = glob.glob(os.path.join(args.indirectory, '*output.h5'))
print(candidate_dirs)
print('\n')
flavour = args.flav_jet
PCBT = args.PCBT_istrue

# Start the for loop
for candidate_dir in candidate_dirs:
    print(len(candidate_dirs))
    print(candidate_dir)
    type = args.syst_type
    supported_systtypes = get_supported_systtypes(type)
    print(len(supported_systtypes))


    syst_name = _get_syst_type(candidate_dir, supported_systtypes)
    print(syst_name)
    # Define the HTCondor job description file for this iteration
    job_file = 'Eff_prod_' + str(syst_name) + '.submit'
    print(job_file)
    #print(supported_systtypes)
    with open(job_file, 'w') as f:
        f.write('executable = EffProd.sh\n')
        f.write('arguments = ' + str(args.indirectory) + ' ' + str(args.out_dir)+ ' ' + str(args.syst_type)+' ' +str(flavour) + ' '+ str(PCBT)+ '\n')
        f.write('output = output/output_' + syst_name+ '.out\n')
        f.write('error = error/error_' + syst_name + '.err\n')
        f.write('log = log/log.'+ syst_name +'.log'+'\n')
        f.write('initialdir = ' + str(os.getcwd()) + '\n')
        f.write('queue\n')
  
    # Submit the job for this iteration
    os.system('condor_submit ' + job_file)
