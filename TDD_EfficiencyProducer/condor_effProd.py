#!/usr/bin/env python3
import sys, os, glob, sys
import ROOT
from ROOT import TFile, TH1D
from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
from collections import defaultdict

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('indirectory', action = "store")
    parser.add_argument('-o', '--out-file', default='roc1d.h5',
                        help='output hist file')
    parser.add_argument('-m', '--dr-matching', default=0.2, type=float,
                        help='default %(default)s')
    parser.add_argument('-C', '--CDI-file', 
                        default="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root",
                        help="Input CDI file to extract tagger cut values")
    parser.add_argument('-S', '--syst_type', default="Tracking", help="Please specifiy type from: Tracking, Jet, Modelling")
    parser.add_argument('-F', '--flav_jet', default="b", help="Please specify jet flavour of interest: b, c or l")
    return parser.parse_args()

# offline matching: 
def is_offline_matched(name):
    return name.startswith('OfflineMatched')
    # return False   

# retrieve value of discriminat for jet flavour, tagger and WP alongside flavour fraction used to define discriminant
def get_cutvalue(tagger, CDI_File, jet_collection, WP = 60):
    # open input CDI file, containing all taggers, WPs and respective discriminant cut values:
    CDI_File = ROOT.TFile.Open(CDI_File)

    # navigate to specified tagger and WP directory:
    dir = CDI_File.GetDirectory("/"+str(tagger) +"/"+ str (jet_collection) +"/"+ "FixedCutBEff_" + str(WP)+"/")
    dir.ls
    print("Getting cut value from: " +"/"+str(tagger) +"/"+ str (jet_collection) +"/"+ "FixedCutBEff_" + str(WP)+"/")
    # retrieve TVector cutvalue object from WP directory:
    cut_value = dir.Get("cutvalue")

    # also retrieve TVector flavour fraction for each tagger:
    fc = dir.Get("fraction")

    # print the cutvalue
    print(cut_value[0])

    # print the flavour fraction
    print(fc[0])

    return cut_value[0], fc[0]


def get_supported_systtypes(type):
    types = [modelling_systtypes,tracking_systtypes,jet_systtypes]
    type_check = ['modelling', 'tracking', 'jet']
    if type == "all":
        overall_list = [syst for syst_type in nested_list for syst in sublist]
        return overall_list
    else:
        return types[type_check.index(type)]
    return types[type_check.index(type)]

# current binning used for fixed WPs in extrapolation regime
edges = np.array([0.0, 85.0, 140.0, 250.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1250.0, 1400.0, 1550.0, 1750.0, 2000.0, 2250.0, 2500.0, 2750.0, 3000.0], dtype='float64')

def _check_match(path, keywords):
    for kw in keywords:
        if kw in path:
            return kw
            print(kw)

def _declare_hists(name):
    h1 = TH1D(name , name, len(edges) - 1, edges)
    return h1
def _get_syst_type(path, available_types):
    # insert user-specific requirements if using non-standard TDD grid-submit output naming conventions. Please ensure the nominal case returns "Nominal" or "nominal"
    if _check_match(path, available_types) == "pythia8":
        return "Nominal"

    else:
        return _check_match(path, available_types)

def _get_path_name(path, type):
    if type.lower() == "tracking":
        return(path.split("/")[-2].split(".")[0])+"."+(path.split("/")[-2].split(".")[1])+"."+(path.split("/")[-2].split(".")[2])+"."+("h5")+".TUNC_NBLS."
    elif type.lower() == "modelling":
        return(path.split("/")[-2].split(".")[0])+"."+(path.split("/")[-2].split(".")[1])+".PS"+"."+("h5")+".MUNC_PS."
    elif type.lower() == "jet":
        return(path.split("/")[-2].split(".")[0])+"."+(path.split("/")[-2].split(".")[1])+"."+(path.split("/")[-2].split(".")[2])+"."+("h5")+".JUNC_retagging."
    else:
        print("Type not specified correctly, only tracking, modelling or jet available")
        sys.exit()
    return(path.split("/")[-2].split(".")[0])+"."+(path.split("/")[-2].split(".")[1])+"."+(path.split("/")[-2].split(".")[2])+"."+("h5")+".TUNC_NBLS."
# function to obtain PDG truth label
def _get_truth_label(flavour):
    if flavour.lower() == ("b" or "bottom"):
        return 5
    elif flavour.lower() == ("c" or "charm"):
        return 4
    elif flavour.lower() == ("l" or "light"):
        return 0
    else:
        print("Type not found, available flavours: b,c,l")
        sys.exit()

def _get_file_name(path):
    return (path.split("/"))[-1].split(".h5")[0]

# definitions of systematics to run over - enter all variations you're running over
modelling_systtypes = ['pythia8', 'herwig7']
tracking_systtypes = ["nominal","TRK_BIAS_Z0_WM","TRK_FAKE_RATE_LOOSE_TIDE","TRK_EFF_LOOSE_IBL","TRK_FAKE_RATE_LOOSE_ROBUST","TRK_BIAS_D0_WM","TRK_EFF_LOOSE_PHYSMODEL","TRK_RES_D0_DEAD","TRK_BIAS_QOVERP_SAGITTA_WM","TRK_EFF_LOOSE_PP0","TRK_RES_D0_MEAS","TRK_BIAS_Z0_WM","TRK_EFF_LOOSE_TIDE","TRK_RES_Z0_DEAD","TRK_EFF_LARGED0_GLOBAL","TRK_RES_Z0_MEAS","TRK_EFF_LOOSE_GLOBAL","TRK_FAKE_RATE_LOOSE"]
jet_systtypes = ["nominal", "JET_JESUnc_mc20vsmc21_MC21_PreRec_DOWN","JET_JESUnc_mc20vsmc21_MC21_PreRec_UP","JET_JERUnc_mc20vsmc21_MC21_PreRec_DOWN","JET_JERUnc_mc20vsmc21_MC21_PreRec_UP", "JET_GroupedNP_1_UP","JET_GroupedNP_1_DOWN", "JET_GroupedNP_2_UP","JET_GroupedNP_2_DOWN", "JET_GroupedNP_3_DOWN", "JET_GroupedNP_3_UP", "JET_EtaIntercalibration_NonClosure_PreRec_UP","JET_EtaIntercalibration_NonClosure_PreRec_DOWN", "JET_InSitu_NonClosure_PreRec_DOWN","JET_InSitu_NonClosure_PreRec_UP", "JET_JESUnc_mc20vsmc21_MC20_PreRec_DOWN", "JET_JESUnc_mc20vsmc21_MC20_PreRec_UP", "JET_JESUnc_Noise_PreRec_DOWN","JET_JESUnc_Noise_PreRec_UP", "JET_JESUnc_VertexingAlg_PreRec_DOWN","JET_JESUnc_VertexingAlg_PreRec_UP", "JET_JER_DataVsMC_MC16_DOWN", "JET_JER_DataVsMC_MC16_UP", "JET_JER_EffectiveNP_1_UP", "JET_JER_EffectiveNP_1_DOWN", "JET_JER_EffectiveNP_2_DOWN","JET_JER_EffectiveNP_2_UP", "JET_JER_EffectiveNP_3_DOWN","JET_JER_EffectiveNP_3_UP", "JET_JER_EffectiveNP_4_UP", "JET_JER_EffectiveNP_4_DOWN","JET_JER_EffectiveNP_5_DOWN","JET_JER_EffectiveNP_5_UP","JET_JER_EffectiveNP_6_DOWN","JET_JER_EffectiveNP_6_UP","JET_JER_EffectiveNP_7restTerm_DOWN","JET_JER_EffectiveNP_7restTerm_UP","JET_JERUnc_mc20vsmc21_MC20_PreRec_DOWN","JET_JERUnc_mc20vsmc21_MC20_PreRec_UP", "JET_JERUnc_Noise_PreRec_DOWN", "JET_JERUnc_Noise_PreRec_UP"]


if __name__ == '__main__':
    xtrap_rootdir = os.environ["XTRAP_ROOTDIR"]
    args = get_args()
    path = args.out_file
    CDI_File = args.CDI_file
    type = args.syst_type
    supported_systtypes = get_supported_systtypes(type)
    path = os.path.join(xtrap_rootdir, path)

    taggers = [
       #'GN120220509',
        'DL1dv01'
    ]

    jet_collection = "AntiKt4EMPFlowJets"
    dr = args.dr_matching
    flavour = args.flav_jet
    label = _get_truth_label(flavour)
    effs= []

    candidate_dirs = glob.glob(os.path.join(args.indirectory, '*output.h5'))
    print(candidate_dirs)
    for inputdir in candidate_dirs:
        print(_get_syst_type(inputdir, supported_systtypes), "getsysttype")
        candidate_files = glob.glob(os.path.join(inputdir, '*.h5'))
        print(candidate_files, "cand")
        for input_file in candidate_files:
            with File(input_file, 'r') as h5file:
                for tagger in taggers:
                    print(tagger, "tagger")
                    syst_name = _get_syst_type(input_file, supported_systtypes)
                    print(syst_name, "syst_name")
                    outPathName =path+"/"+ _get_path_name(input_file,type)+"_"+tagger+"_"+str(flavour)+"_jet_01"
                    print(outPathName, "outpathName")
                    if not os.path.exists(outPathName):
                        os.mkdir(outPathName)
                    outFileName =outPathName+"/"+ _get_file_name(input_file)+"_"+tagger+".root"
                    if not os.path.exists(outFileName):
                        outHistFile = TFile.Open(outFileName , "RECREATE")
                        outHistFile.cd()
                        jet_dir = outHistFile.mkdir(jet_collection)
                        jet_dir.cd()
                        syst_dir = jet_dir.mkdir(syst_name)
                        syst_dir.mkdir("nominal").cd()

                        print(outFileName)
                        jets = h5file['jets']
                        if is_offline_matched(tagger):
                            valid = jets['OfflineMatchedHadronConeExclTruthLabelID'] == label
                            valid &= jets['deltaRToOfflineJet'] < dr_match
                        else:
                            valid = jets['HadronConeExclTruthLabelID'] == label
                        truth_tagged = jets[valid]
                        print(truth_tagged)
                        flav = {f:truth_tagged[f'{tagger}_p{f}'] for f in 'buc'}
                        pt_all = truth_tagged['pt']*0.001
                        pt_b = truth_tagged
                        h2 = _declare_hists("truth_bootstrap_0")
                        for j in pt_all:
                            h2.Fill(j)
                        h2.Write()
                        # charm flavour fraction in background training sample, different for GN1 and Dl1d. Flavour fraction constant across WP, so 60 is chosen as default
                        fc = get_cutvalue(tagger, CDI_File, jet_collection, 60)[1]
                        # Always apply b discriminant, for charm and light jets this calculates the mis-tag rates (charm or light jet being mistakenly identified as a b-jet)
                        discrim = np.log(flav['b'] / (fc * flav['c'] + (1-fc) * flav['u']))
                        for wp in [60, 70, 77, 85]:
                            cut = get_cutvalue(tagger,CDI_File, jet_collection, wp)[0]
                            syst_dir.cd("nominal")
                            in_wp = discrim > cut
                            tagged = truth_tagged[in_wp]
                            cutvalue = np.percentile(discrim, (100 - wp))
                            print(cutvalue, cut)
                            pt_tagged = tagged['pt'] * 0.001
                            # bincenter = 0.5 * (bin_edges[1:] + bin_edges[:-1])
                            h1 = _declare_hists("tagged_FixedCutBEff_"+str(wp)+"_bootstrap_0")
                            for i in pt_tagged:
                                h1.Fill(i)
                                
                            h1.Write()
            
                            var = "pt"
                            var_type = "pt"
                            effs.append(h1/h2)
                            print(wp, tagger, len(effs))
                            print((h2/h1).GetBinContent(2), h1.GetBinContent(2), h2.GetBinContent(2), h1.GetBinError(2), h2.GetBinError(2))
                        outHistFile.Close()

    effs.clear()
