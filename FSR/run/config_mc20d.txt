LibraryNames libTopEventSelectionTools libTopEventReconstructionTools 

### Good Run List
GRLDir  GoodRunsLists 
GRLFile data17_13TeV/20190708/data17_13TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml 

### Pile-up reweighting tool - this is now mandatory
PRWConfigFiles_FS extendedZprime.root 
PRWActualMu_FS extendedZprime.root 
PRWLumiCalcFiles GoodRunsLists/data17_13TeV/20190708/ilumicalc_histograms_None_325713-340453_OflLumi-13TeV-010.root

#setup of TDP file, comment the following line to use the default one instead
TDPPath XSection-MC16-13TeV.data 

IsAFII False
MCGeneratorWeights Nominal

### Object container names
ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMPFlowJets
LargeJetCollectionName None
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName None
TrackJetCollectionName None
FilterBranches DL1,jet_MV2*,el_*,weight_leptonSF_EL_*,weight_trigger_EL_*,weight_bTagSF_DL1r_*,jet_DL1r*,weight_bTagSF_DL1dv00_*,weight_bTagSF_DL1dv01_*,*leptonSF*,*up*,*down*,*UP*,*DOWN*
BTagCDIPath xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root

UseEgammaLeakageCorrection False
ElectronTriggerEfficiencyConfig SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0

### Truth configuration
TruthCollectionName None
TruthJetCollectionName AntiKt4TruthDressedWZJets
TruthLargeRJetCollectionName None
TruthBlockInfo False
PDFInfo False

### Object loader/ output configuration
ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::EventSaverFlatNtuple
OutputEvents SelectedEvents
OutputFilename output.root
PerfStats No

### Systematics configuration
Systematics Nominal
JetUncertainties_NPModel SR_Scenario1
JetJERSmearingModel Simple

### Electron configuration
ElectronID MediumLH
#ElectronIsolationWPs HighPtCaloOnly TightTrackOnly_VarRad TightTrackOnly_FixedRad Tight_VarRad Loose_VarRad
ElectronIsolation Loose_VarRad
ElectronIsolationSF None
ElectronPt 10000

### Muon configuration
MuonQuality Tight
MuonQualityLoose Tight
MuonIsolation PflowTight_FixedRad
MuonIsolationLoose None

###Soft muons
UseSoftMuons True #default is False
SoftMuonQuality Tight #LowPt, Loose, Medium, Tight
SoftMuonPt 4000.
SoftMuonEta 4.5
SoftMuonDRJet 0.4 #can be set to 999 if no selection has to be applied here
SoftMuonAdditionalTruthInfo True
SoftMuonAdditionalTruthInfoCheckPartonOrigin True #this can be true if the PS is Pythia, better turn it off if you're using Sherpa

##### -- JETS -- #####
JetPt 20000.
JetEta 2.5

# DoTight/DoLoose to activate the loose and tight trees
# each should be one in: Data, MC, Both, False
DoTight Both
DoLoose False

UseGlobalLeptonTriggerSF False

### B-tagging configuration
BTaggingCaloJetWP DL1dv01:FixedCutBEff_77 DL1dv01:FixedCutBEff_70 DL1dv01:FixedCutBEff_85 DL1dv01:FixedCutBEff_60 DL1dv01:Continuous
BTaggingCaloJetUncalibWP GN120220509:FixedCutBEff_77 GN120220509:FixedCutBEff_70 GN120220509:FixedCutBEff_85 GN120220509:FixedCutBEff_60 GN120220509:Continuous 

########################
### basic selection with mandatory cuts for reco level
########################

SUB BASIC
INITIAL
GOODCALO
PRIVTX
RECO_LEVEL

########################
### quality  selections
########################
SUB quality
JETCLEAN LooseBad
EXAMPLEPLOTS
NOBADMUON

SELECTION sel_2017_notrig
. BASIC
. quality
SAVE

