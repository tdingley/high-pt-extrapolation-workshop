#!/usr/bin/env python

# Copyright (C) 2023 CERN for the benefit of the ATLAS collaboration
import TopExamples.grid
import DerivationTags
import Data_rel21
import MC16_TOPQ1

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.settingsFile  = 'config_mc20d.txt'
config.gridUsername  = 'tdingley'
config.suffix        = 'mc21-30-06-23'
config.excludedSites = ''
config.noSubmit      = True # set to True if you just want to test the submission
config.CMake         = True # need to set to True for CMake-based releases (release 22)
config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCALGROUPDISK'
config.otherOptions = '--extFile=extendedZprime.root'

###Data - look in Data_rel21.py #TO BE UPDATED
###MC Simulation - look in MC16_TOPQ1.py #TO BE UPDATED
###Using a few test samples produced with release 22
###Edit these lines if you don't want to run everything!
names = [
    'ZPrime',
]

samples = TopExamples.grid.Samples(names)
TopExamples.grid.submit(config, samples)

