from ROOT import *
import os,shutil,math,glob
from array import array
from argparse import ArgumentParser
from pathlib import Path

def get_args():
    parser = ArgumentParser(description=__doc__)
   # parser.add_argument('indirectory', action = "store")
    #parser.add_argument('')
    parser.add_argument('-i','--in_file', action='store')
    parser.add_argument('-F', '--flav_jet', default="b", help="Please specify jet flavour of interest: b, c or l")

    return parser.parse_args()

# function to declare histogram with "name", reduces clutter within main code
def _declare_hists(name):
    h1 = TH1F(name , name, 50, 0, 5000)
    return h1

# get name of the weight from 
# https://www.evernote.com/shard/s521/client/snv?isnewsnv=true&noteGuid=b44f63ed-3bee-839f-6121-13fab98ab43f&noteKey=24dc4bd45d1555a7ea5e9d24952be259&sn=https%3A%2F%2Fwww.evernote.com%2Fshard%2Fs521%2Fsh%2Fb44f63ed-3bee-839f-6121-13fab98ab43f%2F24dc4bd45d1555a7ea5e9d24952be259&title=Modeling%2BSystematic%2BNames
def _get_muR_value(index):
    map = ["Default", "nominal", "Var3cUp", 
    "Var3cDown", "MC_FSR_2_2", "MC_FSR_2_1",
    "MC_FSR_2_05","MC_FSR_1_2","MC_FSR_1_05",
    "MC_FSR_05_2","MC_FSR_05_1","MC_FSR_05_05",
    "MC_FSR_175_1","MC_FSR_15_1","MC_FSR_125_1",
    "MC_FSR_0625_1","MC_FSR_75_1","MC_FSR_0875_1",
    "MC_FSR_1_175","MC_FSR_1_15","MC_FSR_1_125",
    "MC_FSR_1_0625","MC_FSR_1_075","MC_FSR_1_0875"]
    return map[index]

# function to obtain PDG truth label
def _get_truth_label(flavour):
    if flavour.lower() == ("b" or "bottom"):
        return 5
    elif flavour.lower() == ("c" or "charm"):
        return 4
    elif flavour.lower() == ("l" or "light"):
        return 0
    else:
        print("Type not found, available flavours: b,c,l")
        sys.exit()

if __name__ == '__main__':
    # Read in AnalysisTop output.root file
    args = get_args()
    infile = args.in_file
    flavour = args.flav_jet
    PDG_value = _get_truth_label(flavour)
    #infile = "/home/dingleyt/QT/FSRExtra/output5.root"
    datapaths=[]
    datapaths.append(infile)
    print("Running FSR analysis with file: ", infile)
    treeNames = [""]
    # weights to run over, find naming convention here: https://www.evernote.com/shard/s521/client/snv?isnewsnv=true&noteGuid=b44f63ed-3bee-839f-6121-13fab98ab43f&noteKey=24dc4bd45d1555a7ea5e9d24952be259&sn=https%3A%2F%2Fwww.evernote.com%2Fshard%2Fs521%2Fsh%2Fb44f63ed-3bee-839f-6121-13fab98ab43f%2F24dc4bd45d1555a7ea5e9d24952be259&title=Modeling%2BSystematic%2BNames
    weights = [1, 15]
    taggers = [
        'GN120220509',
        'DL1dv01'
    ]
    # Define output file
    for File in datapaths:
        # open file
        file = TFile(File)

        # open nominal tree of ATOP output and load in the particular WP
        tree = file.Get("nominal")

        # get number of entries to run over
        N = tree.GetEntries()
        for tagger in taggers:
            print("Running tagger: ", tagger)
            # open new root file to be used as an input for RunCDIInputProduction.py
            outFile = TFile.Open(str(tagger)+ "_MUNC_FSR_800030_EMPFlow_" + str(flavour)+ "_jet.root", 'RECREATE')
            outFile.cd()
            jet_dir = outFile.mkdir("AntiKt4EMPFlowJets")
            jet_dir.cd()

            for i in range(len(weights)):
                weight_name = _get_muR_value(weights[i])

                print("Running weight: ", weight_name)
                # name and open output file for this particular MC weight
                truth_nobtag = _declare_hists("truth_bootstrap_0")

                hists = [_declare_hists("tagged_FixedCutBEff_60_bootstrap_0"),
                _declare_hists("tagged_FixedCutBEff_70_bootstrap_0"),
                _declare_hists("tagged_FixedCutBEff_77_bootstrap_0"),
                _declare_hists("tagged_FixedCutBEff_85_bootstrap_0")]

                for j in range(0,N):
                    # retrieve each entry
                    tree.GetEntry(j)

                    # store each WP cut within tree
                    tree_weight = tree.mc_generator_weights[weights[i]]
                                            
                    # run over the size of the tree
                    for num in range(0,tree.jet_pt.size()):
                        # cluncky code to set fixed WP's from ATOP file
                        if tagger == "DL1dv01":
                            WP = [tree.jet_isbtagged_DL1dv01_60[num],
                            tree.jet_isbtagged_DL1dv01_70[num], 
                            tree.jet_isbtagged_DL1dv01_77[num], 
                            tree.jet_isbtagged_DL1dv01_85[num]]
                        elif tagger == "GN120220509":
                            WP = [tree.jet_isbtagged_GN120220509_60[num],
                            tree.jet_isbtagged_GN120220509_70[num], 
                            tree.jet_isbtagged_GN120220509_77[num], 
                            tree.jet_isbtagged_GN120220509_85[num]]
                        else:
                            raise NameError("Tagger not supported") 

                        if tree.jet_truthflavExtended[num] == PDG_value:
                            # fill tree, no tagging
                            truth_nobtag.Fill(tree.jet_pt[num]*0.001, tree_weight)

                            for k in range(len(WP)):
                                if ord(WP[k]) == 1:
                                    # fill tagged tree
                                    hists[k].Fill(tree.jet_pt[num]*0.001, tree_weight)

                # find appropriate naming convention for branches
                weight_dir = jet_dir.mkdir(str(weight_name))
                weight_dir.cd()
                default_nominal_dir = weight_dir.mkdir("nominal")
                default_nominal_dir.cd()
                # write histograms and close file
                for hist in hists:
                    hist.Write()
                truth_nobtag.Write()
            outFile.Close()  
        



                            







                    





